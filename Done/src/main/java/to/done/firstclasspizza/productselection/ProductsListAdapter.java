package to.done.firstclasspizza.productselection;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import to.done.firstclasspizza.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.cart.Cart;
import to.done.lib.database.DBManager;
import to.done.lib.entity.nearestoutlets.Outlet;
import to.done.lib.entity.outletmenu.Category;
import to.done.lib.entity.outletmenu.Product;
import to.done.lib.entity.outletmenu.Product_attribute;
import to.done.lib.utils.Toolbox;

/**
 * Created by shadab mirza on 4/21/14.
 */
public class ProductsListAdapter extends BaseAdapter implements Filterable {

    private Activity context;
    private DBManager dbMan;
    private Cart cart;
    private List<Product> productList;
    private List<Product> fullProductList;
    private LayoutInflater inflater;
    private long outletId;
    private long companyId;
    private Filter filter;
    //ImageFetcher imgFetcher;
    List<Category>categoryList;
    private Outlet outlet;
    float widthDP,heightDP;
    private List<ProductsNavDrawerExpListAdapter.GroupObject> filterGroupList;

    public ProductsListAdapter(Activity context, long companyId, long outletId) {
        this.context = context;
        this.outletId = outletId;
        this.companyId = companyId;
        widthDP=32;//Toolbox.convertDpToPixel(35f,context);
        heightDP=32;//Toolbox.convertDpToPixel(35f,context);
        //imgFetcher = new ImageFetcher(context, (int)widthDP, (int)heightDP);
        dbMan = DBManager.getInstance(context.getApplication());
        cart = Cart.getInstance();
        productList = dbMan.getProductsForOutlet(outletId, Product.SortType.CATEGORY);
        fullProductList = new ArrayList<Product>(productList);
        inflater = LayoutInflater.from(context);
        outlet=dbMan.getOutletById(outletId);
        categoryList=dbMan.getCategories();
//        imgFetcher.startLoadingImage();
        filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                productList = (List<Product>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {


                FilterResults results = new FilterResults();


                if (constraint == null) {
                    results.count = fullProductList.size();
                    results.values = fullProductList;
                    return results;
                }

                List<Product> filteredList = new ArrayList<Product>();

                List<Product> startsWithList = new ArrayList<Product>();
                List<Product> containsList = new ArrayList<Product>();

                // perform your search here using the searchConstraint String.


                constraint = constraint.toString().toLowerCase();
                for (int i = 0; i < fullProductList.size(); i++) {
                    Product product = fullProductList.get(i);
                    if (product.getName().toLowerCase().startsWith(constraint.toString())) {
                        startsWithList.add(product);
                    } else if (product.getName().toLowerCase().contains(constraint.toString())) {
                        containsList.add(product);
                    }
                }


                filteredList.addAll(startsWithList);
                filteredList.addAll(containsList);

                results.count = filteredList.size();
                results.values = filteredList;

                return results;
            }
        };

    }



//    public ImageFetcher getImgFetcher() {
//        return imgFetcher;
//    }
//
//    public void setImgFetcher(ImageFetcher imgFetcher) {
//        this.imgFetcher = imgFetcher;
//    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Product getItem(int i) {
        return productList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return productList.get(i).getId().longValue();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {


        Product prod = getItem(i);
        List<Product_attribute> mAttribsList = prod.getAttributesForProduct();
        ViewHolder viewHolder;
        if (mAttribsList == null) {
            mAttribsList = dbMan.getAttributesForProduct(prod.getId());
            if (mAttribsList == null) {
                mAttribsList = new ArrayList<Product_attribute>();
            }

            prod.setAttributesForProduct(mAttribsList);
            Log.d("Attributes", "Fetched");

        }
        if (convertView == null) {

            convertView = inflater.inflate(R.layout.products_list_item, null);

            viewHolder = new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ImageView[] img_attrb_arr = {viewHolder.imgAttrib1, viewHolder.imgAttrib2, viewHolder.imgAttrib3, viewHolder.imgAttrib4};
        int img_attrb_index = 0;
//        viewHolder.imgPlus.setTag(getItem(i));
//        viewHolder.imgMinus.setTag(getItem(i));

        viewHolder.txt_attrib.setVisibility(View.GONE);
        viewHolder.imgAttrib1.setVisibility(View.GONE);
        viewHolder.imgAttrib2.setVisibility(View.GONE);
        viewHolder.imgAttrib3.setVisibility(View.GONE);
        viewHolder.imgAttrib4.setVisibility(View.GONE);
        if (mAttribsList != null) {
            for (int j = 0; j < mAttribsList.size(); j++) {
                Product_attribute product_attribute = mAttribsList.get(j);
                //Log.d("Attributes loop", " att:" + product_attribute.toString());
                if (product_attribute.getDisplay_type() != null) {
                    Log.d("Attributes loop", " att:" + product_attribute.getName()+" disp "+product_attribute.getDisplay_type());
                    switch (product_attribute.getDisplay_type()) {
                        case 1:
                            break;
                        case 2:
                            viewHolder.txt_attrib.setText(product_attribute.getName().toUpperCase());
                            //viewHolder.txt_attrib.setVisibility(View.VISIBLE);
                            break;
                        case 3:{
                            if (product_attribute.getPhoto_url() != null) {
                                img_attrb_arr[img_attrb_index].setVisibility(View.VISIBLE);
                                Log.d("Attributes loop", " att:"+product_attribute.getPhoto_url());
                               // imgFetcher.loadImage(product_attribute.getPhoto_url(), img_attrb_arr[img_attrb_index]);
                                Picasso.with(context).load(product_attribute.getPhoto_url()).into(img_attrb_arr[img_attrb_index]);
                                img_attrb_index++;
                            }
                            break;
                        }
                        case 4: {
                            viewHolder.txt_attrib.setText(product_attribute.getName().toUpperCase());
                            //viewHolder.txt_attrib.setVisibility(View.VISIBLE);
                            break;
                        }
                        case 5:
                            if (product_attribute.getPhoto_url() != null) {
                                if(!product_attribute.getPhoto_url().contains("http://api.done.to/images/6872/"))
                                product_attribute.setPhoto_url("http://api.done.to/images/6872/"+product_attribute.getPhoto_url());
                                    Log.d("Attributes loop", " att:"+product_attribute.getPhoto_url());
                                img_attrb_arr[img_attrb_index].setVisibility(View.VISIBLE);
                                //imgFetcher.loadImage(product_attribute.getPhoto_url(), img_attrb_arr[img_attrb_index]);
                                Picasso.with(context).load(product_attribute.getPhoto_url()).into(img_attrb_arr[img_attrb_index]);
                                img_attrb_index++;
                            }
                            break;
                    }
                }

            }
        }

        viewHolder.txtProductCustomize.setTag(getItem(i));


        viewHolder.txtProductCustomize.setOnClickListener(clickListener);

        viewHolder.txtProductName.setText(prod.getName());
        viewHolder.txtProductDesc.setText(prod.getDescription());
        viewHolder.txtProductPrice.setText("" + Toolbox.formatDecimal(prod.getPrice() + prod.getDefault_price()));
        String cat=updateSubtitle(prod);
//        if(cat.toLowerCase().equals("box"))
//            viewHolder.category.setVisibility(View.GONE);
//        else {
//            viewHolder.category.setVisibility(View.VISIBLE);
//            viewHolder.category.setText(cat);
//        }
            if (prod.getDefault_selected() == null || prod.getDefault_selected() == 0) {
            viewHolder.txtDefaultName.setVisibility(View.INVISIBLE);
        }
//        else {
//            viewHolder.txtDefaultName.setText(prod.getDefault_name());
//            viewHolder.txtDefaultName.setVisibility(View.VISIBLE);
//
//        }
        int qty = cart.getProductQtyIgnoreCustomization(outletId, prod.getOutlet_product_id());
        if (qty > 0)
            viewHolder.product_order_count.setVisibility(View.VISIBLE);
        else
            viewHolder.product_order_count.setVisibility(View.INVISIBLE);
        viewHolder.product_order_count.setText("" + qty);

//        if (qty > 0) {
//            viewHolder.imgMinus.setImageResource(R.drawable.minus);
//        } else {
//            viewHolder.imgMinus.setImageResource(R.drawable.minus_unselected);
//        }


        // if (prod.getCust_count() > 0) {
        viewHolder.txtProductCustomize.setVisibility(View.VISIBLE);
//        viewHolder.imgPlus.setVisibility(View.INVISIBLE);
//        viewHolder.imgMinus.setVisibility(View.INVISIBLE);
//        } else {
//            viewHolder.txtProductCustomize.setVisibility(View.INVISIBLE);
//        }

        return convertView;
    }
    public String updateSubtitle(Product p) {
        if (p == null || categoryList == null) {  return "";}

        for (Category c : categoryList) {
            if (c == null || p.getCategory_id() == null || c.getId() == null) return "";

            if (p.getCategory_id().longValue() == c.getId().longValue()) {
                return (c.getName().toUpperCase());
            }

        }
        return "";
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Product prod = (Product) view.getTag();

            switch (view.getId()) {
                case R.id.txt_product_customize: {

                    Product p = (Product) view.getTag();
                    if (p.getCust_count() > 0) {
                        Bundle bundle = new Bundle();
                        bundle.putLong(Constants.PRODUCT_ID, p.getId());
                        bundle.putLong(Constants.OUTLET_PRODUCT_ID, p.getOutlet_product_id());
                        bundle.putLong(Constants.OUTLET_ID, outletId);
                        bundle.putLong(Constants.COMPANY_ID, companyId);
                        bundle.putString(Constants.PRODUCT_NAME, prod.getName());
                        bundle.putString(Constants.CUST_SIZE, "yes");
                        Toolbox.changeScreen(context, Constants.SCREEN_PRODUCT_CUSTOMIZATION, true, bundle);
                    } else {
//                        createCustomTextDialog(prod);
                        cart.addToCart(companyId,outletId,p);
//                        Bundle bundle = new Bundle();
//                        bundle.putLong(Constants.PRODUCT_ID, p.getId());
//                        Log.d(Constants.OUTLET_PRODUCT_ID,""+p.getOutlet_product_id());
//                        bundle.putLong(Constants.OUTLET_PRODUCT_ID, p.getOutlet_product_id());
//                        bundle.putLong(Constants.OUTLET_ID, outletId);
//                        bundle.putLong(Constants.COMPANY_ID, companyId);
//                        bundle.putString(Constants.PRODUCT_NAME, prod.getName());
//                        bundle.putString(Constants.CUST_SIZE, "no");
//                        Toolbox.changeScreen(context, Constants.SCREEN_PRODUCT_CUSTOMIZATION, true, bundle);
                        notifyDataSetChanged();
                    }

                    break;
//                case R.id.img_plus: {
//
//
//                    List<Long> tempList = new LinkedList<Long>();
//                    tempList.add(prod.getOutlet_product_id());
//
//                    List<Product> defaultChildren = dbMan.getDefaultChildren(tempList);
//                    if (defaultChildren != null && defaultChildren.size() > 0) {
//                        tempList.clear();
//                        for (Product p : defaultChildren) {
//                            tempList.add(p.getOutlet_product_id());
//                        }
//                        List<Product> secondLevelCustList = dbMan.getDefaultChildren(tempList);
//                        if (secondLevelCustList != null)
//                            defaultChildren.addAll(secondLevelCustList);
//
//                        List<Customization> defaultCustomizationList = new ArrayList<Customization>();
//
//                        for (Product p : defaultChildren) {
//                            defaultCustomizationList.add(new Customization(p.getOutlet_product_id(), p.getId(), p.getParent_outlet_product_id(), p.getPrice()));
//                        }
//
//                        prod.setCustomization(defaultCustomizationList);
//                    }
//                    cart.addToCart(companyId, outletId, prod);
//
//                    View txtQty = ((View) view.getParent()).findViewById(R.id.txt_product_quantity);
//
//                    Toolbox.animateQuantityIncrease(txtQty);
//
//                    notifyDataSetChanged();
//                    break;
//                }
//                case R.id.img_minus: {
//                    cart.removeFromCart(outletId, prod);
//
//                    TextView txtQty = (TextView) ((View) view.getParent()).findViewById(R.id.txt_product_quantity);
//
//                    if (Integer.parseInt(txtQty.getText().toString()) > 0) {
//
//                        Toolbox.animateQuantityDecrease(txtQty);
//
//                    }
//
//
//                    notifyDataSetChanged();
//                    break;
//                }

                }

            }
        }
    };


    @Override
    public Filter getFilter() {
        return filter;
    }

    public void setFilterGroupList(List<ProductsNavDrawerExpListAdapter.GroupObject> filterGroupList) {
        this.filterGroupList = filterGroupList;

        List<Long> attrIds = new ArrayList<Long>();
        List<Long> catIds = new ArrayList<Long>();
        Product.SortType sortType = null;
        for (ProductsNavDrawerExpListAdapter.GroupObject group : filterGroupList) {

            for (ProductsNavDrawerExpListAdapter.ChildObject child : group.getChildObjectList()) {
                if (child.isSelected()) {
                    if (group.getType() == ProductsNavDrawerExpListAdapter.GroupObject.GroupType.ATTRIBUTE) {
                        attrIds.add(child.getId());
                    } else if (group.getType() == ProductsNavDrawerExpListAdapter.GroupObject.GroupType.CATEGORY) {
                        catIds.add(child.getId());
                    } else if (group.getType() == ProductsNavDrawerExpListAdapter.GroupObject.GroupType.SORT) {

                        if (child.getId() == Product.SortType.PRICE.getId()) {
                            sortType = Product.SortType.PRICE;
                        } else if (child.getId() == Product.SortType.CATEGORY.getId()) {
                            sortType = Product.SortType.CATEGORY;
                        } else if (child.getId() == Product.SortType.ALPHABETICAL.getId()) {
                            sortType = Product.SortType.ALPHABETICAL;
                        }

                    }
                }
            }
        }

        if (attrIds.size() > 0 || catIds.size() > 0) {
            productList = dbMan.getProductsForOutletByFilters(outletId, attrIds, catIds, sortType);
            fullProductList = new ArrayList<Product>(productList);
        }
        else if(attrIds.size() > 0 && catIds.size() > 0){
            productList = dbMan.getProductsForOutletByFilters(outletId, attrIds, catIds, sortType);
            fullProductList = new ArrayList<Product>(productList);
        }
        else {
            productList = dbMan.getProductsForOutlet(outletId, sortType);
            fullProductList = new ArrayList<Product>(productList);
        }
        notifyDataSetChanged();
    }

    static class ViewHolder {

        @InjectView(R.id.txt_product_name)
        TextView txtProductName;

        @InjectView(R.id.txt_product_desc)
        TextView txtProductDesc;

        @InjectView(R.id.txt_product_price)
        TextView txtProductPrice;

        @InjectView(R.id.txt_default_name)
        TextView txtDefaultName;
        @InjectView(R.id.txt_product_cat)
        TextView category;
//        @InjectView(R.id.txt_product_quantity)
//        TextView txtProductQuantity;

        @InjectView(R.id.txt_product_customize)
        TextView txtProductCustomize;

        @InjectView(R.id.product_order_count)
        TextView product_order_count;
        @InjectView(R.id.atrributesHolder)
        LinearLayout attributesHolder;
        @InjectView(R.id.txt_product_attrib)
        TextView txt_attrib;

        @InjectView(R.id.img_product_attrib_1)
        ImageView imgAttrib1;
        @InjectView(R.id.img_product_attrib_2)
        ImageView imgAttrib2;
        @InjectView(R.id.img_product_attrib_3)
        ImageView imgAttrib3;
        @InjectView(R.id.img_product_attrib_4)
        ImageView imgAttrib4;



        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

}
