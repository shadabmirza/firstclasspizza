package to.done.firstclasspizza.productselection;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import to.done.firstclasspizza.R;
import to.done.firstclasspizza.common.FilterUpdateListener;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;

/**
 * Created by shadab mirza on 4/17/14.
 */
public class ProductsNavDrawerContentFrag extends Fragment implements FilterUpdateListener{

    @InjectView(R.id.ex_lst_filters)
    ExpandableListView exLstFilters;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v=inflater.inflate(R.layout.outlets_nav_drawer,null);

        ButterKnife.inject(this,v);

        long outletId=getArguments().getLong(Constants.OUTLET_ID);

        exLstFilters.setAdapter(new ProductsNavDrawerExpListAdapter(getActivity(),outletId,null));

        return v;
    }

    @Override
    public void onFilterUpdate() {

    }
}
