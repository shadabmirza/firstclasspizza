package to.done.firstclasspizza.productselection;

import android.app.Activity;
import android.util.Log;

import to.done.firstclasspizza.common.FilterExpandableListAdapter;
import to.done.firstclasspizza.common.FilterUpdateListener;

import java.util.ArrayList;
import java.util.List;

import to.done.lib.entity.outletmenu.Category;
import to.done.lib.entity.outletmenu.Product;
import to.done.lib.entity.outletmenu.Product_attribute;
import to.done.lib.entity.outletmenu.Product_attribute_group;
import to.done.lib.utils.Toolbox;

/**
 * Created by shadab mirza on 4/21/14.
 */
public class ProductsNavDrawerExpListAdapter extends FilterExpandableListAdapter {

//    private Activity context;
//    private DBManager dbMan;
//    private List<GroupObject>groupObjectList=new ArrayList<GroupObject>();
//    private  LayoutInflater inflater;
//    private FilterUpdateListener filterUpdateListener;

    public ProductsNavDrawerExpListAdapter(Activity context,long outletId,FilterUpdateListener filterUpdateListener) {
        super(context,outletId,filterUpdateListener);
    }

    @Override
    public String getSortHint(long id) {
        if(id == Product.SortType.CATEGORY.getId()){
            return "Group by category";
        }
        else if(id == Product.SortType.ALPHABETICAL.getId()) {
            return "A to Z";
        }
        else if(id == Product.SortType.PRICE.getId()) {
            return "Low to High";
        }
        return "";
    }

    @Override
    public List<GroupObject> getGroupsList() {

        List<GroupObject>groupsList=new ArrayList<GroupObject>();

        List<Product_attribute_group> productAttributesGroupList=dbMan.getProductAttributeGroupsWithAttrs(outletId);

        List<ChildObject>sortChildList=new ArrayList<ChildObject>();

        ChildObject sortCategory=new ChildObject(Product.SortType.CATEGORY.getId(), Product.SortType.CATEGORY.getName(),0);
        sortCategory.setSelected(true);

        sortChildList.add(sortCategory);
        //sortChildList.add(new ChildObject(Product.SortType.PRICE.getId(),Product.SortType.PRICE.getName(),0));
        //sortChildList.add(new ChildObject(Product.SortType.ALPHABETICAL.getId(),Product.SortType.ALPHABETICAL.getName(),0));

        GroupObject sortGroupObject=new GroupObject(sortChildList,-200l,"Sort By", GroupObject.GroupType.SORT,1,1);

        groupsList.add(sortGroupObject);


        List<Category>categoryList=dbMan.getCategoriesByOutletIdWithCount(outletId);


        List<ChildObject>catChildList=new ArrayList<ChildObject>();
        Log.e("PrdoctNavDrawer","categories size:"+categoryList.size());
        for(Category c: categoryList){
            Toolbox.writeToLog("Categories"+c.getName());

            catChildList.add(new ChildObject(c.getId(),c.getName(),c.getProducts_count()));
        }


        for(Product_attribute_group pag:productAttributesGroupList){

            List<ChildObject> childObjectList=new ArrayList<ChildObject>();

            for(Product_attribute pa:pag.getProductAttributeList()){

                childObjectList.add(new ChildObject(pa.getId(),pa.getName(),pa.getProducts_count()));

            }

            GroupObject groupObject=new GroupObject(childObjectList,pag.getId(),pag.getName(), GroupObject.GroupType.ATTRIBUTE,pag.getSelection_min(),pag.getSelection_max());
            groupsList.add(groupObject);




        }
        GroupObject catGroupObject=new GroupObject(catChildList,-100l,"Categories", GroupObject.GroupType.CATEGORY,0,0);

        groupsList.add(catGroupObject);
        return groupsList;
    }


//
//    @Override
//    public int getGroupCount() {
//        return groupObjectList.size();
//    }
//
//    @Override
//    public int getChildrenCount(int i) {
//        return groupObjectList.get(i).getChildObjectList().size();
//    }
//
//    @Override
//    public GroupObject getGroup(int i) {
//        return groupObjectList.get(i);
//    }
//
//    @Override
//    public ChildObject getChild(int i, int i2) {
//        return groupObjectList.get(i).getChildObjectList().get(i2);
//    }
//
//    @Override
//    public long getGroupId(int i) {
//        return groupObjectList.get(i).getId().longValue();
//    }
//
//    @Override
//    public long getChildId(int i, int i2) {
//        return groupObjectList.get(i).getChildObjectList().get(i2).getId().longValue();
//    }
//
//    @Override
//    public boolean hasStableIds() {
//        return false;
//    }
//
//    @Override
//    public View getGroupView(int i, boolean b, View convertView, ViewGroup viewGroup) {
//
//
//        ViewHolderGroup viewHolderGroup;
//
//        if(convertView==null){
//
//            convertView=inflater.inflate(R.layout.outlets_nav_drawer_ex_list_group,null);
//            viewHolderGroup=new ViewHolderGroup(convertView);
//            convertView.setTag(viewHolderGroup);
//        }
//        else{
//            viewHolderGroup=(ViewHolderGroup)convertView.getTag();
//        }
//        viewHolderGroup.txtOutletAttrGroup.setText(getGroup(i).getName());
//
//        return convertView;
//    }
//
//    @Override
//    public View getChildView(final int i, final int i2, boolean b, View convertView, ViewGroup viewGroup) {
//
//        ViewHolderChild viewHolderChild;
//
//        if(convertView==null){
//
//            convertView=inflater.inflate(R.layout.outlets_nav_drawer_ex_list_child,null);
//
//            viewHolderChild=new ViewHolderChild(convertView);
//
//            convertView.setTag(viewHolderChild);
//        }
//        else{
//            viewHolderChild=(ViewHolderChild)convertView.getTag();
//        }
//
////        if(i2==getChildrenCount(i)-1){
////            viewHolderChild.lineWhite.setVisibility(View.INVISIBLE);
////            viewHolderChild.lineGray.setVisibility(View.INVISIBLE);
////        }
////        else
////        {
////            viewHolderChild.lineWhite.setVisibility(View.VISIBLE);
////            viewHolderChild.lineGray.setVisibility(View.VISIBLE);
////        }
//        viewHolderChild.txtOutletAttr.setTag(getChild(i,i2));
//        viewHolderChild.imgRadio.setTag(getChild(i,i2));
//
//        if(getGroup(i).getSelection_min()==1 && getGroup(i).getSelection_max()==1) {
//
//            int drawableId=Toolbox.getAttributeResourceId(context, R.attr.themeRadioSelector, MainActivity.currentThemeId);
////            Drawable radioButtonDrawable = context.getResources().getDrawable(drawableId);
//            viewHolderChild.imgRadio.setImageResource(drawableId);
//        }
//        else
//        {
//            viewHolderChild.imgRadio.setImageResource(R.drawable.red_bright_checkbox_selector);
//        }
//        viewHolderChild.txtOutletAttr.setText(getChild(i,i2).getName());
//
//        if(getGroup(i).getType()== GroupObject.GroupType.SORT){
//
//            if(getChild(i,i2).getId() == Product.SortType.CATEGORY.getId()){
//                viewHolderChild.txtCount.setText("Group by category");
//            }
//            else if(getChild(i,i2).getId() == Product.SortType.ALPHABETICAL.getId()) {
//                viewHolderChild.txtCount.setText("A to Z");
//            }
//            else if(getChild(i,i2).getId() == Product.SortType.PRICE.getId()) {
//                viewHolderChild.txtCount.setText("Low to High");
//            }
//
//        }
//        else{
//            viewHolderChild.txtCount.setText("("+getChild(i,i2).getCount()+")");
//        }
//
//        viewHolderChild.navDrawerExListChild.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                onFilterClicked(view,i,i2);
//
////                getChild(i,i2).setSelected(!getChild(i,i2).isSelected());
//                if(filterUpdateListener!=null)
//                {
//                    filterUpdateListener.onFilterUpdate();
//                }
//                notifyDataSetChanged();
//            }
//        });
//
//        viewHolderChild.imgRadio.setSelected(getChild(i,i2).isSelected());
//        viewHolderChild.txtOutletAttr.setSelected(getChild(i,i2).isSelected());
//        viewHolderChild.txtCount.setSelected(getChild(i,i2).isSelected());
//
//        return convertView;
//    }
//
////    View.OnClickListener clickListener=new View.OnClickListener() {
////        @Override
////        public void onClick(View view) {
////
//////            ChildObject child=(ChildObject)view.getTag();
////                    getChild(i,i2).setSelected(!getChild(i, i2).isSelected());
////
////            child.setSelected(!child.isSelected());
////            if(filterUpdateListener!=null)
////            {
////                filterUpdateListener.onFilterUpdate();
////            }
////            notifyDataSetChanged();
////        }
////    };
//    @Override
//    public boolean isChildSelectable(int i, int i2) {
//        return false;
//    }
//
//    public List<GroupObject> getFilterGroupList() {
//        return groupObjectList;
//    }
//
//    public void setFilterGroupList(List<GroupObject> groupObjectList) {
//        this.groupObjectList = groupObjectList;
//    }
//
//
//    private void onFilterClicked(View view, int i, int i2){
//
//        View imgRadio=view.findViewById(R.id.img_radio);
//
//        GroupObject groupObject=getGroup(i);
//        ChildObject childObject=getChild(i, i2);
//
//        boolean radio=false;
//
//        if(groupObject.getSelection_min()==1&&groupObject.getSelection_max()==1){
//            radio=true;
//        }
//
//
//
//        if(childObject.isSelected()){
//
//            if(!radio){
//
//                if(groupObject.getSelection_min()>0) {
//
//                    int currSelectionCount=getCurrentSelectionCount(groupObject);
//
//                    if (currSelectionCount > groupObject.getSelection_min()) {
//                        childObject.setSelected(false);
//                        imgRadio.setSelected(false);
//                    }
//                }
//                else
//                {
//                    childObject.setSelected(false);
//                    imgRadio.setSelected(false);
//                }
//            }
//
//
//
//        }
//        else {
//
//            if(!radio) {
//                int currSelectionCount=getCurrentSelectionCount(groupObject);
//
//
//                if(groupObject.getSelection_max()<0){
//                    childObject.setSelected(true);
//                    imgRadio.setSelected(true);
//                }
//                else if (currSelectionCount < groupObject.getSelection_max()) {
//                    childObject.setSelected(true);
//                    imgRadio.setSelected(true);
//                }
//            }
//            else
//            {
//
//                for(ChildObject c:groupObject.getChildObjectList()){
//                    if(c.isSelected()){
//                        c.setSelected(false);
//                    }
//
//                }
//
//                childObject.setSelected(true);
//                imgRadio.setSelected(true);
//                notifyDataSetChanged();
//
//            }
//        }
//    }
//
//    private int getCurrentSelectionCount(GroupObject groupObject){
//
//        int currSelectionCount=0;
//
//        for(ChildObject c:groupObject.getChildObjectList()){
//            if(c.isSelected()){
//                currSelectionCount++;
//            }
//        }
//
//        return currSelectionCount;
//    }
//    static class ViewHolderGroup{
//
//        @InjectView(R.id.txt_outlet_attr_group)
//        TextView txtOutletAttrGroup;
//
//        public ViewHolderGroup(View view) {
//            ButterKnife.inject(this, view);
//        }
//    }
//
//    static class ViewHolderChild{
//
//        @InjectView(R.id.txt_filter_name)
//        TextView txtOutletAttr;
//
//        @InjectView(R.id.txt_count)
//        TextView txtCount;
//
//        @InjectView(R.id.img_radio)
//        ImageView imgRadio;
//
//        @InjectView(R.id.line_white)
//        View lineWhite;
//
//        @InjectView(R.id.line_gray)
//        View lineGray;
//
//        @InjectView(R.id.nav_drawer_ex_list_child)
//        ViewGroup navDrawerExListChild;
//
//        public ViewHolderChild(View view) {
//            ButterKnife.inject(this, view);
//        }
//    }
//
//
//    public static class GroupObject{
//
//        private List<ChildObject>childObjectList=new ArrayList<ChildObject>();
//        private Long id;
//        private String name;
//        public static enum GroupType{CATEGORY,ATTRIBUTE,SORT};
//        private GroupType groupType;
//        private int selection_min,selection_max;
//
//        private GroupObject(List<ChildObject> childObjectList, Long id, String name, GroupType groupType,int selection_min,int selection_max) {
//            this.childObjectList = childObjectList;
//            this.id = id;
//            this.name = name;
//            this.groupType = groupType;
//            this.selection_min=selection_min;
//            this.selection_max=selection_max;
//        }
//
//        public List<ChildObject> getChildObjectList() {
//            return childObjectList;
//        }
//
//        public void setChildObjectList(List<ChildObject> childObjectList) {
//            this.childObjectList = childObjectList;
//        }
//
//        public Long getId() {
//            return id;
//        }
//
//        public void setId(Long id) {
//            this.id = id;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public GroupType getType() {
//            return groupType;
//        }
//
//        public void setType(GroupType groupType) {
//            this.groupType= groupType;
//        }
//
//        public GroupType getGroupType() {
//            return groupType;
//        }
//
//        public void setGroupType(GroupType groupType) {
//            this.groupType = groupType;
//        }
//
//        public int getSelection_min() {
//            return selection_min;
//        }
//
//        public void setSelection_min(int selection_min) {
//            this.selection_min = selection_min;
//        }
//
//        public int getSelection_max() {
//            return selection_max;
//        }
//
//        public void setSelection_max(int selection_max) {
//            this.selection_max = selection_max;
//        }
//    }
//
//    public static class ChildObject{
//
//        private Long id;
//        private String name;
//        private int count;
//        private boolean selected;
//        private ChildObject(Long id, String name, int count) {
//            this.id = id;
//            this.name = name;
//            this.count = count;
//        }
//
//        public Long getId() {
//            return id;
//        }
//
//        public void setId(Long id) {
//            this.id = id;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public int getCount() {
//            return count;
//        }
//
//        public void setCount(int count) {
//            this.count = count;
//        }
//
//        public boolean isSelected() {
//            return selected;
//        }
//
//        public void setSelected(boolean selected) {
//            this.selected = selected;
//        }
//    }

}
