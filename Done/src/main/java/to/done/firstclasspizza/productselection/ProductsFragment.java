package to.done.firstclasspizza.productselection;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.ImageView;

import org.apache.commons.lang3.text.WordUtils;

import to.done.firstclasspizza.MainActivity;
import to.done.firstclasspizza.R;
import to.done.firstclasspizza.common.FilterUpdateListener;
import to.done.firstclasspizza.common.NavDrawerFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import to.done.lib.Constants;
import to.done.lib.cart.Cart;
import to.done.lib.database.DBManager;
import to.done.lib.entity.nearestoutlets.Outlet;
import to.done.lib.entity.outletmenu.Category;
import to.done.lib.entity.outletmenu.Product;
import to.done.lib.sync.SyncListener;
import to.done.lib.sync.SyncManager;
import to.done.lib.utils.GoogleAnalyticsManager;
import to.done.lib.utils.Toolbox;

/**
 * Created by shadab mirza on 4/17/14.
 */
public class ProductsFragment extends NavDrawerFragment {




    private long outletId;
    private long companyId;
    private DBManager dbMan;
    private List<Product> productList;
    private Cart cart;
    private FilterUpdateListener filterUpdateListener;
    private ProductsNavDrawerExpListAdapter filtersAdapter;
    private ProductsListAdapter productsAdapter;
    private List<Category> categoryList;
    private Outlet o = null;
    private Handler mHandler;
    private ArrayList<Product>tempProds;
    int qty;
//    int paddingPixel,paddingDp,dp;
//    float density;

//    private CartListener cartlistener= new CartListener() {
//        @Override
//        public void onCartChange() {
//           Toolbox.writeToLog("Cart event recieved by ProductsFragment");
//            if(view.getVisibility()==View.GONE && bottomBar.getVisibility()==View.VISIBLE) {
//                view.setVisibility(View.VISIBLE);
//            }
//        }
//    };
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        dbMan = DBManager.getInstance(activity.getApplication());
        cart = Cart.getInstance();
        mHandler= new Handler();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((MainActivity)getActivity()).drawerOpen=false;
        if((ActionBarActivity)getActivity()!=null) {
            ImageView img = (ImageView) ((ActionBarActivity) getActivity()).getSupportActionBar().getCustomView().findViewById(R.id.action_home);
            img.setImageResource(R.drawable.logo_in_head);
            ((ActionBarActivity) getActivity()).getSupportActionBar().show();
            ImageView img1=(ImageView)((ActionBarActivity)getActivity()).getSupportActionBar().getCustomView().findViewById(R.id.imgBack);
            img1.setVisibility(View.INVISIBLE);
        }
            //((ActionBarActivity) getActivity()).getSupportActionBar().show();
        View v = inflater.inflate(R.layout.nav_drawer_template, null);
        ButterKnife.inject(this, v);
        super.onCreateView(inflater, container, savedInstanceState);

        //Toolbox.changeActionBarButton(getActivity(), Constants.AB_BTN_HOME);
        categoryList = dbMan.getCategories();

       // String outletName = getArguments().getString(Constants.OUTLET_NAME);
        //String subareaName = getArguments().getString(Constants.SUBAREA_NAME);
        outletId =Constants.OUTLET_ID_INT;// getArguments().getLong(Constants.OUTLET_ID);
        companyId =Constants.COMPANY_ID_INT;// getArguments().getLong(Constants.COMPANY_ID);


        filterUpdateListener = new FilterUpdateListener() {
            @Override
            public void onFilterUpdate() {
                productsAdapter.setFilterGroupList(filtersAdapter.getGroupObjectList());
                edtSearch.getText().clear();
                edtSearch.setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);
              //  mHandler.postDelayed(closeDrawer,300);
                //drawerLayout.closeDrawer(Gravity.LEFT);

            }
        };


        mHandler.postDelayed(animateQuant,1000);
        lstContent.addFooterView(LayoutInflater.from(getActivity()).inflate(R.layout.footer_50dp, null));

//        if (outletId != 0) {
//            o = dbMan.getOutletById(outletId);
//            productList = dbMan.getProductsForOutlet(outletId, Product.SortType.CATEGORY);
//
//            if (productList == null || productList.size() == 0) {

                SyncManager.getOutletMenu(getActivity(), outletId, outletMenuSyncListener);

//            } else {
//                Fragment f = new ProductsNavDrawerContentFrag();
//                Bundle b = new Bundle();
//                b.putLong(Constants.OUTLET_ID, outletId);
//
//
//                filtersAdapter = new ProductsNavDrawerExpListAdapter(getActivity(), outletId, filterUpdateListener);
//
//                exLstFilters.setAdapter(filtersAdapter);
//                for (int i = 0; i < filtersAdapter.getGroupCount(); i++) {
//                    exLstFilters.expandGroup(i);
//                }
//
//                productsAdapter = new ProductsListAdapter(getActivity(), companyId, outletId);
//
//                lstContent.setAdapter(productsAdapter);
//                lstContent.setOnScrollListener(scrollListener);
//                updateSubtitle((Product) lstContent.getItemAtPosition(lstContent.getFirstVisiblePosition()));
//
//            }
//        }
        Toolbox.changeActionBarTitle(getActivity(), "First Class Pizza", "Select Products");
        navHandleTitle.setText("Menu");
        navHandleSubTitle.setText("");
        SyncManager.getAreaAndSubareaForOutlet(getActivity(), outletId, areaSubareaListener);
        //cart.addCartListener(cartlistener);
//        if(cart.getFirstOrder()!=null && cart.getFirstOrder().getProducts()!=null && cart.getFirstOrder().getProducts().size()>0)
//        cart.sendCartEvent();
        v.setOnKeyListener(backKeyListen);
        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).drawerOpen=false;
        if((ActionBarActivity)getActivity()!=null) {
            ImageView img = (ImageView) ((ActionBarActivity) getActivity()).getSupportActionBar().getCustomView().findViewById(R.id.action_home);
            img.setImageResource(R.drawable.logo_in_head);
            ((ActionBarActivity) getActivity()).getSupportActionBar().show();
            ImageView img1=(ImageView)((ActionBarActivity)getActivity()).getSupportActionBar().getCustomView().findViewById(R.id.imgBack);
            img1.setVisibility(View.INVISIBLE);
        }

//        if(bottomBar.getVisibility()==View.VISIBLE){
//            lstContent.setMa
//        }
        //mHandler.postDelayed(animateQuant,500);



//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                Toolbox.animateQuantityIncrease(txtCartTotalVal);
//            }
//        },1000);

    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalyticsManager.sendScreenView(getActivity(), Constants.GA_SCREEN_MENU);


    }

    @Override
    public void onProceed() {

        if (Cart.getInstance().getOrders().keySet().size() > 0) {
//            if (!dbMan.isOutletOpenAt(o.getId(), Calendar.getInstance())) {
//                CustomDialog.createCustomDialog(getActivity(), "This outlet is closed now.", "OK", null, null, true, null).show();
//            } else {
                Bundle bundle = new Bundle();
                bundle.putLong(Constants.OUTLET_ID, outletId);
                Toolbox.changeScreen(getActivity(), Constants.SCREEN_CART, true);
//            }
        } else {
            Toolbox.showToastShort(getActivity(), "You must add products to cart before proceeding");
        }
    }
    public static void setMargins (View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
    }


    @Override
    public String getProceedButtonText() {
        return "Proceed";
    }

    AbsListView.OnScrollListener scrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView absListView, int i) {

        }

        @Override
        public void onScroll(AbsListView absListView, int i, int i2, int i3) {

            updateSubtitle((Product) lstContent.getItemAtPosition(lstContent.getFirstVisiblePosition()));


        }
    };


    public void updateSubtitle(Product p) {
        if (p == null || categoryList == null) {  return;}

        for (Category c : categoryList) {
            if (c == null || p.getCategory_id() == null || c.getId() == null) return;

            if (p.getCategory_id().longValue() == c.getId().longValue()) {
                navHandleSubTitle.setText(WordUtils.capitalizeFully(c.getName()));

            }

        }
    }

    SyncListener outletMenuSyncListener = new SyncListener() {
        @Override
        public void onSyncStart() {
            loadingStarted();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {

            if (getActivity() == null) return;

            loadingComplete();


            filtersAdapter = new ProductsNavDrawerExpListAdapter(getActivity(), outletId, filterUpdateListener);
            exLstFilters.setAdapter(filtersAdapter);
            for (int i = 0; i < filtersAdapter.getGroupCount(); i++) {
                exLstFilters.expandGroup(i);
            }
            categoryList = dbMan.getCategories();
            productsAdapter = new ProductsListAdapter(getActivity(), companyId, outletId);
            lstContent.setAdapter(productsAdapter);
            lstContent.setOnScrollListener(scrollListener);
            updateSubtitle((Product) lstContent.getItemAtPosition(lstContent.getFirstVisiblePosition()));

        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            if (getActivity() == null) return;
            loadingComplete();

            showError(ErrorType.NETWORK, reason);

        }
    };

    private SyncListener areaSubareaListener = new SyncListener() {
        @Override
        public void onSyncStart() {

        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
          //Toast.makeText(getActivity(), "area subarea success", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
          //Toast.makeText(getActivity(), "area subarea failed", Toast.LENGTH_SHORT).show();
        }
    };
    private Runnable closeDrawer= new Runnable() {
        @Override
        public void run() {
            drawerLayout.closeDrawer(Gravity.LEFT);
        }
    };
    private Runnable animateQuant= new Runnable() {
        @Override
        public void run() {
           Toolbox.animateQuantityIncrease(txtCartTotalVal);
        }
    };


    @Override
    public void onDestroy() {
        super.onDestroy();
//        productsAdapter.getImgFetcher().clearCache();
//        productsAdapter.getImgFetcher().closeCache();

    }


    View.OnKeyListener backKeyListen= new View.OnKeyListener() {
        @Override
        public boolean onKey(View view, int keyCode, KeyEvent event) {
            if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){


                // handle back button
                Toolbox.writeToLog("Navigation drawer:Handling back press");

                if(drawerLayout.isDrawerOpen(Gravity.LEFT)){
                    drawerLayout.closeDrawer(Gravity.LEFT);
                    Toolbox.writeToLog("Navigation drawer is open");
                }else{
                    getActivity().onBackPressed();
                    Toolbox.writeToLog("Navigation drawer is closed");
                }
                return true;

            }
            Toolbox.writeToLog("Navigation drawer:Handling back press");
            return false;


        }
    };

}
