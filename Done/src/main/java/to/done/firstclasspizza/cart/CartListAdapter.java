package to.done.firstclasspizza.cart;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.cart.Cart;
import to.done.lib.database.DBManager;
import to.done.lib.entity.order.Customization;
import to.done.lib.entity.outletmenu.Product;
import to.done.lib.ui.CustomDialog;
import to.done.lib.ui.DialogListener;
import to.done.lib.utils.Toolbox;

import to.done.firstclasspizza.MainActivity;
import to.done.firstclasspizza.R;

/**
 * Created by shadab mirza on 4/21/14.
 */
public class CartListAdapter extends BaseAdapter {

    private Context context;
    private DBManager dbMan;
    private Cart cart;
    private List<Product> productList;
    private LayoutInflater inflater;
    private long outletId;
    private long companyId;

    public CartListAdapter(Activity context, long companyId, long outletId) {
        this.context=context;
        this.outletId=outletId;
        this.companyId=companyId;
        dbMan= DBManager.getInstance(context.getApplication());
        cart= Cart.getInstance();
//        productList=dbMan.getProductsForOutlet(outletId);
        inflater=LayoutInflater.from(context);
        productList=cart.getProductsWithoutDuplicate(outletId);

        for(Product p:productList){
            p.getAdditionalProperties().put("customizationString",dbMan.getCustomizationString(p.getCustomization()));
        }

    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Product getItem(int i) {
        return productList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return productList.get(i).getId().longValue();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {


        ViewHolder viewHolder;

        if(convertView==null){

            convertView=inflater.inflate(R.layout.cart_list_item,null);

            viewHolder=new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        }
        else{
            viewHolder=(ViewHolder)convertView.getTag();
        }

        viewHolder.imgPlus.setTag(getItem(i));
        viewHolder.imgMinus.setTag(getItem(i));
        viewHolder.imgDelete.setTag(getItem(i));
        viewHolder.txtProductName.setTag(getItem(i));
        viewHolder.txtProductDesc.setTag(getItem(i));
        viewHolder.txtProductPrice.setTag(getItem(i));

        viewHolder.imgPlus.setOnClickListener(clickListener);
        viewHolder.imgMinus.setOnClickListener(clickListener);
        viewHolder.imgDelete.setOnClickListener(clickListener);

        viewHolder.txtProductName.setOnClickListener(clickListener);
        viewHolder.txtProductDesc.setOnClickListener(clickListener);
        viewHolder.txtProductPrice.setOnClickListener(clickListener);

        int qty=cart.getProductQtyWithCustomization(outletId,getItem(i));
        viewHolder.txtProductName.setText(getItem(i).getName());

        if(getItem(i).getAdditionalProperties().get("customizationString")!=null)
        {
            viewHolder.txtProductDesc.setText(getItem(i).getAdditionalProperties().get("customizationString").toString());
        }

        double productTotal=getItem(i).getPrice();
        for(Customization c: getItem(i).getCustomization()){
            productTotal+=c.getPrice();
        }

        viewHolder.txtProductPrice.setText(Toolbox.formatDecimal(qty * productTotal));
        viewHolder.txtProductQuantity.setText(""+qty);
        return convertView;
    }

    private View.OnClickListener clickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            final Product prod =(Product)view.getTag();

            switch (view.getId()){

                case R.id.img_plus:{
                    cart.addToCart(companyId,outletId,prod);

                    TextView txtQty=(TextView)((View)view.getParent()).findViewById(R.id.txt_product_quantity);

                    Toolbox.animateQuantityIncrease(txtQty);
                    Toolbox.animateButtonPress(view);

                    break;
                }
                case R.id.img_minus:{
                    TextView txtProductQuantity=(TextView)((ViewGroup)view.getParent()).findViewById(R.id.txt_product_quantity);
                    if(Integer.parseInt(txtProductQuantity.getText().toString())>1) {
                        cart.removeFromCart(outletId, prod);
                        TextView txtQty=(TextView)((View)view.getParent()).findViewById(R.id.txt_product_quantity);
                        Toolbox.animateQuantityDecrease(txtQty);
                        Toolbox.animateButtonPress(view);
                    }
                    break;
                }
                case R.id.img_delete:{
                   int qty= Cart.getInstance().getProductQtyWithCustomization(cart.getFirstOrder().getOutlet_id().longValue(), prod);
                    Toolbox.animateButtonPress(view);
                    if(cart.getFirstOrder().getProducts().size()-qty<=0){
                        {
                            CustomDialog.createCustomDialog(context, "Are you sure you want to empty the cart?", "Yes", "No", null, false, new DialogListener() {
                                @Override
                                public void onPositiveBtnClick() {
                                    cart.removeAllMatching(outletId, prod);

//                                    productList=cart.getProductsWithoutDuplicate(outletId);
//                                    Toolbox.writeToLog("for producct "+productList);
                                    notifyDataSetChanged();
                                    ((MainActivity) context).onBackPressed();
                                }

                                @Override
                                public void onNegativeBtnClick() {

                                }

                                @Override
                                public void onMiddleBtnClick() {

                                }
                            });
                        }
                    }else{
                        cart.removeAllMatching(outletId,prod);
                        //productList=cart.getProductsWithoutDuplicate(outletId);

                        //
                    }

                    break;
                }

            }
            //productList.clear();
            productList = cart.getProductsWithoutDuplicate(outletId);
            notifyDataSetChanged();

        }
    };
    static class ViewHolder{

        @InjectView(R.id.txt_product_name)
        TextView txtProductName;

        @InjectView(R.id.txt_product_desc)
        TextView txtProductDesc;

        @InjectView(R.id.txt_product_price)
        TextView txtProductPrice;

        @InjectView(R.id.txt_product_quantity)
        TextView txtProductQuantity;

        @InjectView(R.id.img_plus)
        ImageView imgPlus;

        @InjectView(R.id.img_minus)
        ImageView imgMinus;


        @InjectView(R.id.img_delete)
        ImageView imgDelete;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

    }
}
