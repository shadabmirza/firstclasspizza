package to.done.firstclasspizza.common;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import to.done.firstclasspizza.R;

/**
 * Created by viva on 18/6/14.
 */
public class GCMPopupDialogFragment extends DialogFragment {


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        String msg = bundle.getString("message");
        final Dialog dialog = new Dialog(this.getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.gcm_popup_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(false);

        Button okButton = (Button) dialog.findViewById(R.id.gcm_popup_ok_button);
        TextView gcm_popup_text = (TextView) dialog.findViewById(R.id.gcm_popup_text);

        if (msg != null)
            gcm_popup_text.setText(msg);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        dialog.show();


        return dialog;
    }
}
