package to.done.firstclasspizza.common;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ExpandableListView;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nineoldandroids.animation.ObjectAnimator;

import butterknife.InjectView;
import to.done.lib.ui.FontEditText;
import to.done.lib.ui.LoadingViewWithThread;
import to.done.lib.utils.Toolbox;
import to.done.firstclasspizza.MainActivity;
import to.done.firstclasspizza.R;


import static to.done.lib.Constants.INTENT_NAV_DRAWER;
import static to.done.lib.Constants.NAV_DRAWER_ACTION;
import static to.done.lib.Constants.NAV_DRAWER_ACTON_CLOSE;
import static to.done.lib.Constants.NAV_DRAWER_ACTON_OPEN;
import static to.done.lib.Constants.NAV_DRAWER_ACTON_SWITCH_STATUS;
import static to.done.lib.Constants.TAG;

/**
 * Created by shadab mirza on 4/17/14.
 */
public abstract class NavDrawerFragment extends BottomBarFragment {

    private BroadcastReceiver drawerEventReceiver;

    @InjectView(R.id.view)
    public View view;

    @InjectView(R.id.drawer_layout)
    public DrawerLayout drawerLayout;

    @InjectView(R.id.img_drawer_handle)
    public ImageView drawerHandle;

    @InjectView(R.id.img_search)
    public ImageView imgSearch;

    @InjectView(R.id.lst_content)
    public ListView lstContent;

    @InjectView(R.id.progress_bar)
    public LoadingViewWithThread progressBar;

    @InjectView(R.id.txt_nav_handle_title)
    public TextView navHandleTitle;

    @InjectView(R.id.txt_nav_handle_subtitle)
    public TextView navHandleSubTitle;

    @InjectView(R.id.edt_search)
    public FontEditText edtSearch;

    @InjectView(R.id.nav_drawer_handle)
    public ViewGroup navDrawerHandle;

    @InjectView(R.id.ex_lst_filters)
    public ExpandableListView exLstFilters;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        edtSearch.getText().clear();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);
    }

    @Override
    public void onPause() {
        super.onPause();
        edtSearch.getText().clear();
        InputMethodManager imm = (InputMethodManager) this.getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        createDrawerEventReceiver();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(drawerEventReceiver, new IntentFilter(INTENT_NAV_DRAWER));


        View.OnClickListener navHandleClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toolbox.changeNavDrawerStatus(getActivity(), NAV_DRAWER_ACTON_SWITCH_STATUS);
            }
        };

        navDrawerHandle.setOnClickListener(navHandleClickListener);
        navHandleTitle.setOnClickListener(navHandleClickListener);
        drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

                if (slideOffset < 0.5) {
                    ObjectAnimator.ofFloat(drawerHandle, "translationX", -drawerHandle.getWidth() * slideOffset).start();
                }

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                ((MainActivity) getActivity()).drawerOpen = true;
                Log.e(TAG, "onDrawerOpened()");
                edtSearch.getText().clear();

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);
                edtSearch.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                ((MainActivity) getActivity()).drawerOpen = false;
                Log.e(TAG, "onDrawerClosed()");
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                Log.e(TAG, "onDrawerStateChanged()");
            }
        });
        lstContent.addFooterView(LayoutInflater.from(getActivity()).inflate(R.layout.footer_50dp, null));
        edtSearch.addEditCancelListener(new FontEditText.EditCancelListener() {
            @Override
            public void onCancelEvent(FontEditText editText, int cancelCode) {
                if (editText.getText().toString().length() == 0) {
                    edtSearch.getText().clear();

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);
                    edtSearch.setVisibility(View.INVISIBLE);
                }
            }
        });
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() > 0 && lstContent.getAdapter() != null) {
                    ((Filterable) lstContent.getAdapter()).getFilter().filter(editable.toString());
                } else if (lstContent.getAdapter() != null) {
                    ((Filterable) lstContent.getAdapter()).getFilter().filter(null);
                }

            }
        });

        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toolbox.changeNavDrawerStatus(getActivity(), NAV_DRAWER_ACTON_CLOSE);
                if (edtSearch.getVisibility() != View.VISIBLE) {
                    edtSearch.setVisibility(View.VISIBLE);
                    edtSearch.requestFocus();
                    edtSearch.getText().clear();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(edtSearch, InputMethodManager.SHOW_IMPLICIT);
                } else {
//                    if(edtSearch.getText()!=null && edtSearch.getText().length()>=0)
                    edtSearch.getText().clear();
                    edtSearch.setVisibility(View.INVISIBLE);
                }
            }
        });
        lstContent.setOnKeyListener(backKeyListen);
        return null;
    }

    @Override
    public void onStart() {
        super.onStart();

    }


    public BroadcastReceiver createDrawerEventReceiver() {

        drawerEventReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int drawerAction = intent.getIntExtra(NAV_DRAWER_ACTION, 0);


                switch (drawerAction) {

                    case NAV_DRAWER_ACTON_SWITCH_STATUS: {

                        if (drawerLayout.isDrawerOpen(Gravity.LEFT) || drawerLayout.isDrawerVisible(Gravity.LEFT)) {
                            drawerLayout.closeDrawer(Gravity.LEFT);
                            //((MainActivity) getActivity()).drawerOpen = false;
                        } else {
                            drawerLayout.openDrawer(Gravity.LEFT);
                            //((MainActivity) getActivity()).drawerOpen = true;
                        }

                        break;
                    }

                    case NAV_DRAWER_ACTON_OPEN: {
                        if (!drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                            drawerLayout.openDrawer(Gravity.LEFT);
                            //((MainActivity) getActivity()).drawerOpen = false;
                        }
                        break;
                    }


                    case NAV_DRAWER_ACTON_CLOSE: {
                        if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                            drawerLayout.closeDrawer(Gravity.LEFT);
                            //((MainActivity) getActivity()).drawerOpen = false;
                        }
                        break;
                    }

                }


            }
        };
        return drawerEventReceiver;
    }

    @Override
    public void onDestroyView() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(drawerEventReceiver);
        super.onDestroyView();


    }

//    @Override
//    public void onProceed()
//    {
//        SyncManager.checkOrder(getActivity(), checkOrderSyncListener);
//        Toolbox.showToastShort(getActivity(), "Calling check-order");
//    }

    public void replaceDrawerContentWithFrag(Fragment frag, Bundle b) {

        if (getActivity() == null || getActivity().getSupportFragmentManager() == null) return;

        if (b == null) {
            b = new Bundle();
        }

        frag.setArguments(b);
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.left_drawer, frag);
        ft.commit();
    }

    public void replaceScreenContentWithFrag(Fragment frag, Bundle b) {

        if (getActivity() == null || getActivity().getSupportFragmentManager() == null) return;

        if (b == null) {
            b = new Bundle();
        }

        frag.setArguments(b);
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, frag);
        ft.commit();
    }

    public void loadingStarted() {

        progressBar.setVisibility(View.VISIBLE);
        progressBar.startLoadingAnimation();
        lstContent.setVisibility(View.GONE);

    }

    public void loadingComplete() {
        if (progressBar.isShown()) {
            progressBar.stopAnimation();
            progressBar.setVisibility(View.GONE);
        }
        lstContent.setVisibility(View.VISIBLE);

    }


//    SyncListener checkOrderSyncListener=new SyncListener() {
//        @Override
//        public void onSyncStart() {
//
//        }
//
//        @Override
//        public void onSyncProgress(float percentProgress, long requestTimestamp) {
//
//        }
//
//        @Override
//        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
//
//            if(getActivity()==null)return;
//            Toolbox.showToastShort(getActivity(),"check-order completed successfully");
//        }
//
//        @Override
//        public void onSyncFailure(String url, String reason, long requestTimestamp) {
//            Toolbox.showToastShort(getActivity(),"check-order failed "+reason);
//
//        }
//    };

    View.OnKeyListener backKeyListen = new View.OnKeyListener() {
        @Override
        public boolean onKey(View view, int keyCode, KeyEvent event) {
            if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                // handle back button
                Toolbox.writeToLog("Navigation drawer:Handling back press");

                if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    drawerLayout.closeDrawer(Gravity.LEFT);
                    Toolbox.writeToLog("Navigation drawer is open");
                } else {
                    getActivity().onBackPressed();
                    Toolbox.writeToLog("Navigation drawer is closed");
                }
                return true;

            }

            return false;


        }
    };
}
