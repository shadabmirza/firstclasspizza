package to.done.firstclasspizza.common;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import to.done.firstclasspizza.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import butterknife.Optional;
import to.done.lib.Constants;
import to.done.lib.ui.ProgressDotView;

/**
 * Created by shadab mirza on 4/17/14.
 */
public abstract class TopAndBottomBarFragment extends BottomBarFragment {


    @InjectView(R.id.txt_cart)
    @Optional
    TextView txtCart;

    @InjectView(R.id.txt_order_type)
    TextView txtOrderType;

    @InjectView(R.id.txt_order_confirm)
    TextView txtOrderConfirm;



    @InjectView(R.id.prog_dot_cart)
    ProgressDotView progDotCart;

    @InjectView(R.id.prog_dot_order_type)
    ProgressDotView progDotOrderType;

    @InjectView(R.id.prog_dot_order_confirm)
    ProgressDotView progDotOrderConfirm;


    @InjectView(R.id.rl_progress_dots)
    ViewGroup rlProgressDots;

    public static final int CART=0,ORDER_TYPE=1,ORDER_CONFIRM=2,ADDRESS=3,HIDDEN=4;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);


        switch (getScreenIndex()){

            case CART:{
                txtCart.setSelected(true);
                progDotCart.setState(ProgressDotView.STATE_IN_PROGRESS);
                break;
            }
            case ORDER_TYPE:{
                txtCart.setSelected(true);
                txtOrderType.setSelected(true);
                progDotCart.setState(ProgressDotView.STATE_COMPLETE);
                progDotOrderType.setState(ProgressDotView.STATE_IN_PROGRESS);
                break;
            }
            case ORDER_CONFIRM:{
                txtCart.setSelected(true);
                txtOrderType.setSelected(true);
                txtOrderConfirm.setSelected(true);
                progDotCart.setState(ProgressDotView.STATE_COMPLETE);
                progDotOrderType.setState(ProgressDotView.STATE_COMPLETE);
                progDotOrderConfirm.setState(ProgressDotView.STATE_IN_PROGRESS);
                break;
            }
            case ADDRESS:{
                txtCart.setSelected(true);
                txtOrderType.setSelected(true);
                txtOrderConfirm.setSelected(true);
                //txtAddress.setSelected(true);
                progDotCart.setState(ProgressDotView.STATE_COMPLETE);
                progDotOrderType.setState(ProgressDotView.STATE_COMPLETE);
                progDotOrderConfirm.setState(ProgressDotView.STATE_COMPLETE);
//                progDotAddress.setState(ProgressDotView.STATE_IN_PROGRESS);
                break;
            }
            default:{
                rlProgressDots.setVisibility(View.GONE);
                break;
            }
        }


//        txtCart.setOnClickListener(clickListener);
//        txtOrderType.setOnClickListener(clickListener);
//        txtOrderConfirm.setOnClickListener(clickListener);
//        //txtAddress.setOnClickListener(clickListener);
//
//        progDotCart.setOnClickListener(clickListener);
//        progDotOrderType.setOnClickListener(clickListener);
//        progDotOrderConfirm.setOnClickListener(clickListener);
//        progDotAddress.setOnClickListener(clickListener);

        return null;
    }

    @Override
    public void onStart() {
        super.onStart();

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }


    public void replaceDrawerContentWithFrag(Fragment frag, Bundle b){

        if(getActivity()==null || getActivity().getSupportFragmentManager()==null)return;

        if(b==null)
        {
            b=new Bundle();
        }

        frag.setArguments(b);
        FragmentTransaction ft=getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.left_drawer, frag);
        ft.commit();
    }
    public void replaceScreenContentWithFrag(Fragment frag, Bundle b){

        if(getActivity()==null || getActivity().getSupportFragmentManager()==null)return;

        if(b==null)
        {
            b=new Bundle();
        }

        frag.setArguments(b);
        FragmentTransaction ft=getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, frag);
        ft.commit();
    }

    public void loadingStarted(){


    }
    public void loadingComplete(){

    }

    public abstract int getScreenIndex();


    View.OnClickListener clickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            List<String>backStackEntryList=new ArrayList<String>();
            for(int i=0;i<getActivity().getSupportFragmentManager().getBackStackEntryCount();i++){
                FragmentManager.BackStackEntry  entry=getActivity().getSupportFragmentManager().getBackStackEntryAt(i);
                backStackEntryList.add(entry.getName());
            }


            switch(view.getId()){

                case R.id.txt_cart:;
                case R.id.prog_dot_cart:
                    if(backStackEntryList.indexOf("screen_id_"+Constants.SCREEN_CART)!=-1) {
                        if(getScreenIndex()!=CART) {
                            getActivity().getSupportFragmentManager().popBackStack("screen_id_" + Constants.SCREEN_CART, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        }

                    }
                    break;

                case R.id.txt_order_type:;
                case R.id.prog_dot_order_type:
                    if(backStackEntryList.indexOf("screen_id_"+Constants.SCREEN_PERSONAL_DETAILS)!=-1) {
                        if(getScreenIndex()!=ORDER_TYPE) {
                            getActivity().getSupportFragmentManager().popBackStack("screen_id_" + Constants.SCREEN_PERSONAL_DETAILS, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        }
                        }else{
                        //Toolbox.changeScreen(getActivity(),Constants.SCREEN_PERSONAL_DETAILS.);
                    }
                    break;
                case R.id.txt_order_confirm:;
                case R.id.prog_dot_order_confirm:
                    if(backStackEntryList.indexOf("screen_id_"+Constants.SCREEN_CONFIRM_ORDER)!=-1) {
                        if(getScreenIndex()!=ADDRESS)
                        getActivity().getSupportFragmentManager().popBackStack("screen_id_" + Constants.SCREEN_CONFIRM_ORDER, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                    break;

                case R.id.txt_address:;
                //case R.id.prog_dot_address:
                    break;

            }

        }
    };

}
