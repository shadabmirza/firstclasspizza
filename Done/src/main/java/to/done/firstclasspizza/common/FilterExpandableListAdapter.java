package to.done.firstclasspizza.common;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.commons.lang3.text.WordUtils;

import to.done.firstclasspizza.MainActivity;
import to.done.firstclasspizza.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.database.DBManager;
import to.done.lib.utils.Toolbox;

/**
 * Created by shadab mirza on 5/7/14.
 */
public abstract class FilterExpandableListAdapter extends BaseExpandableListAdapter{

    protected Activity context;
    protected DBManager dbMan;
    protected List<GroupObject>groupObjectList=new ArrayList<GroupObject>();
    protected LayoutInflater inflater;
    protected FilterUpdateListener filterUpdateListener;
    protected long outletId;


    protected FilterExpandableListAdapter(Activity context, FilterUpdateListener filterUpdateListener) {
        this.context = context;
        this.filterUpdateListener = filterUpdateListener;
        dbMan=DBManager.getInstance(context.getApplication());
        inflater = LayoutInflater.from(context);
        groupObjectList=getGroupsList();
    }

    protected FilterExpandableListAdapter(Activity context,long outletId, FilterUpdateListener filterUpdateListener) {
        this.context = context;
        this.filterUpdateListener = filterUpdateListener;
        this.outletId=outletId;
        dbMan=DBManager.getInstance(context.getApplication());
        inflater = LayoutInflater.from(context);
        groupObjectList=getGroupsList();
    }

    @Override
    public View getGroupView(int i, boolean b, View convertView, ViewGroup viewGroup) {


        ViewHolderGroup viewHolderGroup;

        if(convertView==null){

            convertView=inflater.inflate(R.layout.outlets_nav_drawer_ex_list_group,null);
            viewHolderGroup=new ViewHolderGroup(convertView);
            convertView.setTag(viewHolderGroup);
        }
        else{
            viewHolderGroup=(ViewHolderGroup)convertView.getTag();
        }
//        if(getGroup(i).getName().toLowerCase().contains("sort by")){
//            convertView.setVisibility(View.GONE);
//        }else
        viewHolderGroup.txtOutletAttrGroup.setText(getGroup(i).getName());
        Log.e("FilterExpandableListAdapter","getGroupView()-->"+getGroup(i).getName());

        return convertView;
    }

    @Override
    public View getChildView(final int i, final int i2, boolean b, View convertView, ViewGroup viewGroup) {
        ViewHolderChild viewHolderChild;
        if(convertView==null){

            convertView=inflater.inflate(R.layout.outlets_nav_drawer_ex_list_child,null);

            viewHolderChild=new ViewHolderChild(convertView);

            convertView.setTag(viewHolderChild);
        }
        else{
            viewHolderChild=(ViewHolderChild)convertView.getTag();
        }

//        if(i2==getChildrenCount(i)-1){
//            viewHolderChild.lineWhite.setVisibility(View.INVISIBLE);
//            viewHolderChild.lineGray.setVisibility(View.INVISIBLE);
//        }
//        else
//        {
//            viewHolderChild.lineWhite.setVisibility(View.VISIBLE);
//            viewHolderChild.lineGray.setVisibility(View.VISIBLE);
//        }
        viewHolderChild.txtOutletAttr.setTag(getChild(i,i2));
        viewHolderChild.imgRadio.setTag(getChild(i,i2));

        if(getGroup(i).getSelection_min()==1 && getGroup(i).getSelection_max()==1) {

            int drawableId= Toolbox.getAttributeResourceId(context, R.attr.themeRadioSelector, MainActivity.currentThemeId);
//            Drawable radioButtonDrawable = context.getResources().getDrawable(drawableId);
            viewHolderChild.imgRadio.setImageResource(drawableId);
            viewHolderChild.imgRadio.setBackgroundColor(context.getResources().getColor(R.color.transparent));
        }
        else
        {
            int drawableId= Toolbox.getAttributeResourceId(context, R.attr.themeCheckboxSelector, MainActivity.currentThemeId);

            viewHolderChild.imgRadio.setImageResource(drawableId);//R.drawable.red_bright_checkbox_selector);
            viewHolderChild.imgRadio.setBackgroundColor(context.getResources().getColor(R.color.white));
        }
        viewHolderChild.txtOutletAttr.setText(WordUtils.capitalizeFully(getChild(i, i2).getName()));
        Log.e("FilterExpandableListAdapter","getChildView()-->"+getChild(i, i2).getName());
        if(getGroup(i).getType()== GroupObject.GroupType.SORT){

            viewHolderChild.txtCount.setText(getSortHint(getChild(i, i2).getId()));
        }
        else{
            viewHolderChild.txtCount.setText("("+getChild(i,i2).getCount()+")");
        }


        viewHolderChild.navDrawerExListChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onFilterClicked(view,i,i2);

//                getChild(i,i2).setSelected(!getChild(i,i2).isSelected());
                if(filterUpdateListener!=null)
                {
                    filterUpdateListener.onFilterUpdate();
                }
//                notifyDataSetChanged();

            }
        });

        viewHolderChild.imgRadio.setSelected(getChild(i,i2).isSelected());
        viewHolderChild.txtOutletAttr.setSelected(getChild(i,i2).isSelected());
        viewHolderChild.txtCount.setSelected(getChild(i,i2).isSelected());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i2) {
        return false;
    }

    public List<GroupObject> getGroupObjectList() {
        return groupObjectList;
    }

    public void setGroupObjectList(List<GroupObject> groupObjectList) {
        this.groupObjectList = groupObjectList;
    }
    @Override
    public int getGroupCount() {
        return groupObjectList.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return groupObjectList.get(i).getChildObjectList().size();
    }

    @Override
    public GroupObject getGroup(int i) {
        return groupObjectList.get(i);
    }

    @Override
    public ChildObject getChild(int i, int i2) {
        return groupObjectList.get(i).getChildObjectList().get(i2);
    }

    @Override
    public long getGroupId(int i) {
        return groupObjectList.get(i).getId().longValue();
    }

    @Override
    public long getChildId(int i, int i2) {
        return groupObjectList.get(i).getChildObjectList().get(i2).getId().longValue();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    private void onFilterClicked(View view, int i, int i2){

        View imgRadio=view.findViewById(R.id.img_radio);

        GroupObject groupObject=getGroup(i);
        ChildObject childObject=getChild(i, i2);

        boolean radio=false;

        if(groupObject.getSelection_min()==1&&groupObject.getSelection_max()==1){
            radio=true;
        }



        if(childObject.isSelected()){

            if(!radio){

                if(groupObject.getSelection_min()>0) {

                    int currSelectionCount=getCurrentSelectionCount(groupObject);

                    if (currSelectionCount > groupObject.getSelection_min()) {
                        childObject.setSelected(false);
                        imgRadio.setSelected(false);
                    }
                }
                else
                {
                    childObject.setSelected(false);
                    imgRadio.setSelected(false);
                }
            }



        }
        else {

            if(!radio) {
                int currSelectionCount=getCurrentSelectionCount(groupObject);


                if(groupObject.getSelection_max()<=0){
                    childObject.setSelected(true);
                    imgRadio.setSelected(true);
                }
                else if (currSelectionCount < groupObject.getSelection_max()) {
                    childObject.setSelected(true);
                    imgRadio.setSelected(true);
                }
            }
            else
            {

                for(ChildObject c:groupObject.getChildObjectList()){
                    if(c.isSelected()){
                        c.setSelected(false);
                    }

                }

                childObject.setSelected(true);
                imgRadio.setSelected(true);

            }
        }
        notifyDataSetChanged();

    }

    private int getCurrentSelectionCount(GroupObject groupObject){

        int currSelectionCount=0;

        for(ChildObject c:groupObject.getChildObjectList()){
            if(c.isSelected()){
                currSelectionCount++;
            }
        }

        return currSelectionCount;
    }

    public abstract String getSortHint(long id);

    public abstract List<GroupObject>getGroupsList();

    static class ViewHolderGroup{

        @InjectView(R.id.txt_outlet_attr_group)
        TextView txtOutletAttrGroup;

        public ViewHolderGroup(View view) {
            ButterKnife.inject(this, view);
        }
    }

    static class ViewHolderChild{

        @InjectView(R.id.txt_filter_name)
        TextView txtOutletAttr;

        @InjectView(R.id.txt_count)
        TextView txtCount;

        @InjectView(R.id.img_radio)
        ImageView imgRadio;

        @InjectView(R.id.line_white)
        View lineWhite;

        @InjectView(R.id.line_gray)
        View lineGray;

        @InjectView(R.id.nav_drawer_ex_list_child)
        ViewGroup navDrawerExListChild;

        public ViewHolderChild(View view) {
            ButterKnife.inject(this, view);
        }
    }


    public static class GroupObject{

        private List<ChildObject> childObjectList=new ArrayList<ChildObject>();
        private Long id;
        private String name;
        public static enum GroupType{CATEGORY,ATTRIBUTE,SORT};
        private GroupType groupType;
        private int selection_min,selection_max;

        public GroupObject(List<ChildObject> childObjectList, Long id, String name, GroupType groupType, int selection_min, int selection_max) {
            this.childObjectList = childObjectList;
            this.id = id;
            this.name = name;
            this.groupType = groupType;
            this.selection_min=selection_min;
            this.selection_max=selection_max;
        }

        public List<ChildObject> getChildObjectList() {
            return childObjectList;
        }

        public void setChildObjectList(List<ChildObject> childObjectList) {
            this.childObjectList = childObjectList;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public GroupType getType() {
            return groupType;
        }

        public void setType(GroupType groupType) {
            this.groupType= groupType;
        }

        public GroupType getGroupType() {
            return groupType;
        }

        public void setGroupType(GroupType groupType) {
            this.groupType = groupType;
        }

        public int getSelection_min() {
            return selection_min;
        }

        public void setSelection_min(int selection_min) {
            this.selection_min = selection_min;
        }

        public int getSelection_max() {
            return selection_max;
        }

        public void setSelection_max(int selection_max) {
            this.selection_max = selection_max;
        }
    }

    public static class ChildObject{

        private Long id;
        private String name;
        private int count;
        private boolean selected;
        public ChildObject(Long id, String name, int count) {
            this.id = id;
            this.name = name;
            this.count = count;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }
    }
}
