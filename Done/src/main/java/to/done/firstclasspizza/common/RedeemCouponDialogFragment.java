package to.done.firstclasspizza.common;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import to.done.firstclasspizza.R;

import java.util.Map;

import to.done.lib.Constants;
import to.done.lib.cart.Cart;
import to.done.lib.entity.order.Applied_offer;
import to.done.lib.entity.order.Order;
import to.done.lib.sync.SyncListener;
import to.done.lib.sync.SyncManager;
import to.done.lib.utils.Toolbox;

/**
 * Created by viva on 17/6/14.
 */
public class RedeemCouponDialogFragment extends DialogFragment {
    public static final String tag = "RedeemCouponDialogFragment";

    public RedeemCouponDialogFragment() {

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(this.getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.redeem_coupon_dialog);

//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.width = width;
//        lp.height = height;
//        dialog.getWindow().setAttributes(lp);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.TOP ;

        lp.y = 80;
        window.setAttributes(lp);

        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(false);

        Button okButton = (Button) dialog.findViewById(R.id.redeem_coupon_ok_button);
        final EditText redeem_coupon_text = (EditText) dialog.findViewById(R.id.redeem_coupon_text);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String coupon = redeem_coupon_text.getText().toString();
                if (coupon != null && !coupon.isEmpty()) {
                    Cart cart = Cart.getInstance();
                    //cart.setCouponCode(coupon);
                    Map<Long, Order> orders = cart.getOrders();
                    for (long outletId : orders.keySet()) {
                        Order o = orders.get(outletId);

                        if (o != null)
                            o.setCoupon_code(coupon);

                    }

                    SyncManager.checkOrder(getActivity(), checkOrder);
                    //dismiss();
                }
            }
        });

        dialog.show();
        return dialog;
    }


    SyncListener checkOrder = new SyncListener() {
        @Override
        public void onSyncStart() {
            //hideError();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
//            Log.e(tag, "onSyncSuccess()");
            if (RedeemCouponDialogFragment.this.getActivity() == null) {
//                Log.e(tag, "getActivity() is null");
                return;
            }
            if(Cart.getInstance().getRespCheckOrder().getData().getOrders().get(0).getApplied_offer().get(0)!=null) {
                Applied_offer aof=Cart.getInstance().getRespCheckOrder().getData().getOrders().get(0).getApplied_offer().get(0);
                Toolbox.showToastLong(getActivity(), "Offer " +aof.getId()+" "+aof.getCoupon_code()+" "+aof.getName()+" rs"+aof.getDiscount_amount());
            }

            Toolbox.changeScreen(RedeemCouponDialogFragment.this.getActivity(), Constants.SCREEN_CONFIRM_ORDER, false);
            dismiss();
        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            //showError(ErrorFragment.ErrorType.NETWORK, reason);
            dismiss();
        }
    };
}
