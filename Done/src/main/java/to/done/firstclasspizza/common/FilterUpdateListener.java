package to.done.firstclasspizza.common;

/**
 * Created by shadab mirza on 4/30/14.
 */
public interface FilterUpdateListener {

    void onFilterUpdate();
}
