package to.done.firstclasspizza.common;

import android.graphics.Color;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

import to.done.lib.utils.Toolbox;
import to.done.firstclasspizza.R;

import butterknife.InjectView;
import butterknife.Optional;
import to.done.lib.Constants;

/**
 * Created by shadab mirza on 5/26/14.
 */
public class ErrorFragment extends Fragment {


    @InjectView(R.id.error_bar)
    @Optional
    RelativeLayout rlErrorBar;

    @InjectView(R.id.txt_error)
    @Optional
    TextView txtError;

    @InjectView(R.id.img_error)
    @Optional
    ImageView imgError;

    private Handler handler = new Handler();

    public static enum ErrorType {NETWORK, ANNOUNCEMENT, NO_RESULTS}

    ;

    private boolean readyForHiding = false;

    @Override
    public void onStart() {
        super.onStart();


    }

    public void showError(ErrorType errorType, String message, int color) {

        if (rlErrorBar == null || txtError == null || imgError == null) {

            String exceptionMessage = "error_bar not present in layout. Make sure to add <include layout=\"@layout/error_bar/> to your layout";

//            Toolbox.writeToLog(Log.ERROR,exceptionMessage);
            throw new IllegalStateException(exceptionMessage);

        }

        if (message == null || message.length() == 0) {
            message = Constants.ERROR_MESSAGE_GENERIC;
        }

        txtError.setText(message);
        rlErrorBar.setBackgroundColor(color);
        if (color == Color.parseColor("#629632")) {
            txtError.setTextColor(Color.BLACK);
        } else {
            txtError.setTextColor(Color.parseColor("#FFFCDC"));
        }

        imgError.setVisibility(View.GONE);
        switch (errorType) {

            case NETWORK:
                imgError.setImageResource(R.drawable.notification);
                break;
            case ANNOUNCEMENT:
                imgError.setImageResource(R.drawable.announcement);
                break;
            case NO_RESULTS:
                imgError.setImageResource(R.drawable.not_found);
                break;

        }


        AnimatorSet animSetDown = new AnimatorSet();

        Animator animSlideDown = ObjectAnimator.ofFloat(rlErrorBar, "translationY", -rlErrorBar.getHeight(), 0);
        Animator animFadeIn = ObjectAnimator.ofFloat(rlErrorBar, "alpha", 0, 1);

        animSetDown.playTogether(animSlideDown, animFadeIn);
        animSetDown.setDuration(1000);

        animSetDown.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                rlErrorBar.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                Toolbox.wobbleView(getActivity(), imgError);
                readyForHiding = true;
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        animSetDown.start();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                hideError();
            }
        }, Constants.ERROR_TIMEOUT);


    }

    public void showError(ErrorType errorType, String message) {

        if (rlErrorBar == null || txtError == null || imgError == null) {

            String exceptionMessage = "error_bar not present in layout. Make sure to add <include layout=\"@layout/error_bar/> to your layout";

//            Toolbox.writeToLog(Log.ERROR,exceptionMessage);
            throw new IllegalStateException(exceptionMessage);

        }

        if (message == null || message.length() == 0) {
            message = Constants.ERROR_MESSAGE_GENERIC;
        }

        txtError.setText(message);


        switch (errorType) {

            case NETWORK:
                imgError.setImageResource(R.drawable.notification);
                break;
            case ANNOUNCEMENT:
                imgError.setImageResource(R.drawable.announcement);
                break;
            case NO_RESULTS:
                imgError.setImageResource(R.drawable.not_found);
                break;

        }


        AnimatorSet animSetDown = new AnimatorSet();

        Animator animSlideDown = ObjectAnimator.ofFloat(rlErrorBar, "translationY", -rlErrorBar.getHeight(), 0);
        Animator animFadeIn = ObjectAnimator.ofFloat(rlErrorBar, "alpha", 0, 1);

        animSetDown.playTogether(animSlideDown, animFadeIn);
        animSetDown.setDuration(1000);

        animSetDown.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                rlErrorBar.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                Toolbox.wobbleView(getActivity(), imgError);
                readyForHiding = true;
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        animSetDown.start();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                hideError();
            }
        }, Constants.ERROR_TIMEOUT);


    }

    public void hideError() {
        if (rlErrorBar == null || txtError == null || imgError == null) {
            String exceptionMessage = "error_bar not present in layout. Make sure to add <include layout=\"@layout/error_bar/> to your layout";

//            Toolbox.writeToLog(Log.ERROR,exceptionMessage);
            throw new IllegalStateException(exceptionMessage);
        }

        if (rlErrorBar != null && rlErrorBar.getVisibility() == View.VISIBLE && readyForHiding) {

            AnimatorSet animatorSet = new AnimatorSet();

            Animator animSlideUp = ObjectAnimator.ofFloat(rlErrorBar, "translationY", 0, -rlErrorBar.getHeight());
            animatorSet.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    rlErrorBar.setVisibility(View.INVISIBLE);
                    readyForHiding = false;
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });

            Animator animFadeOut = ObjectAnimator.ofFloat(rlErrorBar, "alpha", 1, 0);
            animatorSet.playTogether(animSlideUp, animFadeOut);
            animatorSet.setDuration(1000);
            animatorSet.start();

        }


    }


}
