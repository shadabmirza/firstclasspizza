package to.done.firstclasspizza.common;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nineoldandroids.animation.ObjectAnimator;

import to.done.firstclasspizza.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.cart.Cart;
import to.done.lib.cart.CartListener;
import to.done.lib.entity.order.Order;
import to.done.lib.utils.Toolbox;

/**
 * Created by shadab mirza on 4/24/14.
 */
public abstract class BottomBarFragment extends ErrorFragment {

    @InjectView(R.id.bottom_bar)
    public ViewGroup bottomBar;

    @InjectView(R.id.txt_cart_quantity)
    TextView txtCartQuantity;

    @InjectView(R.id.img_cart)
    ImageView imgCart;

    @InjectView(R.id.img_proc)
    ImageView imgProceed;

    @InjectView(R.id.txt_proceed)
    TextView txtProceed;

    @InjectView(R.id.txt_cart_total_val)
    public
    TextView txtCartTotalVal;

    private Cart cart;
    Handler handler = new Handler();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        cart = Cart.getInstance();
        cart.addCartListener(cartListener);
        bottomBar.setVisibility(View.VISIBLE);
        if (cart.getOrdersCount() > 0) {
            ObjectAnimator.ofFloat(bottomBar, "translationX", 0).start();
            bottomBar.setVisibility(View.VISIBLE);

//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    ObjectAnimator.ofFloat(bottomBar, "translationY",   -bottomBar.getHeight()).setDuration(0).start();
//                }
//            },100);

        } else {
            bottomBar.setVisibility(View.GONE);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ObjectAnimator.ofFloat(bottomBar, "translationY", bottomBar.getHeight()).setDuration(0).start();
                }
            }, 100);

        }


        imgProceed.setOnClickListener(proceedClickListener);
        txtProceed.setOnClickListener(proceedClickListener);
        imgCart.setOnClickListener(cartClickListener);
        txtCartQuantity.setOnClickListener(cartClickListener);


        if (cart.getOrdersCount() > 0) {
            bottomBar.setVisibility(View.VISIBLE);

            Order o = null;
            for (long l : cart.getOrders().keySet()) {
                o = cart.getOrders().get(l);
                break;
            }
            txtCartQuantity.setText("" + cart.getProductsCount(o.getOutlet_id()));
            txtCartTotalVal.setText(Toolbox.formatDecimal(cart.getTotal()));

        }


        txtProceed.setText(getProceedButtonText());

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
//        ButterKnife.inject(this, getView());


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        cart.removeCartListener(cartListener);

    }


    CartListener cartListener = new CartListener() {
        @Override
        public void onCartChange() {
            if (cart.getOrdersCount() > 0) {

                ObjectAnimator.ofFloat(bottomBar, "translationX", 0).start();
                bottomBar.setVisibility(View.VISIBLE);


                Order o = null;
                for (long l : cart.getOrders().keySet()) {
                    o = cart.getOrders().get(l);
                    break;
                }

                final int cartQty = cart.getProductsCount(o.getOutlet_id());
                if (Integer.parseInt(txtCartQuantity.getText().toString()) != cartQty) {
                    ObjectAnimator.ofFloat(txtCartQuantity, "translationY", 0, -txtCartQuantity.getHeight(), 0).setDuration(300).start();
                }
                txtCartQuantity.setText("" + cartQty);
                final double prevTotal = Double.parseDouble(txtCartTotalVal.getText().toString());
                if (prevTotal < cart.getTotal()) {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            double tempTotal = Double.parseDouble(txtCartTotalVal.getText().toString());
                            if (tempTotal < cart.getTotal()) {
                                txtCartTotalVal.setText(Toolbox.formatDecimal((tempTotal + (int) (0.04 * (int) (cart.getTotal() - prevTotal)))));
                                handler.postDelayed(this, 20);
                            } else {
                                String totalString = Toolbox.formatDecimal(cart.getTotal());
                                txtCartTotalVal.setText(totalString);
                            }
                        }
                    }, 20);
                } else {

                    handler.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            double tempTotal = Double.parseDouble(txtCartTotalVal.getText().toString());

                            if (tempTotal > cart.getTotal()) {
                                txtCartTotalVal.setText(Toolbox.formatDecimal(tempTotal - (int) (0.04 * (int) (prevTotal - cart.getTotal()))));
                                handler.postDelayed(this, 20);

                            } else {
                                String totalString = Toolbox.formatDecimal(cart.getTotal());
                                txtCartTotalVal.setText(totalString);
                            }

                        }
                    }, 20);
                }


            } else {


                bottomBar.setVisibility(View.GONE);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ObjectAnimator.ofFloat(bottomBar, "translationY", bottomBar.getHeight()).setDuration(0).start();
                    }
                }, 100);

            }
        }
    };

    View.OnClickListener proceedClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            onProceed();
        }
    };
    View.OnClickListener cartClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            onCartClicked();
        }
    };

    public void onCartClicked() {

        Cart cart = Cart.getInstance();
        if (cart.getOrders().entrySet().size() > 0) {
            List<String> backStackEntryList = new ArrayList<String>();
            for (int i = 0; i < getActivity().getSupportFragmentManager().getBackStackEntryCount(); i++) {
                FragmentManager.BackStackEntry entry = getActivity().getSupportFragmentManager().getBackStackEntryAt(i);
                backStackEntryList.add(entry.getName());
            }
            if (backStackEntryList.indexOf("screen_id_" + Constants.SCREEN_ORDER_DETAILS) != -1) {
                getActivity().getSupportFragmentManager().popBackStack("screen_id_" + Constants.SCREEN_ORDER_DETAILS, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            } else if (backStackEntryList.indexOf("screen_id_" + Constants.SCREEN_CART) != -1) {
                getActivity().getSupportFragmentManager().popBackStack("screen_id_" + Constants.SCREEN_CART, 0);
            }else {
                Toolbox.changeScreen(getActivity(), Constants.SCREEN_CART, true);
            }
        }

    }

    public abstract void onProceed();

    public abstract String getProceedButtonText();

    Runnable r = new Runnable() {
        @Override
        public void run() {

        }
    };
}
