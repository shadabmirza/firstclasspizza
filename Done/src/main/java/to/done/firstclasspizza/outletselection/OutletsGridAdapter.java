package to.done.firstclasspizza.outletselection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import to.done.firstclasspizza.R;

import java.util.ArrayList;
import java.util.List;

import to.done.lib.entity.nearestoutlets.Outlet;

/**
 * Created by root on 4/6/14.
 */
public class OutletsGridAdapter extends BaseAdapter {
   List<Outlet>outlets;
    Context context;
    LayoutInflater infl;
    public OutletsGridAdapter(Context context,List<Outlet>data){
       this.context=context;
        outlets=new ArrayList<Outlet>(data);
        infl=LayoutInflater.from(context);
   }

    public List<Outlet> getOutlets() {
        return outlets;
    }

    public void setOutlets(List<Outlet> outlets) {
        this.outlets = outlets;
    }

    @Override
    public int getCount() {
        if(outlets!=null)
        return outlets.size();
        return 0;
    }

    @Override
    public Outlet getItem(int i) {
        if(outlets!=null && outlets.get(i)!=null)
            return outlets.get(i);
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view==null){
            view= infl.inflate(R.layout.outlets_grid_element,null);
            holder= new ViewHolder();
            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }

        holder.outletsName= (TextView) view.findViewById(R.id.txt_grid_element);
        holder.outletsName.setText(outlets.get(i).toString());

        return view;
    }
    static class ViewHolder{
        TextView outletsName;
    }
}
