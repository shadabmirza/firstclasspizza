package to.done.firstclasspizza.outletselection;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.cart.Cart;
import to.done.lib.database.DBManager;
import to.done.lib.entity.nearestoutlets.Outlet;
import to.done.lib.entity.outletmenu.ResponseCompanyOutlet;
import to.done.lib.sync.SyncListener;
import to.done.lib.sync.SyncManager;
import to.done.lib.ui.CustomDialog;
import to.done.lib.ui.DialogListener;
import to.done.lib.ui.LoadingViewWithThread;
import to.done.lib.utils.GoogleAnalyticsManager;
import to.done.lib.utils.Toolbox;
import to.done.firstclasspizza.R;
import to.done.firstclasspizza.common.ErrorFragment;

/**
 * Created by root on 4/6/14.
 */
public class OutletsGridFragment extends ErrorFragment {
    @InjectView(R.id.outlets_grid)
    GridView outletsGrid;
    @InjectView(R.id.progress_bar)
    LoadingViewWithThread pBar;
    @InjectView(R.id.parent)
    RelativeLayout rel;
    @InjectView(R.id.holder)
    RelativeLayout rel1;
    @InjectView(R.id.txt_track_order)
    TextView track_order;
    OutletsGridAdapter adapter=null;
    DBManager dbMan;
    @InjectView(R.id.txt_nearest_out)
    TextView nearestOut;
    @InjectView(R.id.txt_out_dist)
    TextView outDist;
    List<Outlet>outletsList=new ArrayList<Outlet>();
    private LocationManager locMan;
    LocationListener locationListener;
    Location loc;
    boolean nearestSelected=false;
    double outDistance;
    @InjectView(R.id.holder_rel)
    RelativeLayout nearestView;
    @InjectView(R.id.splash)
    ImageView splashBG;
    @InjectView(R.id.img_location_pin)
    ImageView locImg;
    boolean flag=false;
    Handler mHandler= new Handler();
    Outlet o=null;
    public static final String TAG="OutletsGridFragment";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.outlets_grid_layout,null);
        ButterKnife.inject(this,rootView);

        //((MainActivity)getActivity()).showHomeButton();
        //((ActionBarActivity)getActivity()).getSupportActionBar().

        super.onCreateView(inflater, container, savedInstanceState);
        init();

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

//        nearestSelected=false;
    }

    @Override
    public void onStart() {
        super.onStart();
    //    Log.e(TAG, "onStart()");
        GoogleAnalyticsManager.sendScreenView(getActivity(), Constants.GA_SCREEN_OUTLETS);

    }

    @Override
    public void onResume() {
        super.onResume();
        ImageView img=(ImageView)((ActionBarActivity)getActivity()).getSupportActionBar().getCustomView().findViewById(R.id.imgBack);
        img.setVisibility(View.GONE);
        ( (ImageView) ((ActionBarActivity) getActivity()).getSupportActionBar().getCustomView().findViewById(R.id.action_home)).setImageResource(R.drawable.logo_head);
        Toolbox.changeActionBarTitle(getActivity(),"First Class Pizza","");
        ((ActionBarActivity) getActivity()).getSupportActionBar().hide();
        try {
            if(pBar.isShown()){
                pBar.stopAnimation(); rel.setVisibility(View.VISIBLE);
                pBar.setVisibility(View.GONE);
                splashBG.setVisibility(View.GONE);
            }
            //pBar.setVisibility(View.GONE);
            rel.setVisibility(View.VISIBLE);
            rel1.setVisibility(View.VISIBLE);
            //outletsList=dbMan.getOutletsByCompany((long) Constants.COMPANY_ID_INT);

                SyncManager.getOutletByCompany(getActivity(),outletsListener,Toolbox.getAppVersionName(getActivity()));

                //initialiseGrid();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    Runnable anim= new Runnable() {
        @Override
        public void run() {
      if(track_order.getVisibility()==View.VISIBLE){
          Toolbox.wobbleView(getActivity(),track_order);
      }
        }
    };

    private void init(){

    dbMan=DBManager.getInstance(getActivity());
        try {
//            outletsList=dbMan.getOutletsByCompany((long) Constants.COMPANY_ID_INT);
//            if(outletsList== null || outletsList.size()==0)
            SyncManager.getOutletByCompany(getActivity(),outletsListener,Toolbox.getAppVersionName(getActivity()));
//            else{
                //initialiseGrid();
//            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
       {
            track_order.setVisibility(View.GONE);


            //track_order.setVisibility(View.VISIBLE);

//                            mHandler.postDelayed(new Runnable() {
//
//                                @Override
//                                public void run() {
//                                    // TODO Auto-generated method stub
//                                    // Write your code here to update the UI.
//                                    if(getActivity()!=null && track_order.getVisibility()==View.VISIBLE)
//                                    Toolbox.wobbleView(getActivity(),track_order);
//                                }
//                            },1000);

        }
    track_order.setOnClickListener(track);
        locMan=(LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);

        //edtLocation.setOnTouchListener(editTouch);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                loc=location;
                if(loc!=null){
                    //SyncManager.getClosestSubareas(getActivity(),loc.getLatitude(),loc.getLongitude(),closestSubareaListener);
                setNearestData();
                }
                locMan.removeUpdates(this);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        loc = Toolbox.getLastKnownLocation(getActivity(),locationListener);
//        nearestOut.setOnClickListener(nearestOutletListener);
//        outDist.setOnClickListener(nearestOutletListener);
        rel1.setOnClickListener(nearestOutletListener);
    }

    SyncListener outletsListener= new SyncListener() {
        @Override
        public void onSyncStart() {
            pBar.startLoadingAnimation();
            pBar.setVisibility(View.VISIBLE);
            splashBG.setVisibility(View.VISIBLE);

            rel.setVisibility(View.GONE);

            hideError();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
          if(getActivity()==null) return;
            ResponseCompanyOutlet resp = (ResponseCompanyOutlet) responseObject;
            if (resp.getResponseCode() == Constants.RESP_CODE_APP_UPDATE) {


                CustomDialog.createCustomDialog(getActivity(), "This is an outdated version of First Class Pizza app./nPlease update the app", "Update", "Close", null, false, new DialogListener() {
                    @Override
                    public void onPositiveBtnClick() {
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW)
                                    .setData(Uri.parse("market://details?id=" + getActivity().getPackageName()));
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                            startActivity(intent);
                        } catch (android.content.ActivityNotFoundException anfe) {

                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + getActivity().getPackageName()));
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onNegativeBtnClick() {
                        getActivity().finish();
                    }

                    @Override
                    public void onMiddleBtnClick() {

                    }
                });

            }
            else{
                outletsList = dbMan.getOutletsByCompany((long) Constants.COMPANY_ID_INT);
                initialiseGrid();
            }
        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            if(pBar.isShown()){
                pBar.stopAnimation();
                pBar.setVisibility(View.GONE);
                splashBG.setVisibility(View.GONE);
            }
            showError(ErrorType.NETWORK,reason);
        }
    };

    private View.OnClickListener nearestOutletListener= new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (flag) {
                o = (Outlet) outDist.getTag();
                if (o != null) {
                    if(Cart.getInstance().getOrders()!=null){
                        Cart.getInstance().clearCart();
                    }
                    Bundle bundle = new Bundle();
                    bundle.putLong(Constants.OUTLET_ID, o.getId());
                    bundle.putLong(Constants.COMPANY_ID, o.getCompany_id());
                    bundle.putString(Constants.SUBAREA_NAME, o.getName());
                    bundle.putString(Constants.OUTLET_NAME, o.getName());

                    SyncManager.getOutletMenu(getActivity(), o.getId(), outletMenuSyncListener);

                }
            }else{
                loc=Toolbox.getLastKnownLocation(getActivity(),locationListener);
            }
        }
    };
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private Outlet nearestOutlet(){

        if(loc==null || outletsList==null || outletsList.size()==0){
            Outlet outlet= new Outlet();
            outlet.setName("Tap for nearest outlet...");
            outlet.setDistanceReal(0.0f);
if(Build.VERSION.SDK_INT>Build.VERSION_CODES.HONEYCOMB)
            locImg.setAlpha(0.1f);
            flag=false;
            return outlet;
        }else{
            if(Build.VERSION.SDK_INT>Build.VERSION_CODES.HONEYCOMB)
            locImg.setAlpha(1.0f);
        }
        if(outletsList.size()>1)
        Collections.sort(outletsList,compareOutletsByDist);
        else{
            double distance1=Toolbox.calculateDistance(loc.getLatitude(),loc.getLongitude(),outletsList.get(0).getLat(),outletsList.get(0).getLon());
            outletsList.get(0).setDistanceReal((float)(distance1));

        }
        if(adapter!=null) {
            adapter.setOutlets(outletsList);
            adapter.notifyDataSetChanged();
        }
        flag=true;
        return outletsList.get(0);
    }

    private void setNearestData(){

            Outlet out= nearestOutlet();
            nearestOut.setText(out.getName());
            outDist.setText(""+(out.getDistanceReal())+" miles");
        outDist.setTag(out);

    }
    private void initialiseGrid(){

        if(outletsList!=null && outletsList.size()>0 ){
            if(adapter==null)
            adapter= new OutletsGridAdapter(getActivity(),outletsList);
            else{
                adapter.setOutlets(outletsList);
                adapter.notifyDataSetChanged();
            }
        }

        //Toolbox.showToastShort(getActivity(),"Sucesss");
        outletsGrid.setAdapter(adapter);
        if(pBar.isShown()){
            pBar.stopAnimation();
            pBar.setVisibility(View.GONE);
            splashBG.setVisibility(View.GONE);
        }
        rel.setVisibility(View.VISIBLE);
        rel1.setVisibility(View.VISIBLE);
     //   track_order.setVisibility(View.VISIBLE);
       if((ActionBarActivity)getActivity()!=null) {
           ImageView img = (ImageView) ((ActionBarActivity) getActivity()).getSupportActionBar().getCustomView().findViewById(R.id.action_home);
           img.setImageResource(R.drawable.logo_head);

           ((ActionBarActivity) getActivity()).getSupportActionBar().show();
           Toolbox.changeActionBarTitle(getActivity(), "First Class Pizza", "");
       }
        outletsGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> adapterView, View view, final int i, long l) {
//                    Cart.getInstance().clearCart();
//                    Outlet o = (Outlet) adapterView.getAdapter().getItem(i);
//
//                    Bundle bundle = new Bundle();
//                    bundle.putLong(Constants.OUTLET_ID, o.getId());
//                    bundle.putLong(Constants.COMPANY_ID, o.getCompany_id());
//                    bundle.putString(Constants.SUBAREA_NAME, o.getName());
//                    bundle.putString(Constants.OUTLET_NAME, ((Outlet) adapterView.getItemAtPosition(i)).getName());
//                    Toolbox.changeScreen(getActivity(), Constants.SCREEN_PRODUCTS, true, bundle);
                 o = (Outlet) adapterView.getAdapter().getItem(i);
                if (Cart.getInstance().getOrders().entrySet().size() > 0) {



                    Outlet outlet = dbMan.getOutletById(Cart.getInstance().getFirstOrder().getOutlet_id());

                    String message = "Cart contains " + Cart.getInstance().getFirstOrder().getProducts().size() + " items from " + outlet.getCompany_outlet_name()
                            + "\nCart contents will be cleared.";
                    if(Cart.getInstance().getFirstOrder().getOutlet_id()!=o.getId()){
                        CustomDialog.createCustomDialog(getActivity(), message, "OK", null, null, true, new DialogListener() {
                            @Override
                            public void onPositiveBtnClick() {
                                Cart.getInstance().clearCart();
//                            Bundle bundle = new Bundle();
//                            bundle.putLong(Constants.OUTLET_ID, o.getId());
//                            bundle.putLong(Constants.COMPANY_ID, o.getCompany_id());
//                            bundle.putString(Constants.SUBAREA_NAME,o.getName());
//                            bundle.putString(Constants.OUTLET_NAME, o.getName());
                                SyncManager.getOutletMenu(getActivity(), o.getId(), outletMenuSyncListener);

                            }

                            @Override
                            public void onNegativeBtnClick() {

                            }

                            @Override
                            public void onMiddleBtnClick() {

                            }
                        });
                    }else{
//                Bundle bundle = new Bundle();
//                bundle.putLong(Constants.OUTLET_ID, o.getId());
//                bundle.putLong(Constants.COMPANY_ID, o.getCompany_id());
//                bundle.putString(Constants.SUBAREA_NAME,o.getName());
//                bundle.putString(Constants.OUTLET_NAME, o.getName());
//                SyncManager.getOutletMenu(getActivity(), o.getId(), outletMenuSyncListener);
//                Toolbox.changeScreen(getActivity(), Constants.SCREEN_PRODUCTS, true, bundle);
            //            SyncManager.getOutletMenu(getActivity(), o.getId(), outletMenuSyncListener);

                    }

                } else {
                    {
                        Bundle bundle = new Bundle();
                        bundle.putLong(Constants.OUTLET_ID, o.getId());
                        bundle.putLong(Constants.COMPANY_ID, o.getCompany_id());
                        bundle.putString(Constants.SUBAREA_NAME, o.getName());
                        bundle.putString(Constants.OUTLET_NAME, o.getName());
                        SyncManager.getOutletMenu(getActivity(), o.getId(), outletMenuSyncListener);
                    }

                }
            }
        });
        setNearestData();
    }
Comparator<Outlet>compareOutletsByDist=new Comparator<Outlet>() {
    @Override
    public int compare(Outlet outlet, Outlet outlet2) {
        if(outlet==null || outlet2==null) return 1;
        double distance1=Toolbox.calculateDistance(loc.getLatitude(),loc.getLongitude(),outlet.getLat(),outlet.getLon());
        double distance2=Toolbox.calculateDistance(loc.getLatitude(),loc.getLongitude(),outlet2.getLat(),outlet2.getLon());
        outlet.setDistanceReal((float)distance1);
        outlet2.setDistanceReal((float)distance2);
        if(distance1<distance2) return -1;
        else if(distance1>distance2) return 1;
        else
        return 0;
    }
};
    private View.OnClickListener track= new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                if (dbMan.getOrderDetailsForOrderId() != null)
                    Toolbox.changeScreen(getActivity(), Constants.SCREEN_TRACK, true, null);
                else {
                    Toolbox.showToastLong(getActivity(), "Place an order first");
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    };
    public GpsStatus.Listener mGPSStatusListener = new GpsStatus.Listener()
    {
        public void onGpsStatusChanged(int event)
        {
            switch(event)
            {
                case GpsStatus.GPS_EVENT_STARTED:
                    Toast.makeText(getActivity(), "GPS_SEARCHING", Toast.LENGTH_SHORT).show();
                    System.out.println("TAG - GPS searching: ");
                    break;
                case GpsStatus.GPS_EVENT_STOPPED:
                    System.out.println("TAG - GPS Stopped");
                    break;
                case GpsStatus.GPS_EVENT_FIRST_FIX:

                /*
                 * GPS_EVENT_FIRST_FIX Event is called when GPS is locked
                 */
                    Toast.makeText(getActivity(), "GPS_LOCKED", Toast.LENGTH_SHORT).show();
                    Location gpslocation = locMan
                            .getLastKnownLocation(LocationManager.GPS_PROVIDER);

                    if(gpslocation != null)
                    {
                        System.out.println("GPS Info:"+gpslocation.getLatitude()+":"+gpslocation.getLongitude());

                    /*
                     * Removing the GPS status listener once GPS is locked
                     */
                        locMan.removeGpsStatusListener(mGPSStatusListener);
                    }

                    break;
                case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                    //                 System.out.println("TAG - GPS_EVENT_SATELLITE_STATUS");
                    break;
            }
        }
    };
    SyncListener outletMenuSyncListener = new SyncListener() {
        @Override
        public void onSyncStart() {
            pBar.startLoadingAnimation();
            pBar.setVisibility(View.VISIBLE);
            outletsGrid.setVisibility(View.INVISIBLE);
            nearestView.setVisibility(View.INVISIBLE);
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {

            if (getActivity() == null) return;
//            if(pBar.isShown()){
//                pBar.stopAnimation();
//                pBar.setVisibility(View.GONE);
//                splashBG.setVisibility(View.GONE);
//            }
            Outlet outlet = dbMan.getOutletById(o.getId());

            Bundle bundle = new Bundle();
            bundle.putLong(Constants.OUTLET_ID, outlet.getId());
            bundle.putLong(Constants.COMPANY_ID, outlet.getCompany_id());
            bundle.putString(Constants.SUBAREA_NAME, outlet.getName());
            bundle.putString(Constants.OUTLET_NAME, outlet.getName());
            Toolbox.changeScreen(getActivity(), Constants.SCREEN_PRODUCTS, true, bundle);
        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            if (getActivity() == null) return;
            if(pBar.isShown()){
                pBar.stopAnimation();
                pBar.setVisibility(View.GONE);
                splashBG.setVisibility(View.GONE);
            }
            outletsGrid.setVisibility(View.VISIBLE);
            nearestView.setVisibility(View.VISIBLE);
            showError(ErrorType.NETWORK, reason);

        }
    };
}