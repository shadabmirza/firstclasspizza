package to.done.firstclasspizza;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;

import to.done.lib.Constants;
import to.done.lib.cart.Cart;
import to.done.lib.ui.CustomDialog;
import to.done.lib.ui.DialogListener;
import to.done.lib.utils.GCMRegisterManager;
import to.done.lib.utils.GoogleAnalyticsManager;
import to.done.lib.utils.Toolbox;
import to.done.firstclasspizza.cart.CartFragment;
import to.done.firstclasspizza.order.ConfirmOrderFragment;
import to.done.firstclasspizza.order.PersonalDetailsFragment;
import to.done.firstclasspizza.order.ThankyouFragment;
import to.done.firstclasspizza.outletselection.OutletsGridFragment;
import to.done.firstclasspizza.productcustomization.ProductCustomizationFragment;
import to.done.firstclasspizza.productselection.ProductsFragment;


import static to.done.lib.Constants.AB_BTN;
import static to.done.lib.Constants.AB_BTN_BACK;
import static to.done.lib.Constants.AB_BTN_HOME;
import static to.done.lib.Constants.AB_SUBTITLE_TEXT;
import static to.done.lib.Constants.AB_TITLE_TEXT;
import static to.done.lib.Constants.ADD_TO_BACKSTACK;
import static to.done.lib.Constants.BUNDLE_EXTRA;
import static to.done.lib.Constants.INTENT_AB_BUTTON;
import static to.done.lib.Constants.INTENT_AB_TITLE;
import static to.done.lib.Constants.INTENT_SCREEN_UPDATE;
import static to.done.lib.Constants.NAV_DRAWER_ACTON_ENABLE;
import static to.done.lib.Constants.SCREEN_CART;
import static to.done.lib.Constants.SCREEN_CONFIRM_ORDER;
import static to.done.lib.Constants.SCREEN_ID;
import static to.done.lib.Constants.SCREEN_LOC_SELECTION;
import static to.done.lib.Constants.SCREEN_ORDER_DETAILS;
import static to.done.lib.Constants.SCREEN_OUTLETS;
import static to.done.lib.Constants.SCREEN_PERSONAL_DETAILS;
import static to.done.lib.Constants.SCREEN_PRODUCTS;
import static to.done.lib.Constants.SCREEN_PRODUCT_CUSTOMIZATION;
import static to.done.lib.Constants.SCREEN_THANKS;


public class MainActivity extends ActionBarActivity {

    public static final String TAG = "MainActivity";
    private BroadcastReceiver screenEventReceiver, actionBarTitleChangeReceiver, actionBarButtonChangeReceiver;

    private int currentScreenId;

    Toast toast;
    private long back_pressed;
    public static int currentThemeId = R.style.AppTheme_Box8;
    private Handler handler = new Handler();
    static String DB_PATH;

    public boolean drawerOpen = false;

    public void findHash() {
        PackageInfo info;
        try {

            info = getPackageManager().getPackageInfo(
                    "to.done.zaffran", PackageManager.GET_SIGNATURES);


            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                Log.e("Hash key", something);
                System.out.println("Hash key" + something);
            }

        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//findHash();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            DB_PATH = getApplicationContext().getFilesDir().getAbsolutePath().replace("files", "databases") + File.separator;
        } else {
            DB_PATH = getApplicationContext().getFilesDir().getPath() + getApplicationContext().getPackageName() + "/databases/";
        }
        //////////////////////////////////////////////////////////////////////
        // GCMRegisterManager.registerToGCM(this);


        int themeNo = 1;//SharedPreferencesManager.getSharedPreferences(this).getInt(Constants.PREFS_THEME_ID,1);

//        if(themeNo<4 ){
//            SharedPreferencesManager.writeInt(this,Constants.PREFS_THEME_ID,themeNo+1);
//        }
//        else
//        {
//            SharedPreferencesManager.writeInt(this,Constants.PREFS_THEME_ID,0);
//        }

//        switch (themeNo){
//
//            case 1:setTheme( com.poncho.R.style.AppTheme_Blue);currentThemeId=  com.poncho.R.style.AppTheme_Box8;break;
//            default:setTheme( com.poncho.R.style.AppTheme_Blue);currentThemeId=  com.poncho.R.style.AppTheme_Box8;break;
//        }
        super.onCreate(savedInstanceState);
        Crashlytics.start(this);
        if (getIntent().hasExtra("message")) {
            String msg = getIntent().getStringExtra("message");
            String title = getIntent().getStringExtra("title");
            Log.e(TAG, "received msg:" + msg);
            showGCMPopup(title, msg);
        }

        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        createScreenReceiver();
        createActionBarTitleChangeReceiver();
        createActionBarButtonChangeReceiver();

        LocalBroadcastManager.getInstance(this).registerReceiver(screenEventReceiver, new IntentFilter(INTENT_SCREEN_UPDATE));
        LocalBroadcastManager.getInstance(this).registerReceiver(actionBarTitleChangeReceiver, new IntentFilter(INTENT_AB_TITLE));
        LocalBroadcastManager.getInstance(this).registerReceiver(actionBarButtonChangeReceiver, new IntentFilter(INTENT_AB_BUTTON));


        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
//                currentScreenId=Constants.SCREEN_MAIN_MENU;


            }
        });

        ActionBar actionBar = getSupportActionBar();
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
//        actionBar.setLogo(R.drawable.home);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setCustomView(getLayoutInflater().inflate(R.layout.action_bar, null), new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT));


        ((ImageView) actionBar.getCustomView().findViewById(R.id.imgBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                for(int i =0;i<getSupportFragmentManager().getBackStackEntryCount();i++){
//
//                    int id=getSupportFragmentManager().getBackStackEntryAt(i).getId();
//                    getSupportFragmentManager().popBackStack(id,FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                }
//                getSupportFragmentManager().popBackStackImmediate();

                switch (currentScreenId) {

                    case SCREEN_OUTLETS: {
                        Toolbox.changeNavDrawerStatus(MainActivity.this, NAV_DRAWER_ACTON_ENABLE);
                        //Toolbox.changeScreen(MainActivity.this, SCREEN_LOC_SELECTION, false);

                        break;
                    }

                    case SCREEN_LOC_SELECTION: {
                        Toolbox.changeNavDrawerStatus(MainActivity.this, NAV_DRAWER_ACTON_ENABLE);
                        break;
                    }
                    default:
                        onBackPressed();
                        break;
                }


            }
        });

        ((ImageView) actionBar.getCustomView().findViewById(R.id.action_home)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                for(int i =0;i<getSupportFragmentManager().getBackStackEntryCount();i++){
//
//                    int id=getSupportFragmentManager().getBackStackEntryAt(i).getId();
//                    getSupportFragmentManager().popBackStack(id,FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                }
//                getSupportFragmentManager().popBackStackImmediate();

                switch (currentScreenId) {

                    case SCREEN_OUTLETS: {
                        Toolbox.changeNavDrawerStatus(MainActivity.this, NAV_DRAWER_ACTON_ENABLE);
                        //Toolbox.changeScreen(MainActivity.this, SCREEN_LOC_SELECTION, false);

                        break;
                    }

                    case SCREEN_LOC_SELECTION: {
                        Toolbox.changeNavDrawerStatus(MainActivity.this, NAV_DRAWER_ACTON_ENABLE);
                        break;
                    }

                    default:
                        onBackPressed();
                        break;
                }


            }
        });
        //

        if (savedInstanceState == null) {
            Toolbox.changeScreen(this, SCREEN_PRODUCTS, true);

            //  Toolbox.changeScreen(this,SCREEN_ORDER_DETAILS,true);
        }


    }
//
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }


    private void clearBackstack() {
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); i++) {
            fm.popBackStack();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.e(TAG, "onNewIntent()");
        if (intent.hasExtra("message")) {
            String msg = intent.getStringExtra("message");
            String title = intent.getStringExtra("title");
            showGCMPopup(title, msg);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void showGCMPopup(String title, String msg) {
//        GCMPopupDialogFragment gcmDialog = new GCMPopupDialogFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString("message", msg);
//        gcmDialog.setArguments(bundle);
//        gcmDialog.show(getSupportFragmentManager(), "");
        GoogleAnalyticsManager.sendScreenView(this, Constants.GA_SCREEN_NOTIFICATION_POPUP);
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.gcm_popup_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(false);

        Button okButton = (Button) dialog.findViewById(R.id.gcm_popup_ok_button);
        TextView gcm_popup_text = (TextView) dialog.findViewById(R.id.gcm_popup_text);
        TextView gcm_popup_title_text = (TextView) dialog.findViewById(R.id.gcm_popup_title_text);

        if (msg != null)
            gcm_popup_text.setText(msg);
        if (title != null)
            gcm_popup_title_text.setText(title);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        Toolbox.changeScreen(this, SCREEN_OUTLETS, false);
//
//    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        //  Toolbox.changeScreen(this, SCREEN_OUTLETS, false);
    }

    public BroadcastReceiver createScreenReceiver() {

        screenEventReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                int screenId = intent.getIntExtra(SCREEN_ID, -1);
                Bundle bundle = intent.getBundleExtra(BUNDLE_EXTRA);
                currentScreenId = screenId;
                boolean addToBackstack = intent.getBooleanExtra(ADD_TO_BACKSTACK, false);

                switch (screenId) {

                    case SCREEN_LOC_SELECTION: {
//                        showHomeButton();
                        Fragment f = null;
//                        if(getSupportFragmentManager().findFragmentByTag("screen_id_"+Constants.SCREEN_LOC_SELECTION)!=null){
//                            f=getSupportFragmentManager().findFragmentByTag("screen_id_"+Constants.SCREEN_LOC_SELECTION);
//                        }else {
//                        f = new LocationSelectFragment();
//                        }
//                       clearBackstack();
//                        replaceContainerWithFrag(f, addToBackstack, bundle);
                        Constants.CURRENT_SCREEN = SCREEN_LOC_SELECTION;
                        break;
                    }

                    case SCREEN_OUTLETS: {
                        //showHomeButton();
                        ((ImageView) getSupportActionBar().getCustomView().findViewById(R.id.imgBack)).setVisibility(View.GONE);
                        Fragment f = new OutletsGridFragment();
                        replaceContainerWithFrag(f, addToBackstack, bundle);
                        Constants.CURRENT_SCREEN = SCREEN_OUTLETS;
                        break;
                    }
                    case SCREEN_PRODUCTS: {
//                        showBackButton();
                        showHomeButton();
                        Fragment f = new ProductsFragment();
                        replaceContainerWithFrag(f, addToBackstack, bundle);
                        Constants.CURRENT_SCREEN = SCREEN_PRODUCTS;
                        break;
                    }
                    case SCREEN_PRODUCT_CUSTOMIZATION: {
//                        showBackButton();
                        showHomeButton();
                        Fragment f = new ProductCustomizationFragment();
                        // Fragment f = new ProductCustomizationCategoryFragment();
                        replaceContainerWithFrag(f, addToBackstack, bundle);
                        Constants.CURRENT_SCREEN = SCREEN_PRODUCT_CUSTOMIZATION;
                        break;
                    }
                    case SCREEN_ORDER_DETAILS: {
//                        showBackButton();
                        showHomeButton();
//                        Fragment f = new OrderDetailsFragment();
//                        replaceContainerWithFrag(f, addToBackstack, bundle);
                        Constants.CURRENT_SCREEN = SCREEN_ORDER_DETAILS;
                        break;
                    }
                    case SCREEN_PERSONAL_DETAILS: {
//                        showBackButton();
                        showHomeButton();
                        Fragment f = new PersonalDetailsFragment();
                        replaceContainerWithFrag(f, addToBackstack, bundle);
                        Constants.CURRENT_SCREEN = SCREEN_PERSONAL_DETAILS;
                        break;
                    }
                    case SCREEN_CART: {
//                        showBackButton();
                        showHomeButton();
                        Fragment f = new CartFragment();
                        replaceContainerWithFrag(f, addToBackstack, bundle);
                        Constants.CURRENT_SCREEN = SCREEN_CART;
                        break;
                    }
                    case SCREEN_CONFIRM_ORDER: {
                        showHomeButton();
                        Fragment f = new ConfirmOrderFragment();
                        replaceContainerWithFrag(f, addToBackstack, bundle);
                        Constants.CURRENT_SCREEN = SCREEN_CONFIRM_ORDER;
                        break;
                    }

                    case SCREEN_THANKS: {
                        showHomeButton();
                        Fragment f = new ThankyouFragment();
                        replaceContainerWithFrag(f, addToBackstack, bundle);
                        Constants.CURRENT_SCREEN = SCREEN_THANKS;
                        break;
                    }
//                    case SCREEN_TRACK: {
//                        //showHomeButton();
//                        Fragment f = new TrackOrderFragment();
//                        replaceContainerWithFrag(f, addToBackstack, bundle);
//                        Constants.CURRENT_SCREEN = SCREEN_TRACK;
//                        break;
//                    }


                }

            }
        };

        return screenEventReceiver;
    }


    public BroadcastReceiver createActionBarTitleChangeReceiver() {


        actionBarTitleChangeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                String title = intent.getStringExtra(AB_TITLE_TEXT);

                if (title != null) {
                    ((TextView) getSupportActionBar().getCustomView().findViewById(R.id.action_bar_title)).setText(title);
                }


                String subtitle = intent.getStringExtra(AB_SUBTITLE_TEXT);
                if (subtitle != null) {
                    ((TextView) getSupportActionBar().getCustomView().findViewById(R.id.action_bar_subtitle)).setText(subtitle);
                }
                if (subtitle.equalsIgnoreCase("hide")) {
                    getSupportActionBar().hide();
                }

            }
        };
        return actionBarTitleChangeReceiver;
    }


    public BroadcastReceiver createActionBarButtonChangeReceiver() {


        actionBarButtonChangeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                int buttonType = intent.getIntExtra(AB_BTN, AB_BTN_HOME);

                switch (buttonType) {

                    case AB_BTN_HOME: {
                        showHomeButton();
                        break;
                    }
                    case AB_BTN_BACK: {
                        showBackButton();
                        break;
                    }
                }


            }
        };
        return actionBarButtonChangeReceiver;
    }

    private void replaceContainerWithFrag(Fragment frag, boolean addToBackstack) {
        replaceContainerWithFrag(frag, addToBackstack, null);
    }

    private void replaceContainerWithFrag(Fragment frag, boolean addToBackstack, Bundle b) {


        if (b != null) {
            frag.setArguments(b);
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.right_to_left_enter, R.anim.right_to_left_exit, R.anim.left_to_right_enter, R.anim.left_to_right_exit);
        ft.replace(R.id.container, frag);
        if (addToBackstack)
            ft.addToBackStack("screen_id_" + currentScreenId);

        ft.commitAllowingStateLoss();
    }

    private void replaceContainerWithFrag(Fragment frag, boolean addToBackstack, int animEnter, int animExit, int animEnterPop, int animExitPop, Bundle b) {
        if (b == null)
            b = new Bundle();
        frag.setArguments(b);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(animEnter, animExit, animEnterPop, animExitPop);
        ft.replace(R.id.container, frag);

        if (addToBackstack)
            ft.addToBackStack("screen_id_" + currentScreenId);
        ft.commit();
    }

    private void replaceContainerWithFrag(Fragment frag, boolean addToBackstack, int animEnter, int animExit, int animEnterPop, int animExitPop) {

        replaceContainerWithFrag(frag, addToBackstack, animEnter, animExit, animEnterPop, animExitPop, null);

    }

    private void showHomeButton() {
        if (Constants.CURRENT_SCREEN == SCREEN_OUTLETS) {
            ((ImageView) getSupportActionBar().getCustomView().findViewById(R.id.action_home)).setImageResource(R.drawable.logo_head);
            ((ImageView) getSupportActionBar().getCustomView().findViewById(R.id.imgBack)).setVisibility(View.GONE);
        } else {
            ((ImageView) getSupportActionBar().getCustomView().findViewById(R.id.action_home)).setImageResource(R.drawable.logo_in_head);
            ((ImageView) getSupportActionBar().getCustomView().findViewById(R.id.imgBack)).setVisibility(View.VISIBLE);
        }
    }

    private void showBackButton() {
        ((ImageView) getSupportActionBar().getCustomView().findViewById(R.id.action_home)).setImageResource(R.drawable.logo_in_head);
    }

    @Override
    protected void onDestroy() {
        clearCart();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(screenEventReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(actionBarTitleChangeReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(actionBarButtonChangeReceiver);
        super.onDestroy();
    }

    public void clearCart() {
        Cart cart = Cart.getInstance();
        if (cart != null) {
            cart.clearCart();
        }
    }

    @Override
    public void onBackPressed() {

        if (drawerOpen) {
            Toolbox.changeNavDrawerStatus(this, Constants.NAV_DRAWER_ACTON_CLOSE);
            Toolbox.writeToLog("Navigation drawer:Handling back press");
            return;
        }
        FragmentManager fm = getSupportFragmentManager();
        Fragment currentFragment = fm.findFragmentById(R.id.container);
//        if (currentFragment != null && currentFragment instanceof ProductsFragment && Cart.getInstance().getOrders().entrySet().size() > 0) {
//            final FragmentManager fragmentManager = getSupportFragmentManager();
//            String message = "Cart contains " + Cart.getInstance().getFirstOrder().getProducts().size() + " items."// from " + outlet.getCompany_outlet_name()
//                    + "\nGoing Back will clear cart contents.\n\nDo you want to proceed?";
//
//            CustomDialog.createCustomDialog(this, message, "Yes", "No", null, true, new DialogListener() {
//                @Override
//                public void onPositiveBtnClick() {
//                    Cart.getInstance().clearCart();
//                    fragmentManager.popBackStack();
//
//                }
//
//                @Override
//                public void onNegativeBtnClick() {
//
//                }
//
//                @Override
//                public void onMiddleBtnClick() {
//
//                }
//            });
//        } else
        if (fm.getBackStackEntryCount() > 01) {
            Log.i("MainActivity", "BackStackEntryCount : " + fm.getBackStackEntryCount());
            Log.i("MainActivity", "popping backstack");
            fm.popBackStack();
        } else {
            if (back_pressed + 2000 > System.currentTimeMillis()) finish();
            else {
                if (toast != null) {
                    toast.cancel();
                }

                toast = Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT);
                toast.show();
            }
            back_pressed = System.currentTimeMillis();
        }
    }

    public static void writeToSD() {
        try {
            File sd = Environment.getExternalStorageDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "done.db";
                String backupDBPath = "backupDone.db";
                File currentDB = new File(DB_PATH, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
