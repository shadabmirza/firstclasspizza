package to.done.firstclasspizza.tracking;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import to.done.firstclasspizza.MainActivity;
import to.done.firstclasspizza.R;
import to.done.firstclasspizza.common.ErrorFragment;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.database.DBManager;
import to.done.lib.entity.order.Order;
import to.done.lib.entity.tracking.Order_status_log;
import to.done.lib.entity.tracking.TrackOrderData;
import to.done.lib.sync.SyncListener;
import to.done.lib.sync.SyncManager;
import to.done.lib.utils.SharedPreferencesManager;
import to.done.lib.utils.Toolbox;

/**
 * Created by root on 24/6/14.
 */
public class TrackOrderFragment extends ErrorFragment {

    DBManager db;
    @InjectView(R.id.orderNo)
    TextView orderNo;
    @InjectView(R.id.orderTime)
    TextView orderTime;
    @InjectView(R.id.items)
    TextView orderItems;
   @InjectView(R.id.billAmount)
   TextView orderBillAmt;
    @InjectView(R.id.accepted)
    ImageView accepted;
    @InjectView(R.id.acceptedline)
    ImageView acceptedLine;
    @InjectView(R.id.inkitchen)
    ImageView inkitchen;
    @InjectView(R.id.inkitchenline)
    ImageView inkitchenLine;
    @InjectView(R.id.delivered)
    ImageView delivered;
    @InjectView(R.id.btn_refresh)
    Button refresh;
    @InjectView(R.id.txt_delivered)
    TextView txtDel;
    @InjectView(R.id.txt_dispatched)
    TextView txtDisp;
    @InjectView(R.id.txt_accepted)
    TextView txtAcc;
    @InjectView(R.id.btn_order_details)
    TextView detailsBtn;
    @InjectView(R.id.acctime)
    TextView acctext;
    @InjectView(R.id.kitchen_time)
    TextView kitchen_time;
    @InjectView(R.id.disp_time)
    TextView disp_time;
    Order order;
    ProgressDialog pDialog;
    SimpleDateFormat simpleDF= new SimpleDateFormat("h:mm a");//HHmm
    Date date= new Date();
    View lastSelectedView=null,footerView=null;
int selectedColor;
    ImageView close;
    String order_no;
    Dialog track;
    boolean added=false;
    SharedPreferences.Editor editor;
    SharedPreferences preferencesManager;
    private View.OnClickListener click= new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            SyncManager.trackOrder(getActivity(),order.getOrder_id(),trackOrderListener);
        }
    };
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.track_order,null);
        ButterKnife.inject(this, rootView);
        init();
        return rootView;
    }
    private void init(){
        ((MainActivity)getActivity()).drawerOpen=false;
        pDialog= new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        refresh.setOnClickListener(click);
        if((ActionBarActivity)getActivity()!=null) {
            ImageView img = (ImageView) ((ActionBarActivity) getActivity()).getSupportActionBar().getCustomView().findViewById(R.id.action_home);
            img.setImageResource(R.drawable.logo_in_head);
            ((ActionBarActivity) getActivity()).getSupportActionBar().show();
            ImageView img1=(ImageView)((ActionBarActivity)getActivity()).getSupportActionBar().getCustomView().findViewById(R.id.imgBack);
            img1.setVisibility(View.VISIBLE);
            Toolbox.changeActionBarTitle(getActivity(),"Track your order","");
        }
        db=DBManager.getInstance(getActivity());
        if(getActivity()!=null)
            preferencesManager = SharedPreferencesManager.getSharedPreferences(getActivity());
        editor = preferencesManager.edit();

        try {
            order=((ArrayList<Order>)db.getOrderDetailsForOrderId()).get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        detailsBtn.setOnClickListener(details);
        selectedColor=getActivity().getResources().getColor(R.color.theme_box8);
        if(order!=null){
            date.setTime(order.getOrder_date());
            String time=simpleDF.format(date);
            order_no=preferencesManager.getString(Constants.ORDER_NUMBER,null);
            if(order_no==null)
            orderNo.setText(""+order.getOrder_number());
            else{
                orderNo.setText(""+order_no);
            }
            orderTime.setText("" + time);
//            Toolbox.writeToLog("Order time "+time);
            orderItems.setText("" + order.getQuantity());
            orderBillAmt.setText(""+order.getTotal());
            SyncManager.trackOrder(getActivity(),order.getOrder_id(),trackOrderListener);
        }
                   acctext.setVisibility(View.GONE);
                   kitchen_time.setVisibility(View.GONE);
                  disp_time .setVisibility(View.GONE);

    }

    private View.OnClickListener details = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          try{if(db!=null){
              if(db.getOrderDetailsForOrderId()!=null){
                  openTrackDetailsDialog();
              }
          }}
          catch (Exception e){
              e.printStackTrace();
          }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).drawerOpen=false;
        if((ActionBarActivity)getActivity()!=null) {
            ImageView img = (ImageView) ((ActionBarActivity) getActivity()).getSupportActionBar().getCustomView().findViewById(R.id.action_home);
            img.setImageResource(R.drawable.logo_in_head);
            ((ActionBarActivity) getActivity()).getSupportActionBar().show();
            ImageView img1=(ImageView)((ActionBarActivity)getActivity()).getSupportActionBar().getCustomView().findViewById(R.id.imgBack);
            img1.setVisibility(View.VISIBLE);
            Toolbox.changeActionBarTitle(getActivity(),"Track your order","");
        }
    }

    private SyncListener trackOrderListener= new SyncListener() {
        @Override
        public void onSyncStart() {
            if(!pDialog.isShowing()){
                pDialog.setMessage("Fetching order status....");
                pDialog.show();
            }else{
                pDialog.dismiss();
            }
            hideError();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            if (getActivity() == null) return;
            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }

            hideError();
            TrackOrderData data= (TrackOrderData) responseObject;
            if(responseObject!=null){
//                date= new Date();
//                date.setTime(data.getOrder_time());
//                String time=simpleDF.format(date);
//                Toolbox.writeToLog("Order time "+time);
                if(order_no==null)
                    orderNo.setText(""+order.getOrder_number());
                else{
                    orderNo.setText(""+order_no);
                }
               // orderTime.setText(""+time);
                orderItems.setText(""+data.getProduct_quantity());
                orderBillAmt.setText(""+data.getTotal_amount());
                //SyncManager.trackOrder(getActivity(),order.getOrder_id(),trackOrderListener);
                if(data.getOrder_status_log()!=null && data.getOrder_status_log().size()>0){
                    for(Order_status_log log:data.getOrder_status_log()){
                        if(log.getOrder_status_id()==12L){
                            accepted.setImageResource(R.drawable.on_accepted);
                            txtAcc.setTextColor(selectedColor);
                            Date date = new Date(log.getCreated_at()*1000L);
                            acctext.setVisibility(View.VISIBLE);
                            acctext.setText(simpleDF.format(date));
                            lastSelectedView=accepted;

                        }else if(log.getOrder_status_id()==13L){
                            accepted.setImageResource(R.drawable.on_accepted);
//                            Toolbox.animateQuantityIncrease(accepted);
                            txtAcc.setTextColor(selectedColor);
                            acctext.setVisibility(View.VISIBLE);
                            acceptedLine.setImageResource(R.drawable.on_line);
                            inkitchen.setImageResource(R.drawable.on_kitchen);
                            Date date = new Date(log.getCreated_at()*1000L);
                            kitchen_time.setVisibility(View.VISIBLE);
                            kitchen_time.setText(simpleDF.format(date));
                            txtDisp.setTextColor(selectedColor);
                            lastSelectedView=inkitchen;
                        }else if(log.getOrder_status_id()==14L){
                            accepted.setImageResource(R.drawable.on_accepted);
                            txtAcc.setTextColor(selectedColor);
                            acctext.setVisibility(View.VISIBLE);
                            acceptedLine.setImageResource(R.drawable.on_line);
                            inkitchen.setImageResource(R.drawable.on_kitchen);
                            kitchen_time.setVisibility(View.VISIBLE);
                            txtDisp.setTextColor(selectedColor);
                            inkitchenLine.setImageResource(R.drawable.on_line);
                            delivered.setImageResource(R.drawable.on_dispatched);
                            disp_time .setVisibility(View.VISIBLE);
                            Date date = new Date(log.getCreated_at()*1000L);
                            disp_time.setText(simpleDF.format(date));
                            txtDel.setTextColor(selectedColor);
                            lastSelectedView=delivered;
                        }
//                        else{
//                            Toolbox.showToastLong(getActivity(),"could not track order");
//                        }
                    }
                }
                if(lastSelectedView!=null)
                Toolbox.animateQuantityIncrease(lastSelectedView);
            }

        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {

            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }
                    showError(ErrorType.ANNOUNCEMENT,reason);
        }
    };


    private void openTrackDetailsDialog(){
        try {
         track= new Dialog(getActivity());
            track.requestWindowFeature(Window.FEATURE_NO_TITLE);
        track.setContentView(R.layout.track_order_details);

            Window window = track.getWindow();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(track.getWindow().getAttributes());
            lp.gravity = Gravity.TOP ;

            lp.y = 80;
            window.setAttributes(lp);
        ListView list= (ListView) track.findViewById(R.id.lstDetails);
            LastOrderAdapter adapter = new LastOrderAdapter(getActivity());
            list.addFooterView(setOrderDetailsFooterView());
            list.addHeaderView(setOrderDetailsListHeader());
            list.setAdapter(adapter);

            track.setCancelable(true);
            track.show();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    private View setOrderDetailsFooterView(){
        footerView=((LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.od_list_footer,null);
//        TextView delCharges,taxText,price,taxAmt,discountText,discountAmt;
//        price= (TextView) footerView.findViewById(R.id.price);
//        ImageView proceed= (ImageView) footerView.findViewById(R.id.img_done_logo);
//        Button redeem_coupon=(Button) footerView.findViewById(R.id.btn_rdm_cpn);
//       redeem_coupon.setVisibility(View.GONE);
//
//        taxText=(TextView) footerView.findViewById(R.id.txt_payment_method);
//        taxAmt=(TextView) footerView.findViewById(R.id.tax_amt);
//        delCharges=(TextView) footerView.findViewById(R.id.del_charges_amt);
//        discountText=(TextView) footerView.findViewById(R.id.disc_text);
//        discountAmt=(TextView) footerView.findViewById(R.id.disc_amt);
//        discountAmt.setVisibility(View.GONE);
//        discountText.setVisibility(View.GONE);
//
//
//        if(order.getApplied_extra_charges()!=null && order.getApplied_extra_charges().size()>0) {
//            price.setText("+₹ " + order.getApplied_extra_charges().get(0).getExtra_charged_amt());
//            taxAmt.setText("₹"+(order.getTotal()+order.getApplied_extra_charges().get(0).getExtra_charged_amt()));
//            if(order.getApplied_extra_charges().get(0).getType().toLowerCase().contains("perce")){
//                taxText.setText("Taxes applicable "+order.getApplied_extra_charges().get(0).getValue()+"%");
//            }else{
//                taxText.setText("Taxes applicable "+"+₹ "+order.getApplied_extra_charges().get(0).getValue());
//            }
//        }else {
//            price.setText("+₹ " + 00);
//            taxText.setText("Taxes applicable     "+"+₹ "+00);
//        }//resp.getData().getApplied_extra_charges().get(0).getExtra_charged_amt()
//
//        delCharges.setText("+₹00");
//
//        if(order.getApplied_offer()!=null && order.getApplied_offer().size()>0) {
//            if (order.getApplied_offer().get(0) != null) {
//                discountText.setVisibility(View.VISIBLE);
//                discountAmt.setVisibility(View.VISIBLE);
//                Applied_offer aof = order.getApplied_offer().get(0);
//                Toolbox.showToastLong(getActivity(), "Offer " + aof.getId() + " " + aof.getCoupon_code() + " " + aof.getName());
//                discountText.setText(aof.getName());
//                discountAmt.setText("-₹"+aof.getDiscount_amount());
//                taxAmt.setText("₹"+(order.getTotal()-aof.getDiscount_amount()));
//            }
//        }
//        //taxAmt.setText("₹"+order.getTotal());
     return footerView;

    }

    private View setOrderDetailsListHeader() {
        View headerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.od_list_header, null);
        TextView name = (TextView) headerView.findViewById(R.id.txt_user_name);
        TextView phone = (TextView) headerView.findViewById(R.id.txt_user_phone);
        TextView address = (TextView) headerView.findViewById(R.id.txt_useraddress);
        TextView label1 = (TextView) headerView.findViewById(R.id.txt_del_label);
        TextView label2 = (TextView) headerView.findViewById(R.id.txt_del_addr_label);
        close= (ImageView) headerView.findViewById(R.id.close);
        close.setVisibility(View.VISIBLE);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(track!=null)
                track.dismiss();
            }
        });

        {
            phone.setVisibility(View.GONE);
            name.setVisibility(View.GONE);
            address.setVisibility(View.GONE);
            label1.setVisibility(View.GONE);
            label2.setVisibility(View.GONE);
        }
        return (headerView);
    }

}
