package to.done.firstclasspizza.order;

/**
 * Created by HP on 6/25/2014.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import to.done.firstclasspizza.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.entity.outletsubareas.Subarea;

/**
 * Created by shadab mirza on 11/4/14.
 */
public class SubareasAutoCompleteAdapter extends BaseAdapter {

    private List<Subarea> subareasList= new ArrayList<Subarea>();
    private Context context;

    public SubareasAutoCompleteAdapter(Context context,List<Subarea> subareasList) {
        if(subareasList!=null){
            this.subareasList = subareasList;
        }
        this.context=context;
    }

    @Override
    public int getCount() {
        return subareasList.size();
    }

    @Override
    public Subarea getItem(int i) {
        return subareasList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getId();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        ViewHolder viewHolder;

        if(convertView==null){

            convertView= LayoutInflater.from(context).inflate(R.layout.loc_search_list_item, null);

            viewHolder=new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        }
        else{
            viewHolder=(ViewHolder)convertView.getTag();
        }

        viewHolder.txtLocationListItem.setText(getItem(i).getSubarea_name());
        return convertView;
    }

    public List<Subarea> getSubareasList() {
        return subareasList;
    }

    public void setSubareasList(List<Subarea> subareasList) {
        this.subareasList = subareasList;
        notifyDataSetChanged();
    }

    public void clear(){
        subareasList.clear();
        notifyDataSetInvalidated();
    }


    static class ViewHolder {

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }


        @InjectView(R.id.txt_location_list_item)
        TextView txtLocationListItem;
    }


}

