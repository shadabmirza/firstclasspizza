package to.done.firstclasspizza.order;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import to.done.firstclasspizza.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import to.done.lib.database.DBManager;
import to.done.lib.entity.nearestoutlets.Outlet;
import to.done.lib.entity.order.Customization;
import to.done.lib.entity.order.Order;
import to.done.lib.entity.outletmenu.Product;

/**
 * Created by HP on 5/3/2014.
 */
public class OrderDetailsExpListAdapter extends BaseExpandableListAdapter {

    DBManager db;
    private HashMap<Long,Order>orders;
    private HashMap<Outlet,ArrayList<Product>>dataToshow;
    Context context;
    ArrayList<Product>products;
    LayoutInflater inflater;
    ArrayList<Outlet>outletsList;

    public OrderDetailsExpListAdapter(Context ctx, HashMap<Long, Order> o){
        db=DBManager.getInstance(ctx);
        orders=o;
        context=ctx;
        dataToshow=new HashMap<Outlet, ArrayList<Product>>();
        ArrayList<Long>outletIds=new ArrayList<Long>(orders.keySet());
        for(Long l :outletIds) {
           dataToshow.put(db.getOutletById(l), (ArrayList<Product>) getProductsWithoutDuplicate(l));

        }
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        outletsList=new ArrayList<Outlet>(dataToshow.keySet());
    }
    @Override
    public int getGroupCount() {
        return outletsList.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return dataToshow.get(outletsList.get(i)).size();
    }

    @Override
    public Outlet getGroup(int i) {
        return outletsList.get(i);
    }

    @Override
    public Object getChild(int i, int i2) {
        return dataToshow.get(outletsList.get(i)).get(i2);
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int i, int i2) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        ParentViewHolder parent;

        if(view==null){
            view=inflater.inflate(R.layout.od_header,null);
            parent= new ParentViewHolder();
            view.setTag(parent);
        }else{
            parent= (ParentViewHolder) view.getTag();
        }
        parent.txt_outlet_name= (TextView) view.findViewById(R.id.txt_outlet_name);
        parent.txt_outlet_name.setText(outletsList.get(i).getName());
        //view.setVisibility(View.GONE);
        return view;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,boolean isLastChild, View convertView, ViewGroup parent) {
        ChildViewHolder child;
        if(convertView==null){
            convertView=inflater.inflate(R.layout.order_details_list_cell,null);
        }
        child= new ChildViewHolder();
        convertView.setTag(convertView);

//        else
//        {
//            child= (ChildViewHolder) convertView.getTag();
//        }

        child.txt_price= (TextView) convertView.findViewById(R.id.outlet_wise_price);
        child.txt_product_name= (TextView) convertView.findViewById(R.id.txt_product_name);
        child.txt_descr= (TextView) convertView.findViewById(R.id.txt_product_features);
        child.txt_quantity= (TextView) convertView.findViewById(R.id.product_quantity);


            int qty = getProductQtyWithCustomization(outletsList.get(groupPosition).getId(),dataToshow.get(outletsList.get(groupPosition)).get(childPosition));
            double price = 0;
            price = dataToshow.get(outletsList.get(groupPosition)).get(childPosition).getPrice_with_customization()*qty;
            child.txt_quantity.setText("" + qty);
            child.txt_product_name.setText(dataToshow.get(outletsList.get(groupPosition)).get(childPosition).getName());
            for (Customization c : dataToshow.get(outletsList.get(groupPosition)).get(childPosition).getCustomization()) {
                child.txt_descr.setText(c.getName() + ",");
            }
        child.txt_descr.setVisibility(View.GONE);
            child.txt_price.setText("" + price + " ₹");

            return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i2) {
        return false;
    }


    static class ChildViewHolder{
        TextView txt_quantity;
        TextView txt_product_name;
        TextView txt_price;
        TextView txt_descr;
    }

    static class ParentViewHolder{
        TextView txt_outlet_name;

    }


    public synchronized int getProductQtyWithCustomization(long outletId, Product product){
        int count=0;
        if(orders.containsKey(outletId)){

            for(Product p: orders.get(outletId).getProducts()){

                if(p.getOutlet_product_id().longValue()==product.getOutlet_product_id()){
                    int matchCount=0;

                    for(Customization c:p.getCustomization()){

                        for(Customization c2:product.getCustomization()){
                            if(c.getOutlet_product_id().longValue()==c2.getOutlet_product_id().longValue()){
                                matchCount++;
                            }
                        }

                    }
                    if(matchCount==product.getCustomization().size()){
                        count++;
                    }
                }
            }

        }

        return count;

    }
    public synchronized List<Product> getProductsWithoutDuplicate(long outletId){

        List<Product> productList=new ArrayList<Product>();


        if (orders.containsKey(outletId)) {

            List<Product> cartProducts=new ArrayList<Product>(orders.get(outletId).getProducts());
            List<Product> cartProducts2=new ArrayList<Product>(orders.get(outletId).getProducts());


            while(cartProducts2.size()>0) {


                for (Product p : cartProducts) {

                    ListIterator<Product> iterator=cartProducts2.listIterator();
                    Product productToAdd=null;
                    while(iterator.hasNext()) {
                        Product p2 = iterator.next();
                        int matchCount = 0;
                        if (p.getCustomization().size() == 0) {
                            if(p2.getCustomization().size()==0){
                                if(p.getOutlet_product_id()==p2.getOutlet_product_id()){
                                    productToAdd = p;
                                    iterator.remove();
                                }
                            }

                        } else {

                            for (Customization c : p.getCustomization()) {

                                for (Customization c2 : p2.getCustomization()) {

                                    if (c.getOutlet_product_id().longValue() == c2.getOutlet_product_id().longValue()) {
                                        matchCount++;
                                    }

                                }

                            }

                            if (p.getCustomization().size() == matchCount) {
                                productToAdd = p;
                                iterator.remove();
                            }

                        }
                    }
                    if(productToAdd!=null){
                        if(!productList.contains(productToAdd))
                            productList.add(productToAdd);
                    }

                }
            }



        }



        for(Product p : productList){
            if(p.getCustomization()!=null) {
                StringBuilder custStringBuilder=new StringBuilder();

                Collections.sort(p.getCustomization(), customizationComparator);

                for (Customization c : p.getCustomization()){
                    custStringBuilder.append(c.getOutlet_product_id());
                    custStringBuilder.append(":");
                }
                p.getAdditionalProperties().put("custString",custStringBuilder.toString());

            }

        }

        Collections.sort(productList, productComparator);

        return productList;

    }
    private Comparator<Product> productComparator =new Comparator<Product>() {
        @Override
        public int compare(Product product, Product product2) {
            int result=product.getName().compareTo(product2.getName());

            if(result==0){
                if(product.getAdditionalProperties().get("custString")!=null && product2.getAdditionalProperties().get("custString")!=null) {
                    return product.getAdditionalProperties().get("custString").toString().compareTo(product2.getAdditionalProperties().get("custString").toString());
                }
                else{
                    return 0;
                }
            }
            else{
                return result;
            }
        }
    };
    private Comparator<Customization> customizationComparator =new Comparator<Customization>() {
        @Override
        public int compare(Customization customization, Customization customization2) {
            return customization.getOutlet_product_id().compareTo(customization2.getOutlet_product_id());
        }
    };




}

//        extends BaseAdapter {
//    Context context;
//    ArrayList<Order> ordersInCart;
//    LayoutInflater inflater;
//    ArrayList<Outlet>listOfOutlets;
//    ArrayList<Product>productsPerOutlet;
//    DBManager dbMan;
//    int currentOutletIndex;
//    boolean showHeader=true;
//    public OrderDetailsListAdapter(Context ctx,ArrayList<Order>orders,List<Product> products){
//        super();
//        context=ctx;
//        ordersInCart=orders;
//
//        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        listOfOutlets= new ArrayList<Outlet>();
//        productsPerOutlet= new ArrayList<Product>();
//        dbMan=DBManager.getInstance(context);
//        for(Order o:ordersInCart){
//            listOfOutlets.add(dbMan.getOutletById(o.getOutlet_id()));
//        }
//        productsPerOutlet= (ArrayList<Product>) getProductsWithoutDuplicate(listOfOutlets.get(0).getId());
//
//        currentOutletIndex=0;
//    }
//    @Override
//    public int getCount() {
//        return productsPerOutlet.size();
//    }
//
//    @Override
//    public Product getItem(int i) {
//        return productsPerOutlet.get(i);
//    }
//
//    @Override
//    public long getItemId(int i) {
//        return 0;
//    }
//
//    @Override
//    public View getView(int i, View view, ViewGroup viewGroup) {
//        ViewHolder holder;
//        if(view==null){
//            view=inflater.inflate(R.layout.order_details_list_cell,null);
//            holder=new ViewHolder();
//            view.setTag(holder);
//        }else {
//            holder =(ViewHolder) view.getTag();
//        }
//        holder.txt_outlet_name= (TextView) view.findViewById(R.id.txt_outlet_name);
//        holder.txt_price= (TextView) view.findViewById(R.id.outlet_wise_price);
//        holder.txt_product_name= (TextView) view.findViewById(R.id.txt_product_name);
//        holder.txt_descr= (TextView) view.findViewById(R.id.txt_product_features);
//        holder.txt_quantity= (TextView) view.findViewById(R.id.product_quantity);
//        if(!productsPerOutlet.get(i).isShownInList()) {
//
//            if (currentOutletIndex==0) {
//                holder.txt_outlet_name.setText(listOfOutlets.get(currentOutletIndex).getName());
//                showHeader = false;
//                currentOutletIndex=1;
//            }else{
//                holder.txt_outlet_name.setVisibility(View.GONE);
//            }
//            int qty = getProductQtyWithCustomization(listOfOutlets.get(0).getId(),productsPerOutlet.get(i));
//            if(qty>0)productsPerOutlet.get(i).setShownInList(true);
//            double price = 0;
//            price = productsPerOutlet.get(i).getPrice_with_customization()*qty;
//            holder.txt_quantity.setText("" + qty);
//            holder.txt_product_name.setText(productsPerOutlet.get(i).getName());
//            for (Customization c : productsPerOutlet.get(i).getCustomization()) {
//                holder.txt_descr.setText(c.getName() + ",");
//            }
//            holder.txt_price.setText("" + price + " ₹");
//            return view;
//        }
//        return view;
//    }
//
//    static class ViewHolder{
//        TextView txt_outlet_name;
//        TextView txt_quantity;
//        TextView txt_product_name;
//        TextView txt_price;
//        TextView txt_descr;
//    }
//
//    public synchronized int getProductQtyWithCustomization(long outletId, Product product){
//        int count=0;
//
//            for(Product p: ordersInCart.get(0).getProducts()){
//
//                if(p.getOutlet_product_id().longValue()==product.getOutlet_product_id()){
//                    int matchCount=0;
//
//                    for(Customization c:p.getCustomization()){
//
//                        for(Customization c2:product.getCustomization()){
//                            if(c.getOutlet_product_id().longValue()==c2.getOutlet_product_id().longValue()){
//                                matchCount++;
//                            }
//                        }
//
//                    }
//                    if(matchCount==product.getCustomization().size()){
//                        count++;
//                    }
//                }
//            }
//
//
//        return count;
//
//    }
//    private Comparator<Product> productComparator =new Comparator<Product>() {
//        @Override
//        public int compare(Product product, Product product2) {
//            int result=product.getName().compareTo(product2.getName());
//
//            if(result==0){
//                if(product.getAdditionalProperties().get("custString")!=null && product2.getAdditionalProperties().get("custString")!=null) {
//                    return product.getAdditionalProperties().get("custString").toString().compareTo(product2.getAdditionalProperties().get("custString").toString());
//                }
//                else{
//                    return 0;
//                }
//            }
//            else{
//                return result;
//            }
//        }
//    };
//    private Comparator<Customization> customizationComparator =new Comparator<Customization>() {
//        @Override
//        public int compare(Customization customization, Customization customization2) {
//            return customization.getOutlet_product_id().compareTo(customization2.getOutlet_product_id());
//        }
//    };
//
//
//    public synchronized List<Product> getProductsWithoutDuplicate(long outletId){
//
//        List<Product> productList=new ArrayList<Product>();
//
//
//
//
//            List<Product> cartProducts=new ArrayList<Product>(ordersInCart.get(0).getProducts());
//            List<Product> cartProducts2=new ArrayList<Product>(ordersInCart.get(0).getProducts());
//
//
//            while(cartProducts2.size()>0) {
//
//
//                for (Product p : cartProducts) {
//
//                    ListIterator<Product> iterator=cartProducts2.listIterator();
//                    Product productToAdd=null;
//                    while(iterator.hasNext()){
//                        Product p2=iterator.next();
//                        int matchCount=0;
//                        for(Customization c:p.getCustomization()){
//
//                            for(Customization c2:p2.getCustomization()){
//
//                                if(c.getOutlet_product_id().longValue()==c2.getOutlet_product_id().longValue()){
//                                    matchCount++;
//                                }
//
//                            }
//
//                        }
//
//                        if(p.getCustomization().size()==matchCount){
//                            productToAdd=p;
//                            iterator.remove();
//                        }
//
//                    }
//
//                    if(productToAdd!=null){
//                        productList.add(productToAdd);
//                    }
//
//                }
//            }
//
//
//
//
//
//
//
//        for(Product p : productList){
//            if(p.getCustomization()!=null) {
//                StringBuilder custStringBuilder=new StringBuilder();
//
//                Collections.sort(p.getCustomization(), customizationComparator);
//
//                for (Customization c : p.getCustomization()){
//                    custStringBuilder.append(c.getOutlet_product_id());
//                    custStringBuilder.append(":");
//                }
//                p.getAdditionalProperties().put("custString",custStringBuilder.toString());
//
//            }
//
//        }
//
//        Collections.sort(productList, productComparator);
//
//        return productList;
//
//    }
//
//
//
//
//}
