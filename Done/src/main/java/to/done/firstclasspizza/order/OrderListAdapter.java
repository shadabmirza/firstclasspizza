package to.done.firstclasspizza.order;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import to.done.firstclasspizza.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;

import to.done.lib.Constants;
import to.done.lib.cart.Cart;
import to.done.lib.database.DBManager;
import to.done.lib.entity.order.Customization;
import to.done.lib.entity.outletmenu.Product;
import to.done.lib.utils.Toolbox;

/**
 * Created by HP on 6/23/2014.
 */
public class OrderListAdapter extends BaseAdapter {
    private Context context;
    private DBManager dbMan;
    private Cart cart;
    private List<Product> productList;
    private LayoutInflater inflater;
    private long outletId;
    private long companyId;

    public OrderListAdapter(Activity context, long companyId, long outletId){
        this.context=context;
        this.outletId=outletId;
        this.companyId=companyId;
        dbMan=DBManager.getInstance(context.getApplication());
        cart=Cart.getInstance();
//        productList=dbMan.getProductsForOutlet(outletId);
        inflater=LayoutInflater.from(context);
        productList=cart.getProductsWithoutDuplicate(outletId);

        for(Product p:productList){
            p.getAdditionalProperties().put("customizationString",dbMan.getCustomizationString(p.getCustomization()));
        }

    }
    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Product getItem(int i) {
        return productList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return productList.get(i).getId().longValue();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) { ChildViewHolder child;
        if(convertView==null){
            convertView=inflater.inflate(R.layout.order_details_list_cell,null);
        }
        child= new ChildViewHolder();
        convertView.setTag(convertView);


        child.txt_price= (TextView) convertView.findViewById(R.id.outlet_wise_price);
        child.txt_product_name= (TextView) convertView.findViewById(R.id.txt_product_name);
        child.txt_descr= (TextView) convertView.findViewById(R.id.txt_product_features);
        child.txt_quantity= (TextView) convertView.findViewById(R.id.product_quantity);


        int qty = cart.getProductQtyWithCustomization(outletId, productList.get(i));
        double productTotal=getItem(i).getPrice();
        for(Customization c: getItem(i).getCustomization()){
            productTotal+=c.getPrice();
        }
        double price = 0;
        price = productTotal*qty;
        child.txt_quantity.setText("" + qty);
        child.txt_product_name.setText(productList.get(i).getName());
        //int j=0,size=productList.get(i).getCustomization().size();
        if(productList.get(i).getAdditionalProperties().get("customizationString")!=null)
        {
            child.txt_descr.setText(getItem(i).getAdditionalProperties().get("customizationString").toString());
        }else{
            child.txt_descr.setText(" ");
        }

//        for (Customization c : productList.get(i).getCustomization()) {
//            StringBuilder descr= new StringBuilder();
//
//                descr.append(c.getName());
//                if (++j == size) descr.append(".");
//                else descr.append(",");
//                child.txt_descr.setText(descr.toString());
//
//        }
        child.txt_descr.setVisibility(View.VISIBLE);
        child.txt_price.setText(Constants.RUPEE_SYMBOL + Toolbox.formatDecimal(price) + "");
        return convertView;
    }

    static class ChildViewHolder{
        TextView txt_quantity;
        TextView txt_product_name;
        TextView txt_price;
        TextView txt_descr;
    }



    public synchronized int getProductQtyWithCustomization(long outletId, Product product){
        int count=0;
        if(Cart.getInstance().getOrders().containsKey(outletId)){

            for(Product p: Cart.getInstance().getOrders().get(outletId).getProducts()){

                if(p.getOutlet_product_id().longValue()==product.getOutlet_product_id()){
                    int matchCount=0;

                    for(Customization c:p.getCustomization()){

                        for(Customization c2:product.getCustomization()){
                            if(c.getOutlet_product_id().longValue()==c2.getOutlet_product_id().longValue()){
                                matchCount++;
                            }
                        }

                    }
                    if(matchCount==product.getCustomization().size()){
                        count++;
                    }
                }
            }

        }

        return count;

    }
    public synchronized List<Product> getProductsWithoutDuplicate(long outletId){

        List<Product> productList=new ArrayList<Product>();


        if (Cart.getInstance().getOrders().containsKey(outletId)) {

            List<Product> cartProducts=new ArrayList<Product>(Cart.getInstance().getOrders().get(outletId).getProducts());
            List<Product> cartProducts2=new ArrayList<Product>(Cart.getInstance().getOrders().get(outletId).getProducts());


            while(cartProducts2.size()>0) {


                for (Product p : cartProducts) {

                    ListIterator<Product> iterator=cartProducts2.listIterator();
                    Product productToAdd=null;
                    while(iterator.hasNext()) {
                        Product p2 = iterator.next();
                        int matchCount = 0;
                        if (p.getCustomization().size() == 0) {
                            if(p2.getCustomization().size()==0){
                                if(p.getOutlet_product_id()==p2.getOutlet_product_id()){
                                    productToAdd = p;
                                    iterator.remove();
                                }
                            }

                        } else {

                            for (Customization c : p.getCustomization()) {

                                for (Customization c2 : p2.getCustomization()) {

                                    if (c.getOutlet_product_id().longValue() == c2.getOutlet_product_id().longValue()) {
                                        matchCount++;
                                    }

                                }

                            }

                            if (p.getCustomization().size() == matchCount) {
                                productToAdd = p;
                                iterator.remove();
                            }

                        }
                    }
                    if(productToAdd!=null){
                        if(!productList.contains(productToAdd))
                            productList.add(productToAdd);
                    }

                }
            }



        }



        for(Product p : productList){
            if(p.getCustomization()!=null) {
                StringBuilder custStringBuilder=new StringBuilder();

                Collections.sort(p.getCustomization(), customizationComparator);

                for (Customization c : p.getCustomization()){
                    custStringBuilder.append(c.getOutlet_product_id());
                    custStringBuilder.append(":");
                }
                p.getAdditionalProperties().put("custString",custStringBuilder.toString());

            }

        }

        Collections.sort(productList, productComparator);

        return productList;

    }
    private Comparator<Product> productComparator =new Comparator<Product>() {
        @Override
        public int compare(Product product, Product product2) {
            int result=product.getName().compareTo(product2.getName());

            if(result==0){
                if(product.getAdditionalProperties().get("custString")!=null && product2.getAdditionalProperties().get("custString")!=null) {
                    return product.getAdditionalProperties().get("custString").toString().compareTo(product2.getAdditionalProperties().get("custString").toString());
                }
                else{
                    return 0;
                }
            }
            else{
                return result;
            }
        }
    };
    private Comparator<Customization> customizationComparator =new Comparator<Customization>() {
        @Override
        public int compare(Customization customization, Customization customization2) {
            return customization.getOutlet_product_id().compareTo(customization2.getOutlet_product_id());
        }
    };



}
