package to.done.firstclasspizza.order;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import to.done.firstclasspizza.MainActivity;
import to.done.firstclasspizza.R;
import to.done.firstclasspizza.common.TopAndBottomBarFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.cart.Cart;
import to.done.lib.database.DBManager;
import to.done.lib.entity.nearestoutlets.Outlet;
import to.done.lib.entity.order.Applied_extra_charge;
import to.done.lib.entity.order.Applied_offer;
import to.done.lib.entity.order.Order;
import to.done.lib.entity.order.RespCheckOrder;
import to.done.lib.entity.order.RespSaveOrder;
import to.done.lib.entity.outletsubareas.Area;
import to.done.lib.entity.outletsubareas.Subarea;
import to.done.lib.entity.user.User;
import to.done.lib.sync.SyncListener;
import to.done.lib.sync.SyncManager;
import to.done.lib.ui.CustomDialog;
import to.done.lib.ui.DialogListener;
import to.done.lib.utils.GoogleAnalyticsManager;
import to.done.lib.utils.SharedPreferencesManager;
import to.done.lib.utils.Toolbox;

/**
 * Created by HP on 5/2/2014.
 */
public class ConfirmOrderFragment extends TopAndBottomBarFragment {
    @InjectView(R.id.lst_order_details)
    ListView order_details_list;

    LinearLayout extra_charges_holder;
    LinearLayout applied_offers;
    User user;
    Cart cart;
    ArrayList<Order>orders;
    OrderListAdapter detailsListAdapter;
    double totalAmount=0;
    HashMap<Long,Order>ordersMap;
    RespCheckOrder resp;
    Outlet outlet=new Outlet();
    View footerView=null;
    ImageView img;
TextView taxAmt;
    Subarea userSubarea;
    Area userArea;
    DBManager db;
    ProgressDialog pDialog;
    SharedPreferences.Editor editor;
    SharedPreferences preferencesManager;

    LayoutInflater inflater;
    boolean placed=false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.order_confirmation_page,null);
        ButterKnife.inject(this, view);
        super.onCreateView(inflater,container,savedInstanceState);
        init();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).drawerOpen=false;

    }

    @Override
    public void onProceed() {
        if(!placed) {
            placed=true;
            //getImgProceed().setOnClickListener(null);
            SyncManager.saveOrder(getActivity(), saveOrder, Toolbox.getAppVersionName(getActivity()));
        }
        Toolbox.writeToLog("Can place order now?" + (placed));
//        else{
//            Cart.getInstance().clearCart();
//            List<String> backStackEntryList=new ArrayList<String>();
//            for(int i=0;i<getActivity().getSupportFragmentManager().getBackStackEntryCount();i++){
//                android.support.v4.app.FragmentManager.BackStackEntry  entry=getActivity().getSupportFragmentManager().getBackStackEntryAt(i);
//                backStackEntryList.add(entry.getName());
//                // Toolbox.writeToLog("Backstack@"+i+ "= "+entry.getName());
//            }
//            if(backStackEntryList.indexOf("screen_id_"+Constants.SCREEN_OUTLETS)!=-1) {
//                getActivity().getSupportFragmentManager().popBackStackImmediate("screen_id_" + Constants.SCREEN_OUTLETS, 0);
//            }
////            editor.putString(Constants.ORDER_NUMBER,Long.toString(oid));
////            editor.commit();
//            Toolbox.changeScreen(getActivity(), Constants.SCREEN_THANKS,true,null);
//        }
    }

    @Override
    public String getProceedButtonText() {
        return "Confirm";
    }

    private void init(){
        inflater=getActivity().getLayoutInflater();
        pDialog= new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        if(getActivity()==null) {return;}
        Toolbox.writeToLog("Can place order now?" + (!placed));
        ((MainActivity)getActivity()).drawerOpen=false;

    ordersMap= new HashMap<Long, Order>();
        db= DBManager.getInstance(getActivity());
        if(getActivity()!=null)
            preferencesManager = SharedPreferencesManager.getSharedPreferences(getActivity());
        editor = preferencesManager.edit();
    Toolbox.changeActionBarTitle(getActivity(), "Confirm order", "");
        ImageView img=(ImageView)((ActionBarActivity)getActivity()).getSupportActionBar().getCustomView().findViewById(R.id.imgBack);
        img.setVisibility(View.VISIBLE);
    cart= Cart.getInstance();
        if(cart==null || cart.getOrders()==null || cart.getRespCheckOrder()==null || cart.getUser()==null) {
         //  forceChangetoOutletScreen();
            getActivity().finish();
        }
    resp=cart.getRespCheckOrder();
        if(resp==null || resp.getData()==null){
            SyncManager.checkOrder(getActivity(), checkOrder);
        }
        else {
        initAll();
        }


    }

    private void initAll(){
        user = cart.getUser();

        for (Order o : resp.getData().getOrders()) {
            ordersMap.put(o.getOutlet_id(), o);
        }
        orders = new ArrayList<Order>();
        orders.addAll(resp.getData().getOrders());
        if (detailsListAdapter == null) {
            detailsListAdapter = new OrderListAdapter(getActivity(), 6872L, cart.getFirstOrder().getOutlet_id());
        }
//        }else{
////            for(int i=0;i<order_details_list.getHeaderViewsCount();i++){
////                order_details_list.rem
////            }
//            detailsListAdapter.notifyDataSetChanged();
//        }
        if (order_details_list.getHeaderViewsCount() == 0)
            setOrderDetailsListHeader();
        if (order_details_list.getFooterViewsCount() == 0)
            setOrderDetailsFooterView();
        order_details_list.setAdapter(detailsListAdapter);
        for (Order o : orders) {
            totalAmount += o.getTotal();
        }
    }

   View.OnClickListener proceedListener= new View.OnClickListener() {
       @Override
       public void onClick(View view) {

       }
   };

    private void setOrderDetailsListHeader() {
        View headerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.od_list_header, null);
        TextView name = (TextView) headerView.findViewById(R.id.txt_user_name);
        TextView phone = (TextView) headerView.findViewById(R.id.txt_user_phone);
        TextView address = (TextView) headerView.findViewById(R.id.txt_useraddress);
        TextView label2 = (TextView) headerView.findViewById(R.id.txt_del_addr_label);
        name.setText(user.getName());
        phone.setText(user.getPrimary_phone());
        if (user.getAddress() != null){
            if (user.getAddress().getArea_id() != null) {
                userArea = db.getAreaById(user.getAddress().getArea_id());
                if (user.getAddress().getSubarea_id() != null) {
                    userSubarea = db.getSubareaById(user.getAddress().getSubarea_id());
                }
            }
        String addr = user.getAddress().getFlat_no() + "," + user.getAddress().getStreet();
            if(user.getAddress().getLandmark()!=null && user.getAddress().getLandmark().trim().length()>0)
                addr=addr+ "," + user.getAddress().getLandmark();
        if (userArea != null && userSubarea != null) {
            addr = addr + "," + userSubarea.getSubarea_name() + "," +userArea.getArea_name() ;
        }
        address.setText(addr);
//            Toolbox.writeToLog("Address area "+userArea.getArea_name()+" subarea "+userSubarea.getSubarea_name());
    }
        else {
            address.setVisibility(View.GONE);
            label2.setVisibility(View.GONE);
        }
        order_details_list.addHeaderView(headerView);
    }

    private void setOrderDetailsFooterView(){
        footerView=((LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.od_list_footer,null);
//         price= (TextView) footerView.findViewById(R.id.price);
        ImageView proceed= (ImageView) footerView.findViewById(R.id.img_done_logo);
        Button redeem_coupon=(Button) footerView.findViewById(R.id.btn_rdm_cpn);
       TextView delCharges,taxText;
        img=(ImageView)footerView.findViewById(R.id.img_done_logo);
//        taxText=(TextView) footerView.findViewById(R.id.txt_payment_method);
        taxAmt=(TextView) footerView.findViewById(R.id.tax_amt);
        delCharges=(TextView) footerView.findViewById(R.id.del_charges_amt);

        proceed.setOnClickListener(proceedListener);
        taxAmt.setText(Constants.RUPEE_SYMBOL+resp.getData().getTotal_amount());
        Toolbox.animateQuantityIncrease(taxAmt);
         extra_charges_holder= (LinearLayout) footerView.findViewById(R.id.discounts);
        applied_offers=(LinearLayout) footerView.findViewById(R.id.offers);
//        if(resp.getData().getOrders().get(0).getApplied_extra_charges()!=null && resp.getData().getOrders().get(0).getApplied_extra_charges().size()>0) {
//
//
//            price.setText("+₹ " + resp.getData().getOrders().get(0).getApplied_extra_charges().get(0).getExtra_charged_amt());
//            if(resp.getData().getOrders().get(0).getApplied_extra_charges().get(0).getType().toLowerCase().contains("perce")){
//                taxText.setText("Taxes applicable "+resp.getData().getOrders().get(0).getApplied_extra_charges().get(0).getValue()+"%");
//            }else{
//                taxText.setText("Taxes applicable "+"+₹ "+resp.getData().getOrders().get(0).getApplied_extra_charges().get(0).getValue());
//            }
//        }else {
//            price.setText("+₹ " + 00);
//            taxText.setText("Taxes applicable "+"+₹ "+00);
//        }//resp.getData().getApplied_extra_charges().get(0).getExtra_charged_amt()
//
//        delCharges.setText("+₹ 00");
        redeem_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                RedeemCouponDialogFragment redeemDialog=new RedeemCouponDialogFragment();
//                redeemDialog.show(getActivity().getSupportFragmentManager(),"");
        createCouponDialog();
            }
        });
        if(resp.getData().getOrders().get(0).getApplied_offer()!=null && resp.getData().getOrders().get(0).getApplied_offer().size()>0) {
            if (resp.getData().getOrders().get(0).getApplied_offer().get(0) != null) {
//                discountText.setVisibility(View.VISIBLE);
//                discountAmt.setVisibility(View.VISIBLE);
                Applied_offer aof = resp.getData().getOrders().get(0).getApplied_offer().get(0);
                //Toolbox.showToastLong(getActivity(), "Offer " + aof.getId() + " " + aof.getCoupon_code() + " " + aof.getName());
//                discountText.setText(aof.getName());
//                discountAmt.setText("-₹ "+aof.getDiscount_amount());

            }
        }
        addExtraChargesViewToLayout(extra_charges_holder,resp.getData().getOrders().get(0).getApplied_extra_charges());
        order_details_list.addFooterView(footerView);

    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalyticsManager.sendScreenView(getActivity(), Constants.GA_SCREEN_CONFIRM_ORDER);
    }
    private void forceChangetoOutletScreen(){
        List<String> backStackEntryList = new ArrayList<String>();
        for (int i = 0; i < getActivity().getSupportFragmentManager().getBackStackEntryCount(); i++) {
            android.support.v4.app.FragmentManager.BackStackEntry entry = getActivity().getSupportFragmentManager().getBackStackEntryAt(i);
            backStackEntryList.add(entry.getName());
        }
        if (backStackEntryList.indexOf("screen_id_" + Constants.SCREEN_OUTLETS) != -1) {
            getActivity().getSupportFragmentManager().popBackStackImmediate("screen_id_" + Constants.SCREEN_OUTLETS, 0);
        }

        Toolbox.changeScreen(getActivity(), Constants.SCREEN_THANKS, true, null);
    }

    @Override
    public int getScreenIndex() {
        return TopAndBottomBarFragment.ADDRESS;
    }

    SyncListener saveOrder= new SyncListener() {
        @Override
        public void onSyncStart() {

           // imgProceed.setOnClickListener(null);
            if(!pDialog.isShowing()){
                pDialog.setMessage("Placing your order....");
                pDialog.show();
            }else{
                pDialog.dismiss();
            }
           // Toolbox.showToastShort(getActivity(), "Calling save-order");

        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            Toolbox.writeToLog("Can place order now?" + (!placed));
            if(getActivity()==null)return;
            RespSaveOrder response=(RespSaveOrder)responseObject;

            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }
            final Long oid=response.getData().getOrder_number();
            String message="Order placed succesfully.Your order number is "+oid;
            showError(ErrorType.ANNOUNCEMENT,message);
            editor.putString(Constants.ORDER_NUMBER,Long.toString(oid));
            editor.commit();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    hideError();
                }
            },200);
//            placed=true;
            if(response.getResponseCode()== Constants.RESPONSE_CODE_SUCCESS) {
                CustomDialog.createCustomDialog(getActivity(), message, "OK", null, null, false, new DialogListener() {
                    @Override
                    public void onPositiveBtnClick() {
                        Cart.getInstance().clearCart();
//                            MainActivity.writeToSD();
                        List<String> backStackEntryList = new ArrayList<String>();
                        for (int i = 0; i < getActivity().getSupportFragmentManager().getBackStackEntryCount(); i++) {
                            android.support.v4.app.FragmentManager.BackStackEntry entry = getActivity().getSupportFragmentManager().getBackStackEntryAt(i);
                            backStackEntryList.add(entry.getName());
                            // Toolbox.writeToLog("Backstack@"+i+ "= "+entry.getName());
                        }
                        if (backStackEntryList.indexOf("screen_id_" + Constants.SCREEN_PRODUCTS) != -1) {
                            getActivity().getSupportFragmentManager().popBackStackImmediate("screen_id_" + Constants.SCREEN_PRODUCTS, 0);
                        }
                        editor.putString(Constants.ORDER_NUMBER, Long.toString(oid));
                        editor.commit();
                        //Toolbox.changeScreen(getActivity(), Constants.SCREEN_THANKS, true, null);
                    }

                    @Override
                    public void onNegativeBtnClick() {

                    }

                    @Override
                    public void onMiddleBtnClick() {

                    }
                });
            }else {
                if (response.getResponseCode() == Constants.RESP_CODE_PRICE_CHANGE) {
                    CustomDialog.createCustomDialog(getActivity(), "There is a mismatch in the prices of products that" +
                            " you have and /n the actual prices.Please press ok and select your outlet again.", "OK", null, null, false, new DialogListener() {
                        @Override
                        public void onPositiveBtnClick() {
                            Cart.getInstance().clearCart();
                            List<String> backStackEntryList = new ArrayList<String>();
                            for (int i = 0; i < getActivity().getSupportFragmentManager().getBackStackEntryCount(); i++) {
                                android.support.v4.app.FragmentManager.BackStackEntry entry = getActivity().getSupportFragmentManager().getBackStackEntryAt(i);
                                backStackEntryList.add(entry.getName());

                            }
                            if (backStackEntryList.indexOf("screen_id_" + Constants.SCREEN_OUTLETS) != -1) {
                                getActivity().getSupportFragmentManager().popBackStackImmediate("screen_id_" + Constants.SCREEN_OUTLETS, 0);
                            }

                            Toolbox.changeScreen(getActivity(), Constants.SCREEN_THANKS, true, null);
                        }

                        @Override
                        public void onNegativeBtnClick() {

                        }

                        @Override
                        public void onMiddleBtnClick() {

                        }
                    });
                }
            }
      }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }
            showError(ErrorType.ANNOUNCEMENT, reason);
            //getImgProceed().setOnClickListener(getProceedClickListener());
            placed=false;
            Toolbox.writeToLog("Can place order now?" + (!placed));
        }

    };

    private ExpandableListView.OnGroupClickListener groupClick= new ExpandableListView.OnGroupClickListener() {
        @Override
        public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
            return true;
        }
    };
    private void createCouponDialog(){
        //shift dialogfragment here,easier to refresh page.
        final Dialog dialog= new Dialog(this.getActivity());

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.redeem_coupon_dialog);

        Window window = dialog.getWindow();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.gravity = Gravity.TOP ;

        lp.y = 80;
        window.setAttributes(lp);

        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        Button okButton = (Button) dialog.findViewById(R.id.redeem_coupon_ok_button);
        final EditText redeem_coupon_text = (EditText) dialog.findViewById(R.id.redeem_coupon_text);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String coupon = redeem_coupon_text.getText().toString();
                if (coupon != null && !coupon.isEmpty()) {
                    Cart cart = Cart.getInstance();
                    //cart.setCouponCode(coupon);
                    Map<Long, Order> orders = cart.getOrders();
                    for (long outletId : orders.keySet()) {
                        Order o = orders.get(outletId);

                        if (o != null) {
                         cart.setCouponCode(coupon);
                            o.setCoupon_code(coupon);
                        }
                    }

                    SyncManager.checkOrder(getActivity(), checkOrder);
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(redeem_coupon_text.getWindowToken(), 0);
                    dialog.dismiss();
                }
            }
        });

//        setOnTouchListener(new View.OnTouchListener() {
//
//    @Override
//    public boolean onTouch(View v, MotionEvent event) {
//        Configuration config = getResources().getConfiguration();
//        if (config.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
//            v.requestFocusFromTouch();
//        } else {
//            v.requestFocus();
//            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
//        }
//        return false;
//    }
//});
        dialog.show();
        redeem_coupon_text.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

    }
    SyncListener checkOrder = new SyncListener() {
        @Override
        public void onSyncStart() {
            if(!pDialog.isShowing()){
                pDialog.setMessage("Verifying discounts...");
                pDialog.show();
            }else{
                pDialog.dismiss();
            }
            hideError();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            RespCheckOrder response=(RespCheckOrder)responseObject;
            if(getActivity()==null)return;
            initAll();
            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }
            resp=Cart.getInstance().getRespCheckOrder();
            addExtraChargesViewToLayout(extra_charges_holder,Cart.getInstance().getRespCheckOrder().getData().getOrders().get(0).getApplied_extra_charges());
            taxAmt.setText(Constants.RUPEE_SYMBOL+resp.getData().getTotal_amount());
            List<Applied_offer>offers=Cart.getInstance().getRespCheckOrder().getData().getOrders().get(0).getApplied_offer();
            if(offers!=null && offers.size()>0) {
 //               if (Cart.getInstance().getRespCheckOrder().getData().getOrders().get(0).getApplied_offer().get(0) != null) {
     //               Applied_offer aof = Cart.getInstance().getRespCheckOrder().getData().getOrders().get(0).getApplied_offer().get(0);
//                    discountText.setVisibility(View.VISIBLE);
//                    discountAmt.setVisibility(View.VISIBLE);
//                    discountText.setText(aof.getName());
//                    discountAmt.setText("-₹ "+aof.getDiscount_amount());
 //                   Toolbox.animateQuantityIncrease(discountAmt);
                addDiscountViewToLayout(applied_offers,offers);
                showError(ErrorType.ANNOUNCEMENT,"Discount applied!");

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            hideError();
                        }
                    },100);

                }

 //               }
        else{
//                discountText.setVisibility(View.VISIBLE);
//                discountAmt.setVisibility(View.VISIBLE);
//                discountText.setText("Discount not applicable");
//                discountAmt.setText("-₹ " + 00);
//                Toolbox.animateQuantityIncrease(discountAmt);
               applied_offers.setVisibility(View.GONE);
                showError(ErrorType.ANNOUNCEMENT,"Discount not applicable");

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hideError();
                    }
                },100);
            }

            if(Cart.getInstance().getRespCheckOrder().getResponseCode()==600){
                showError(ErrorType.ANNOUNCEMENT, Cart.getInstance().getRespCheckOrder().getResponseMsg());

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hideError();
                    }
                },100);
            }

             {
                if (response.getResponseCode() == Constants.RESP_CODE_PRICE_MISMATCH) {
                    CustomDialog.createCustomDialog(getActivity(), "There is a mismatch in the prices of products that" +
                            " you have and /n the actual prices.Please press ok and select your outlet again.", "OK",
                            null, null, false, new DialogListener() {
                        @Override
                        public void onPositiveBtnClick() {
                            Cart.getInstance().clearCart();
                            List<String> backStackEntryList = new ArrayList<String>();
                            for (int i = 0; i < getActivity().getSupportFragmentManager().getBackStackEntryCount(); i++) {
                                android.support.v4.app.FragmentManager.BackStackEntry entry = getActivity().getSupportFragmentManager().getBackStackEntryAt(i);
                                backStackEntryList.add(entry.getName());

                            }
                            if (backStackEntryList.indexOf("screen_id_" + Constants.SCREEN_OUTLETS) != -1) {
                                getActivity().getSupportFragmentManager().popBackStackImmediate("screen_id_" + Constants.SCREEN_OUTLETS, 0);
                            }

                            Toolbox.changeScreen(getActivity(), Constants.SCREEN_THANKS, true, null);
                        }

                        @Override
                        public void onNegativeBtnClick() {

                        }

                        @Override
                        public void onMiddleBtnClick() {

                        }
                    });

                }
            }

        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }
            showError(ErrorType.NO_RESULTS, reason);
//            discountAmt.setVisibility(View.GONE);
//            discountText.setVisibility(View.GONE);
            applied_offers.setVisibility(View.GONE);

        }
    };


    public void addExtraChargesViewToLayout(LinearLayout parent,List<Applied_extra_charge>chargesList){
        if(parent.getChildCount()>0) parent.removeAllViews();
        if(chargesList!=null && chargesList.size()>0){
            for(Applied_extra_charge aec:chargesList) {

                View childHolderView = inflater.inflate(R.layout.order_details_extra_charges_view, null, false);
                TextView chargesText = (TextView) childHolderView.findViewById(R.id.txt_payment_method);
                TextView chargesAmount = (TextView) childHolderView.findViewById(R.id.price);

                if(aec.getType().toLowerCase().equals("percent"))
                    chargesText.setText(aec.getName()+" "+aec.getValue()+"%");
                else
                    chargesText.setText(aec.getName());

                chargesAmount.setText(Constants.RUPEE_SYMBOL+" "+Toolbox.formatDecimal(aec.getExtra_charged_amt()));

                parent.addView(childHolderView);
            }
        }

    }
    public void addDiscountViewToLayout(LinearLayout parent,List<Applied_offer>offersList){
        if(parent.getChildCount()>0) parent.removeAllViews();
        if(offersList!=null && offersList.size()>0){
            if(parent.getVisibility()==View.GONE) parent.setVisibility(View.VISIBLE);
            for(Applied_offer aof:offersList) {

                View childHolderView = inflater.inflate(R.layout.order_details_extra_charges_view, null, false);
                TextView chargesText = (TextView) childHolderView.findViewById(R.id.txt_payment_method);
                TextView chargesAmount = (TextView) childHolderView.findViewById(R.id.price);
                chargesText.setText(aof.getName()+" ");//+aof.getCoupon_code());
                chargesAmount.setText(Constants.RUPEE_SYMBOL+" -"+Toolbox.formatDecimal(aof.getDiscount_amount()));

                parent.addView(childHolderView);
            }
        }

    }


}
