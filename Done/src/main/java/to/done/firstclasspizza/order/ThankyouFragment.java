package to.done.firstclasspizza.order;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import to.done.firstclasspizza.MainActivity;
import to.done.firstclasspizza.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.utils.Toolbox;

/**
 * Created by HP on 5/3/2014.
 */


public class ThankyouFragment extends Fragment {

    @InjectView(R.id.thank_move_out)
    TextView proceed;

    @Override
    public void onResume() {
    super.onResume();
        ((MainActivity)getActivity()).drawerOpen=false;
    //    getActivity().getSupportFragmentManager().popBackStack();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.thankyou_page,null);
        ButterKnife.inject(this, view);
        ((MainActivity)getActivity()).drawerOpen=false;
        Toolbox.hideActionBar(getActivity(),"Thank You","");
//        getActivity().getSupportActionBar().hide();
        proceed.setOnClickListener(proceedListener);
        view.setVisibility(View.GONE);
        return view;
    }
    View.OnClickListener proceedListener= new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //MainActivity.writeToSD();
//            Toolbox.changeScreen(getActivity(), Constants.SCREEN_LOC_SELECTION,true,null);
          getActivity().getSupportFragmentManager().popBackStack();
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        getActivity().getSupportFragmentManager().popBackStack();
    }
}
