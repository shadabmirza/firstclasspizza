package to.done.firstclasspizza.order;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import to.done.firstclasspizza.MainActivity;
import to.done.firstclasspizza.R;
import to.done.firstclasspizza.common.TopAndBottomBarFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.cart.Cart;
import to.done.lib.database.DBManager;
import to.done.lib.entity.closestsubareas.DocSubareas;
import to.done.lib.entity.nearestoutlets.Outlet;
import to.done.lib.entity.nearestoutlets.Outlet_timing;
import to.done.lib.entity.order.RespCheckOrder;
import to.done.lib.entity.outletsubareas.Area;
import to.done.lib.entity.outletsubareas.Subarea;
import to.done.lib.entity.user.Address;
import to.done.lib.entity.user.FindMeData;
import to.done.lib.entity.user.ResponseFindMe;
import to.done.lib.entity.user.User;
import to.done.lib.sync.SyncListener;
import to.done.lib.sync.SyncManager;
import to.done.lib.ui.CustomDialog;
import to.done.lib.ui.FontEditText;
import to.done.lib.ui.FontTextView;
import to.done.lib.utils.GoogleAnalyticsManager;
import to.done.lib.utils.SharedPreferencesManager;
import to.done.lib.utils.Toolbox;

/**
 * Created by root on 23/4/14.
 */
public class PersonalDetailsFragment extends TopAndBottomBarFragment {

    Handler h = new Handler();
    @InjectView(R.id.edt_mobile_no)
    FontEditText edt_mobile_no;

    @InjectView(R.id.edt_name)
    FontEditText edt_name;
    @InjectView(R.id.outlet_address)
    FontTextView outAddr;

    @InjectView(R.id.txt_address_info)
    FontTextView txt_address_info;

    @InjectView(R.id.spnr_addresses)
    Spinner addressSpinner;

    @InjectView(R.id.spn_out_area)
    Spinner areasSpinner;

    @InjectView(R.id.spn_out_subarea)
    Spinner subareasSpinner;

    Cart cart;
    DBManager dbMan;


    @InjectView(R.id.edt_address_1)
    FontEditText edtAddressLineOne;
    @InjectView(R.id.edt_address_2)
    FontEditText edtAddressLineTwo;
//    @InjectView(R.id.edt_address_3)
//    FontEditText edtAddressLineThree;

    @InjectView(R.id.edt_address_zipcode)
    FontEditText edt_address_zipcode;

//    @InjectView(R.id.edt_address_tag)
//    FontEditText edtAddressTag;

    @InjectView(R.id.edt_email)
    FontEditText edtEmail;
    User selectedUser;
    Address selectedAddress = new Address();
    boolean goodToGo = false, validateFields = true;
    ArrayList<Address> addresses = null;
    ArrayList<Area> areas = null;
    ArrayList<Subarea> subareas = null;

    @InjectView(R.id.rel_address_fields)
    RelativeLayout addressFields;

    SharedPreferences preferencesManager;
    ArrayAdapter<Address> dataAdapter;
    ArrayAdapter<Area> areaAdapter;
    ArrayAdapter<Subarea> subareaAdapter;
    Outlet outlet = null;
    Subarea selectedSubarea = null;
    Area selectedArea = null;
    SharedPreferences.Editor editor;
    private Pattern pattern;
    private Matcher matcher;
    private ProgressDialog pDialog;

    private DocSubareas autoSubarea;
    private long subareaSearchTimestamp, lastKeystrokeTimestamp;

    private String subareaSearchKeyword;

    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            if (validateMobile(edt_mobile_no, false) && !editable.toString().contains(" ")) {
                if (dbMan.getUserByPrimaryMobile(edt_mobile_no.getText().toString()) != null)
                    createUserFromMobile(edt_mobile_no.getText().toString());
                else {
                    SyncManager.findMe(getActivity(), edt_mobile_no.getText().toString(), findMe);
                }
            }

        }
    };
    private SyncListener areaSubareaListener = new SyncListener() {
        @Override
        public void onSyncStart() {
            if (!pDialog.isShowing()) {
                pDialog.setMessage("Fetching delivery areas...");
                pDialog.show();
            } else {
                pDialog.dismiss();
            }

        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            if (getActivity() == null) return;
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            hideError();

            Toolbox.changeScreen(getActivity(), Constants.SCREEN_CONFIRM_ORDER, true);

            //Toast.makeText(getActivity(), "area subarea success", Toast.LENGTH_SHORT).show();
            initialiseSubareasSpinner();
        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            showError(ErrorType.NETWORK, reason);
            // Toast.makeText(getActivity(), "area subarea failed", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragLayout = inflater.inflate(R.layout.personal_details_fragment, null);
        ButterKnife.inject(this, fragLayout);
        super.onCreateView(inflater, container, savedInstanceState);
        init();
        return fragLayout;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).drawerOpen = false;
        if (getActivity() != null)
            preferencesManager = SharedPreferencesManager.getSharedPreferences(getActivity());
        //databaseInstance = DBManager.getInstance(getActivity());
        editor = preferencesManager.edit();
        prefillData();
        if (cart.getDelivery_type().toLowerCase().equalsIgnoreCase("deliver")) {
            initialiseSubareasSpinner();
        }
        MainActivity.writeToSD();

    }

    @Override
    public int getScreenIndex() {
        return TopAndBottomBarFragment.ORDER_TYPE;
    }


    private void prefillData() {

        String mobile = preferencesManager.getString(Constants.PREFS_LAST_USED_MOBILE, null);
        String name = preferencesManager.getString(Constants.PREFS_LAST_USED_NAME, null);
        String email = preferencesManager.getString(Constants.PREFS_LAST_USED_EMAIL, null);
        if (mobile != null) {
            edt_mobile_no.setText(mobile);
            if (validateMobile(edt_mobile_no, false)) {
                createUserFromMobile(mobile);
            }
        }
        if (name != null) {
            edt_name.setText(name);
            validateName(edt_name, false);
        }
        if (email != null) {
            edtEmail.setText(email);
            validateEmail(edtEmail, false);
        }

    }


    @Override
    public void onProceed() {
        if (validateMobile(edt_mobile_no, true) && validateName(edt_name, true) && validateAddressFields() && validateEmail(edtEmail, true)) {
            if (addressSpinner.getVisibility() == View.VISIBLE) {
                if (addressSpinner.getSelectedItemPosition() == 0) {
                    Toolbox.showToastLong(getActivity(), "Select address first, it is not selected from list");
                    return;
                }

            }
            if (areasSpinner.getVisibility() == View.VISIBLE) {
                {
                    if (areasSpinner.getSelectedItemPosition() == 0) {
                        Toolbox.showToastLong(getActivity(), "Select area first, it is not selected from list");
                        return;
                    }
                }
            }
            if (subareasSpinner.getVisibility() == View.VISIBLE) {
                {
                    if (subareasSpinner.getSelectedItemPosition() == 0) {
                        Toolbox.showToastLong(getActivity(), "Select locality first, it is not selected from list");
                        return;
                    }
                }
            }
            if (selectedAddress == null) {
                if (cart.getDelivery_type().equalsIgnoreCase("deliver")) {
                    Toolbox.showToastLong(getActivity(), "Select address first, it is not selected");
                    return;
                } else if (cart.getDelivery_type().contains("Take")) {
                    selectedAddress = null;

                }
            }

//            if (edtAddressTag.getVisibility() == View.VISIBLE) {
//                selectedAddress.setTag(edtAddressTag.getText().toString());
//            }

            if (cart.getDelivery_type().contains("Take")) {
                selectedAddress = null;

            }


            if (selectedAddress != null) {
                selectedAddress.setCity_id(preferencesManager.getLong(Constants.PREFS_SELECTED_CITY_ID, 1));
                selectedAddress.setState_id(preferencesManager.getLong(Constants.PREFS_SELECTED_STATE_ID, 1));
            }
            selectedUser.setAddress(selectedAddress);
            Cart.getInstance().setUser(selectedUser);

//            Toolbox.writeToLog("In here 1" + selectedUser);
            buildAlertDialog();

        } else {
            showError(ErrorType.ANNOUNCEMENT, "Please enter correct details");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleAnalyticsManager.sendScreenView(getActivity(), Constants.GA_SCREEN_PERSONAL_DETAILS);
    }

    @Override
    public String getProceedButtonText() {
        return "Next";
    }

    private void buildAlertDialog() {


        if (dbMan.isOutletOpenAt(outlet.getId(), Calendar.getInstance())) {

            Cart.getInstance().setCouponCode(null);
            Cart.getInstance().getOrders().get(outlet.getId()).setCoupon_code(null);
            SyncManager.checkOrder(getActivity(), checkOrder);
        } else {
            Calendar cal = Calendar.getInstance();
            List<Outlet_timing> timings = dbMan.getOutletTimings(outlet.getId(), cal);
            if (timings != null && timings.size() > 0) {
                Outlet_timing today = null;
                for (Outlet_timing ot : timings) {
                    if (ot.getDay_of_week().equalsIgnoreCase(Integer.toString(cal.get(Calendar.DAY_OF_WEEK)))) {
                        today = ot;
                        break;
                    }
                }
                if (today != null)
                    CustomDialog.createCustomDialog(getActivity(), "Sorry, we’re closed now.We’re Open \n" + today.getOpen_time() + "AM to" + today.getClose_time() + "PM", "OK", null, null, true, null).show();
                else
                    CustomDialog.createCustomDialog(getActivity(), "Sorry, we’re closed now.", "OK", null, null, true, null).show();
            } else {
                CustomDialog.createCustomDialog(getActivity(), "Sorry, we’re closed now.", "OK", null, null, true, null).show();
            }
        }

//        dialogBuilder.show();
    }


    private void init() {
        ((MainActivity) getActivity()).drawerOpen = false;
        String outletName = null;
        cart = Cart.getInstance();
        Cart.getInstance().setRespCheckOrder(new RespCheckOrder());
        cart.setCouponCode("");
        if (Cart.getInstance().getDelivery_type().equalsIgnoreCase("deliver"))
            txt_address_info.setText("Delivery Address");
        else
            txt_address_info.setText("Pickup Address");
        pattern = Pattern.compile(Constants.EMAIL_PATTERN);
        dbMan = DBManager.getInstance(getActivity());

        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        ImageView img = (ImageView) ((ActionBarActivity) getActivity()).getSupportActionBar().getCustomView().findViewById(R.id.imgBack);
        img.setVisibility(View.VISIBLE);
        if (Cart.getInstance().getFirstOrder() != null && Cart.getInstance().getFirstOrder().getOutlet_id() != null) {
            outlet = dbMan.getOutletById(Cart.getInstance().getFirstOrder().getOutlet_id());
        }

        if (outlet == null || outlet.getName() == null) {
            outletName = "Select Details";
        } else {
            outletName = outlet.getName();
        }
        Toolbox.changeActionBarTitle(getActivity(), "Order Details", outletName);
        edt_mobile_no.addTextChangedListener(watcher);

        subareas = (ArrayList<Subarea>) dbMan.getAllSubsForOutlet(outlet.getId());
        if (subareas == null) {
            SyncManager.getAreaAndSubareaForOutlet(getActivity(), outlet.getId(), areaSubareaListener);
        }
//        else
//            initialiseSubareasSpinner();//AreasSpinner();

//        prefillData();
        initialiseSubareasSpinner();
        if (cart.getDelivery_type().contains("Take")) {
            deactivateSpinnerAndFieldsForTakeaway();
        }
        edt_mobile_no.addEditCancelListener(mobileListener);
        edt_name.addEditCancelListener(nameListener);
        edtEmail.addEditCancelListener(emailListener);
        edt_mobile_no.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
                }
            }
        });

        edt_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
                }
            }
        });
        edtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
                }
            }
        });


    }

    public void deactivateSpinnerAndFieldsForTakeaway() {
        addressSpinner.setVisibility(View.GONE);
        //addressFields.setVisibility(View.GONE);
        edtAddressLineOne.setVisibility(View.GONE);
        edtAddressLineTwo.setVisibility(View.GONE);
        /////////////////////////////////////////////////////////
        // edtAddressLineThree.setVisibility(View.GONE);
        edt_address_zipcode.setVisibility(View.GONE);
        //edtAddressTag.setVisibility(View.GONE);
        areasSpinner.setVisibility(View.GONE);
        subareasSpinner.setVisibility(View.GONE);
        outAddr.setVisibility(View.VISIBLE);
        outAddr.setText(outlet.getAddress_string());
    }

    private void createUserFromMobile(String mobile) {
        //Toolbox.writeToLog("Method", "createUserFromMobile");
        selectedUser = dbMan.getUserByPrimaryMobile(mobile);
        if (cart.getDelivery_type().equalsIgnoreCase("deliver")) {
            if (selectedUser != null) {
                selectedUser.setPrimary_phone(mobile);
                edt_name.setText(selectedUser.getName());
                if (selectedUser.getPrimary_email() != null) {

                    edtEmail.setText("" + selectedUser.getPrimary_email());
                    if (validateEmail(edtEmail, true)) {
                        selectedUser.setPrimary_email(edtEmail.getText().toString());
                    }
                }
                Toolbox.showToastShort(getActivity(), "Welcome " + selectedUser.getName());
                addresses = (ArrayList<Address>) (dbMan.getAddressesForUser(selectedUser.getId(), outlet.getId()));
                Log.d("Addresses for user id " + selectedUser.getId(), "" + addresses);
                if (addresses != null && addresses.size() > 0) {
                    activateAddressSpinner();
                } else {
                    addressSpinner.setVisibility(View.GONE);
                    // addressFields.setVisibility(View.VISIBLE);
                    edtAddressLineOne.setVisibility(View.VISIBLE);
                    edtAddressLineTwo.setVisibility(View.VISIBLE);
                    edt_address_zipcode.setVisibility(View.VISIBLE);
                    ///////////////////////////////////////////////////////////////
                    // edtAddressLineThree.setVisibility(View.VISIBLE);
                    //edtAddressTag.setVisibility(View.VISIBLE);
                    areasSpinner.setVisibility(View.GONE);
                    initialiseSubareasSpinner();
                    if (edtAddressLineOne.getText().toString().equals("") || edtAddressLineOne.getText() == null)
                        showError(ErrorType.ANNOUNCEMENT, "Please enter a new address..");

                }
            } else {
                Toolbox.showToastShort(getActivity(), "Hi, you seem to be new here..");
                selectedUser = new User();
                addressSpinner.setVisibility(View.GONE);
                //addressFields.setVisibility(View.VISIBLE);
                edtAddressLineOne.setVisibility(View.VISIBLE);
                edtAddressLineTwo.setVisibility(View.VISIBLE);
                ///////////////////////////////////////////////////////////////
                // edtAddressLineThree.setVisibility(View.VISIBLE);
                //edtAddressTag.setVisibility(View.VISIBLE);
                edt_address_zipcode.setVisibility(View.VISIBLE);
                edt_name.getText().clear();
                edtEmail.getText().clear();
                areasSpinner.setVisibility(View.GONE);
                initialiseSubareasSpinner();

            }
//            initialiseSubareasSpinner();
        } else if (cart.getDelivery_type().contains("Take")) {
            if (selectedUser != null) {
                edt_name.setText(selectedUser.getName());
            } else {
                selectedUser = new User();
            }
            deactivateSpinnerAndFieldsForTakeaway();

        }
        selectedUser.setPrimary_phone(mobile);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideError();
            }
        }, 200);
    }

    FontEditText.EditCancelListener emailListener = new FontEditText.EditCancelListener() {
        @Override
        public void onCancelEvent(FontEditText editText, int cancelCode) {
            if (validateEmail(editText, true)) {
                if (selectedUser != null) {
                    selectedUser.setPrimary_email(editText.getText().toString());
                }
            }
            editor.putString(Constants.PREFS_LAST_USED_EMAIL, editText.getText().toString());
            editor.commit();
        }
    };

    FontEditText.EditCancelListener mobileListener = new FontEditText.EditCancelListener() {
        @Override
        public void onCancelEvent(FontEditText customPonchoEditText, int cancelCode) {
            validateMobile(edt_mobile_no, true);
            editor.putString(Constants.PREFS_LAST_USED_MOBILE, customPonchoEditText.getText().toString());
            editor.commit();
        }
    };
    FontEditText.EditCancelListener nameListener = new FontEditText.EditCancelListener() {
        @Override
        public void onCancelEvent(FontEditText customPonchoEditText, int cancelCode) {
            validateName(customPonchoEditText, true);
            editor.putString(Constants.PREFS_LAST_USED_NAME, customPonchoEditText.getText().toString());
            editor.commit();
        }
    };

    private void activateAddressSpinner() {
        addressSpinner.setVisibility(View.VISIBLE);
        //initialiseSubareasSpinner();
        edtAddressLineOne.setVisibility(View.GONE);
        edtAddressLineTwo.setVisibility(View.GONE);
        edt_address_zipcode.setVisibility(View.GONE);
        ////////////////////////////////////////////////////
        // edtAddressLineThree.setVisibility(View.GONE);
        //edtAddressTag.setVisibility(View.GONE);
        Address address = new Address();
        address.setFlat_no("Select Address");
        address.setStreet(null);
        address.setLandmark(null);
        addresses.add(0, address);
        address = new Address();
        address.setFlat_no("Add new address");
        address.setStreet(null);
        address.setLandmark(null);
        addresses.add(address);
        if (dataAdapter == null) {
            dataAdapter = new ArrayAdapter<Address>(getActivity(),
                    R.layout.address_spnr_dropdown, addresses);
        } else {
            dataAdapter.clear();
            for (Address a : addresses) {
                dataAdapter.add(a);
            }

        }
        dataAdapter.setDropDownViewResource(R.layout.address_spnr_dropdown);
        if (addressSpinner.getAdapter() == null)
            addressSpinner.setAdapter(dataAdapter);
        else dataAdapter.notifyDataSetChanged();

        addressSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    return;
                } else if (i > 0 && i < dataAdapter.getCount() - 1) {
                    if (edtAddressLineOne.getVisibility() == View.VISIBLE && edtAddressLineTwo.getVisibility() == View.VISIBLE && edt_address_zipcode.getVisibility() == View.VISIBLE) {//// && edtAddressLineThree.getVisibility() == View.VISIBLE) {
                        //addressFields.setVisibility(View.GONE);
                        //edtAddressTag.setVisibility(View.GONE);
                        edtAddressLineOne.setVisibility(View.GONE);
                        edtAddressLineTwo.setVisibility(View.GONE);
                        edt_address_zipcode.setVisibility(View.GONE);
                        ///////////////////////////////////////////////
                        // edtAddressLineThree.setVisibility(View.GONE);
                    }
                    selectedAddress = new Address();
                    selectedAddress = dataAdapter.getItem(i);
                    selectedAddress.setId(dataAdapter.getItem(i).getId());
                    selectedAddress.setFlat_no(dataAdapter.getItem(i).getFlat_no());
                    selectedAddress.setStreet(dataAdapter.getItem(i).getStreet());
                    selectedAddress.setLandmark(dataAdapter.getItem(i).getLandmark());
                    selectedAddress.setTag(dataAdapter.getItem(i).getTag());
                    selectedAddress.setPincode(dataAdapter.getItem(i).getPincode());
                    areasSpinner.setVisibility(View.GONE);
                  //  validateFields = false;
                    //////////////////////////////////////////////
                    // edtAddressTag.setVisibility(View.VISIBLE);
                    edtAddressLineOne.setVisibility(View.VISIBLE);
                    edtAddressLineTwo.setVisibility(View.VISIBLE);
                    edt_address_zipcode.setVisibility(View.VISIBLE);
                    ///////////////////////////////////////////////////////////////////
                    // edtAddressLineThree.setVisibility(View.VISIBLE);
                    // edtAddressTag.setText(selectedAddress.getTag());
                    edtAddressLineOne.setText(selectedAddress.getFlat_no());
                    edtAddressLineTwo.setText(selectedAddress.getStreet());
                    edt_address_zipcode.setText(selectedAddress.getPincode());
                    //////////////////////////////////////////////////
                    // edtAddressLineThree.setText(selectedAddress.getLandmark());

                    if (dataAdapter.getItem(i).getSubarea_id() != null) {
                        selectedAddress.setSubarea_id(dataAdapter.getItem(i).getSubarea_id());
                        selectedSubarea = dbMan.getSubareaById(dataAdapter.getItem(i).getSubarea_id());
                        selectedAddress.setArea_id(selectedSubarea.getArea_id());
                        subareasSpinner.setVisibility(View.GONE);
                    } else {
                        initialiseSubareasSpinner();
                        showError(ErrorType.ANNOUNCEMENT, "Please select locality from below");

                    }

                    //            Toolbox.writeToLog("" + selectedAddress);

                } else if (i == dataAdapter.getCount() - 1) {
                    //addressFields.setVisibility(View.VISIBLE);
                    //edtAddressTag.setVisibility(View.VISIBLE);
                    edtAddressLineOne.setVisibility(View.VISIBLE);
                    edtAddressLineTwo.setVisibility(View.VISIBLE);
                    edt_address_zipcode.setVisibility(View.VISIBLE);
                    ///////////////////////////////////////////////////
                    // edtAddressLineThree.setVisibility(View.VISIBLE);
                    //edtAddressTag.getText().clear();//;setText(selectedAddress.getTag());
                    edtAddressLineOne.getText().clear();//.setText(selectedAddress.getFlat_no());
                    edtAddressLineTwo.getText().clear();//.setText(selectedAddress.getStreet());
                    edt_address_zipcode.getText().clear();
                    /////////////////////////////////////////////////////
                    // edtAddressLineThree.getText().clear();//.setText(selectedAddress.getLandmark());
                    areasSpinner.setVisibility(View.GONE);
                    initialiseSubareasSpinner();
                    selectedAddress = new Address();
                    validateFields = true;


                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private boolean validateName(EditText view, boolean setError) {
        String name = view.getText().toString();
        if (name == null | name.length() == 0) {
            //view.setError("Please enter valid name");
            if (setError)
                showError(ErrorType.ANNOUNCEMENT, "Please enter valid name");
            goodToGo = false;
            return false;
        } else {
            if (selectedUser != null)
                selectedUser.setName(name);
            //view.setError(null);
            hideError();
        }
        goodToGo = true;
        return true;
    }

    private boolean validateEmail(EditText edit, boolean showError) {
        String email = edit.getText().toString();
        matcher = pattern.matcher(email);
        if (email == null || email.length() == 0 || !matcher.matches()) {
            //edit.setError("Please enter valid email");
            if (showError)
                showError(ErrorType.ANNOUNCEMENT, "Please enter valid email");
            goodToGo = false;
            return false;
        } else {
            if (selectedUser != null && matcher.matches())
                selectedUser.setPrimary_email(email);
//            edit.setError(null);
            hideError();
        }
        goodToGo = true;
        return matcher.matches();


    }

    private boolean validateMobile(EditText view, boolean showError) {
        String number = view.getText().toString();
        if (number == null || number.length() <= 9) {
            // view.setError("Please enter valid mobile number");

            if (showError)
                showError(ErrorType.NETWORK, "Please enter valid mobile number");
            goodToGo = false;
            return false;
        } else {
            if (selectedUser == null)
                selectedUser = new User();
            selectedUser.setPrimary_phone(number);
            //view.setError(null);
            hideError();
        }
        goodToGo = true;
        return true;
    }

    private boolean validateAddressFields() {
        if (edtAddressLineOne.getVisibility() == View.GONE || cart.getDelivery_type().toLowerCase().contains("take") || !validateFields) {
            if (!validateFields) {
                selectedAddress.setFlat_no(edtAddressLineOne.getText().toString());
                selectedAddress.setStreet(edtAddressLineTwo.getText().toString());
                selectedAddress.setPincode(edt_address_zipcode.getText().toString());
                ///////////////////////////////////////////////////////////////////////////////
                // selectedAddress.setLandmark(edtAddressLineThree.getText().toString());
            }

            return true;
        }

        //selectedAddress = new Address();
//        selectedAddress.setId(null);
        //  Toolbox.showToastShort(getActivity(), "Validating address");
        String line1 = null, line2 = null, zip = null;
        ////////////////////////////////////////////////////////////
        // String line3 = null;
        String finalAddress = "";
        line1 = edtAddressLineOne.getText().toString();
        line2 = edtAddressLineTwo.getText().toString();
        zip = edt_address_zipcode.getText().toString();

        //////////////////////////////////////////////////////////
        // line3 = edtAddressLineThree.getText().toString();
        if (validateAddressLine(line1)) {
            edtAddressLineOne.setError(null);
            hideError();
            finalAddress += line1;
            selectedAddress.setFlat_no(line1);
        } else {
            //edtAddressLineOne.setError("Cannot be empty");
            showError(ErrorType.ANNOUNCEMENT, "Address line cannot be empty");
            goodToGo = false;
            return false;
        }
        if (validateAddressLine(line2)) {
            edtAddressLineTwo.setError(null);
            hideError();
            finalAddress += line2;
            selectedAddress.setStreet(line2);
        } else {
            //edtAddressLineTwo.setError("Cannot be empty");
            showError(ErrorType.ANNOUNCEMENT, "Address line cannot be empty");
            goodToGo = false;
            return false;
        }
        if (validateAddressLine(zip)) {
            edt_address_zipcode.setError(null);
            hideError();
            finalAddress += zip;
            selectedAddress.setPincode(zip);
        } else {
            //edtAddressLineTwo.setError("Cannot be empty");
            showError(ErrorType.ANNOUNCEMENT, "ZipCode cannot be empty");
            goodToGo = false;
            return false;
        }
////////////////////////////////////////////////////////////////////////////
//        if (validateAddressLine(line3)) {
//            //edtAddressLineThree.setError(null);
//            hideError();
//            goodToGo = false;
//            finalAddress += line3;
//            selectedAddress.setLandmark(line3);
//        } else {
////            edtAddressLineTwo.setError("Cannot be empty");
//            //showError(ErrorType.ANNOUNCEMENT, "Address line cannot be empty");
//            goodToGo = true;
//            return true;
//        }
        //    Toolbox.writeToLog("" + finalAddress);
        goodToGo = true;
        return true;
    }

    private boolean validateAddressLine(String line) {
        if (line != null) {
            if (line.isEmpty()) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }


    SyncListener checkOrder = new SyncListener() {
        @Override
        public void onSyncStart() {

            if (!pDialog.isShowing()) {
                pDialog.setMessage("Verifying order and details....");
                pDialog.show();
            } else {
                pDialog.dismiss();
            }

            hideError();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {

            if (getActivity() == null) return;
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            hideError();

            Toolbox.changeScreen(getActivity(), Constants.SCREEN_CONFIRM_ORDER, true);

        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            showError(ErrorType.NETWORK, reason);
        }
    };


    public void initialiseAreasSpinner() {
        areasSpinner.setVisibility(View.GONE);

        if (areasSpinner.getAdapter() == null) {
            Area area = new Area();
            area.setArea_name("Select Area");
            areas.add(0, area);
            areaAdapter = new ArrayAdapter<Area>(getActivity(), R.layout.address_spnr_dropdown, areas);
            areaAdapter.setDropDownViewResource(R.layout.address_spnr_dropdown);
            areasSpinner.setAdapter(areaAdapter);
        } else {
            areaAdapter.notifyDataSetChanged();
        }
        areasSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    selectedArea = areaAdapter.getItem(i);
                    //subareas = (ArrayList) dbMan.getSubareasForOutlet(selectedArea.getId());
                    selectedAddress.setArea_id(selectedArea.getId());
                    //selectedAddress.setLandmark(selectedAddress.getLandmark()+","+selectedArea.getArea_name());
                    initialiseSubareasSpinner();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void initialiseSubareasSpinner() {
        subareasSpinner.setVisibility(View.VISIBLE);
//        if(subareaAdapter!=null){
//            subareaAdapter.clear();
//        }
        subareas = (ArrayList<Subarea>) dbMan.getAllSubsForOutlet(outlet.getId());
        Toolbox.writeToLog("subareas " + subareas);
        if (subareas == null) {
            SyncManager.getAreaAndSubareaForOutlet(getActivity(), outlet.getId(), areaSubareaListener);
        } else {
            Subarea area = new Subarea();
            area.setSubarea_name("Select locality..");
            subareas.add(0, area);
            if (subareasSpinner.getAdapter() == null) {
                subareaAdapter = new ArrayAdapter<Subarea>(getActivity(), R.layout.address_spnr_dropdown, subareas);
                subareaAdapter.setDropDownViewResource(R.layout.address_spnr_dropdown);
                subareasSpinner.setAdapter(subareaAdapter);
            } else {
                subareaAdapter.clear();
                for (Subarea s : subareas) {
                    subareaAdapter.add(s);
                }
                subareaAdapter.notifyDataSetChanged();
            }
            Toolbox.writeToLog("subareas " + subareas);
            subareasSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i > 0) {
                        selectedSubarea = subareaAdapter.getItem(i);
                        selectedAddress.setSubarea_id(selectedSubarea.getId());
                        selectedAddress.setArea_id(selectedSubarea.getArea_id());
                        // selectedAddress.setLandmark(selectedAddress.getLandmark()+","+selectedSubarea.getSubarea_name());
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }

//    private TextWatcher textWatcher = new TextWatcher() {
//        @Override
//        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
//
//        }
//
//        @Override
//        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
//
//        }
//
//        @Override
//        public void afterTextChanged(final Editable editable) {
//
//            Toolbox.writeToLog("subarea search text changed == " + editable.toString());
//            subareaSearchKeyword = editable.toString();
//            lastKeystrokeTimestamp = System.currentTimeMillis();
//
//            final long currTime = System.currentTimeMillis();
//
//            if (editable.toString().length() == 0) {
//                ((SubareasAutoCompleteAdapter) lstSubareas.getAdapter()).clear();
//            } else {
//                ((SubareasAutoCompleteAdapter) lstSubareas.getAdapter()).clear();
//            }
//
//                Runnable r = new Runnable() {
//                    @Override
//                    public void run() {
//                        if (currTime < lastKeystrokeTimestamp) {
//                            Toolbox.writeToLog("subarea search currTime less than last keystroke ==" + subareaSearchKeyword);
//                            return;
//                        }
//
//
//                        if (System.currentTimeMillis() - lastKeystrokeTimestamp > 200) {
//                            if (subareaSearchKeyword != null && subareaSearchKeyword != null && subareaSearchKeyword.length() > 2) {
//                                Toolbox.writeToLog("subarea search making request== " + dbMan.searchSubareasForOutlet(outlet.getId(),subareaSearchKeyword));
//
//                                try {
//                                    ((SubareasAutoCompleteAdapter) lstSubareas.getAdapter()).setSubareasList(dbMan.searchSubareasForOutlet(outlet.getId(),subareaSearchKeyword));
//                                    lstSubareas.setVisibility(View.VISIBLE);
//                                    // SyncManager.searchSubareas(getActivity(), URLEncoder.encode(subareaSearchKeyword, "UTF-8"), syncListenerSearchSubareas);
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        } else {
//                            Toolbox.writeToLog("subarea search System.currTimeMillis() - last keystroke less than 1s ==" + subareaSearchKeyword);
//                            h.postDelayed(this,200);
//                        }
//
//                    }
//                };
//                h.postDelayed(r,200);
//
//            }
//
//
//    };

    SyncListener findMe = new SyncListener() {
        @Override
        public void onSyncStart() {
            if (!pDialog.isShowing()) {
                pDialog.setMessage("Finding your details....");
                pDialog.show();
            } else {
                pDialog.dismiss();
            }

            hideError();
        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            if (getActivity() == null) return;
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            hideError();

            FindMeData fd = ((ResponseFindMe) responseObject).getData();

            if (fd != null) {
                String mobile = String.valueOf(fd.getPrimary_phone());
                createUserFromMobile(mobile);
            }

        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (reason.toLowerCase().contains("user not")) {
                showError(ErrorType.ANNOUNCEMENT, "You seem to be new here..");
                edtEmail.getText().clear();
                edt_name.getText().clear();
//                addressSpinner.setVisibility(View.GONE);
//                edtAddressLineOne.setVisibility(View.VISIBLE);
//                edtAddressLineTwo.setVisibility(View.VISIBLE);
//                edtAddressLineThree.setVisibility(View.VISIBLE);
//                edtAddressTag.setVisibility(View.VISIBLE);
                h.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hideError();
                    }
                }, 200);
            } else
                showError(ErrorType.NETWORK, reason);
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        editor.putString(Constants.PREFS_LAST_USED_MOBILE, edt_mobile_no.getText().toString());
        editor.putString(Constants.PREFS_LAST_USED_NAME, edt_name.getText().toString());
        editor.putString(Constants.PREFS_LAST_USED_EMAIL, edtEmail.getText().toString());
        editor.commit();
    }

}

