package to.done.firstclasspizza.productcustomization;

import android.app.Activity;
import android.database.DataSetObserver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Optional;
import to.done.firstclasspizza.MainActivity;
import to.done.firstclasspizza.R;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import to.done.lib.Constants;
import to.done.lib.database.DBManager;
import to.done.lib.entity.outletmenu.Category;
import to.done.lib.entity.outletmenu.Product;
import to.done.lib.images.util.ImageFetcher;
import to.done.lib.utils.Toolbox;

/**
 * Created by shadab mirza on 4/21/14.
 */
public class ProductCustomizationExpListAdapter extends BaseExpandableListAdapter {

    private static final String TAG = "ProductCustomizationExpListAdapter";
    private Activity context;
    private DBManager dbMan;
    private LayoutInflater inflater;
    private List<Category> categoryList;
    private long outletProductId;
    ImageFetcher fetcher;
    float widthDp, heightDp;


    public ProductCustomizationExpListAdapter(Activity context, long outletProductId) {
        this.context = context;
        this.outletProductId = outletProductId;
        fetcher = new ImageFetcher(context, 150, 100);

        dbMan = DBManager.getInstance(context.getApplication());

        List<Long> tempList = new LinkedList<Long>();
        tempList.add(outletProductId);
        categoryList = dbMan.getCustomCatsWithProdsByMappingId(tempList);
        //Toolbox.writeToLog("ExtraFeatureCategories "+categoryList.toString());
        tempList.clear();
        if (categoryList != null && categoryList.size() > 0) {
            for (Category c : categoryList) {
                for (Product p : c.getProductList()) {

                    if (p.getDefault_selected() != null && p.getDefault_selected() == 1) {
                        tempList.add(p.getOutlet_product_id());
                        p.setSelected(true);
                    }
                }

            }

            categoryList.addAll(dbMan.getCustomCatsWithProdsByMappingId(tempList));

            //Toolbox.writeToLog("ExtraFeatureCategories "+categoryList.toString());
            for (Category c : categoryList) {
                for (Product p : c.getProductList()) {

                    if (p.getDefault_selected() != null && p.getDefault_selected() == 1) {
                        p.setSelected(true);
                    }
                }

            }
        }
        inflater = LayoutInflater.from(context);

    }

    @Override
    public int getGroupCount() {
        Log.e(TAG, "getGroupCount()-->" + categoryList.size());
        return categoryList.size();
    }

    @Override
    public int getChildrenCount(int i) {
        Log.e(TAG, "getChildrenCount()-->" + i + "-->" + categoryList.get(i).getProductList().size());
        return categoryList.get(i).getProductList().size();
    }

    @Override
    public Category getGroup(int i) {
        return categoryList.get(i);
    }

    @Override
    public Product getChild(int i, int i2) {
        return categoryList.get(i).getProductList().get(i2);
    }

    @Override
    public long getGroupId(int i) {
        return categoryList.get(i).getId().longValue();
    }

    @Override
    public long getChildId(int i, int i2) {
        return categoryList.get(i).getProductList().get(i2).getId().longValue();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View convertView, ViewGroup viewGroup) {


        ViewHolderGroup viewHolderGroup;

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.product_customization_ex_list_group, null);
            viewHolderGroup = new ViewHolderGroup(convertView);

            convertView.setTag(viewHolderGroup);
        } else {
            viewHolderGroup = (ViewHolderGroup) convertView.getTag();
        }
        viewHolderGroup.txtProdCustCategory.setText(getGroup(i).getName());
        // Toolbox.writeToLog("ExtraFeatureCategory "+getGroup(i).getName()+" id "+getGroup(i).getId());
        return convertView;
    }

    @Override
    public View getChildView(final int i, final int i2, boolean b, View convertView, ViewGroup viewGroup) {
        Product prod = getChild(i, i2);
        final ViewHolderChild viewHolderChild;

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.product_customization_ex_list_child_no_images, null);
            viewHolderChild = new ViewHolderChild(convertView);

            convertView.setTag(viewHolderChild);
        } else {
            viewHolderChild = (ViewHolderChild) convertView.getTag();
        }

        viewHolderChild.txtProdCustName.setText(prod.getName());
        viewHolderChild.txtProdCustPrice.setText(Constants.RUPEE_SYMBOL + Toolbox.formatDecimal(prod.getPrice()));
//        if(prod.getPhoto_url()!=null && viewHolderChild.img.getDrawable()==null) {
//            fetcher.loadImage(prod.getPhoto_url(), viewHolderChild.img);
//        }
//        }else
//        {
//            viewHolderChild.img.setImageResource(R.drawable.logo_in_head);
//        }

        if (getGroup(i).getSelection_min() == 1 && getGroup(i).getSelection_max() == 1) {
            int drawableId = Toolbox.getAttributeResourceId(context, R.attr.themeRadioSelector, MainActivity.currentThemeId);
            viewHolderChild.imgCustRadio.setImageResource(drawableId);
        } else {
            int drawableId = Toolbox.getAttributeResourceId(context, R.attr.themeCheckboxSelector, MainActivity.currentThemeId);

            viewHolderChild.imgCustRadio.setImageResource(drawableId);//R.drawable.red_bright_checkbox_selector);
        }


        if (prod.isSelected()) {
            //   Toolbox.writeToLog("selected childview "+prod.getName());

            viewHolderChild.imgCustRadio.setSelected(true);
        } else {
            viewHolderChild.imgCustRadio.setSelected(false);
        }

        viewHolderChild.prodCustExListChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCustomizationClicked(view, i, i2);
            }
        });

        //  Toolbox.writeToLog("ExtraFeatureCategory "+prod.getName()+" id "+prod.getId());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i2) {
        return false;
    }

    private void onCustomizationClicked(View view, int i, int i2) {

        View imgRadio = view.findViewById(R.id.img_cust_radio);

        Category cat = getGroup(i);
        Product prod = getChild(i, i2);

        boolean radio = false;

        if (cat.getSelection_min() == 1 && cat.getSelection_max() == 1) {
            radio = true;
        }


        if (prod.isSelected()) {

            if (!radio) {

                if (cat.getSelection_min() > 0) {

                    int currSelectionCount = getCurrentSelectionCount(cat);

                    if (currSelectionCount > cat.getSelection_min()) {
                        prod.setSelected(false);
                        imgRadio.setSelected(false);
                    }
                } else {
                    prod.setSelected(false);
                    imgRadio.setSelected(false);
                }
            }


        } else {

            if (!radio) {
                int currSelectionCount = getCurrentSelectionCount(cat);
                if (cat.getSelection_max() <= 0) {
                    prod.setSelected(true);
                    imgRadio.setSelected(true);
                } else if (currSelectionCount < cat.getSelection_max()) {
                    prod.setSelected(true);
                    imgRadio.setSelected(true);
                }
//                if (currSelectionCount < cat.getSelection_max()) {
//                    prod.setSelected(true);
//                    imgRadio.setSelected(true);
//                }
            } else {

                for (Product p : cat.getProductList()) {
                    if (p.isSelected()) {
                        p.setSelected(false);
                    }

                }

                prod.setSelected(true);
                imgRadio.setSelected(true);

            }
        }

        List<Long> selectedOPMIdList = new ArrayList<Long>();
        List<Long> selectedProdIdList = new ArrayList<Long>();

        for (Category c : categoryList) {
            for (Product p : c.getProductList()) {

                if (p.isSelected()) {
                    selectedOPMIdList.add(p.getOutlet_product_id());
                    selectedProdIdList.add(p.getId());
                }
            }
        }
        //   Toolbox.writeToLog("selected "+selectedProdIdList);
        // notifyDataSetChanged();
        getChildExtraFeaturesAndCategories(selectedOPMIdList, selectedProdIdList);
    }

    private void getChildExtraFeaturesAndCategories(List<Long> selectedOPMIdList, List<Long> selectedProdIdList) {
        List<Long> tempList = new ArrayList<Long>();
        tempList.add(outletProductId);

        List<Category> updatedList = dbMan.getCustomCatsWithProdsByMappingId(tempList);
//        categoryList.clear();

        updatedList.addAll(dbMan.getCustomCatsWithProdsByMappingId(selectedOPMIdList));

        for (Category c : updatedList) {


            boolean prevSelected = false;

            for (Product p : c.getProductList()) {

//                    for(long id:selectedProdIdList){

                if (selectedProdIdList.contains(p.getId().longValue())) {
                    p.setSelected(true);
                    prevSelected = true;
                } else {
                    p.setSelected(false);
                }

//                    }
            }

            if (!prevSelected) {
                for (Product p : c.getProductList()) {
                    if (p.getDefault_selected() != null && p.getDefault_selected() == 1) {
                        p.setSelected(true);
                        //            Toolbox.writeToLog("default selected = "+p.getName());
                    }
                }
            }


        }
        categoryList.clear();
        categoryList.addAll(updatedList);
        if (updatedList != null && updatedList.size() > 0)
            notifyDataSetChanged();


    }

    private int getCurrentSelectionCount(Category category) {

        int currSelectionCount = 0;

        for (Product p : category.getProductList()) {
            if (p.isSelected()) {
                currSelectionCount++;
            }
        }

        return currSelectionCount;
    }

    static class ViewHolderGroup {

        @InjectView(R.id.txt_prod_cust_category)
        TextView txtProdCustCategory;


        public ViewHolderGroup(View view) {
            ButterKnife.inject(this, view);
        }


    }

    static class ViewHolderChild {

        @InjectView(R.id.prod_cust_ex_list_child)
        ViewGroup prodCustExListChild;


        @InjectView(R.id.txt_prod_cust_name)
        TextView txtProdCustName;

        @InjectView(R.id.txt_prod_cust_price)
        TextView txtProdCustPrice;
        @Optional
        @InjectView(R.id.txt_prod_cust_currency)
        TextView txtProdCustCurrency;

        @InjectView(R.id.img_cust_radio)
        ImageView imgCustRadio;

        //
        public ViewHolderChild(View view) {
            ButterKnife.inject(this, view);
        }
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        super.registerDataSetObserver(observer);
    }
}
