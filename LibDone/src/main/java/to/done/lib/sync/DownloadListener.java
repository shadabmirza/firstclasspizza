package to.done.lib.sync;

/**
 * Created by shadab mirza on 29/11/13.
 */
public interface DownloadListener {

    void onDownloadStart(String url, long requestTimestamp);
    void onDownloadSuccess(String url, String response, int httpStatusCode, long requestTimestamp);
    void onDownloadSuccess(String url, byte[] response, long requestTimestamp);
    void onDownloadFail(String url, Exception e, String reason, long requestTimestamp);

}
