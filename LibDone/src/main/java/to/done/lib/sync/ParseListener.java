package to.done.lib.sync;

/**
 * Created by shadab mirza on 29/11/13.
 */
public interface ParseListener {

    void onParseStart(String url);
    void onParseSuccess(String url);
    void onParseFail(String url);
}
