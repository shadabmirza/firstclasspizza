package to.done.lib.sync;

import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import to.done.lib.Constants;
import to.done.lib.cart.Cart;
import to.done.lib.database.DBConstants;
import to.done.lib.database.DBManager;
import to.done.lib.entity.cachingtables.CacheForData;
import to.done.lib.entity.closestsubareas.RespSolrSubareas;
import to.done.lib.entity.gcm.RequestGCMRegistration;
import to.done.lib.entity.gcm.ResponseGCMRegistration;
import to.done.lib.entity.nearestoutlets.OutletData;
import to.done.lib.entity.nearestoutlets.RequestNearestOutlets;
import to.done.lib.entity.nearestoutlets.RespNearestOutlets;
import to.done.lib.entity.order.Order;
import to.done.lib.entity.order.RequestCheckOrder;
import to.done.lib.entity.order.RequestSaveOrder;
import to.done.lib.entity.order.RespCheckOrder;
import to.done.lib.entity.order.RespSaveOrder;
import to.done.lib.entity.outletmenu.OutletMenuData;
import to.done.lib.entity.outletmenu.RequestCompId;
import to.done.lib.entity.outletmenu.RequestOutletMenu;
import to.done.lib.entity.outletmenu.RespOutletMenu;
import to.done.lib.entity.outletmenu.ResponseCompanyOutlet;
import to.done.lib.entity.outletsubareas.RequestOutletSubarea;
import to.done.lib.entity.outletsubareas.ResponseOutletSubarea;
import to.done.lib.entity.tracking.RequestTrackOrder;
import to.done.lib.entity.tracking.ResponseTrackOrder;
import to.done.lib.entity.tracking.TrackOrderData;
import to.done.lib.entity.user.Address;
import to.done.lib.entity.user.RequestFindMe;
import to.done.lib.entity.user.ResponseFindMe;
import to.done.lib.entity.user.User;
import to.done.lib.utils.SharedPreferencesManager;
import to.done.lib.utils.Toolbox;
import to.done.lib.webservice.HttpHelper;

import static to.done.lib.Constants.ERROR_MESSAGE_GENERIC;
import static to.done.lib.Constants.ERROR_MESSAGE_INTERNET_UNAVAILABLE;
import static to.done.lib.Constants.RESPONSE_CODE_SUCCESS;
import static to.done.lib.Constants.URL_CHECK_ORDER;
import static to.done.lib.Constants.URL_CLOSEST_SUBAREA;
import static to.done.lib.Constants.URL_FIND_ME;
import static to.done.lib.Constants.URL_GET_AREA_SUBAREA_FOR_OUTLET;
import static to.done.lib.Constants.URL_NEAREST_OUTLETS;
import static to.done.lib.Constants.URL_OUTLET_MENU;
import static to.done.lib.Constants.URL_REGISTER_DEVICE;
import static to.done.lib.Constants.URL_SAVE_ORDER;
import static to.done.lib.Constants.URL_SEARCH_SUBAREA;


/**
 * Created by shadab mirza on 29/11/13.
 */
public class SyncManager {


    //    private Context context;
//    private static final Gson gson = new Gson();
//    private static final Gson gsonWOExpose =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    private static final String SERVER_ERROR = "Server Error";
    private static final ObjectMapper objMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    private static String tag;
//
//    private SyncListener syncListener;
//
//    public Context getContext() {
//        return context;
//    }
//
//    public void setContext(Context context) {
//        this.context = context;
//    }
//
//    public SyncListener getSyncListener() {
//        return syncListener;
//    }
//
//    public void setSyncListener(SyncListener syncListener) {
//        this.syncListener = syncListener;
//    }
//


    public static void getClosestSubareas(final Activity context, double latitude, double longitude, final SyncListener syncListener) {
        sendSyncStartEvent(syncListener, context);


        ResponseHandler responseHandler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTimestamp) {
                try {
                    final RespSolrSubareas resp = objMapper.readValue(response, RespSolrSubareas.class);

                    if (resp.getResponseHeader().getStatus() != RESPONSE_CODE_SUCCESS) {
                        sendSyncFailureEvent(url, syncListener, context, resp.getError().getMsg(), requestTimestamp);
                        return;
                    }


                    sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                } catch (Exception e) {
                    e.printStackTrace();
                    sendSyncFailureEvent(url, syncListener, context, SERVER_ERROR, requestTimestamp);

                }
            }
        };

        AsyncJsonGet asyncJsonGet = new AsyncJsonGet(context, URL_CLOSEST_SUBAREA + latitude + "," + longitude, HttpHelper.EncodingType.PLAIN_TEXT, responseHandler);
        asyncJsonGet.setDownloadListener(buildDownloadListener(syncListener, context));
        if (Toolbox.isInternetAvailable(context))
            asyncJsonGet.execute();
        else {
            sendSyncFailureEvent(URL_CLOSEST_SUBAREA, syncListener, context, ERROR_MESSAGE_INTERNET_UNAVAILABLE, 0);
        }
    }


    public static void searchSubareas(final Activity context, String searchKeyword, final SyncListener syncListener) {
        sendSyncStartEvent(syncListener, context);

        ResponseHandler responseHandler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTimestamp) {
                try {
                    final RespSolrSubareas resp = objMapper.readValue(response, RespSolrSubareas.class);

                    if (resp.getResponseHeader().getStatus() != RESPONSE_CODE_SUCCESS) {
                        sendSyncFailureEvent(url, syncListener, context, resp.getError().getMsg(), requestTimestamp);
                        return;
                    }


                    sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                } catch (Exception e) {
                    e.printStackTrace();
                    sendSyncFailureEvent(url, syncListener, context, SERVER_ERROR, requestTimestamp);

                }
            }
        };

        AsyncJsonGet asyncJsonGet = new AsyncJsonGet(context, URL_SEARCH_SUBAREA + "*" + searchKeyword + "*", HttpHelper.EncodingType.PLAIN_TEXT, responseHandler);
        asyncJsonGet.setDownloadListener(buildDownloadListener(syncListener, context));
        asyncJsonGet.execute();
    }


    public static void getNearestOutlets(final Activity context, long subareaId, String serviceType, final SyncListener syncListener) {
        sendSyncStartEvent(syncListener, context);
        final DBManager dbMan = DBManager.getInstance(context.getApplication());
        ResponseHandler responseHandler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTimestamp) {
                try {


                    final RespNearestOutlets resp = objMapper.readValue(response, RespNearestOutlets.class);

                    if (resp != null && resp.getResponseCode() != RESPONSE_CODE_SUCCESS) {
                        sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                        return;
                    }


                    OutletData outletData = resp.getData();
                    if (outletData != null) {

                        dbMan.clearOutletData();

                        dbMan.insertOutletList(outletData.getOutlets());
                        dbMan.insertOutletAttributeList(outletData.getOutlet_attributes());
                        dbMan.insertOutletAttributesGroupList(outletData.getOutlet_attributes_group());
                        dbMan.insertOutletAttributesMappingList(outletData.getOutlet_attributes_mapping());
                        dbMan.insertOutletTimingList(outletData.getOutlet_timing());

                    }


                    sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                } catch (Exception e) {
                    e.printStackTrace();
                    sendSyncFailureEvent(url, syncListener, context, SERVER_ERROR, requestTimestamp);

                }
            }
        };

        RequestNearestOutlets req = new RequestNearestOutlets(subareaId, serviceType);

        try {
            AsyncJsonPost asyncJsonPost = new AsyncJsonPost(context, URL_NEAREST_OUTLETS, objMapper.writeValueAsString(req), HttpHelper.EncodingType.GZIP, responseHandler);
            asyncJsonPost.setDownloadListener(buildDownloadListener(syncListener, context));
            asyncJsonPost.execute();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }


    private static RequestOutletMenu getTimestampsWithObject(Long outletid, DBManager db) {
        RequestOutletMenu menuReq = new RequestOutletMenu(outletid);
        List<CacheForData> timeStamps = db.getTimeStamp(outletid);
        if (timeStamps != null && timeStamps.size() > 0) {
            for (CacheForData data : timeStamps) {
                if (data != null) {
                    if (data.getName().equalsIgnoreCase(DBConstants.CATEGORY_TIMESTAMP)) {
                        if (data.getTimestamp() != null)
                            menuReq.setCategories_last_updated_timestamp(data.getTimestamp());
                    }
                    if (data.getName().equalsIgnoreCase(DBConstants.OUTLET_PRODUCT_MAPPING_TIMESTAMP)) {
                        if (data.getTimestamp() != null)
                            menuReq.setOpm_last_updated_timestamp(data.getTimestamp());
                    }
                    if (data.getName().equalsIgnoreCase(DBConstants.OUTLET_TIMESTAMP)) {
                        if (data.getTimestamp() != null)
                            menuReq.setOutlet_last_updated_timestamp(data.getTimestamp());
                    }
                    if (data.getName().equalsIgnoreCase(DBConstants.PRODUCT_ATTRIBUTE_GROUP_TIMESTAMP)) {
                        if (data.getTimestamp() != null)
                            menuReq.setPag_last_updated_timestamp(data.getTimestamp());
                    }
                    if (data.getName().equalsIgnoreCase(DBConstants.PRODUCT_ATTRIBUTE_TIMESTAMP)) {
                        if (data.getTimestamp() != null)
                            menuReq.setPa_last_updated_timestamp(data.getTimestamp());
                    }
                    if (data.getName().equalsIgnoreCase(DBConstants.PRODUCT_TIMESTAMP)) {
                        if (data.getTimestamp() != null)
                            menuReq.setProducts_last_updated_timestamp(data.getTimestamp());
                    }
                    if (data.getName().equalsIgnoreCase(DBConstants.PRODUCT_ATTRIBUTE_MAPPING_TIMESTAMP)) {
                        if (data.getTimestamp() != null)
                            menuReq.setPam_last_updated_timestamp(data.getTimestamp());
                    }
                    if (data.getName().equalsIgnoreCase(DBConstants.PRODUCT_CATEGORY_MAPPING)) {
                        if (data.getTimestamp() != null)
                            menuReq.setPcm_last_updated_timestamp(data.getTimestamp());
                    }
                    Toolbox.writeToLog("Tester " + menuReq.toString());
                }
            }

        } else {
            Toolbox.writeToLog("Tester " + menuReq.toString());
        }
        Toolbox.writeToLog("All ok");
        return menuReq;
    }

    public static void saveOrder(final Activity context, final SyncListener syncListener, String version) {
        sendSyncStartEvent(syncListener, context);
        final Cart cart = Cart.getInstance();
        final DBManager dbManager = DBManager.getInstance(context);
        RequestSaveOrder saveOrder = new RequestSaveOrder();
        saveOrder.setIs_preorder(cart.isPreorder());
        ArrayList<Order> orders = new ArrayList<Order>();
        orders.add(cart.getFirstOrder());
        saveOrder.setOrders(orders);
        saveOrder.setDelivery_type(cart.getDelivery_type());
        saveOrder.setOrder_date(cart.getOrderDate().getTime() / 1000);
        saveOrder.setTotal_amount(cart.getTotal());
        saveOrder.setPayment_method("");
        saveOrder.setUser(cart.getUser());


        ResponseHandler responseHandler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTime) {

                final RespSaveOrder resp;
                try {
                    resp = objMapper.readValue(response, RespSaveOrder.class);
                    if (resp != null && resp.getResponseCode() != RESPONSE_CODE_SUCCESS) {
                        sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTime);
                        return;
                    }
                    Toolbox.writeToLog("Response save-order: " + response);
                    if (resp.getData() != null) {
                        try {
                            User user = resp.getData().getUser();
                            Address address = cart.getUser().getAddress();

                            dbManager.insertUser(user);
                            if (address != null) {
                                address.setId(resp.getData().getUser().getAddress().getId());
                                address.setUser_id(user.getId());
                                address.setTag(cart.getUser().getAddress().getTag());

                                dbManager.clearOrders();

                                dbManager.insertAddress(address);
                                dbManager.insertAddressTag(address.getId(), cart.getUser().getAddress().getTag());
                                for (Order o : resp.getData().getOrders()) {
                                    o.setOrder_date(resp.getData().getOrder_date() * 1000L);
                                    o.setQuantity(cart.getProductsCount(o.getOutlet_id()));
                                }


                                if (cart.getDelivery_type().toLowerCase().contains("deliver")) {
                                    dbManager.insertOrders(resp.getData().getOrders());
                                    dbManager.insertExtraCharges(cart.getExtra_charges());
                                    dbManager.insertOffers(cart.getOffers());
                                }
//                            dbManager.clearOutletMenuData();
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                            sendSyncFailureEvent(url, syncListener, context, "Database error", requestTime);
                        }
                        sendSyncSuccessEvent(url, syncListener, context, resp, requestTime);


                    } else {
                        sendSyncFailureEvent(url, syncListener, context, response, requestTime);
                    }


                } catch (IOException e) {
                    sendSyncFailureEvent(url, syncListener, context, SERVER_ERROR, requestTime);
                    e.printStackTrace();
                }

            }
        };


        try {
            AsyncJsonPost asyncJsonPost = new AsyncJsonPost(context, URL_SAVE_ORDER, objMapper.writeValueAsString(saveOrder), HttpHelper.EncodingType.GZIP, responseHandler);
            asyncJsonPost.setVersion(version);
            asyncJsonPost.setDownloadListener(buildDownloadListener(syncListener, context));
            asyncJsonPost.execute();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }

    public static void getAreaAndSubareaForOutlet(final Activity context, final long outletId, final SyncListener syncListener) {
        sendSyncStartEvent(syncListener, context);
        final RequestOutletSubarea request = new RequestOutletSubarea(outletId);
        final DBManager db = DBManager.getInstance(context);
        ResponseHandler responseHandler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTime) {

                try {
                    ResponseOutletSubarea responseOutletSubarea = objMapper.readValue(response, ResponseOutletSubarea.class);

                    if (responseOutletSubarea != null && responseOutletSubarea.getResponseCode() != RESPONSE_CODE_SUCCESS) {
                        sendSyncFailureEvent(url, syncListener, context, ERROR_MESSAGE_GENERIC, requestTime);
                        return;
                    }
                    Toolbox.writeToLog("Response " + url + " : " + response);
                    if (responseOutletSubarea.getData() != null) {
                        db.insertAreas(responseOutletSubarea.getData().getAreas(), outletId);
                        db.insertSubareas(responseOutletSubarea.getData().getSubareas(), outletId);
                        sendSyncSuccessEvent(URL_GET_AREA_SUBAREA_FOR_OUTLET, syncListener, context, responseOutletSubarea.getData(), requestTime);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    sendSyncFailureEvent(URL_GET_AREA_SUBAREA_FOR_OUTLET, syncListener, context, ERROR_MESSAGE_GENERIC, requestTime);
                }
            }
        };

        AsyncJsonPost asyncJsonPost = null;
        try {
            asyncJsonPost = new AsyncJsonPost(context, URL_GET_AREA_SUBAREA_FOR_OUTLET, objMapper.writeValueAsString(request), HttpHelper.EncodingType.GZIP, responseHandler);
            asyncJsonPost.setDownloadListener(buildDownloadListener(syncListener, context));
            Toolbox.writeToLog("Request " + URL_GET_AREA_SUBAREA_FOR_OUTLET + " : " + request.toString());
            asyncJsonPost.execute();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


    }

    public static void getOutletMenu(final Activity context, final long outletId, final SyncListener syncListener) {
        sendSyncStartEvent(syncListener, context);
        final DBManager dbMan = DBManager.getInstance(context.getApplication());


        ResponseHandler responseHandler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTimestamp) {
                try {


                    final RespOutletMenu resp = objMapper.readValue(response, RespOutletMenu.class);

                    if (resp != null && resp.getResponseCode() != RESPONSE_CODE_SUCCESS) {
                        sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                        return;
                    }


                    if (resp.getDataArray().size() > 0) {
                        List<OutletMenuData> outletMenuDataList = resp.getDataArray();
                        //dbMan.clearCache();

                        for (final OutletMenuData outletMenuData : outletMenuDataList) {
                            if (outletMenuData != null) {

                                dbMan.clearOutletMenuData();

                                Callable<Void> callable = new Callable<Void>() {
                                    @Override
                                    public Void call() throws Exception {
                                        dbMan.insertCategoryList(outletMenuData.getCategories());
                                        dbMan.insertProductList(outletMenuData.getProducts());
                                        dbMan.insertProductCategoryMappingList(outletMenuData.getProduct_category_mapping());
                                        dbMan.insertOutletProductMappingList(outletMenuData.getOutlet_product_mapping());
                                        dbMan.insertProductAttributeList(outletMenuData.getProduct_attributes());
                                        dbMan.insertProductAttributeGroupList(outletMenuData.getProduct_attribute_group());
                                        dbMan.insertProductAttributeMappingList(outletMenuData.getProduct_attribute_mapping());
                                        return null;
                                    }
                                };
                                dbMan.callInTransaction(callable);
//


                                Toolbox.writeToLog("IS this written");
                                ArrayList<CacheForData> cacheForDataList = new ArrayList<CacheForData>();
                                cacheForDataList.add(CacheForData.getCacheObject(DBConstants.CATEGORY_TIMESTAMP, outletMenuData.getCategories_last_updated_timestamp(), outletMenuData.getOutlet_id()));
                                cacheForDataList.add(CacheForData.getCacheObject(DBConstants.PRODUCT_TIMESTAMP, outletMenuData.getProducts_last_updated_timestamp(), outletMenuData.getOutlet_id()));
                                cacheForDataList.add(CacheForData.getCacheObject(DBConstants.PRODUCT_CATEGORY_MAPPING, outletMenuData.getPcm_last_updated_timestamp(), outletMenuData.getOutlet_id()));
                                cacheForDataList.add(CacheForData.getCacheObject(DBConstants.OUTLET_PRODUCT_MAPPING_TIMESTAMP, outletMenuData.getOpm_last_updated_timestamp(), outletMenuData.getOutlet_id()));
                                cacheForDataList.add(CacheForData.getCacheObject(DBConstants.PRODUCT_ATTRIBUTE_TIMESTAMP, outletMenuData.getPa_last_updated_timestamp(), outletMenuData.getOutlet_id()));
                                cacheForDataList.add(CacheForData.getCacheObject(DBConstants.PRODUCT_ATTRIBUTE_GROUP_TIMESTAMP, outletMenuData.getPag_last_updated_timestamp(), outletMenuData.getOutlet_id()));
                                cacheForDataList.add(CacheForData.getCacheObject(DBConstants.PRODUCT_ATTRIBUTE_MAPPING_TIMESTAMP, outletMenuData.getPam_last_updated_timestamp(), outletMenuData.getOutlet_id()));
                                dbMan.insertTimestamp(cacheForDataList);

                            }
                        }

                        sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                    } else {

                        sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    sendSyncFailureEvent(url, syncListener, context, SERVER_ERROR, requestTimestamp);

                }
            }
        };

        RequestOutletMenu req = new RequestOutletMenu(outletId);//getTimestampsWithObject(outletId,dbMan);//;new RequestOutletMenu(outletId);
        List<RequestOutletMenu> reqList = new ArrayList<RequestOutletMenu>();
        reqList.add(req);
        try {
            AsyncJsonPost asyncJsonPost = new AsyncJsonPost(context, URL_OUTLET_MENU, objMapper.writeValueAsString(reqList), HttpHelper.EncodingType.GZIP, responseHandler);
            asyncJsonPost.setDownloadListener(buildDownloadListener(syncListener, context));
            asyncJsonPost.execute();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }


    public static void getOutletByCompany(final Activity context, final SyncListener syncListener, String version) throws JSONException {
        sendSyncStartEvent(syncListener, context);
        final DBManager dbMan = DBManager.getInstance(context);
        RequestCompId request = new RequestCompId((long) Constants.COMPANY_ID_INT);
        // request.setCompany_id((long) Constants.COMPANY_ID_INT);
        ResponseHandler handler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTime) {
                final ResponseCompanyOutlet resp;
                final OutletData comp;
                try {
                    resp = objMapper.readValue(response, ResponseCompanyOutlet.class);
                    Toolbox.writeToLog("Response company-outlet: " + response);
                    if (resp != null && resp.getResponseCode() != RESPONSE_CODE_SUCCESS) {
                        if (resp.getResponseCode() == Constants.RESP_CODE_APP_UPDATE) {
                            sendSyncSuccessEvent(url, syncListener, context, resp, requestTime);
                        } else
                            sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTime);
                        return;
                    }

                    if (resp.getData() != null) {
                        {
                            comp = resp.getData();
                            try {

                                dbMan.clearOutletData();

                                dbMan.insertOutletList(comp.getOutlets());
                                dbMan.insertOutletAttributeList(comp.getOutlet_attributes());
                                dbMan.insertOutletAttributesGroupList(resp.getData().getOutlet_attributes_group());
                                dbMan.insertOutletAttributesMappingList(resp.getData().getOutlet_attributes_mapping());
                                dbMan.insertOutletTimingList(resp.getData().getOutlet_timing());

                            } catch (Exception e) {
                                sendSyncFailureEvent(url, syncListener, context, "Parsing error", requestTime);
                            }
                        }

                        sendSyncSuccessEvent(url, syncListener, context, resp, requestTime);
                    }

                } catch (IOException e) {
                    sendSyncFailureEvent(url, syncListener, context, SERVER_ERROR, requestTime);
                    e.printStackTrace();
                }
            }
        };
        try {
            AsyncJsonPost asyncJsonPost = new AsyncJsonPost(context, Constants.URL_GET_OUTLET_BY_COMPANY, objMapper.writeValueAsString(request), HttpHelper.EncodingType.PLAIN_TEXT, handler);
            asyncJsonPost.setDownloadListener(buildDownloadListener(syncListener, context));
            asyncJsonPost.setVersion(version);
            asyncJsonPost.execute();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }

    public static void trackOrder(final Activity context, Long orderId, final SyncListener syncListener) {
        sendSyncStartEvent(syncListener, context);
        final DBManager dbMan = DBManager.getInstance(context);
        RequestTrackOrder request = new RequestTrackOrder(orderId);
        // request.setCompany_id((long) Constants.COMPANY_ID_INT);
        ResponseHandler handler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTime) {
                final ResponseTrackOrder resp;
                final TrackOrderData comp;
                try {
                    resp = objMapper.readValue(response, ResponseTrackOrder.class);
                    Toolbox.writeToLog("Response track-order: " + response);
                    if (resp != null && resp.getResponseCode() != RESPONSE_CODE_SUCCESS) {
                        sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTime);
                        return;
                    }

                    if (resp.getData() != null) {
                        {
                            comp = resp.getData();
                            try {
                                Toolbox.writeToLog("Data :" + comp);

                            } catch (Exception e) {
                                sendSyncFailureEvent(url, syncListener, context, "Parsing error", requestTime);
                            }
                        }

                        sendSyncSuccessEvent(url, syncListener, context, comp, requestTime);
                    }

                } catch (IOException e) {
                    sendSyncFailureEvent(url, syncListener, context, SERVER_ERROR, requestTime);
                    e.printStackTrace();
                }
            }
        };
        try {
            AsyncJsonPost asyncJsonPost = new AsyncJsonPost(context, Constants.URL_TRACK_ORDER, objMapper.writeValueAsString(request), HttpHelper.EncodingType.PLAIN_TEXT, handler);
            asyncJsonPost.setDownloadListener(buildDownloadListener(syncListener, context));
            asyncJsonPost.execute();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }

    public static void checkOrder(final Activity context, final SyncListener syncListener) {
        sendSyncStartEvent(syncListener, context);
        final DBManager dbMan = DBManager.getInstance(context.getApplication());
        final Cart cart = Cart.getInstance();
        ResponseHandler responseHandler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTimestamp) {
                try {

                    final RespCheckOrder resp = objMapper.readValue(response, RespCheckOrder.class);
                    if (resp != null && (resp.getResponseCode() == RESPONSE_CODE_SUCCESS || (resp.getResponseCode() == 600))) {
                        cart.setRespCheckOrder(resp);
                        if (resp.getData().getOrders() != null) {
                            if (resp.getData().getOrders().size() > 0) {
                                if (resp.getData().getOrders().get(0).getApplied_offer() != null)
                                    cart.setOffers(resp.getData().getOrders().get(0).getApplied_offer());
                                if (resp.getData().getOrders().get(0).getApplied_extra_charges() != null)
                                    cart.setExtra_charges(resp.getData().getOrders().get(0).getApplied_extra_charges());
                            }
                        }
                        sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                    } else //if(resp == null)
                    {

                        sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                        return;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    sendSyncFailureEvent(url, syncListener, context, SERVER_ERROR, requestTimestamp);

                }
            }
        };
        cart.setOrderDate(new Date());
        RequestCheckOrder req = new RequestCheckOrder(cart.isPreorder(), cart.getOrderDate().getTime() / 1000, cart.getCouponCode(), cart.getTotal(), cart.getDelivery_type(), cart.getOrders().values());
        try {
            Log.e(tag, "check order req:" + objMapper.writeValueAsString(req));
            AsyncJsonPost asyncJsonPost = new AsyncJsonPost(context, URL_CHECK_ORDER, objMapper.writeValueAsString(req), HttpHelper.EncodingType.GZIP, responseHandler);
            asyncJsonPost.setDownloadListener(buildDownloadListener(syncListener, context));
            asyncJsonPost.execute();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }

    public static void findMe(final Activity context, String mobile, final SyncListener syncListener) {
        sendSyncStartEvent(syncListener, context);
        final DBManager dbMan = DBManager.getInstance(context.getApplication());
        final Cart cart = Cart.getInstance();
        RequestFindMe req = new RequestFindMe();
        req.setMobile_number(mobile);
        req.setCompany_id(Constants.COMPANY_ID_INT);
        ResponseHandler responseHandler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTimestamp) {
                try {

                    final ResponseFindMe resp = objMapper.readValue(response, ResponseFindMe.class);
                    if (resp != null && resp.getResponseCode() != RESPONSE_CODE_SUCCESS) {
                        sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                        return;
                    }
                    if (resp.getData() != null) {
                        if (resp.getData().getUser() != null && resp.getData().getUser().getId() != null) {
                            dbMan.insertUser(resp.getData().getUser());
                            for (Address a : resp.getData().getAddresses()) {
                                a.setUser_id(resp.getData().getId());
                                dbMan.insertAddress(a);
                                dbMan.insertAddressTag(a.getId(), a.getTag());
                            }
                        }

                        sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    sendSyncFailureEvent(url, syncListener, context, SERVER_ERROR, requestTimestamp);

                }
            }
        };
        try {
            Log.e(tag, "find me req:" + objMapper.writeValueAsString(req));
            AsyncJsonPost asyncJsonPost = new AsyncJsonPost(context, URL_FIND_ME, objMapper.writeValueAsString(req), HttpHelper.EncodingType.GZIP, responseHandler);
            asyncJsonPost.setDownloadListener(buildDownloadListener(syncListener, context));
            asyncJsonPost.execute();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }

    public static void registerDevice(final Activity context, final SyncListener syncListener) {
        sendSyncStartEvent(syncListener, context);

        ResponseHandler responseHandler = new ResponseHandler() {
            @Override
            public void handleStringResponse(String url, String response, long requestTimestamp) {
                try {

                    final ResponseGCMRegistration resp = objMapper.readValue(response, ResponseGCMRegistration.class);
                    if (resp != null && resp.getResponseCode() != RESPONSE_CODE_SUCCESS) {
                        sendSyncFailureEvent(url, syncListener, context, resp.getResponseMsg(), requestTimestamp);
                        return;
                    }

                    sendSyncSuccessEvent(url, syncListener, context, resp, requestTimestamp);
                } catch (Exception e) {
                    e.printStackTrace();
                    sendSyncFailureEvent(url, syncListener, context, SERVER_ERROR, requestTimestamp);

                }
            }
        };
        SharedPreferences sharedPreferences = SharedPreferencesManager.getSharedPreferences(context);
        String token = sharedPreferences.getString(Constants.PREFS_GCM_ID_KEY, "");
        String uid = sharedPreferences.getString(Constants.PREFS_GCM_UUID_KEY, "");


        RequestGCMRegistration req = new RequestGCMRegistration(token, uid, Constants.getDeviceName(), Constants.getdevicePlatform(), Constants.getDeviceVersion(), Constants.COMPANY_ID_INT);
        try {
            AsyncJsonPost asyncJsonPost = new AsyncJsonPost(context, URL_REGISTER_DEVICE, objMapper.writeValueAsString(req), HttpHelper.EncodingType.PLAIN_TEXT, responseHandler);
            asyncJsonPost.setDownloadListener(buildDownloadListener(syncListener, context));
            asyncJsonPost.execute();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }


    private static void sendSyncStartEvent(final SyncListener syncListener, Activity context) {
        if (syncListener != null && context != null) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    syncListener.onSyncStart();
                }
            });
        }
    }


    private static void sendSyncFailureEvent(final String url, final SyncListener syncListener, Activity context, final String message, final long requestTimestamp) {
        if (syncListener != null && context != null) {

            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    syncListener.onSyncFailure(url, message, requestTimestamp);
                }
            });
        }
    }

    private static void sendSyncSuccessEvent(final String url, final SyncListener syncListener, Activity context, final Object response, final long requestTimestamp) {
        if (syncListener != null && context != null) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    float timeTaken = (System.currentTimeMillis() - requestTimestamp) / 1000f;
                    Toolbox.writeToLog(url + " Total time = " + timeTaken + " seconds");
                    syncListener.onSyncSuccess(url, response, requestTimestamp);
                }
            });
        }
    }

    private static DownloadListener buildDownloadListener(final SyncListener syncListener, final Activity context) {

        return new DownloadListener() {
            @Override
            public void onDownloadStart(String url, long requestTimestamp) {

            }

            @Override
            public void onDownloadSuccess(String url, String response, int httpStatusCode, long requestTimestamp) {

            }

            @Override
            public void onDownloadSuccess(String url, byte[] response, long requestTimestamp) {

            }

            @Override
            public void onDownloadFail(String url, Exception e, String reason, long requestTimestamp) {
                sendSyncFailureEvent(url, syncListener, context, reason, requestTimestamp);
            }
        };

    }

}