package to.done.lib.sync;

import android.content.Context;
import android.os.AsyncTask;
import android.os.PowerManager;

import java.io.File;

import to.done.lib.utils.Toolbox;
import to.done.lib.webservice.HttpHelper;

/**
 * Created by shadab mirza on 29/11/13.
 */
public class AsyncJsonGet extends AsyncTask<String, Integer, String> implements DownloadListener {


    private Context context;
    private File file;
    private ResponseHandler responseHandler;
    private String url;
    private DownloadListener downloadListener;
    private long requestTimestamp;
    private HttpHelper.EncodingType encodingType;

    public AsyncJsonGet(Context context, String url, HttpHelper.EncodingType encodingType,ResponseHandler responseHandler) {
        this.context = context;
        this.responseHandler=responseHandler;
        this.url=url;
        this.encodingType=encodingType;
        requestTimestamp=System.currentTimeMillis();
    }

    @Override
    protected String doInBackground(String... sUrl) {
        // take CPU lock to prevent CPU from going off if the user
        // presses the power button during download
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                getClass().getName());
        wl.acquire();
        Toolbox.writeToLog("request "+url);
        if(Toolbox.isInternetAvailable(context)){
            long startTimestamp=System.currentTimeMillis();
            HttpHelper.doHttpGet(url,encodingType,requestTimestamp, this);
            float timeTaken=(System.currentTimeMillis()-startTimestamp)/1000f;

            Toolbox.writeToLog(url+" Download time = "+timeTaken+" seconds");
        }
        else
        {
            onDownloadFail(url,null,"No internet connection",requestTimestamp);
        }

        wl.release();
        return null;
    }

    @Override
    public void onDownloadStart(String url, long requestTimestamp) {

    }

    @Override
    public void onDownloadSuccess(String url, String response, int httpStatusCode, long requestTimestamp) {
        Toolbox.writeToLog("successfully downloaded "+url+" response- "+response);
        if(httpStatusCode==200)
        {
            responseHandler.handleStringResponse(url,response,requestTimestamp);
        }
        else
        {
            Toolbox.writeToLog("Error: HTTP status "+httpStatusCode+response);
            onDownloadFail(url,null,"Server Error",requestTimestamp);
        }

        if(downloadListener!=null){
            downloadListener.onDownloadSuccess(url, response, httpStatusCode,requestTimestamp);
        }
    }

    @Override
    public void onDownloadSuccess(String url, byte[] response, long requestTimestamp) {

    }

    @Override
    public void onDownloadFail(String url, Exception e, String reason, long requestTimestamp) {
        if(downloadListener!=null){
            downloadListener.onDownloadFail(url,e,reason,requestTimestamp);
        }

    }

    public DownloadListener getDownloadListener() {
        return downloadListener;
    }

    public void setDownloadListener(DownloadListener downloadListener) {
        this.downloadListener = downloadListener;
    }
}
