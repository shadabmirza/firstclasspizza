package to.done.lib.sync;

/**
 * Created by shadab mirza on 29/11/13.
 */
public interface ResponseHandler {

    void handleStringResponse(String url, String response, long requestTime);

}
