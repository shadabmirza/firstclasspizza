package to.done.lib;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.View;

/**
 * Created by shadab mirza on 4/7/14.
 */
public final class Constants {


    public static final long RESP_CODE_APP_UPDATE = 500;

    private Constants() {

    }

    public static final String[] MONTHS_LONG = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    public static final String[] MONTHS_SHORT = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    public static final String TAG = "DONE_LOG";

    public static final String PREFS_NAME = "done_prefs";

    public static final String PREFS_GCM_ID_KEY = "GCM_ID";
    public static final String PREFS_GCM_UUID_KEY = "GCM_UUID";

    public static final String PREFS_DBVERSION = "db_version";
    public static final String PREFS_PUSH_TOKEN = "push_token";
    public static final String PREFS_DEVICE_ID = "device_id";
    public static final String PREFS_THEME_ID = "theme_id";
    public static final String PREFS_APP_VERSION = "app_version";
    public static final String PREFS_SELECTED_AREA = "selected_area";
    public static final String PREFS_SELECTED_SUBAREA = "selected_subarea";
    public static final String PREFS_SELECTED_SUBAREA_ID = "selected_subarea_id";
    public static final String PREFS_SELECTED_AREA_ID = "selected_area_id";
    public static final String ORDER_NUMBER = "order_number";

    public static final String PREFS_SELECTED_CITY_ID = "selected_city_id";
    public static final String PREFS_SELECTED_STATE_ID = "selected_state_id";
    public static final String PREFS_IS_DEVICE_REGISTERED = "is_device_registered";

    public static final String PREFS_LAST_USED_MOBILE = "last_used_mobile";
    public static final String PREFS_LAST_USED_NAME = "last_used_name";
    public static final String PREFS_LAST_USED_EMAIL = "last_used_email";
    public static final String PREFS_LAST_USED_ADDRESS = "last_used_address";
    public static final int RESPONSE_CODE_SUCCESS = 0;

    public static final String BASE_IP_MOVIVATION = "chekout.movivation.com";
    public static final String BASE_IP_WEB_WERKS = "api.done.to";//"180.149.244.173";//"fs.done.to";//"180.149.244.173";
    public static final String BASE_IP_PRAYAG = "10.0.0.26";
    public static final String BASE_IP = BASE_IP_WEB_WERKS;
   // public static final String BASE_IP = BASE_IP_PRAYAG;
   //public static final String BASE_IP = "10.0.1.31";

    public static final String BASE_URL_SOLR = "http://" + BASE_IP + ":8983/solr/";
    public static final String URL_CLOSEST_SUBAREA = BASE_URL_SOLR + "subareas/select?wt=json&q=*:*&sfield=latlng&sort=geodist()%20asc&fl=dist:geodist(),subarea_name,id,area_id,area_name,city_id,city_name,state_id,latlng&start=0&rows=1&pt=";
    public static final String URL_SEARCH_SUBAREA = BASE_URL_SOLR + "subareas/spell?wt=json&fl=id,subarea_name,area_id,area_name,city_id,city_name,state_id,latlng&q=subarea_name:";


    public static final String BASE_URL = "http://" + BASE_IP + ":5000/";//"http://180.149.244.173:5000/";http://chekout.movivation.com:5000/";
    public static final String URL_NEAREST_OUTLETS = BASE_URL + "done-nearest-outlets-by-subarea";
    public static final String URL_OUTLET_MENU = BASE_URL + "done-outlet-menu";
    //  public static final String URL_OUTLET_MENU_GZIP=BASE_URL+"done-outlet-menu-gzip";
    public static final String URL_CHECK_ORDER = BASE_URL + "done-check-order";
    public static final String URL_SAVE_ORDER = BASE_URL + "done-save-order";
    public static final String URL_GET_OUTLET_BY_COMPANY = BASE_URL + "done-outlets-by-company";
    public static final String URL_GET_AREA_SUBAREA_FOR_OUTLET = BASE_URL + "done-outlet-subareas";
    public static final String URL_REGISTER_DEVICE = BASE_URL + "done-register-device";
    public static final String URL_TRACK_ORDER = BASE_URL + "done-track-order";
    //public static final String URL_FIND_ME = BASE_URL+"done-get-user";
    public static final String URL_FIND_ME = BASE_URL + "done-get-company-user";

    public static final String ERROR_MESSAGE_INTERNET_UNAVAILABLE = "No internet connection";
    public static final String ERROR_MESSAGE_RECORD_NOT_FOUND = "Sorry, no results found.";
    public static final String ERROR_MESSAGE_GENERIC = "Something went wrong. Please try again.";
    public static final long ERROR_TIMEOUT = 7500l;

    public static final String INTENT_SCREEN_UPDATE = "intent_screen";
    public static final String SCREEN_ID = "screen_id";
    public static final String OUTLET_ID = "outlet_id";
    public static final String OUTLET_NAME = "outlet_name";
    public static final String COMPANY_ID = "company_id";
    public static final String OUTLET_PRODUCT_ID = "outlet_product_id";
    public static final String PRODUCT_ID = "product_id";
    public static final String PRODUCT_NAME = "product_name";
    public static final String SUBAREA_ID = "subarea_id";
    public static final String SUBAREA_NAME = "subarea_name";
    public static final String RESULT_SIZE = "result_size";
    public static final String CUST_SIZE = "cust_size";

    public static final String INTENT_NAV_DRAWER = "intent_navigation_drawer";
    public static final String NAV_DRAWER_ACTION = "drawer_action";
    public static final int NAV_DRAWER_ACTON_ENABLE = 1;
    public static final int NAV_DRAWER_ACTON_DISABLE = 2;
    public static final int NAV_DRAWER_ACTON_OPEN = 3;
    public static final int NAV_DRAWER_ACTON_CLOSE = 4;
    public static final int NAV_DRAWER_ACTON_SWITCH_STATUS = 5;


    public static final String INTENT_AB_TITLE = "intent_actionbar_title";
    public static final String AB_TITLE_TEXT = "title_text";
    public static final String AB_SUBTITLE_TEXT = "subtitle_text";

    public static final String INTENT_AB_BUTTON = "intent_home_button";
    public static final String AB_BTN = "intent_home_button";
    public static final int AB_BTN_HOME = 1;
    public static final int AB_BTN_BACK = 2;

    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    public static final String ADD_TO_BACKSTACK = "add_to_backstack";
    public static final String BUNDLE_EXTRA = "bundle_extra";


    public static final int SCREEN_LOC_SELECTION = 1;
    public static final int SCREEN_OUTLETS = 2;
    public static final int SCREEN_PRODUCTS = 3;
    public static final int SCREEN_PRODUCT_CUSTOMIZATION = 4;
    public static final int SCREEN_ORDER_DETAILS = 5;
    public static final int SCREEN_PERSONAL_DETAILS = 6;
    public static final int SCREEN_CART = 7;
    public static final int SCREEN_CONFIRM_ORDER = 8;
    public static final int SCREEN_THANKS = 9;
    public static final int SCREEN_TRACK = 10;
    public static int CURRENT_SCREEN = SCREEN_LOC_SELECTION;

    public static final String CART_REACHED_BY_PROCEED = "reached_by_proceed";

    public static final String DELIVERY_TYPE_DELIVER = "delivery";
    public static final String DELIVERY_TYPE_TAKEAWAY = "takeaway";

    public static final String DATE_DAY = "date_day";
    public static final String DATE_MONTH = "date_month";
    public static final String DATE_YEAR = "date_year";
    public static final int COMPANY_ID_INT = 6893;
    //6891 zaffran//;
    //6893 first class pizza
    //6883;// 6872;//6868;//6872;

    public static final int OUTLET_ID_INT = 8806;

    public static final long ATTRIBUTE_ID_VEG = 115;
    public static final long ATTRIBUTE_ID_NONVEG = 1;
    public static final long ATTRIBUTE_ID_SPICY = 116;
    public static final long ATTRIBUTE_ID_FAV = 118;
    public static final long ATTRIBUTE_ID_SIGNATURE = 121;
    public static final long ATTRIBUTE_ID_SUPREME = 119;
    public static final long ATTRIBUTE_ID_CLASSIC = 117;
    public static final long ATTRIBUTE_ID_STAR = 120;
    public static final int NUMBER_OF_DAYS_FOR_PREORDER = 3;

    public static final int FILTER_TEXT = 1;
    public static final int USE_IN_FILTER_AND_PRODUCT_CELL_AS_TEXT = 2;
    public static final int USE_IN_FILTER_AND_PRODUCT_CELL_AS_IMAGE = 3;
    public static final int DONT_USE_IN_FILTER_USE_IN_PRODUCT_AS_TEXT = 4;
    public static final int DONT_USE_IN_FILTER_USE_IN_PRODUCT_AS_IMAGE = 5;

    public static final String GA_SCREEN_OUTLETS = "Outlet Screen";
    public static final String GA_SCREEN_MENU = "Menu Screen";
    public static final String GA_SCREEN_CART = "Cart Screen";
    public static final String GA_SCREEN_PERSONAL_DETAILS = "Personal Details Screen";
    public static final String GA_SCREEN_CONFIRM_ORDER = "Confirm Order Screen";
    public static final String GA_SCREEN_NOTIFICATION_POPUP = "Notification Screen";


    public static final int RESP_CODE_PRICE_MISMATCH = 156;
    public static final int RESP_CODE_PRICE_CHANGE = 155;
    public static final String RUPEE_SYMBOL = "\u0024";

    public static String getRupeeSymbolAppedndedAtStart(String text) {
        return RUPEE_SYMBOL + " " + text;
    }

    public static String getRupeeSymbolAppedndedAtEnd(String text) {
        return text + " " + RUPEE_SYMBOL;
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    public static String getdevicePlatform() {
        return "android";
    }

    public static String getDeviceVersion() {
        return Integer.toString(Build.VERSION.SDK_INT);
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static void getRepeatingBackground(Context context, View view) {
        BitmapDrawable bd = (BitmapDrawable) context.getResources().getDrawable(R.drawable.orderlist_bottom);
        bd.setTargetDensity(DisplayMetrics.DENSITY_DEFAULT);
        int width = view.getWidth();
        int intrinsicHeight = bd.getIntrinsicHeight();
        Rect bounds = new Rect(0, 0, width, intrinsicHeight);
        bd.setTileModeX(Shader.TileMode.REPEAT);
        bd.setTileModeY(Shader.TileMode.CLAMP);
        bd.setBounds(bounds);
        Bitmap bitmap = Bitmap.createBitmap(bounds.width(), bounds.height(), bd.getBitmap().getConfig());
        Canvas canvas = new Canvas(bitmap);
        bd.draw(canvas);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(bitmap);
        view.setBackgroundDrawable(bitmapDrawable);

    }

}
