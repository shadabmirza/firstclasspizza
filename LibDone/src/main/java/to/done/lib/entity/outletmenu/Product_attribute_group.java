
package to.done.lib.entity.outletmenu;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "created_at",
    "modified_at",
    "name",
    "row_status",
    "selection_max",
    "selection_min"
})
@DatabaseTable
public class Product_attribute_group {

    @JsonProperty("id")
    @DatabaseField(id = true)
	private Long id;
    @JsonProperty("created_at")
    @DatabaseField
	private String created_at;
    @JsonProperty("modified_at")
    @DatabaseField
	private String modified_at;
    @JsonProperty("name")
    @DatabaseField
	private String name;
    @JsonProperty("row_status")
    @DatabaseField
	private String row_status;

    @JsonProperty("selection_max")
    @DatabaseField
    private Integer selection_max=-1;

    @JsonProperty("selection_min")
    @DatabaseField
    private Integer selection_min=0;

    @JsonIgnore
    private boolean selected;

    @JsonIgnore
    private List<Product_attribute> productAttributeList=new ArrayList<Product_attribute>();

    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    public Product_attribute_group withId(Long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Product_attribute_group withCreated_at(String created_at) {
        this.created_at = created_at;
        return this;
    }

    @JsonProperty("modified_at")
    public String getModified_at() {
        return modified_at;
    }

    @JsonProperty("modified_at")
    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    public Product_attribute_group withModified_at(String modified_at) {
        this.modified_at = modified_at;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Product_attribute_group withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("row_status")
    public String getRow_status() {
        return row_status;
    }

    @JsonProperty("row_status")
    public void setRow_status(String row_status) {
        this.row_status = row_status;
    }

    public Product_attribute_group withRow_status(String row_status) {
        this.row_status = row_status;
        return this;
    }

    @JsonProperty("selection_max")
    public Integer getSelection_max() {
        return selection_max;
    }
    @JsonProperty("selection_max")
    public void setSelection_max(Integer selection_max) {
        this.selection_max = selection_max;
    }

    @JsonProperty("selection_min")
    public int getSelection_min() {
        return selection_min;
    }
    @JsonProperty("selection_min")
    public void setSelection_min(Integer selection_min) {
        this.selection_min = selection_min;
    }

    @JsonIgnore
    public List<Product_attribute> getProductAttributeList() {
        return productAttributeList;
    }

    @JsonIgnore
    public void setProductAttributeList(List<Product_attribute> productAttributeList) {
        this.productAttributeList = productAttributeList;
    }

    @JsonIgnore
    public boolean isSelected() {
        return selected;
    }

    @JsonIgnore
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
