package to.done.lib.entity.tracking;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by HP on 6/25/2014.
 */@JsonPropertyOrder({
        "order_id"
})
   public class RequestTrackOrder {

    public RequestTrackOrder(Long oid){
        order_id=oid;
    }
    @JsonProperty("order_id")
    private Long order_id;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("order_id")
    public Long getOrder_id() {
        return order_id;
    }

    @JsonProperty("order_id")
    public void setOrder_id(Long order_id) {
        this.order_id = order_id;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "RequestTrackOrder{" +
                "order_id=" + order_id +
                '}';
    }
}
