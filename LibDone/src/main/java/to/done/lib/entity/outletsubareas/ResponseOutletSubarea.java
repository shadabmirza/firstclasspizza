package to.done.lib.entity.outletsubareas;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by HP on 6/15/2014.
 */@JsonPropertyOrder({
        "responseCode",
        "responseMsg",
        "data",
        "dataArray"
})
   public class ResponseOutletSubarea {

    @JsonProperty("responseCode")
    private Long responseCode;
    @JsonProperty("responseMsg")
    private String responseMsg;
    @JsonProperty("data")
    private OutletSubareaData data;
    @JsonProperty("dataArray")
    private Object dataArray;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("responseCode")
    public Long getResponseCode() {
        return responseCode;
    }

    @JsonProperty("responseCode")
    public void setResponseCode(Long responseCode) {
        this.responseCode = responseCode;
    }

    @JsonProperty("responseMsg")
    public String getResponseMsg() {
        return responseMsg;
    }

    @JsonProperty("responseMsg")
    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    @JsonProperty("data")
    public OutletSubareaData getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(OutletSubareaData data) {
        this.data = data;
    }

    @JsonProperty("dataArray")
    public Object getDataArray() {
        return dataArray;
    }

    @JsonProperty("dataArray")
    public void setDataArray(Object dataArray) {
        this.dataArray = dataArray;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
