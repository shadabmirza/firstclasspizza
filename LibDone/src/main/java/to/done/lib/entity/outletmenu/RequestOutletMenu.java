
package to.done.lib.entity.outletmenu;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)

@JsonPropertyOrder({
        "outlet_id",
        "outlet_last_updated_timestamp",
        "ot_last_updated_timestamp",
        "categories_last_updated_timestamp",
        "ocm_last_updated_timestamp",
        "products_last_updated_timestamp",
        "pcm_last_updated_timestamp",
        "opm_last_updated_timestamp",
        "pa_last_updated_timestamp",
        "pag_last_updated_timestamp",
        "pam_last_updated_timestamp"
})
public class RequestOutletMenu {

    @JsonProperty("outlet_id")
    private Long outlet_id;
    @JsonProperty("outlet_last_updated_timestamp")
    private Long outlet_last_updated_timestamp;
    @JsonProperty("ot_last_updated_timestamp")
    private Long ot_last_updated_timestamp;
    @JsonProperty("categories_last_updated_timestamp")
    private Long categories_last_updated_timestamp;
    @JsonProperty("ocm_last_updated_timestamp")
    private Long ocm_last_updated_timestamp;
    @JsonProperty("products_last_updated_timestamp")
    private Long products_last_updated_timestamp;
    @JsonProperty("pcm_last_updated_timestamp")
    private Long pcm_last_updated_timestamp;
    @JsonProperty("opm_last_updated_timestamp")
    private Long opm_last_updated_timestamp;
    @JsonProperty("pa_last_updated_timestamp")
    private Long pa_last_updated_timestamp;
    @JsonProperty("pag_last_updated_timestamp")
    private Long pag_last_updated_timestamp;
    @JsonProperty("pam_last_updated_timestamp")
    private Long pam_last_updated_timestamp;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public RequestOutletMenu(Long outletId){
        this.outlet_id=outletId;
        this.outlet_last_updated_timestamp=0l;
        this.ot_last_updated_timestamp=0l;
        this.categories_last_updated_timestamp=0l;
        this.ocm_last_updated_timestamp=null;
        this.products_last_updated_timestamp=0l;
        this.pcm_last_updated_timestamp=0l;
        this.opm_last_updated_timestamp=0l;
        this.pa_last_updated_timestamp=0l;
        this.pag_last_updated_timestamp=0l;
        this.pam_last_updated_timestamp=0l;
    }

    @JsonProperty("outlet_id")
    public Long getOutlet_id() {
        return outlet_id;
    }

    @JsonProperty("outlet_id")
    public void setOutlet_id(Long outlet_id) {
        this.outlet_id = outlet_id;
    }

    @JsonProperty("outlet_last_updated_timestamp")
    public Long getOutlet_last_updated_timestamp() {
        return outlet_last_updated_timestamp;
    }

    @JsonProperty("outlet_last_updated_timestamp")
    public void setOutlet_last_updated_timestamp(Long outlet_last_updated_timestamp) {
        this.outlet_last_updated_timestamp = outlet_last_updated_timestamp;
    }

    @JsonProperty("ot_last_updated_timestamp")
    public Long getOt_last_updated_timestamp() {
        return ot_last_updated_timestamp;
    }

    @JsonProperty("ot_last_updated_timestamp")
    public void setOt_last_updated_timestamp(Long ot_last_updated_timestamp) {
        this.ot_last_updated_timestamp = ot_last_updated_timestamp;
    }

    @JsonProperty("categories_last_updated_timestamp")
    public Long getCategories_last_updated_timestamp() {
        return categories_last_updated_timestamp;
    }

    @JsonProperty("categories_last_updated_timestamp")
    public void setCategories_last_updated_timestamp(Long categories_last_updated_timestamp) {
        this.categories_last_updated_timestamp = categories_last_updated_timestamp;
    }

    @JsonProperty("ocm_last_updated_timestamp")
    public Long getOcm_last_updated_timestamp() {
        return ocm_last_updated_timestamp;
    }

    @JsonProperty("ocm_last_updated_timestamp")
    public void setOcm_last_updated_timestamp(Long ocm_last_updated_timestamp) {
        this.ocm_last_updated_timestamp = ocm_last_updated_timestamp;
    }

    @JsonProperty("products_last_updated_timestamp")
    public Long getProducts_last_updated_timestamp() {
        return products_last_updated_timestamp;
    }

    @JsonProperty("products_last_updated_timestamp")
    public void setProducts_last_updated_timestamp(Long products_last_updated_timestamp) {
        this.products_last_updated_timestamp = products_last_updated_timestamp;
    }

    @JsonProperty("pcm_last_updated_timestamp")
    public Long getPcm_last_updated_timestamp() {
        return pcm_last_updated_timestamp;
    }

    @JsonProperty("pcm_last_updated_timestamp")
    public void setPcm_last_updated_timestamp(Long pcm_last_updated_timestamp) {
        this.pcm_last_updated_timestamp = pcm_last_updated_timestamp;
    }

    @JsonProperty("opm_last_updated_timestamp")
    public Long getOpm_last_updated_timestamp() {
        return opm_last_updated_timestamp;
    }

    @JsonProperty("opm_last_updated_timestamp")
    public void setOpm_last_updated_timestamp(Long opm_last_updated_timestamp) {
        this.opm_last_updated_timestamp = opm_last_updated_timestamp;
    }

    @JsonProperty("pa_last_updated_timestamp")
    public Long getPa_last_updated_timestamp() {
        return pa_last_updated_timestamp;
    }

    @JsonProperty("pa_last_updated_timestamp")
    public void setPa_last_updated_timestamp(Long pa_last_updated_timestamp) {
        this.pa_last_updated_timestamp = pa_last_updated_timestamp;
    }

    @JsonProperty("pag_last_updated_timestamp")
    public Long getPag_last_updated_timestamp() {
        return pag_last_updated_timestamp;
    }

    @JsonProperty("pag_last_updated_timestamp")
    public void setPag_last_updated_timestamp(Long pag_last_updated_timestamp) {
        this.pag_last_updated_timestamp = pag_last_updated_timestamp;
    }

    @JsonProperty("pam_last_updated_timestamp")
    public Long getPam_last_updated_timestamp() {
        return pam_last_updated_timestamp;
    }

    @JsonProperty("pam_last_updated_timestamp")
    public void setPam_last_updated_timestamp(Long pam_last_updated_timestamp) {
        this.pam_last_updated_timestamp = pam_last_updated_timestamp;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}