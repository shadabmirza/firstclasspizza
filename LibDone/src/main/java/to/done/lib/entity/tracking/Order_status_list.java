package to.done.lib.entity.tracking;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by HP on 6/25/2014.
 */
@JsonPropertyOrder({
        "id",
        "status",
        "display_text",
        "sr"
})
public class Order_status_list {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("status")
    private String status;
    @JsonProperty("display_text")
    private String display_text;
    @JsonProperty("sr")
    private Long sr;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("display_text")
    public String getDisplay_text() {
        return display_text;
    }

    @JsonProperty("display_text")
    public void setDisplay_text(String display_text) {
        this.display_text = display_text;
    }

    @JsonProperty("sr")
    public Long getSr() {
        return sr;
    }

    @JsonProperty("sr")
    public void setSr(Long sr) {
        this.sr = sr;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "Order_status_list{" +
                "id=" + id +
                ", status='" + status + '\'' +
                ", display_text='" + display_text + '\'' +
                ", sr=" + sr +
                '}';
    }
}