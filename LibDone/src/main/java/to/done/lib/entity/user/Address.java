package to.done.lib.entity.user;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.HashMap;
import java.util.Map;

import to.done.lib.utils.Toolbox;

@JsonPropertyOrder({
        "id",
        "flat_no",
        "society",
        "street",
        "landmark",
        "subarea_id",
        "area_id",
        "city_id",
        "state_id",
        "tag",
        "pincode"
})
@DatabaseTable
public class Address {

    @DatabaseField(id=true)
    @JsonProperty("id")
    private Long id;
    @DatabaseField
    @JsonProperty("flat_no")
    private String flat_no;
    @DatabaseField
    @JsonProperty("society")
    private String society;
    @DatabaseField
    @JsonProperty("street")
    private String street;
    @DatabaseField
    @JsonProperty("landmark")
    private String landmark;
    @DatabaseField
    @JsonProperty("subarea_id")
    private Long subarea_id;
    @DatabaseField
    @JsonProperty("area_id")
    private Long area_id;
    @DatabaseField
    @JsonProperty("city_id")
    private Long city_id;
    @DatabaseField
    @JsonProperty("state_id")
    private Long state_id;
    @DatabaseField
    @JsonIgnore
    private Long user_id;
    @DatabaseField(canBeNull = true)
    @JsonProperty("tag")
    private String tag;

    @DatabaseField(canBeNull = true)
    @JsonProperty("pincode")
    private String pincode;

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("flat_no")
    public String getFlat_no() {
        return flat_no;
    }

    @JsonProperty("flat_no")
    public void setFlat_no(String flat_no) {
        this.flat_no = flat_no;
    }

    @JsonProperty("society")
    public String getSociety() {
        return society;
    }

    @JsonProperty("society")
    public void setSociety(String society) {
        this.society = society;
    }

    @JsonProperty("street")
    public String getStreet() {
        return street;
    }

    @JsonProperty("street")
    public void setStreet(String street) {
        this.street = street;
    }

    @JsonProperty("landmark")
    public String getLandmark() {
        return landmark;
    }

    @JsonProperty("landmark")
    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    @JsonProperty("subarea_id")
    public Long getSubarea_id() {
        return subarea_id;
    }

    @JsonProperty("subarea_id")
    public void setSubarea_id(Long subarea_id) {
        this.subarea_id = subarea_id;
    }

    @JsonProperty("area_id")
    public Long getArea_id() {
        return area_id;
    }

    @JsonProperty("area_id")
    public void setArea_id(Long area_id) {
        this.area_id = area_id;
    }

    @JsonProperty("city_id")
    public Long getCity_id() {
        return city_id;
    }

    @JsonProperty("city_id")
    public void setCity_id(Long city_id) {
        this.city_id = city_id;
    }

    @JsonProperty("state_id")
    public Long getState_id() {
        return state_id;
    }

    @JsonProperty("state_id")
    public void setState_id(Long state_id) {
        this.state_id = state_id;
    }

    @Override
    public String toString()
    {
//        str != null && !"".equals(str)
        if(tag!=null && !"".equals(tag) && !(tag.trim().length()==0) && !tag.toLowerCase().equalsIgnoreCase("null")){
            Toolbox.writeToLog("addresstag1 "+tag);
            return tag;
        }
        else {
            String address="";
            if(flat_no!=null){
                address+=flat_no;
            }
            if(street!=null){
                address+=","+street;
            }

            if(landmark!=null){
                address+=","+landmark;
            }
            if(society!=null){
                address+=","+society;
            }
            Toolbox.writeToLog("addresstag2"+address);
            return address;
        }

    }


    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}