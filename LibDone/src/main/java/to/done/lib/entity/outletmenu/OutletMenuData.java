
package to.done.lib.entity.outletmenu;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import to.done.lib.entity.nearestoutlets.Outlet;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "outlet",
    "outlet_last_updated_timestamp",
    "categories",
    "categories_last_updated_timestamp",
    "products",
    "products_last_updated_timestamp",
    "outlet_product_mapping",
    "opm_last_updated_timestamp",
    "product_category_mapping",
    "pcm_last_updated_timestamp",
    "product_attributes",
    "pa_last_updated_timestamp",
    "product_attribute_group",
    "pag_last_updated_timestamp",
    "product_attribute_mapping",
    "pam_last_updated_timestamp",
    "outlet_id"
})
public class OutletMenuData {

    @JsonProperty("outlet")
    private List<Outlet> outlet = new ArrayList<Outlet>();
    @JsonProperty("outlet_last_updated_timestamp")
    private Long outlet_last_updated_timestamp;
    @JsonProperty("categories")
    private List<Category> categories = new ArrayList<Category>();
    @JsonProperty("categories_last_updated_timestamp")
    private Long categories_last_updated_timestamp;
    @JsonProperty("products")
    private List<Product> products = new ArrayList<Product>();
    @JsonProperty("products_last_updated_timestamp")
    private Long products_last_updated_timestamp;
    @JsonProperty("outlet_product_mapping")
    private List<Outlet_product_mapping> outlet_product_mapping = new ArrayList<Outlet_product_mapping>();
    @JsonProperty("opm_last_updated_timestamp")
    private Long opm_last_updated_timestamp;
    @JsonProperty("product_category_mapping")
    private List<Product_category_mapping> product_category_mapping = new ArrayList<Product_category_mapping>();
    @JsonProperty("pcm_last_updated_timestamp")
    private Long pcm_last_updated_timestamp;
    @JsonProperty("product_attributes")
    private List<Product_attribute> product_attributes = new ArrayList<Product_attribute>();
    @JsonProperty("pa_last_updated_timestamp")
    private Long pa_last_updated_timestamp;
    @JsonProperty("product_attribute_group")
    private List<Product_attribute_group> product_attribute_group = new ArrayList<Product_attribute_group>(){};
    @JsonProperty("pag_last_updated_timestamp")
    private Long pag_last_updated_timestamp;
    @JsonProperty("product_attribute_mapping")
    private List<Product_attribute_mapping> product_attribute_mapping = new ArrayList<Product_attribute_mapping>();
    @JsonProperty("pam_last_updated_timestamp")
    private Long pam_last_updated_timestamp;
    @JsonProperty("outlet_id")
    private Long outlet_id;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("outlet")
    public List<Outlet> getOutlet() {
        return outlet;
    }

    @JsonProperty("outlet")
    public void setOutlet(List<Outlet> outlet) {
        this.outlet = outlet;
    }

    public OutletMenuData withOutlet(List<Outlet> outlet) {
        this.outlet = outlet;
        return this;
    }

    @JsonProperty("outlet_last_updated_timestamp")
    public Long getOutlet_last_updated_timestamp() {
        return outlet_last_updated_timestamp;
    }

    @JsonProperty("outlet_last_updated_timestamp")
    public void setOutlet_last_updated_timestamp(Long outlet_last_updated_timestamp) {
        this.outlet_last_updated_timestamp = outlet_last_updated_timestamp;
    }

    public OutletMenuData withOutlet_last_updated_timestamp(Long outlet_last_updated_timestamp) {
        this.outlet_last_updated_timestamp = outlet_last_updated_timestamp;
        return this;
    }

    @JsonProperty("categories")
    public List<Category> getCategories() {
        return categories;
    }

    @JsonProperty("categories")
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public OutletMenuData withCategories(List<Category> categories) {
        this.categories = categories;
        return this;
    }

    @JsonProperty("categories_last_updated_timestamp")
    public Long getCategories_last_updated_timestamp() {
        return categories_last_updated_timestamp;
    }

    @JsonProperty("categories_last_updated_timestamp")
    public void setCategories_last_updated_timestamp(Long categories_last_updated_timestamp) {
        this.categories_last_updated_timestamp = categories_last_updated_timestamp;
    }

    public OutletMenuData withCategories_last_updated_timestamp(Long categories_last_updated_timestamp) {
        this.categories_last_updated_timestamp = categories_last_updated_timestamp;
        return this;
    }

    @JsonProperty("products")
    public List<Product> getProducts() {
        return products;
    }

    @JsonProperty("products")
    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public OutletMenuData withProducts(List<Product> products) {
        this.products = products;
        return this;
    }

    @JsonProperty("products_last_updated_timestamp")
    public Long getProducts_last_updated_timestamp() {
        return products_last_updated_timestamp;
    }

    @JsonProperty("products_last_updated_timestamp")
    public void setProducts_last_updated_timestamp(Long products_last_updated_timestamp) {
        this.products_last_updated_timestamp = products_last_updated_timestamp;
    }

    public OutletMenuData withProducts_last_updated_timestamp(Long products_last_updated_timestamp) {
        this.products_last_updated_timestamp = products_last_updated_timestamp;
        return this;
    }

    @JsonProperty("outlet_product_mapping")
    public List<Outlet_product_mapping> getOutlet_product_mapping() {
        return outlet_product_mapping;
    }

    @JsonProperty("outlet_product_mapping")
    public void setOutlet_product_mapping(List<Outlet_product_mapping> outlet_product_mapping) {
        this.outlet_product_mapping = outlet_product_mapping;
    }

    public OutletMenuData withOutlet_product_mapping(List<Outlet_product_mapping> outlet_product_mapping) {
        this.outlet_product_mapping = outlet_product_mapping;
        return this;
    }

    @JsonProperty("opm_last_updated_timestamp")
    public Long getOpm_last_updated_timestamp() {
        return opm_last_updated_timestamp;
    }

    @JsonProperty("opm_last_updated_timestamp")
    public void setOpm_last_updated_timestamp(Long opm_last_updated_timestamp) {
        this.opm_last_updated_timestamp = opm_last_updated_timestamp;
    }

    public OutletMenuData withOpm_last_updated_timestamp(Long opm_last_updated_timestamp) {
        this.opm_last_updated_timestamp = opm_last_updated_timestamp;
        return this;
    }

    @JsonProperty("product_category_mapping")
    public List<Product_category_mapping> getProduct_category_mapping() {
        return product_category_mapping;
    }

    @JsonProperty("product_category_mapping")
    public void setProduct_category_mapping(List<Product_category_mapping> product_category_mapping) {
        this.product_category_mapping = product_category_mapping;
    }

    public OutletMenuData withProduct_category_mapping(List<Product_category_mapping> product_category_mapping) {
        this.product_category_mapping = product_category_mapping;
        return this;
    }

    @JsonProperty("pcm_last_updated_timestamp")
    public Long getPcm_last_updated_timestamp() {
        return pcm_last_updated_timestamp;
    }

    @JsonProperty("pcm_last_updated_timestamp")
    public void setPcm_last_updated_timestamp(Long pcm_last_updated_timestamp) {
        this.pcm_last_updated_timestamp = pcm_last_updated_timestamp;
    }

    public OutletMenuData withPcm_last_updated_timestamp(Long pcm_last_updated_timestamp) {
        this.pcm_last_updated_timestamp = pcm_last_updated_timestamp;
        return this;
    }

    @JsonProperty("product_attributes")
    public List<Product_attribute> getProduct_attributes() {
        return product_attributes;
    }

    @JsonProperty("product_attributes")
    public void setProduct_attributes(List<Product_attribute> product_attributes) {
        this.product_attributes = product_attributes;
    }

    public OutletMenuData withProduct_attributes(List<Product_attribute> product_attributes) {
        this.product_attributes = product_attributes;
        return this;
    }

    @JsonProperty("pa_last_updated_timestamp")
    public Long getPa_last_updated_timestamp() {
        return pa_last_updated_timestamp;
    }

    @JsonProperty("pa_last_updated_timestamp")
    public void setPa_last_updated_timestamp(Long pa_last_updated_timestamp) {
        this.pa_last_updated_timestamp = pa_last_updated_timestamp;
    }

    public OutletMenuData withPa_last_updated_timestamp(Long pa_last_updated_timestamp) {
        this.pa_last_updated_timestamp = pa_last_updated_timestamp;
        return this;
    }

    @JsonProperty("product_attribute_group")
    public List<Product_attribute_group> getProduct_attribute_group() {
        return product_attribute_group;
    }

    @JsonProperty("product_attribute_group")
    public void setProduct_attribute_group(List<Product_attribute_group> product_attribute_group) {
        this.product_attribute_group = product_attribute_group;
    }

    public OutletMenuData withProduct_attribute_group(List<Product_attribute_group> product_attribute_group) {
        this.product_attribute_group = product_attribute_group;
        return this;
    }

    @JsonProperty("pag_last_updated_timestamp")
    public Long getPag_last_updated_timestamp() {
        return pag_last_updated_timestamp;
    }

    @JsonProperty("pag_last_updated_timestamp")
    public void setPag_last_updated_timestamp(Long pag_last_updated_timestamp) {
        this.pag_last_updated_timestamp = pag_last_updated_timestamp;
    }

    public OutletMenuData withPag_last_updated_timestamp(Long pag_last_updated_timestamp) {
        this.pag_last_updated_timestamp = pag_last_updated_timestamp;
        return this;
    }

    @JsonProperty("product_attribute_mapping")
    public List<Product_attribute_mapping> getProduct_attribute_mapping() {
        return product_attribute_mapping;
    }

    @JsonProperty("product_attribute_mapping")
    public void setProduct_attribute_mapping(List<Product_attribute_mapping> product_attribute_mapping) {
        this.product_attribute_mapping = product_attribute_mapping;
    }

    public OutletMenuData withProduct_attribute_mapping(List<Product_attribute_mapping> product_attribute_mapping) {
        this.product_attribute_mapping = product_attribute_mapping;
        return this;
    }

    @JsonProperty("pam_last_updated_timestamp")
    public Long getPam_last_updated_timestamp() {
        return pam_last_updated_timestamp;
    }

    @JsonProperty("pam_last_updated_timestamp")
    public void setPam_last_updated_timestamp(Long pam_last_updated_timestamp) {
        this.pam_last_updated_timestamp = pam_last_updated_timestamp;
    }

    public OutletMenuData withPam_last_updated_timestamp(Long pam_last_updated_timestamp) {
        this.pam_last_updated_timestamp = pam_last_updated_timestamp;
        return this;
    }

    @JsonProperty("outlet_id")
    public Long getOutlet_id() {
        return outlet_id;
    }

    @JsonProperty("outlet_id")
    public void setOutlet_id(Long outlet_id) {
        this.outlet_id = outlet_id;
    }

    public OutletMenuData withOutlet_id(Long outlet_id) {
        this.outlet_id = outlet_id;
        return this;
    }



    	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
