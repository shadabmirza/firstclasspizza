
package to.done.lib.entity.outletmenu;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)

@JsonPropertyOrder({
    "id",
    "outlet_id",
    "product_id",
    "parent_outlet_product_id",
    "price",
    "sort_no",
    "default_selected",
    "row_status",
    "created_at",
    "modified_at"
})
@DatabaseTable
public class Outlet_product_mapping {

    @JsonProperty("id")
    @DatabaseField(id = true)
    private Long id;
    @JsonProperty("outlet_id")
    @DatabaseField(index = true)
    private Long outlet_id;
    @JsonProperty("product_id")
    @DatabaseField(index = true)
    private Long product_id;
    @JsonProperty("parent_outlet_product_id")
    @DatabaseField(index = true)
    private Long parent_outlet_product_id;
    @JsonProperty("price")
    @DatabaseField
    private Double price;
    @JsonProperty("sort_no")
    @DatabaseField
    private Long sort_no;
    @JsonProperty("default_selected")
    @DatabaseField
    private Long default_selected;
    @JsonProperty("row_status")
    @DatabaseField
    private String row_status;
    @JsonProperty("created_at")
    @DatabaseField
    private String created_at;
    @JsonProperty("modified_at")
    @DatabaseField
    private String modified_at;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    public Outlet_product_mapping withId(Long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("outlet_id")
    public Long getOutlet_id() {
        return outlet_id;
    }

    @JsonProperty("outlet_id")
    public void setOutlet_id(Long outlet_id) {
        this.outlet_id = outlet_id;
    }

    public Outlet_product_mapping withOutlet_id(Long outlet_id) {
        this.outlet_id = outlet_id;
        return this;
    }

    @JsonProperty("product_id")
    public Long getProduct_id() {
        return product_id;
    }

    @JsonProperty("product_id")
    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public Outlet_product_mapping withProduct_id(Long product_id) {
        this.product_id = product_id;
        return this;
    }

    @JsonProperty("parent_outlet_product_id")
    public Long getParent_outlet_product_id() {
        return parent_outlet_product_id;
    }

    @JsonProperty("parent_outlet_product_id")
    public void setParent_outlet_product_id(Long parent_outlet_product_id) {
        this.parent_outlet_product_id = parent_outlet_product_id;
    }

    public Outlet_product_mapping withParent_outlet_product_id(Long parent_outlet_product_id) {
        this.parent_outlet_product_id = parent_outlet_product_id;
        return this;
    }

    @JsonProperty("price")
    public Double getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Double price) {
        this.price = price;
    }

    public Outlet_product_mapping withPrice(Double price) {
        this.price = price;
        return this;
    }

    @JsonProperty("sort_no")
    public Long getSort_no() {
        return sort_no;
    }

    @JsonProperty("sort_no")
    public void setSort_no(Long sort_no) {
        this.sort_no = sort_no;
    }

    public Outlet_product_mapping withSort_no(Long sort_no) {
        this.sort_no = sort_no;
        return this;
    }

    @JsonProperty("default_selected")
    public Long getDefault_selected() {
        return default_selected;
    }

    @JsonProperty("default_selected")
    public void setDefault_selected(Long default_selected) {
        this.default_selected = default_selected;
    }

    public Outlet_product_mapping withDefault_selected(Long default_selected) {
        this.default_selected = default_selected;
        return this;
    }

    @JsonProperty("row_status")
    public String getRow_status() {
        return row_status;
    }

    @JsonProperty("row_status")
    public void setRow_status(String row_status) {
        this.row_status = row_status;
    }

    public Outlet_product_mapping withRow_status(String row_status) {
        this.row_status = row_status;
        return this;
    }

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Outlet_product_mapping withCreated_at(String created_at) {
        this.created_at = created_at;
        return this;
    }

    @JsonProperty("modified_at")
    public String getModified_at() {
        return modified_at;
    }

    @JsonProperty("modified_at")
    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    public Outlet_product_mapping withModified_at(String modified_at) {
        this.modified_at = modified_at;
        return this;
    }



    	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
