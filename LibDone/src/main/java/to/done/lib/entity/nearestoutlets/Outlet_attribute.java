
package to.done.lib.entity.nearestoutlets;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "created_at",
    "modified_at",
    "name",
    "value_type",
    "outlet_attributes_group_id"
})
@DatabaseTable
public class Outlet_attribute {

    @JsonProperty("id")
    @DatabaseField(id = true)
	private Long id;
    @JsonProperty("created_at")
    @DatabaseField
	private String created_at;
    @JsonProperty("modified_at")
    @DatabaseField
	private String modified_at;
    @JsonProperty("name")
    @DatabaseField
	private String name;
    @JsonProperty("value_type")
    @DatabaseField
	private String value_type;
    @JsonProperty("outlet_attributes_group_id")
    @DatabaseField
	private Long outlet_attributes_group_id;

    @JsonIgnore
    private int outlets_count;

    @JsonIgnore
    private boolean selected;

	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    public Outlet_attribute withId(Long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Outlet_attribute withCreated_at(String created_at) {
        this.created_at = created_at;
        return this;
    }

    @JsonProperty("modified_at")
    public String getModified_at() {
        return modified_at;
    }

    @JsonProperty("modified_at")
    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    public Outlet_attribute withModified_at(String modified_at) {
        this.modified_at = modified_at;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Outlet_attribute withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("value_type")
    public String getValue_type() {
        return value_type;
    }

    @JsonProperty("value_type")
    public void setValue_type(String value_type) {
        this.value_type = value_type;
    }

    public Outlet_attribute withValue_type(String value_type) {
        this.value_type = value_type;
        return this;
    }

    @JsonProperty("outlet_attributes_group_id")
    public Long getOutlet_attributes_group_id() {
        return outlet_attributes_group_id;
    }

    @JsonProperty("outlet_attributes_group_id")
    public void setOutlet_attributes_group_id(Long outlet_attributes_group_id) {
        this.outlet_attributes_group_id = outlet_attributes_group_id;
    }

    public Outlet_attribute withOutlet_attributes_group_id(Long outlet_attributes_group_id) {
        this.outlet_attributes_group_id = outlet_attributes_group_id;
        return this;
    }

    @JsonIgnore
    public int getOutlets_count() {
        return outlets_count;
    }

    @JsonIgnore
    public void setOutlets_count(int outlets_count) {
        this.outlets_count = outlets_count;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
