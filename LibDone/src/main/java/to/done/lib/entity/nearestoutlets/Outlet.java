
package to.done.lib.entity.nearestoutlets;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)

@JsonPropertyOrder({
        "company_id",
        "id",
        "name",
        "description",
        "code",
        "row_status",
        "created_at",
        "modified_at",
        "min_order_amnt",
        "taxes",
        "enable_push_notification",
        "enable_email",
        "enable_call",
        "enable_sms",
        "delivery_type",
        "address_id",
        "lat",
        "lon",
        "delivery_time",
        "service_type",
        "address_string",
        "rating",
        "distance",
        "company_outlet_name"
})
@DatabaseTable
public class Outlet {

    @JsonProperty("company_id")
    @DatabaseField
    private Long company_id;

    @JsonProperty("id")
    @DatabaseField(id = true)
    private Long id;

    @JsonProperty("name")
    @DatabaseField
    private String name;

    @JsonProperty("description")
    @DatabaseField
    private String description;

    @JsonProperty("code")
    @DatabaseField
    private String code;

    @JsonProperty("row_status")
    @DatabaseField
    private String row_status;

    @JsonProperty("created_at")
    @DatabaseField
    private String created_at;

    @JsonProperty("modified_at")
    @DatabaseField
    private String modified_at;

    @JsonProperty("min_order_amnt")
    @DatabaseField
    private Float min_order_amnt;

    @JsonProperty("taxes")
    @DatabaseField
    private Float taxes;

    @JsonProperty("enable_push_notification")
    @DatabaseField
    private Long enable_push_notification;

    @JsonProperty("enable_email")
    @DatabaseField
    private Long enable_email;

    @JsonProperty("enable_call")
    @DatabaseField
    private Long enable_call;

    @JsonProperty("enable_sms")
    @DatabaseField
    private Long enable_sms;

    @JsonProperty("delivery_type")
    @DatabaseField
    private String delivery_type;

    @JsonProperty("address_id")
    @DatabaseField
    private Long address_id;

    @JsonProperty("lat")
    @DatabaseField
    private Float lat;

    @JsonProperty("lon")
    @DatabaseField
    private Float lon;

    @JsonProperty("delivery_time")
    @DatabaseField
    private Float delivery_time;

    @JsonProperty("service_type")
    @DatabaseField
    private String service_type;

    @JsonProperty("address_string")
    @DatabaseField
    private String address_string;

    @JsonProperty("rating")
    @DatabaseField
    private Float rating;

    @JsonProperty("distance")
    @DatabaseField
    private Float distance;

    @JsonProperty("company_outlet_name")
    @DatabaseField
    private String company_outlet_name;

    @JsonIgnore
    private Float distanceReal;

    @JsonIgnore
    private boolean outletOpen;

    @JsonIgnore
    private double outDist;

    public double getOutDist() {
        return outDist;
    }

    public void setOutDist(double outDist) {
        this.outDist = outDist;
    }

    public static enum SortType {
        DISTANCE(0, "Distance"), RATING(1, "Rating");

        private final long id;
        private final String name;

        private SortType(long id, String name) {
            this.id = id;
            this.name = name;
        }

        public long getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

    ;

    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("company_id")
    public Long getCompany_id() {
        return company_id;
    }

    @JsonProperty("company_id")
    public void setCompany_id(Long company_id) {
        this.company_id = company_id;
    }

    public Outlet withCompany_id(Long company_id) {
        this.company_id = company_id;
        return this;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    public Outlet withId(Long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Outlet withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public Outlet withDescription(String description) {
        this.description = description;
        return this;
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    public Outlet withCode(String code) {
        this.code = code;
        return this;
    }

    @JsonProperty("row_status")
    public String getRow_status() {
        return row_status;
    }

    @JsonProperty("row_status")
    public void setRow_status(String row_status) {
        this.row_status = row_status;
    }

    public Outlet withRow_status(String row_status) {
        this.row_status = row_status;
        return this;
    }

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Outlet withCreated_at(String created_at) {
        this.created_at = created_at;
        return this;
    }

    @JsonProperty("modified_at")
    public String getModified_at() {
        return modified_at;
    }

    @JsonProperty("modified_at")
    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    public Outlet withModified_at(String modified_at) {
        this.modified_at = modified_at;
        return this;
    }

    @JsonProperty("min_order_amnt")
    public Float getMin_order_amnt() {
        return min_order_amnt;
    }

    @JsonProperty("min_order_amnt")
    public void setMin_order_amnt(Float min_order_amnt) {
        this.min_order_amnt = min_order_amnt;
    }

    public Outlet withMin_order_amnt(Float min_order_amnt) {
        this.min_order_amnt = min_order_amnt;
        return this;
    }

    @JsonProperty("taxes")
    public Float getTaxes() {
        return taxes;
    }

    @JsonProperty("taxes")
    public void setTaxes(Float taxes) {
        this.taxes = taxes;
    }

    public Outlet withTaxes(Float taxes) {
        this.taxes = taxes;
        return this;
    }

    @JsonProperty("enable_push_notification")
    public Long getEnable_push_notification() {
        return enable_push_notification;
    }

    @JsonProperty("enable_push_notification")
    public void setEnable_push_notification(Long enable_push_notification) {
        this.enable_push_notification = enable_push_notification;
    }

    public Outlet withEnable_push_notification(Long enable_push_notification) {
        this.enable_push_notification = enable_push_notification;
        return this;
    }

    @JsonProperty("enable_email")
    public Long getEnable_email() {
        return enable_email;
    }

    @JsonProperty("enable_email")
    public void setEnable_email(Long enable_email) {
        this.enable_email = enable_email;
    }

    public Outlet withEnable_email(Long enable_email) {
        this.enable_email = enable_email;
        return this;
    }

    @JsonProperty("enable_call")
    public Long getEnable_call() {
        return enable_call;
    }

    @JsonProperty("enable_call")
    public void setEnable_call(Long enable_call) {
        this.enable_call = enable_call;
    }

    public Outlet withEnable_call(Long enable_call) {
        this.enable_call = enable_call;
        return this;
    }

    @JsonProperty("enable_sms")
    public Long getEnable_sms() {
        return enable_sms;
    }

    @JsonProperty("enable_sms")
    public void setEnable_sms(Long enable_sms) {
        this.enable_sms = enable_sms;
    }

    public Outlet withEnable_sms(Long enable_sms) {
        this.enable_sms = enable_sms;
        return this;
    }

    @JsonProperty("delivery_type")
    public String getDelivery_type() {
        return delivery_type;
    }

    @JsonProperty("delivery_type")
    public void setDelivery_type(String delivery_type) {
        this.delivery_type = delivery_type;
    }

    public Outlet withDelivery_type(String delivery_type) {
        this.delivery_type = delivery_type;
        return this;
    }

    @JsonProperty("address_id")
    public Long getAddress_id() {
        return address_id;
    }

    @JsonProperty("address_id")
    public void setAddress_id(Long address_id) {
        this.address_id = address_id;
    }

    public Outlet withAddress_id(Long address_id) {
        this.address_id = address_id;
        return this;
    }

    @JsonProperty("lat")
    public Float getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Outlet withLat(Float lat) {
        this.lat = lat;
        return this;
    }

    @JsonProperty("lon")
    public Float getLon() {
        return lon;
    }

    @JsonProperty("lon")
    public void setLon(Float lon) {
        this.lon = lon;
    }

    public Outlet withLon(Float lon) {
        this.lon = lon;
        return this;
    }

    @JsonProperty("delivery_time")
    public Float getDelivery_time() {
        return delivery_time;
    }

    @JsonProperty("delivery_time")
    public void setDelivery_time(Float delivery_time) {
        this.delivery_time = delivery_time;
    }

    public Outlet withDelivery_time(Float delivery_time) {
        this.delivery_time = delivery_time;
        return this;
    }

    @JsonProperty("service_type")
    public String getService_type() {
        return service_type;
    }

    @JsonProperty("service_type")
    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public Outlet withService_type(String service_type) {
        this.service_type = service_type;
        return this;
    }

    @JsonProperty("address_string")
    public String getAddress_string() {
        return address_string;
    }

    @JsonProperty("address_string")
    public void setAddress_string(String address_string) {
        this.address_string = address_string;
    }

    public Outlet withAddress_string(String address_string) {
        this.address_string = address_string;
        return this;
    }

    @JsonProperty("rating")
    public Float getRating() {
        return rating;
    }

    @JsonProperty("rating")
    public void setRating(Float rating) {
        this.rating = rating;
    }

    public Outlet withRating(Float rating) {
        this.rating = rating;
        return this;
    }

    @JsonProperty("distance")
    public Float getDistance() {
        return distance;
    }

    @JsonProperty("distance")
    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public Outlet withDistance(Float distance) {
        this.distance = distance;
        return this;
    }

    @JsonProperty("company_outlet_name")
    public String getCompany_outlet_name() {
        return company_outlet_name;
    }

    @JsonProperty("company_outlet_name")
    public void setCompany_outlet_name(String company_outlet_name) {
        this.company_outlet_name = company_outlet_name;
    }

    public Outlet withCompany_outlet_name(String company_outlet_name) {
        this.company_outlet_name = company_outlet_name;
        return this;
    }

    @JsonIgnore
    public Float getDistanceReal() {
        return distanceReal;
    }

    @JsonIgnore
    public void setDistanceReal(Float distanceReal) {
        this.distanceReal = distanceReal;
    }

    @JsonIgnore
    public boolean isOutletOpen() {
        return outletOpen;
    }

    @JsonIgnore
    public void setOutletOpen(boolean outletOpen) {

        this.outletOpen = outletOpen;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return name;
    }
}
