
package to.done.lib.entity.outletmenu;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name",
    "value_type",
    "row_status",
    "product_attribute_group_id",
    "created_at",
    "modified_at"
})
@DatabaseTable
public class Product_attribute {

    @JsonProperty("id")
    @DatabaseField(id = true)
	private Long id;
    @JsonProperty("name")
    @DatabaseField
	private String name;
    @JsonProperty("value_type")
    @DatabaseField
	private String value_type;
    @JsonProperty("row_status")
    @DatabaseField
	private String row_status;
    @JsonProperty("product_attribute_group_id")
    @DatabaseField
	private Long product_attribute_group_id;
    @JsonProperty("created_at")
    @DatabaseField
	private String created_at;
    @JsonProperty("modified_at")
    @DatabaseField
	private String modified_at;
    @DatabaseField
    @JsonProperty("photo_url")
    private String photo_url;
    @DatabaseField
    @JsonProperty("sr")
    private Integer sr;

    public Integer getDisplay_type() {
        return display_type;
    }

    public void setDisplay_type(Integer display_type) {
        this.display_type = display_type;
    }

    public Integer getSr() {
        return sr;
    }

    public void setSr(Integer sr) {
        this.sr = sr;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    @DatabaseField
    @JsonProperty("display_type")
    private Integer display_type;
    @JsonIgnore
    private int products_count;

    @JsonIgnore
    private boolean selected;

	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    public Product_attribute withId(Long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Product_attribute withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("value_type")
    public String getValue_type() {
        return value_type;
    }

    @JsonProperty("value_type")
    public void setValue_type(String value_type) {
        this.value_type = value_type;
    }

    public Product_attribute withValue_type(String value_type) {
        this.value_type = value_type;
        return this;
    }

    @JsonProperty("row_status")
    public String getRow_status() {
        return row_status;
    }

    @JsonProperty("row_status")
    public void setRow_status(String row_status) {
        this.row_status = row_status;
    }

    public Product_attribute withRow_status(String row_status) {
        this.row_status = row_status;
        return this;
    }

    @JsonProperty("product_attribute_group_id")
    public Long getProduct_attribute_group_id() {
        return product_attribute_group_id;
    }

    @JsonProperty("product_attribute_group_id")
    public void setProduct_attribute_group_id(Long product_attribute_group_id) {
        this.product_attribute_group_id = product_attribute_group_id;
    }

    public Product_attribute withProduct_attribute_group_id(Long product_attribute_group_id) {
        this.product_attribute_group_id = product_attribute_group_id;
        return this;
    }

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Product_attribute withCreated_at(String created_at) {
        this.created_at = created_at;
        return this;
    }

    @JsonProperty("modified_at")
    public String getModified_at() {
        return modified_at;
    }

    @JsonProperty("modified_at")
    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    public Product_attribute withModified_at(String modified_at) {
        this.modified_at = modified_at;
        return this;
    }
    @JsonIgnore
    public boolean isSelected() {
        return selected;
    }
    @JsonIgnore
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @JsonIgnore
    public int getProducts_count() {
        return products_count;
    }

    @JsonIgnore
    public void setProducts_count(int products_count) {
        this.products_count = products_count;
    }

    @Override
    public String toString() {
        return id+":"+name+"_"+display_type+"_"+photo_url;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
