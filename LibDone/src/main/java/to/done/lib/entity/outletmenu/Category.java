
package to.done.lib.entity.outletmenu;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "parent_category_id",
    "name",
    "description",
    "photo_url",
    "row_status",
    "selection_min",
    "selection_max",
    "display",
    "created_at",
    "modified_at",
    "company_id"
})
@DatabaseTable
public class Category {

    @JsonProperty("id")
    @DatabaseField(id = true)
	private Long id;
    @JsonProperty("parent_category_id")
    @DatabaseField
	private Long parent_category_id;
    @JsonProperty("name")
    @DatabaseField
	private String name;
    @JsonProperty("description")
    @DatabaseField
	private String description;
    @JsonProperty("photo_url")
    @DatabaseField
	private String photo_url;
    @JsonProperty("row_status")
    @DatabaseField
	private String row_status;
    @JsonProperty("selection_min")
    @DatabaseField
	private Long selection_min=0l;
    @JsonProperty("selection_max")
    @DatabaseField
	private Long selection_max=-1l;
    @JsonProperty("display" )
    @DatabaseField
	private String display;
    @JsonProperty("created_at")
    @DatabaseField
	private String created_at;
    @JsonProperty("modified_at")
    @DatabaseField
	private String modified_at;
    @JsonProperty("company_id")
    @DatabaseField
	private Long company_id;

    @JsonProperty("sr")
    @DatabaseField
    private Integer sr;

    @JsonProperty("pos_name")
    private String pos_name;
    @JsonProperty("sr")
    public Integer getSr() {
        return sr;
    }
    @JsonProperty("sr")
    public void setSr(Integer sr) {
        this.sr = sr;
    }

    public String getPos_name() {
        return pos_name;
    }

    public void setPos_name(String pos_name) {
        this.pos_name = pos_name;
    }

    @JsonIgnore
    private int products_count;

    private List<Product>productList;

	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    public Category withId(Long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("parent_category_id")
    public Long getParent_category_id() {
        return parent_category_id;
    }

    @JsonProperty("parent_category_id")
    public void setParent_category_id(Long parent_category_id) {
        this.parent_category_id = parent_category_id;
    }

    public Category withParent_category_id(Long parent_category_id) {
        this.parent_category_id = parent_category_id;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Category withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("description")
    public String  getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public Category withDescription(String description) {
        this.description = description;
        return this;
    }

    @JsonProperty("photo_url")
    public String getPhoto_url() {
        return photo_url;
    }

    @JsonProperty("photo_url")
    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public Category withPhoto_url(String photo_url) {
        this.photo_url = photo_url;
        return this;
    }

    @JsonProperty("row_status")
    public String getRow_status() {
        return row_status;
    }

    @JsonProperty("row_status")
    public void setRow_status(String row_status) {
        this.row_status = row_status;
    }

    public Category withRow_status(String row_status) {
        this.row_status = row_status;
        return this;
    }

    @JsonProperty("selection_min")
    public Long getSelection_min() {
        return selection_min;
    }

    @JsonProperty("selection_min")
    public void setSelection_min(Long selection_min) {
        this.selection_min = selection_min;
    }

    public Category withSelection_min(Long selection_min) {
        this.selection_min = selection_min;
        return this;
    }

    @JsonProperty("selection_max")
    public Long getSelection_max() {
        return selection_max;
    }

    @JsonProperty("selection_max")
    public void setSelection_max(Long selection_max) {
        this.selection_max = selection_max;
    }

    public Category withSelection_max(Long selection_max) {
        this.selection_max = selection_max;
        return this;
    }

    @JsonProperty("display")
    public String getDisplay() {
        return display;
    }

    @JsonProperty("display")
    public void setDisplay(String display) {
        this.display = display;
    }

    public Category withDisplay(String display) {
        this.display = display;
        return this;
    }

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Category withCreated_at(String created_at) {
        this.created_at = created_at;
        return this;
    }

    @JsonProperty("modified_at")
    public String getModified_at() {
        return modified_at;
    }

    @JsonProperty("modified_at")
    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    public Category withModified_at(String modified_at) {
        this.modified_at = modified_at;
        return this;
    }

    @JsonProperty("company_id")
    public Long getCompany_id() {
        return company_id;
    }

    @JsonProperty("company_id")
    public void setCompany_id(Long company_id) {
        this.company_id = company_id;
    }

    public Category withCompany_id(Long company_id) {
        this.company_id = company_id;
        return this;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    @JsonIgnore
    public int getProducts_count() {
        return products_count;
    }
    @JsonIgnore
    public void setProducts_count(int products_count) {
        this.products_count = products_count;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
