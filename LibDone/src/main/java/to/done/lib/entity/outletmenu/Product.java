
package to.done.lib.entity.outletmenu;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import to.done.lib.Constants;
import to.done.lib.entity.order.Customization;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "code",
        "photo_url",
        "row_status",
        "created_at",
        "modified_at",
        "company_id",
        "price"
})
@DatabaseTable
public class Product implements Serializable {

    @JsonProperty("id")
    @DatabaseField(id = true)
    private Long id;
    @JsonProperty("code")
    @DatabaseField
    private String code;
    //    @JsonProperty("name")
    @JsonIgnore
    @DatabaseField
    private String name;
    //    @JsonProperty("description")
    @JsonIgnore
    @DatabaseField
    private String description;
    @JsonProperty("photo_url")
    @DatabaseField
    private String photo_url;
    @JsonProperty("row_status")
    @DatabaseField
    private String row_status;
    @JsonProperty("created_at")
    @DatabaseField
    private String created_at;
    @JsonProperty("modified_at")
    @DatabaseField
    private String modified_at;
    @JsonProperty("company_id")
    @DatabaseField
    private Long company_id;
    @JsonProperty("price")
//    @JsonDeserialize(using = NumberDeserializers.BigDecimalDeserializer.class)
    @DatabaseField
    private Double price;
    @JsonProperty("pos_name")
    private String pos_name;
    @JsonIgnore
    boolean shownInList;
    @JsonProperty("sr")
    private Integer sr;
    @JsonProperty("comments_allowed")
    private Integer comments_allowed;

    @JsonProperty("comments")
    String comments;

    @JsonProperty("comments")
    public String getComments() {
        return comments;
    }

    @JsonProperty("comments")
    public void setComments(String comments) {
        this.comments = comments;
    }

    private Double price_with_customization;

    private Long outlet_product_id;
    private Long outlet_id;
    private Long parent_outlet_product_id;
    private Long sort_no;
    private Long default_selected;

    private Long default_outlet_product_id;
    private Long default_product_id;
    private Long default_parent_outlet_product_id;

    private Double default_price;
    private String default_name;
    private String default_description;
    private Integer cust_count;

    @JsonIgnore
    public boolean isVeg, isNonVeg, isSpicy, isStar, isFav, isSignature, isSupreme, isClassic;
    @JsonIgnore
    private List<Product_attribute> attributesForProduct = null;

    public List<Product_attribute> getAttributesForProduct() {
        return attributesForProduct;
    }

    public void setAttributesForProduct(List<Product_attribute> attributesForProduct) {
        this.attributesForProduct = attributesForProduct;
        setAttributeBooleans();
    }

    private void setAttributeBooleans() {
        for (Product_attribute pa : this.getAttributesForProduct()) {

            if (pa.getId() == Constants.ATTRIBUTE_ID_VEG) {
                isVeg = true;
            } else if (pa.getId() == Constants.ATTRIBUTE_ID_NONVEG) {
                isNonVeg = true;
            } else if (pa.getId() == Constants.ATTRIBUTE_ID_FAV) {
                isFav = true;
            } else if (pa.getId() == Constants.ATTRIBUTE_ID_CLASSIC) {
                isClassic = true;
            } else if (pa.getId() == Constants.ATTRIBUTE_ID_SIGNATURE) {
                isSignature = true;
            } else if (pa.getId() == Constants.ATTRIBUTE_ID_SUPREME) {
                isSupreme = true;
            } else if (pa.getId() == Constants.ATTRIBUTE_ID_STAR) {
                isStar = true;
            } else if (pa.getId() == Constants.ATTRIBUTE_ID_SPICY) {
                isSpicy = true;
            }
        }
    }

    public static enum SortType {
        PRICE(0, "Price"), ALPHABETICAL(1, "Alphabetical"), CATEGORY(2, "Category");

        private final long id;
        private final String name;

        private SortType(long id, String name) {
            this.id = id;
            this.name = name;
        }

        public long getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

    ;

    private List<Customization> customization = new ArrayList<Customization>();

    @JsonIgnore
    private transient Map<String, Object> additionalProperties = new HashMap<String, Object>();

    private boolean selected = false;
    private Long category_id;

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    public Product withId(Long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    public Product withCode(String code) {
        this.code = code;
        return this;
    }

    //    @JsonProperty("name")
    @JsonIgnore
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Product withName(String name) {
        this.name = name;
        return this;
    }

    @JsonIgnore
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public Product withDescription(String description) {
        this.description = description;
        return this;
    }

    @JsonProperty("photo_url")
    public String getPhoto_url() {
        return photo_url;
    }

    @JsonProperty("photo_url")
    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public Product withPhoto_url(String photo_url) {
        this.photo_url = photo_url;
        return this;
    }

    @JsonProperty("row_status")
    public String getRow_status() {
        return row_status;
    }

    @JsonProperty("row_status")
    public void setRow_status(String row_status) {
        this.row_status = row_status;
    }

    public Product withRow_status(String row_status) {
        this.row_status = row_status;
        return this;
    }

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Product withCreated_at(String created_at) {
        this.created_at = created_at;
        return this;
    }

    @JsonProperty("modified_at")
    public String getModified_at() {
        return modified_at;
    }

    @JsonProperty("modified_at")
    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    public Product withModified_at(String modified_at) {
        this.modified_at = modified_at;
        return this;
    }

    @JsonProperty("company_id")
    public Long getCompany_id() {
        return company_id;
    }

    @JsonProperty("company_id")
    public void setCompany_id(Long company_id) {
        this.company_id = company_id;
    }

    public Product withCompany_id(Long company_id) {
        this.company_id = company_id;
        return this;
    }

    @JsonProperty("price")
    public Double getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Double price) {
        this.price = price;
    }

    public Product withPrice(Double price) {
        this.price = price;
        return this;
    }

    public List<Customization> getCustomization() {

//        if(customization!=null && customization.size()==0){
//
//            customization.add(new Customization(default_outlet_product_id,default_product_id,default_parent_outlet_product_id,default_price));
////            price=default_price.doubleValue();
//        }

        return customization;
    }

    public void setCustomization(List<Customization> customization) {
        this.customization = customization;
    }


    @JsonIgnore
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonIgnore
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Long getOutlet_product_id() {
        return outlet_product_id;
    }

    public void setOutlet_product_id(Long outlet_product_id) {
        this.outlet_product_id = outlet_product_id;
    }

    public Long getOutlet_id() {
        return outlet_id;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", photo_url='" + photo_url + '\'' +
                ", row_status='" + row_status + '\'' +
                ", company_id=" + company_id +
                ", price=" + price +
                ", pos_name='" + pos_name + '\'' +
                ", sr=" + sr +
                ", comments_allowed=" + comments_allowed +
                ", comments='" + comments + '\'' +
                ", price_with_customization=" + price_with_customization +
                ", outlet_product_id=" + outlet_product_id +
                ", outlet_id=" + outlet_id +
                ", parent_outlet_product_id=" + parent_outlet_product_id +
                ", customization=" + customization +
                ", selected=" + selected +
                ", category_id=" + category_id +
                '}';
    }

    public void setOutlet_id(Long outlet_id) {
        this.outlet_id = outlet_id;
    }

    public Long getParent_outlet_product_id() {
        return parent_outlet_product_id;
    }

    public void setParent_outlet_product_id(Long parent_outlet_product_id) {
        this.parent_outlet_product_id = parent_outlet_product_id;
    }

    public Long getSort_no() {
        return sort_no;
    }

    public void setSort_no(Long sort_no) {
        this.sort_no = sort_no;
    }

    public Long getDefault_selected() {
        return default_selected;
    }

    public void setDefault_selected(Long default_selected) {
        this.default_selected = default_selected;
    }


    public Long getDefault_outlet_product_id() {
        return default_outlet_product_id;
    }

    public void setDefault_outlet_product_id(Long default_outlet_product_id) {
        this.default_outlet_product_id = default_outlet_product_id;
    }

    public Long getDefault_parent_outlet_product_id() {
        return default_parent_outlet_product_id;
    }

    public void setDefault_parent_outlet_product_id(Long default_parent_outlet_product_id) {
        this.default_parent_outlet_product_id = default_parent_outlet_product_id;
    }

    public Double getDefault_price() {
        return default_price;
    }

    public void setDefault_price(Double default_price) {
        this.default_price = default_price;
    }

    public Long getDefault_product_id() {
        return default_product_id;
    }

    public void setDefault_product_id(Long default_product_id) {
        this.default_product_id = default_product_id;
    }

    public String getDefault_name() {
        return default_name;
    }

    public void setDefault_name(String default_name) {
        this.default_name = default_name;
    }

    public String getDefault_description() {
        return default_description;
    }

    public void setDefault_description(String default_description) {
        this.default_description = default_description;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Long getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Long category_id) {
        this.category_id = category_id;
    }

    public Double getPrice_with_customization() {
        return price_with_customization;
    }

    public void setPrice_with_customization(Double price_with_customization) {
        this.price_with_customization = price_with_customization;
    }

    public Integer getSr() {
        return sr;
    }

    public void setSr(Integer sr) {
        this.sr = sr;
    }

    public Integer getComments_allowed() {
        return comments_allowed;
    }

    public void setComments_allowed(Integer comments_allowed) {
        this.comments_allowed = comments_allowed;
    }

    public String getPos_name() {
        return pos_name;
    }

    public void setPos_name(String pos_name) {
        this.pos_name = pos_name;
    }

    public boolean isShownInList() {
        return shownInList;
    }

    public void setShownInList(boolean shownInList) {
        this.shownInList = shownInList;
    }

    @JsonIgnore
    public Integer getCust_count() {
        return cust_count;
    }

    @JsonIgnore
    public void setCust_count(Integer cust_count) {
        this.cust_count = cust_count;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof Product)) return false;
//
//        Product product = (Product) o;
//
//        if (!outlet_product_id.equals(product.outlet_product_id)) return false;
//
//        else{
//            if(this.getCustomization().size()==((Product) o).getCustomization().size()){
//                return true;
//            }else return false;
//        }
////        return true;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        if (o == null) return false;
        Product product = (Product) o;

        if (!outlet_product_id.equals(product.outlet_product_id)) return false;

        else {
            int matchCount = 0;
            if (this.getCustomization().size() == ((Product) o).getCustomization().size()) {
                for (Customization c : this.getCustomization()) {

                    for (Customization c2 : ((Product) o).getCustomization()) {


                        if (c.getOutlet_product_id().longValue() == c2.getOutlet_product_id().longValue()) {
                            matchCount++;

                        }
                    }
                }


                int requiredMatchCount;
                if (this.getCustomization().size() > ((Product) o).getCustomization().size()) {
                    requiredMatchCount = this.getCustomization().size();
                } else {
                    requiredMatchCount = ((Product) o).getCustomization().size();
                }
                if (matchCount == requiredMatchCount)
                    return true;
                else return false;
            } else return false;
        }
//        return true;
    }

    @Override
    public int hashCode() {
        return outlet_product_id.hashCode();
    }
}
