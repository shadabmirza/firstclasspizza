package to.done.lib.entity.order;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by root on 11/6/14.
 */
@JsonPropertyOrder({
        "id",
        "type",
        "value",
        "name",
        "description",
        "created_at",
        "modified_at",
        "extra_charged_amt"
})
@DatabaseTable
public class Applied_extra_charge {
@DatabaseField(id = true)
    @JsonProperty("id")
    private Long id;
    @DatabaseField
    @JsonProperty("type")
    private String type;
    @DatabaseField
    @JsonProperty("value")
    private Double value;
    @DatabaseField
    @JsonProperty("name")
    private String name;
    @DatabaseField
    @JsonProperty("description")
    private String description;
    @DatabaseField
    @JsonProperty("created_at")
    private String created_at;
    @DatabaseField
    @JsonProperty("modified_at")
    private String modified_at;
    @DatabaseField
    @JsonProperty("extra_charged_amt")
    private Double extra_charged_amt;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("value")
    public Double getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(Double value) {
        this.value = value;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @JsonProperty("modified_at")
    public String getModified_at() {
        return modified_at;
    }

    @JsonProperty("modified_at")
    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    @JsonProperty("extra_charged_amt")
    public Double getExtra_charged_amt() {
        return extra_charged_amt;
    }

    @JsonProperty("extra_charged_amt")
    public void setExtra_charged_amt(Double extra_charged_amt) {
        this.extra_charged_amt = extra_charged_amt;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}