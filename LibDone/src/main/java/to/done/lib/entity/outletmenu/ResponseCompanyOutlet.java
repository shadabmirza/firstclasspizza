package to.done.lib.entity.outletmenu;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import to.done.lib.entity.nearestoutlets.OutletData;
import to.done.lib.entity.nearestoutlets.RespNearestOutlets;

@JsonInclude(JsonInclude.Include.NON_NULL)

@JsonPropertyOrder({
        "responseCode",
        "responseMsg",
        "data",
        "dataArray"
})
public class ResponseCompanyOutlet {

    @JsonProperty("responseCode")
    private Long responseCode;
    @JsonProperty("responseMsg")
    private String responseMsg;
    @JsonProperty("data")
    private OutletData data;
    @JsonProperty("dataArray")
    private Object dataArray;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("responseCode")
    public Long getResponseCode() {
        return responseCode;
    }

    @JsonProperty("responseCode")
    public void setResponseCode(Long responseCode) {
        this.responseCode = responseCode;
    }

    @JsonProperty("responseMsg")
    public String getResponseMsg() {
        return responseMsg;
    }

    @JsonProperty("responseMsg")
    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    @JsonProperty("data")
    public OutletData getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(OutletData data) {
        this.data = data;
    }

    @JsonProperty("dataArray")
    public Object getDataArray() {
        return dataArray;
    }

    @JsonProperty("dataArray")
    public void setDataArray(Object dataArray) {
        this.dataArray = dataArray;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}