
package to.done.lib.entity.nearestoutlets;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "outlets",
    "outlet_timing",
    "outlet_attributes",
    "outlet_attributes_mapping",
    "outlet_attributes_group"
})
public class OutletData {

    @JsonProperty("outlets")
    private List<Outlet> outlets = new ArrayList<Outlet>();
    @JsonProperty("outlet_timing")
    private List<Outlet_timing> outlet_timing = new ArrayList<Outlet_timing>();
    @JsonProperty("outlet_attributes")
    private List<Outlet_attribute> outlet_attributes = new ArrayList<Outlet_attribute>();
    @JsonProperty("outlet_attributes_mapping")
    private List<Outlet_attributes_mapping> outlet_attributes_mapping = new ArrayList<Outlet_attributes_mapping>();
    @JsonProperty("outlet_attributes_group")
    private List<Outlet_attributes_group> outlet_attributes_group = new ArrayList<Outlet_attributes_group>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("outlets")
    public List<Outlet> getOutlets() {
        return outlets;
    }

    @JsonProperty("outlets")
    public void setOutlets(List<Outlet> outlets) {
        this.outlets = outlets;
    }

    public OutletData withOutlets(List<Outlet> outlets) {
        this.outlets = outlets;
        return this;
    }

    @JsonProperty("outlet_timing")
    public List<Outlet_timing> getOutlet_timing() {
        return outlet_timing;
    }

    @JsonProperty("outlet_timing")
    public void setOutlet_timing(List<Outlet_timing> outlet_timing) {
        this.outlet_timing = outlet_timing;
    }

    public OutletData withOutlet_timing(List<Outlet_timing> outlet_timing) {
        this.outlet_timing = outlet_timing;
        return this;
    }

    @JsonProperty("outlet_attributes")
    public List<Outlet_attribute> getOutlet_attributes() {
        return outlet_attributes;
    }

    @JsonProperty("outlet_attributes")
    public void setOutlet_attributes(List<Outlet_attribute> outlet_attributes) {
        this.outlet_attributes = outlet_attributes;
    }

    public OutletData withOutlet_attributes(List<Outlet_attribute> outlet_attributes) {
        this.outlet_attributes = outlet_attributes;
        return this;
    }

    @JsonProperty("outlet_attributes_mapping")
    public List<Outlet_attributes_mapping> getOutlet_attributes_mapping() {
        return outlet_attributes_mapping;
    }

    @JsonProperty("outlet_attributes_mapping")
    public void setOutlet_attributes_mapping(List<Outlet_attributes_mapping> outlet_attributes_mapping) {
        this.outlet_attributes_mapping = outlet_attributes_mapping;
    }

    public OutletData withOutlet_attributes_mapping(List<Outlet_attributes_mapping> outlet_attributes_mapping) {
        this.outlet_attributes_mapping = outlet_attributes_mapping;
        return this;
    }

    @JsonProperty("outlet_attributes_group")
    public List<Outlet_attributes_group> getOutlet_attributes_group() {
        return outlet_attributes_group;
    }

    @JsonProperty("outlet_attributes_group")
    public void setOutlet_attributes_group(List<Outlet_attributes_group> outlet_attributes_group) {
        this.outlet_attributes_group = outlet_attributes_group;
    }

    public OutletData withOutlet_attributes_group(List<Outlet_attributes_group> outlet_attributes_group) {
        this.outlet_attributes_group = outlet_attributes_group;
        return this;
    }



    	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
