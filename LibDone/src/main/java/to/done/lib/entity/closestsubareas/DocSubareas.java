package to.done.lib.entity.closestsubareas;

/**
 * Created by shadab mirza on 11/4/14.
 */

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)

public class DocSubareas {

    @JsonProperty("city_id")
    private String city_id;
    @JsonProperty("id")
    private String id;
    @JsonProperty("city_name")
    private String city_name;
    @JsonProperty("area_id")
    private String area_id;
    @JsonProperty("subarea_name")
    private String subarea_name;
    @JsonProperty("area_name")
    private String area_name;
    @JsonProperty("state_id")
    private String state_id;
    @JsonProperty("state_name")
    private String state_name;
    @JsonProperty("country_id")
    private String country_id;
    @JsonProperty("country_name")
    private String country_name;

    @JsonProperty("latlng")
    private String latlng;
    @JsonProperty("dist")
    private Double dist;
    @JsonProperty("_version_")
    private Long _version_;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public DocSubareas() {
    }

    public DocSubareas(DocSubareas docSubareas) {
        this.city_id = docSubareas.getCity_id();
        this.id = docSubareas.getId();
        this.city_name = docSubareas.getCity_name();
        this.area_id = docSubareas.getArea_id();
        this.subarea_name = docSubareas.getSubarea_name();
        this.area_name = docSubareas.getArea_name();
        this.latlng = docSubareas.getLatlng();
        this.dist = docSubareas.getDist();
        this._version_ = docSubareas.get_version_();
        this.state_id=docSubareas.getState_id();
        this.state_name=docSubareas.getState_name();
        this.country_id=docSubareas.getCountry_id();
        this.country_name=docSubareas.getCountry_name();
        this.additionalProperties = docSubareas.getAdditionalProperties();
    }

    @JsonProperty("city_id")
    public String getCity_id() {
        return city_id;
    }

    @JsonProperty("city_id")
    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public DocSubareas withCity_id(String city_id) {
        this.city_id = city_id;
        return this;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public DocSubareas withId(String id) {
        this.id = id;
        return this;
    }

    @JsonProperty("city_name")
    public String getCity_name() {
        return city_name;
    }

    @JsonProperty("city_name")
    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public DocSubareas withCity_name(String city_name) {
        this.city_name = city_name;
        return this;
    }

    @JsonProperty("area_id")
    public String getArea_id() {
        return area_id;
    }

    @JsonProperty("area_id")
    public void setArea_id(String area_id) {
        this.area_id = area_id;
    }

    public DocSubareas withArea_id(String area_id) {
        this.area_id = area_id;
        return this;
    }

    @JsonProperty("subarea_name")
    public String getSubarea_name() {
        return subarea_name;
    }

    @JsonProperty("subarea_name")
    public void setSubarea_name(String subarea_name) {
        this.subarea_name = subarea_name;
    }

    public DocSubareas withSubarea_name(String subarea_name) {
        this.subarea_name = subarea_name;
        return this;
    }

    @JsonProperty("area_name")
    public String getArea_name() {
        return area_name;
    }

    @JsonProperty("area_name")
    public void setArea_name(String area_name) {
        this.area_name = area_name;
    }

    public DocSubareas withArea_name(String area_name) {
        this.area_name = area_name;
        return this;
    }

    @JsonProperty("latlng")
    public String getLatlng() {
        return latlng;
    }

    @JsonProperty("latlng")
    public void setLatlng(String latlng) {
        this.latlng = latlng;
    }

    public DocSubareas withLatlng(String latlng) {
        this.latlng = latlng;
        return this;
    }
    @JsonProperty("dist")
    public Double getDist() {
        return dist;
    }

    @JsonProperty("dist")
    public void setDist(Double dist) {
        this.dist = dist;
    }

    public DocSubareas withDist(Double dist) {
        this.dist = dist;
        return this;
    }
    @JsonProperty("_version_")
    public Long get_version_() {
        return _version_;
    }

    @JsonProperty("_version_")
    public void set_version_(Long _version_) {
        this._version_ = _version_;
    }

    public DocSubareas with_version_(Long _version_) {
        this._version_ = _version_;
        return this;
    }
    @JsonProperty("state_id")
    public String getState_id() {
        return state_id;
    }

    @JsonProperty("state_id")
    public void setState_id(String state_id) {
        this.state_id = state_id;
    }
    @JsonProperty("state_name")
    public String getState_name() {
        return state_name;
    }
    @JsonProperty("state_name")
    public void setState_name(String state_name) {
        this.state_name = state_name;
    }
    @JsonProperty("country_id")
    public String getCountry_id() {
        return country_id;
    }
    @JsonProperty("country_id")
    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }
    @JsonProperty("country_name")
    public String getCountry_name() {
        return country_name;
    }
    @JsonProperty("country_name")
    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    @Override
    public String toString() {
        return subarea_name+","+area_name+","+city_name;
    }


    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}