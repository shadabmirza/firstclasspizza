
package to.done.lib.entity.order;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "responseCode",
    "responseMsg",
    "data",
    "dataArray"
})
public class RespCheckOrder {

    @JsonProperty("responseCode")
    private Long responseCode;
    @JsonProperty("responseMsg")
    private String responseMsg;
    @JsonProperty("data")
    private CheckOrderData data;
    @JsonProperty("dataArray")
    private List<CheckOrderData> dataArray;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("responseCode")
    public Long getResponseCode() {
        return responseCode;
    }

    @JsonProperty("responseCode")
    public void setResponseCode(Long responseCode) {
        this.responseCode = responseCode;
    }

    public RespCheckOrder withResponseCode(Long responseCode) {
        this.responseCode = responseCode;
        return this;
    }

    @JsonProperty("responseMsg")
    public String getResponseMsg() {
        return responseMsg;
    }

    @JsonProperty("responseMsg")
    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public RespCheckOrder withResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
        return this;
    }

    @JsonProperty("data")
    public CheckOrderData getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(CheckOrderData data) {
        this.data = data;
    }

    public RespCheckOrder withData(CheckOrderData data) {
        this.data = data;
        return this;
    }

    @JsonProperty("dataArray")
    public List<CheckOrderData> getDataArray() {
        return dataArray;
    }

    @JsonProperty("dataArray")
    public void setDataArray(List<CheckOrderData> dataArray) {
        this.dataArray = dataArray;
    }

    public RespCheckOrder withDataArray(List<CheckOrderData> dataArray) {
        this.dataArray = dataArray;
        return this;
    }



    	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
