
package to.done.lib.entity.outletmenu;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "product_id",
    "category_id",
    "row_status",
    "created_at",
    "modified_at"
})
@DatabaseTable
public class Product_category_mapping {

    @JsonProperty("id")
    @DatabaseField(id = true)
	private Long id;
    @JsonProperty("product_id")
    @DatabaseField(index = true)
	private Long product_id;
    @JsonProperty("category_id")
    @DatabaseField(index = true)
	private Long category_id;
    @JsonProperty("row_status")
    @DatabaseField
	private String row_status;
    @JsonProperty("created_at")
    @DatabaseField
	private String created_at;
    @JsonProperty("modified_at")
    @DatabaseField
	private String modified_at;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    public Product_category_mapping withId(Long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("product_id")
    public Long getProduct_id() {
        return product_id;
    }

    @JsonProperty("product_id")
    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public Product_category_mapping withProduct_id(Long product_id) {
        this.product_id = product_id;
        return this;
    }

    @JsonProperty("category_id")
    public Long getCategory_id() {
        return category_id;
    }

    @JsonProperty("category_id")
    public void setCategory_id(Long category_id) {
        this.category_id = category_id;
    }

    public Product_category_mapping withCategory_id(Long category_id) {
        this.category_id = category_id;
        return this;
    }

    @JsonProperty("row_status")
    public String getRow_status() {
        return row_status;
    }

    @JsonProperty("row_status")
    public void setRow_status(String row_status) {
        this.row_status = row_status;
    }

    public Product_category_mapping withRow_status(String row_status) {
        this.row_status = row_status;
        return this;
    }

    @JsonProperty("created_at")
    public String getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Product_category_mapping withCreated_at(String created_at) {
        this.created_at = created_at;
        return this;
    }

    @JsonProperty("modified_at")
    public String getModified_at() {
        return modified_at;
    }

    @JsonProperty("modified_at")
    public void setModified_at(String modified_at) {
        this.modified_at = modified_at;
    }

    public Product_category_mapping withModified_at(String modified_at) {
        this.modified_at = modified_at;
        return this;
    }



    	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
