package to.done.lib.entity.user;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "primary_phone",
        "primary_email",
        "address"
})
@DatabaseTable
public class User {

    @DatabaseField(id=true)
    @JsonProperty("id")
    private Long id;
    @DatabaseField
    @JsonProperty("name")
    private String name;
    @DatabaseField
    @JsonProperty("primary_phone")
    private String primary_phone;
    @DatabaseField
    @JsonProperty("primary_email")
    private String primary_email;
    @JsonProperty("address")
    private Address address;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("primary_phone")
    public String getPrimary_phone() {
        return primary_phone;
    }

    @JsonProperty("primary_phone")
    public void setPrimary_phone(String primary_phone) {
        this.primary_phone = primary_phone;
    }

    @JsonProperty("primary_email")
    public String getPrimary_email() {
        return primary_email;
    }

    @JsonProperty("primary_email")
    public void setPrimary_email(String primary_email) {
        this.primary_email = primary_email;
    }

    @JsonProperty("address")
    public Address getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", primary_phone='" + primary_phone + '\'' +
                ", primary_email='" + primary_email + '\'' +
                ", address=" + address +
                '}';
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}