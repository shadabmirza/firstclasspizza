package to.done.lib.entity.gcm;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by viva on 17/6/14.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RequestGCMRegistration {


    @JsonProperty("device_token")
    private String device_token;
    @JsonProperty("device_uuid")
    private String device_uuid;
    @JsonProperty("device_name")
    private String device_name;
    @JsonProperty("device_platform")
    private String device_platform;
    @JsonProperty("device_version")
    private String device_version;

    @JsonProperty("company_id")
    private int company_id;

    public RequestGCMRegistration(String device_token, String device_uuid, String device_name, String device_platform, String device_version, int company_id) {
        this.device_token = device_token;
        this.device_uuid = device_uuid;
        this.device_name = device_name;
        this.device_platform = device_platform;
        this.device_version = device_version;
        this.company_id = company_id;
    }

    @JsonProperty("device_token")
    public String getDevice_token() {
        return device_token;
    }

    @JsonProperty("device_token")
    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    @JsonProperty("device_uuid")
    public String getDevice_uuid() {
        return device_uuid;
    }

    @JsonProperty("device_uuid")
    public void setDevice_uuid(String device_uuid) {
        this.device_uuid = device_uuid;
    }

    @JsonProperty("device_name")
    public String getDevice_name() {
        return device_name;
    }

    @JsonProperty("device_name")
    public void setDevice_name(String device_name) {
        this.device_name = device_name;
    }

    @JsonProperty("device_platform")
    public String getDevice_platform() {
        return device_platform;
    }

    @JsonProperty("device_platform")
    public void setDevice_platform(String device_platform) {
        this.device_platform = device_platform;
    }

    @JsonProperty("device_version")
    public String getDevice_version() {
        return device_version;
    }

    @JsonProperty("device_version")
    public void setDevice_version(String device_version) {
        this.device_version = device_version;
    }

    @JsonProperty("company_id")
    public int getCompany_id() {
        return company_id;
    }

    @JsonProperty("company_id")
    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }


}
