package to.done.lib.entity.order;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.UUID;

/**
 * Created by root on 20/5/14.
 */
@DatabaseTable
public class Order_Details {

    @DatabaseField(id=true)
    UUID id;
    @DatabaseField
    Long product_id;
    @DatabaseField
    Long outlet_product_id;
    @DatabaseField
    Long parent_outlet_product_id;
    @DatabaseField
    Double price;
    @DatabaseField
    Long order_id;
    @DatabaseField
    UUID parent_order_detail_id;
@DatabaseField
String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Order_Details(UUID id, Long product_id, Long outlet_product_id, Long parent_outlet_product_id, Double price, Long order_id, UUID parent_order_detail_id) {
        this.id = id;
        this.product_id = product_id;
        this.outlet_product_id = outlet_product_id;
        this.parent_outlet_product_id = parent_outlet_product_id;
        this.price = price;
        this.order_id = order_id;
        this.parent_order_detail_id = parent_order_detail_id;
    }

    public Order_Details() {

    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public Long getOutlet_product_id() {
        return outlet_product_id;
    }

    public void setOutlet_product_id(Long outlet_product_id) {
        this.outlet_product_id = outlet_product_id;
    }

    public Long getParent_outlet_product_id() {
        return parent_outlet_product_id;
    }

    public void setParent_outlet_product_id(Long parent_outlet_product_id) {
        this.parent_outlet_product_id = parent_outlet_product_id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Long order_id) {
        this.order_id = order_id;
    }

    public UUID getParent_order_detail_id() {
        return parent_order_detail_id;
    }

    public void setParent_order_detail_id(UUID parent_order_detail_id) {
        this.parent_order_detail_id = parent_order_detail_id;
    }

    @Override
    public String toString() {
        return "Order_Details{" +
                "id=" + id +
                ", product_id=" + product_id +
                ", outlet_product_id=" + outlet_product_id +
                ", parent_outlet_product_id=" + parent_outlet_product_id +
                ", price=" + price +
                ", order_id=" + order_id +
                ", parent_order_detail_id=" + parent_order_detail_id +
                '}';
    }
}
