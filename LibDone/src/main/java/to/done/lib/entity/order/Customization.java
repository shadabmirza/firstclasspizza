package to.done.lib.entity.order;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;



@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "outlet_product_id",
        "id"
})
public class Customization {

    @JsonProperty("outlet_product_id")
    private Long outlet_product_id;
    @JsonProperty("id")
    private Long id;

    private String name;

    private Long parent_outlet_product_id;

    private Double price;


    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Customization() {
    }

    public Customization(Long outlet_product_id, Long id, Long parent_outlet_product_id, Double price) {
        this.outlet_product_id = outlet_product_id;
        this.id = id;
        this.parent_outlet_product_id = parent_outlet_product_id;
        this.price = price;
    }

    @JsonProperty("outlet_product_id")
    public Long getOutlet_product_id() {
        return outlet_product_id;
    }

    @JsonProperty("outlet_product_id")
    public void setOutlet_product_id(Long outlet_product_id) {
        this.outlet_product_id = outlet_product_id;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }


    public Long getParent_outlet_product_id() {
        return parent_outlet_product_id;
    }

    public void setParent_outlet_product_id(Long parent_outlet_product_id) {
        this.parent_outlet_product_id = parent_outlet_product_id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonIgnore
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonIgnore
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}