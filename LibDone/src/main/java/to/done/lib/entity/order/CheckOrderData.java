
package to.done.lib.entity.order;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "is_preorder",
    "order_date",
    "coupon_code",
    "total_amount",
    "orders",
    "discounted_amt",
    "applied_offer",
    "applied_extra_charges"
})
public class CheckOrderData {

    @JsonProperty("is_preorder")
    private Boolean is_preorder;
    @JsonProperty("order_date")
    private Long order_date;
    @JsonProperty("coupon_code")
    private String coupon_code;
    @JsonProperty("total_amount")
    private Double total_amount;
    @JsonProperty("delivery_type")
    private String delivery_type;
    @JsonProperty("orders")
    private List<Order> orders = new ArrayList<Order>();
    @JsonProperty("discounted_amt")
    private Double discounted_amt;
    @JsonProperty("applied_offer")
    private List<Applied_offer> applied_offer = new ArrayList<Applied_offer>();
    @JsonProperty("applied_extra_charges")
    private List<Applied_extra_charge> applied_extra_charges = new ArrayList<Applied_extra_charge>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("delivery_type")
    public String getDelivery_type() {
        return delivery_type;
    }
    @JsonProperty("delivery_type")
    public void setDelivery_type(String delivery_type) {
        this.delivery_type = delivery_type;
    }

    @JsonProperty("is_preorder")
    public Boolean getIs_preorder() {
        return is_preorder;
    }

    @JsonProperty("is_preorder")
    public void setIs_preorder(Boolean is_preorder) {
        this.is_preorder = is_preorder;
    }
    @JsonProperty("applied_extra_charges")
    public List<Applied_extra_charge> getApplied_extra_charges() {
        return applied_extra_charges;
    }
    @JsonProperty("applied_extra_charges")
    public void setApplied_extra_charges(List<Applied_extra_charge> applied_extra_charges) {
        this.applied_extra_charges = applied_extra_charges;
    }

    public CheckOrderData withIs_preorder(Boolean is_preorder) {
        this.is_preorder = is_preorder;
        return this;
    }

    @JsonProperty("order_date")
    public Long getOrder_date() {
        return order_date;
    }

    @JsonProperty("order_date")
    public void setOrder_date(Long order_date) {
        this.order_date = order_date;
    }

    public CheckOrderData withOrder_date(Long order_date) {
        this.order_date = order_date;
        return this;
    }

    @JsonProperty("coupon_code")
    public String getCoupon_code() {
        return coupon_code;
    }

    @JsonProperty("coupon_code")
    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public CheckOrderData withCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
        return this;
    }

    @JsonProperty("total_amount")
    public Double getTotal_amount() {
        return total_amount;
    }

    @JsonProperty("total_amount")
    public void setTotal_amount(Double total_amount) {
        this.total_amount = total_amount;
    }

    public CheckOrderData withTotal_amount(Double total_amount) {
        this.total_amount = total_amount;
        return this;
    }

    @JsonProperty("orders")
    public List<Order> getOrders() {
        return orders;
    }

    @JsonProperty("orders")
    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public CheckOrderData withOrders(List<Order> orders) {
        this.orders = orders;
        return this;
    }

    @JsonProperty("discounted_amt")
    public Double getDiscounted_amt() {
        return discounted_amt;
    }

    @JsonProperty("discounted_amt")
    public void setDiscounted_amt(Double discounted_amt) {
        this.discounted_amt = discounted_amt;
    }

    public CheckOrderData withDiscounted_amt(Double discounted_amt) {
        this.discounted_amt = discounted_amt;
        return this;
    }

    @JsonProperty("applied_offer")
    public List<Applied_offer> getApplied_offer() {
        return applied_offer;
    }

    @JsonProperty("applied_offer")
    public void setApplied_offer(List<Applied_offer> applied_offer) {
        this.applied_offer = applied_offer;
    }

    public CheckOrderData withApplied_offer(List<Applied_offer> applied_offer) {
        this.applied_offer = applied_offer;
        return this;
    }



    	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
