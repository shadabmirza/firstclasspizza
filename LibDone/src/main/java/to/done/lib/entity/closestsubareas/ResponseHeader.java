
package to.done.lib.entity.closestsubareas;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "status",
    "QTime",
    "params"
})
public class ResponseHeader {

    @JsonProperty("status")
    private Long status;
    @JsonProperty("QTime")
    private Long qTime;
    @JsonProperty("params")
    private Params params;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("status")
    public Long getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(Long status) {
        this.status = status;
    }

    public ResponseHeader withStatus(Long status) {
        this.status = status;
        return this;
    }

    @JsonProperty("QTime")
    public Long getQTime() {
        return qTime;
    }

    @JsonProperty("QTime")
    public void setQTime(Long qTime) {
        this.qTime = qTime;
    }

    public ResponseHeader withQTime(Long qTime) {
        this.qTime = qTime;
        return this;
    }

    @JsonProperty("params")
    public Params getParams() {
        return params;
    }

    @JsonProperty("params")
    public void setParams(Params params) {
        this.params = params;
    }

    public ResponseHeader withParams(Params params) {
        this.params = params;
        return this;
    }



    	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
