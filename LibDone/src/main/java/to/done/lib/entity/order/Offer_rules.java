
package to.done.lib.entity.order;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "value_type",
    "value",
    "valid_from",
    "valid_to"
})
@DatabaseTable
public class Offer_rules implements Serializable {
    @DatabaseField
    @JsonProperty("value_type")
    private String value_type;
    @JsonProperty("value")
    @DatabaseField
    private String value;
    @JsonProperty("valid_from")
    @DatabaseField
    private String valid_from;
    @JsonProperty("valid_to")
    @DatabaseField
    private String valid_to;
    private transient Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("value_type")
    public String getValue_type() {
        return value_type;
    }

    @JsonProperty("value_type")
    public void setValue_type(String value_type) {
        this.value_type = value_type;
    }

    public Offer_rules withValue_type(String value_type) {
        this.value_type = value_type;
        return this;
    }

    @JsonProperty("value")
    public String getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(String value) {
        this.value = value;
    }

    public Offer_rules withValue(String value) {
        this.value = value;
        return this;
    }

    @JsonProperty("valid_from")
    public String getValid_from() {
        return valid_from;
    }

    @JsonProperty("valid_from")
    public void setValid_from(String valid_from) {
        this.valid_from = valid_from;
    }

    public Offer_rules withValid_from(String valid_from) {
        this.valid_from = valid_from;
        return this;
    }

    @JsonProperty("valid_to")
    public String getValid_to() {
        return valid_to;
    }

    @JsonProperty("valid_to")
    public void setValid_to(String valid_to) {
        this.valid_to = valid_to;
    }

    public Offer_rules withValid_to(String valid_to) {
        this.valid_to = valid_to;
        return this;
    }



    	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
