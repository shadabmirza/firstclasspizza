package to.done.lib.entity.tracking;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by HP on 6/25/2014.
 */@JsonPropertyOrder({
        "id",
        "order_status_id",
        "created_at"
})
   public class Order_status_log {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("order_status_id")
    private Long order_status_id;
    @JsonProperty("created_at")
    private Long created_at;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("order_status_id")
    public Long getOrder_status_id() {
        return order_status_id;
    }

    @JsonProperty("order_status_id")
    public void setOrder_status_id(Long order_status_id) {
        this.order_status_id = order_status_id;
    }

    @JsonProperty("created_at")
    public Long getCreated_at() {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at(Long created_at) {
        this.created_at = created_at;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "Order_status_log{" +
                "id=" + id +
                ", order_status_id=" + order_status_id +
                ", created_at=" + created_at +
                '}';
    }
}
