package to.done.lib.entity.outletsubareas;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by HP on 6/15/2014.
 */
@JsonPropertyOrder({
        "subareas",
        "areas"
})
public class OutletSubareaData {
            @JsonProperty("subareas")
        private List<Subarea> subareas = new ArrayList<Subarea>();
        @JsonProperty("areas")
        private List<Area> areas = new ArrayList<Area>();
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("subareas")
        public List<Subarea> getSubareas() {
            return subareas;
        }

        @JsonProperty("subareas")
        public void setSubareas(List<Subarea> subareas) {
            this.subareas = subareas;
        }

        @JsonProperty("areas")
        public List<Area> getAreas() {
            return areas;
        }

        @JsonProperty("areas")
        public void setAreas(List<Area> areas) {
            this.areas = areas;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }


}
