
package to.done.lib.entity.closestsubareas;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "numFound",
    "start",
    "docs"
})
public class ResponseSolr {

    @JsonProperty("numFound")
    private Long numFound;
    @JsonProperty("start")
    private Long start;
    @JsonProperty("docs")
    private List<DocSubareas> docs = new ArrayList<DocSubareas>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("numFound")
    public Long getNumFound() {
        return numFound;
    }

    @JsonProperty("numFound")
    public void setNumFound(Long numFound) {
        this.numFound = numFound;
    }

    public ResponseSolr withNumFound(Long numFound) {
        this.numFound = numFound;
        return this;
    }

    @JsonProperty("start")
    public Long getStart() {
        return start;
    }

    @JsonProperty("start")
    public void setStart(Long start) {
        this.start = start;
    }

    public ResponseSolr withStart(Long start) {
        this.start = start;
        return this;
    }

    @JsonProperty("docs")
    public List<DocSubareas> getDocs() {
        return docs;
    }

    @JsonProperty("docs")
    public void setDocs(List<DocSubareas> docs) {
        this.docs = docs;
    }

    public ResponseSolr withDocs(List<DocSubareas> docs) {
        this.docs = docs;
        return this;
    }



    	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
