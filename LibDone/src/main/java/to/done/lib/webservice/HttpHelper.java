package to.done.lib.webservice;

import android.net.http.AndroidHttpClient;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;

import to.done.lib.Constants;
import to.done.lib.sync.DownloadListener;
import to.done.lib.utils.Toolbox;

/**
 * @author shadab mirza Pradhan - Movivation Labs Pvt. Ltd. (shadab mirza@movivation.com)
 * */

public class HttpHelper {

    private static final String ERR_HTTP_EXEC="Error contacting server";
    private static final String ERR_RESP_CONVERSION="Error while communicating with server";
    private static final String ERR_RESP_NULL="No response from server";
    private static final int CONNECTION_TIMEOUT=60000;
    private static final int SOCKET_TIMEOUT=60000;
    private static final String CONTENT_TYPE_JSON="application/json";

    public static enum EncodingType{PLAIN_TEXT,GZIP};
    private static String version;


    public static String getVersion() {
        return version;
    }

    public static void setVersion(String ver) {
        version = ver;
    }


    public static void doHttpPost(String url, String postData, long requestTimestamp, DownloadListener downloadListener)
	{
        doHttpPost(url,null,EncodingType.PLAIN_TEXT,postData, requestTimestamp, downloadListener);

	}

    public static void doHttpPost(String url, String postData,EncodingType encodingType, long requestTimestamp, DownloadListener downloadListener)
    {
        doHttpPost(url,null,encodingType,postData, requestTimestamp, downloadListener);

    }
    public static void doHttpGet(String url,long requestTimestamp,DownloadListener downloadListener)
    {
        doHttpGet(url, null,EncodingType.PLAIN_TEXT, requestTimestamp,downloadListener);

    }

    public static void doHttpGet(String url,EncodingType encodingType,long requestTimestamp,DownloadListener downloadListener)
    {
        doHttpGet(url, null,encodingType, requestTimestamp,downloadListener);
    }
	public static void doHttpPost(String url,Header[] headers,EncodingType encodingType, String postData, long requestTimestamp,DownloadListener downloadListener){
        downloadListener.onDownloadStart(url,requestTimestamp);


        String strResult=null;

        HttpParams httpParams = new BasicHttpParams();

        HttpConnectionParams.setConnectionTimeout(httpParams,
                CONNECTION_TIMEOUT);
        HttpConnectionParams.setConnectionTimeout(httpParams, SOCKET_TIMEOUT);

        HttpClient client = new DefaultHttpClient(httpParams);

        HttpPost post = new HttpPost(url);

        if(url.equalsIgnoreCase(Constants.URL_SAVE_ORDER))
        {
            //post.setHeaders(headers);
            int version=android.os.Build.VERSION.SDK_INT;
            post.addHeader("client_platform",Constants.getdevicePlatform());
            post.addHeader("client_platform_version","android"+ Constants.getDeviceVersion());
            post.addHeader("client_version",getVersion());
            Toolbox.writeToLog("Logged device version " + (getVersion()));
            post.addHeader(HTTP.CONTENT_TYPE, CONTENT_TYPE_JSON);
        }else if(url.equalsIgnoreCase(Constants.URL_GET_OUTLET_BY_COMPANY)){
            post.addHeader("client_platform",Constants.getdevicePlatform());
            post.addHeader("client_platform_version","android"+ Constants.getDeviceVersion());
            post.addHeader("client_version",String.valueOf(getVersion()));
            post.setHeader(HTTP.CONTENT_TYPE, CONTENT_TYPE_JSON);
            Toolbox.writeToLog("Logged device version "+ (getVersion()));
        }
        else
        {
            post.setHeader(HTTP.CONTENT_TYPE, CONTENT_TYPE_JSON);

        }

        if (postData != null) {
            try {
                post.setEntity(new StringEntity(postData));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                downloadListener.onDownloadFail(url,e,"Error while converting postData to StringEntity",requestTimestamp);
                return;
            }
        }
        if(encodingType==EncodingType.GZIP){
            AndroidHttpClient.modifyRequestToAcceptGzipResponse(post);
        }
        HttpResponse response=null;

        try {
            response=client.execute(post);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            downloadListener.onDownloadFail(url, e, ERR_HTTP_EXEC,requestTimestamp);
            return;

        }
        catch (ConnectException e) {
            e.printStackTrace();
            downloadListener.onDownloadFail(url, e, ERR_HTTP_EXEC,requestTimestamp);
            return;

        }
        catch (IOException e) {
            e.printStackTrace();
            downloadListener.onDownloadFail(url, e, ERR_HTTP_EXEC,requestTimestamp);
            return;

        }
        catch (Exception e){
            e.printStackTrace();
            downloadListener.onDownloadFail(url, e, ERR_HTTP_EXEC,requestTimestamp);
            return;

        }

        if(response==null){
            downloadListener.onDownloadFail(url, null, ERR_RESP_NULL,requestTimestamp);
            return;
        }


        try {
            if(encodingType==EncodingType.GZIP){
            for(Header h:response.getAllHeaders()){
                if(h.getName().equals("Content-Encoding")){


                }

            }



                InputStream inputStream=AndroidHttpClient.getUngzippedContent(response.getEntity());
                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder resultBuilder = new StringBuilder();
                String line;
                while ((line = r.readLine()) != null) {
                    resultBuilder.append(line);
                }
                strResult=resultBuilder.toString();
            }
            else if(encodingType==EncodingType.PLAIN_TEXT){
                strResult = EntityUtils.toString(response.getEntity());
            }
            downloadListener.onDownloadSuccess(url,strResult,response.getStatusLine().getStatusCode(),requestTimestamp);
        } catch (ParseException e) {
            e.printStackTrace();
            downloadListener.onDownloadFail(url, e, ERR_RESP_CONVERSION,requestTimestamp);
            return;
        } catch (IOException e) {
            e.printStackTrace();
            downloadListener.onDownloadFail(url, e, ERR_RESP_CONVERSION,requestTimestamp);
            return;
        }
    }
	public static void doHttpGet(String url,Header[] headers,EncodingType encodingType, long requestTimestamp, DownloadListener downloadListener)
	{
        downloadListener.onDownloadStart(url,requestTimestamp);
		String strResult=null;
		
		HttpParams httpParams = new BasicHttpParams();
		
		HttpConnectionParams.setConnectionTimeout(httpParams,
				CONNECTION_TIMEOUT);
		HttpConnectionParams.setConnectionTimeout(httpParams, SOCKET_TIMEOUT);
		
		HttpClient client = new DefaultHttpClient(httpParams);



        HttpGet get = null;


//        try {
            get = new HttpGet(url);
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }

        if(headers!=null)
        {
            get.setHeaders(headers);
        }
        else
        {
            get.setHeader(HTTP.CONTENT_TYPE, CONTENT_TYPE_JSON);
        }

        if(encodingType==EncodingType.GZIP){
            AndroidHttpClient.modifyRequestToAcceptGzipResponse(get);
        }
		HttpResponse response=null;

		try {
			response = client.execute(get);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
            downloadListener.onDownloadFail(url,e,ERR_HTTP_EXEC,requestTimestamp);
            return;
		} catch (IOException e) {
			e.printStackTrace();
            downloadListener.onDownloadFail(url,e,ERR_HTTP_EXEC,requestTimestamp);
            return;
		}catch (Exception e) {
            e.printStackTrace();
            downloadListener.onDownloadFail(url, e, ERR_HTTP_EXEC, requestTimestamp);
            return;

        }
		if(response==null){
            downloadListener.onDownloadFail(url,null,ERR_RESP_NULL,requestTimestamp);
            return;
		}
		
        	 try {
                 if(encodingType==EncodingType.GZIP){
                     InputStream inputStream=AndroidHttpClient.getUngzippedContent(response.getEntity());
                     BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                     StringBuilder resultBuilder = new StringBuilder();
                     String line;
                     while ((line = r.readLine()) != null) {
                         resultBuilder.append(line);
                     }
                     strResult=resultBuilder.toString();
                 }
                 else if(encodingType==EncodingType.PLAIN_TEXT){
                     strResult = EntityUtils.toString(response.getEntity());
                 }
                 downloadListener.onDownloadSuccess(url,strResult,response.getStatusLine().getStatusCode(), requestTimestamp);
			} catch (ParseException e) {
				e.printStackTrace();
                downloadListener.onDownloadFail(url,e,ERR_RESP_CONVERSION,requestTimestamp);
                 return;
             } catch (IOException e) {
				e.printStackTrace();
                downloadListener.onDownloadFail(url,e,ERR_RESP_CONVERSION,requestTimestamp);
                return;
             }

	}
	
	

}
