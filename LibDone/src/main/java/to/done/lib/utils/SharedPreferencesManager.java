package to.done.lib.utils;

import android.content.Context;
import android.content.SharedPreferences;

import to.done.lib.Constants;

/**
 * Created by shadab mirza on 4/25/14.
 */
public final class SharedPreferencesManager {

    private static SharedPreferences sharedPreferences;

    private SharedPreferencesManager() {
    }

    public static void writeString(Context context,String key, String value){
        getSharedPreferencesEditor(context).putString(key,value).commit();
    }

    public static void writeInt(Context context,String key, int value){
        getSharedPreferencesEditor(context).putInt(key,value).commit();
    }

    public static void writeBoolean(Context context,String key, boolean value){
        getSharedPreferencesEditor(context).putBoolean(key,value).commit();
    }

    public static void writeLong(Context context,String key, long value){
        getSharedPreferencesEditor(context).putLong(key,value).commit();
    }

    public static void writeFloat(Context context,String key, float value){
        getSharedPreferencesEditor(context).putFloat(key,value).commit();
    }

    public static SharedPreferences getSharedPreferences(Context context) {
        if(sharedPreferences==null){
            sharedPreferences = context.getSharedPreferences(Constants.PREFS_NAME,Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }
    public static SharedPreferences.Editor getSharedPreferencesEditor(Context context){

        return getSharedPreferences(context).edit();
    }

}
