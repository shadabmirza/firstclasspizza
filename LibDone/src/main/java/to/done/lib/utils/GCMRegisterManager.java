package to.done.lib.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import to.done.lib.Constants;
import to.done.lib.R;
import to.done.lib.entity.gcm.ResponseGCMRegistration;
import to.done.lib.sync.SyncListener;
import to.done.lib.sync.SyncManager;

public class GCMRegisterManager {

    public static final String EXTRA_MESSAGE = "message";
    //public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "GCMRegisterManager";


    static String SENDER_ID;//project number
    static GoogleCloudMessaging gcm;
    //    AtomicInteger msgId = new AtomicInteger();
//    SharedPreferences prefs;
    static Context context;

    static String regid;


    public static void registerToGCM(Context con) {
        context = con;
        SENDER_ID = context.getResources().getString(R.string.gcm_sender_id);
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(context);
            regid = getRegistrationId(context);
            Log.e(TAG, "old reg id:" + regid);
            if (regid == null || regid.isEmpty()) {
                registerInBackground();
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }
    }

    private static boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {

            return false;
        }
        return true;
    }

    private static String getRegistrationId(Context context) {
        final SharedPreferences prefs = SharedPreferencesManager.getSharedPreferences(context);

        String registrationId = prefs.getString(Constants.PREFS_GCM_ID_KEY, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
                Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private static void registerInBackground() {

        AsyncTask<Void, Void, Void> registerTask = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                // TODO Auto-generated method stub
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;
                    Log.e(TAG, msg);
                    storeRegistrationId(context, regid);
                    //send gcm reg id to server
                    sendRegistrationIdToBackend();


                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();

                }

                return null;
            }
        };
        registerTask.execute();
    }

    private static void sendRegistrationIdToBackend() {
        //SEND REG_ID TO SERVER

        SyncManager.registerDevice((Activity) context, syncListener);
    }

    private static void storeRegistrationId(Context context, String regId) {

        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        String uid = SharedPreferencesManager.getSharedPreferences(context).getString(Constants.PREFS_GCM_UUID_KEY, null);
        SharedPreferences.Editor editor = SharedPreferencesManager.getSharedPreferencesEditor(context);
        editor.putString(Constants.PREFS_GCM_ID_KEY, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        if (uid == null) {
            editor.putString(Constants.PREFS_GCM_UUID_KEY, UUID.randomUUID().toString());
        }
        editor.commit();

    }

    static SyncListener syncListener = new SyncListener() {
        @Override
        public void onSyncStart() {

        }

        @Override
        public void onSyncProgress(float percentProgress, long requestTimestamp) {

        }

        @Override
        public void onSyncSuccess(String url, Object responseObject, long requestTimestamp) {
            Log.e(TAG, "onSyncSuccess()-->" + requestTimestamp);
            ResponseGCMRegistration responseGCMRegistration = (ResponseGCMRegistration) responseObject;

            if (responseGCMRegistration == null || responseGCMRegistration.getResponseCode() != Constants.RESPONSE_CODE_SUCCESS) {

                SharedPreferences.Editor editor = SharedPreferencesManager.getSharedPreferencesEditor(context);
                editor.putString(Constants.PREFS_GCM_ID_KEY, "");
                editor.commit();
                Log.e(TAG, "onSyncSuccess()-->GCM ID cleared");
            }
        }

        @Override
        public void onSyncFailure(String url, String reason, long requestTimestamp) {
            Log.e(TAG, "onSyncFailure()-->GCM ID cleared");
            SharedPreferences.Editor editor = SharedPreferencesManager.getSharedPreferencesEditor(context);
            editor.putString(Constants.PREFS_GCM_ID_KEY, "");
            editor.commit();
        }
    };
}
