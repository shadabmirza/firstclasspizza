package to.done.lib.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

import java.text.DecimalFormat;

import to.done.lib.Constants;
import to.done.lib.R;


/**
 * @author Shadab Mirza
 */
public final class Toolbox {
    private Animation wobbleAnim;

    private Toolbox() {

    }

    private static DecimalFormat df = new DecimalFormat("#.00");

    public static void writeToLog(String logMessage) {
        Log.d(Constants.TAG, logMessage);
    }

    public static void writeToLog(int level, String logMessage) {

        writeToLog(level, Constants.TAG, logMessage);

    }

    public static void writeToLog(String tag, String logMessage) {

        writeToLog(Log.DEBUG, tag, logMessage);

    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    public static void writeToLog(int level, String tag, String logMessage) {
        switch (level) {
            case Log.ASSERT:
                Log.e(tag, logMessage);
                break;
            case Log.DEBUG:
                Log.d(tag, logMessage);
                break;
            case Log.WARN:
                Log.w(tag, logMessage);
                break;
            case Log.VERBOSE:
                Log.v(tag, logMessage);
                break;
            case Log.ERROR:
                Log.e(tag, logMessage);
                break;
            case Log.INFO:
                Log.i(tag, logMessage);
                break;
            default:
                Log.d(tag, logMessage);
                break;

        }
    }

    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm == null) {
            return false;
        }

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }


    public static String getAppVersionName(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

//    public static ClientDetailsWeb getClientDetails(Context context) {
//
//
//        Long deviceId=null;
//        SharedPreferences sharedPrefs=context.getSharedPreferences(Constants.PREFS_NAME,Context.MODE_PRIVATE);
//        deviceId=sharedPrefs.getLong(Constants.PREFS_DEVICE_ID,0);
//
//        if(deviceId==0)deviceId=null;
//
//        String versionName = null;
//        try {
//            versionName = context.getPackageManager()
//                    .getPackageInfo(context.getPackageName(), 0).versionName;
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//            return null;
//        }
//
//        return new ClientDetailsWeb(deviceId,
//                "Android_API_"+android.os.Build.VERSION.SDK_INT,
//                versionName,
//                sharedPrefs.getString(Constants.PREFS_DBVERSION,"0"));
//    }

    public static void updateDbVersion(Context context, String dbVersion) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(Constants.PREFS_DBVERSION, dbVersion);
        editor.commit();
    }

    public static void storeDeviceRegisteredOnServer(Context context, boolean flag) {
        final SharedPreferences prefs = getGcmPreferences(context);
        //Log.i(Constants.TAG, "saved reg id on server " + flag);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(Constants.PREFS_IS_DEVICE_REGISTERED, flag);
        editor.commit();

        //Log.d(Constants.TAG, "is device registered storing" + isDeviceRegisteredOnServer(context));
    }

    public static void storeDeviceId(Context context, Long device_id) {
        final SharedPreferences prefs = getGcmPreferences(context);
        //Log.i(Constants.TAG, "saved reg id on server " + flag);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(Constants.PREFS_DEVICE_ID, device_id);
        editor.commit();

        //Log.d(Constants.TAG, "is device registered storing" + isDeviceRegisteredOnServer(context));
    }

    public static Long getDeviceId(Context context) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        Long deviceId = sharedPrefs.getLong(Constants.PREFS_DEVICE_ID, 0);
        if (deviceId != 0) {
            return deviceId;
        }
        return null;
    }

    /**
     * Stores the registration ID and the app versionCode in the application's {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId   registration ID
     */
    public static void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGcmPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(Constants.TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constants.PREFS_PUSH_TOKEN, regId);
        editor.putInt(Constants.PREFS_APP_VERSION, appVersion);
        editor.commit();
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    public static SharedPreferences getGcmPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences,
        // but
        // how you store the regID in your app is up to you.
        return context.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE);
    }

    public static boolean isDeviceRegisteredOnServer(Context context) {
        final SharedPreferences prefs = getGcmPreferences(context);
        boolean isRegistered = prefs.getBoolean(Constants.PREFS_IS_DEVICE_REGISTERED, false);
        return isRegistered;
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    public static int getAppVersion(Context context) {
        try {

            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Gets the current registration ID for application on GCM service, if there is one.
     * <p/>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing registration ID.
     */
    public static String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGcmPreferences(context);
        String registrationId = prefs.getString(Constants.PREFS_PUSH_TOKEN, "");
        if (registrationId.trim().length() <= 0) {
            Log.i(Constants.TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(Constants.PREFS_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(Constants.TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }


    public static void showDebugToast(Context context, String message) {
        showToastShort(context, message);
    }

    public static void showToastShort(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

    }

    public static void showToastLong(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();

    }

    public static String formatDecimal(double value) {
        return df.format(value);
    }

    public static Location getLastKnownLocation(Context context, LocationListener locationListener) {


        LocationManager locMan = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (!locMan.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locMan.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            buildAlertMessageNoGps(context);
            return null;
        }

        Location locNetwork = null, locGPS = null;
        if (locMan.getAllProviders().contains(LocationManager.NETWORK_PROVIDER))
            locNetwork = locMan.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (locMan.getAllProviders().contains(LocationManager.GPS_PROVIDER))
            locGPS = locMan.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (locMan.getAllProviders().contains(LocationManager.NETWORK_PROVIDER))
            locMan.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

        if (locMan.getAllProviders().contains(LocationManager.GPS_PROVIDER))
            locMan.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        //locMan.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,locationListener);
        //locMan.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,00,0,locationListener);

        if (locNetwork == null && locGPS == null) {
            return null;
        } else if (locGPS == null) {
            return locNetwork;
        } else if (locNetwork == null) {
            return locGPS;
        } else if (locNetwork.getTime() >= locGPS.getTime()) {
            return locNetwork;
        } else {
            return locGPS;
        }
    }

    public static void changeScreen(Context context, int screenId, boolean addToBackStack) {
        Intent intent = new Intent(Constants.INTENT_SCREEN_UPDATE);
        intent.putExtra(Constants.SCREEN_ID, screenId);

        intent.putExtra(Constants.ADD_TO_BACKSTACK, addToBackStack);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        //context.sendBroadcast(intent);
    }

    public static void changeScreen(Context context, int screenId, boolean addToBackStack, Bundle bundle) {
        Intent intent = new Intent(Constants.INTENT_SCREEN_UPDATE);
        intent.putExtra(Constants.SCREEN_ID, screenId);
        intent.putExtra(Constants.ADD_TO_BACKSTACK, addToBackStack);
        intent.putExtra(Constants.BUNDLE_EXTRA, bundle);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        // context.sendBroadcast(intent);
    }

    public static void changeNavDrawerStatus(Context context, int navDrawerAction) {
        Intent intent = new Intent(Constants.INTENT_NAV_DRAWER);
        intent.putExtra(Constants.NAV_DRAWER_ACTION, navDrawerAction);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        //context.sendBroadcast(intent);
    }

    public static void changeActionBarTitle(Context context, String title, String subtitle) {
        Intent intent = new Intent(Constants.INTENT_AB_TITLE);
        intent.putExtra(Constants.AB_TITLE_TEXT, title);
        intent.putExtra(Constants.AB_SUBTITLE_TEXT, subtitle);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        //context.sendBroadcast(intent);
    }

    public static void changeActionBarButton(Context context, int buttonType) {
        Intent intent = new Intent(Constants.INTENT_AB_BUTTON);
        intent.putExtra(Constants.AB_BTN, buttonType);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        //context.sendBroadcast(intent);
    }

    public static void hideActionBar(Context context, String title, String sub) {
        changeActionBarTitle(context, title, sub);

    }

    public static int getAttributeResourceId(Activity activity, int attrId, int currentThemeId) {

        TypedArray a = activity.getTheme().obtainStyledAttributes(currentThemeId,
                new int[]{
                        attrId}
        );

        return a.getResourceId(0, 0);

    }

    public static void animateQuantityIncrease(View v) {
        Animator translateY = ObjectAnimator.ofFloat(v, "translationY", 0, -0.5f * v.getHeight(), 0);
        translateY.setDuration(300);

        Animator scaleX = ObjectAnimator.ofFloat(v, "scaleX", 1, 1.5f, 1);
        scaleX.setDuration(300);

        Animator scaleY = ObjectAnimator.ofFloat(v, "scaleY", 1, 1.5f, 1);
        scaleY.setDuration(300);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(translateY, scaleX, scaleY);
        animatorSet.start();

    }

    public static void animateButtonPress(View v) {
        Animator translateY = ObjectAnimator.ofFloat(v, "translationY", 0, -0.5f * v.getHeight(), 0);
        translateY.setDuration(300);

        Animator scaleX = ObjectAnimator.ofFloat(v, "scaleX", 1, 1.5f, 1);
        scaleX.setDuration(300);

        Animator scaleY = ObjectAnimator.ofFloat(v, "scaleY", 1, 1.5f, 1);
        scaleY.setDuration(300);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(translateY, scaleX, scaleY);
        animatorSet.start();

    }

    public static void wobbleView(Context c, View v) {
        if (c != null) {
            Animation wobbleAnim = AnimationUtils.loadAnimation(c, R.anim.wobble);
            if (wobbleAnim != null && v != null) {
                wobbleAnim.reset();
                wobbleAnim.setFillAfter(true);
                v.startAnimation(wobbleAnim);
            }
        }

    }

    public static void animateQuantityDecrease(View v) {

        Animator translateY = ObjectAnimator.ofFloat(v, "translationY", 0, 0.5f * v.getHeight(), 0);
        translateY.setDuration(300);

        Animator scaleX = ObjectAnimator.ofFloat(v, "scaleX", 1, 0.5f, 1);
        scaleX.setDuration(300);

        Animator scaleY = ObjectAnimator.ofFloat(v, "scaleY", 1, 0.5f, 1);
        scaleY.setDuration(300);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(translateY, scaleX, scaleY);
        animatorSet.start();
    }

//    public static Transaction createGATransFromOrder(DatabaseAdapter db, OrderWeb order){
    //        Transaction transaction = new Transaction.Builder(
    //                String.valueOf(order.getId()),                             // (String) Transaction Id, should be unique.
    //                (long) (order.getTotal_price() * 1000000))      // (long) Order total (in micros)
    //                .setAffiliation("Sandwizzaa-Android")                        // (String) Affiliation
    //                .setTotalTaxInMicros((long) (0 * 1000000))       // (long) Total tax (in micros)
    //                .setShippingCostInMicros(0)                      // (long) Total shipping cost (in micros)
    //                .setCurrencyCode("INR")                          // (String) Set currency code to Euros.
    //                .build();
    //
    //        for(OrderDetailWeb detail:order.getOrder_details())
    //        {
    //            StringBuilder prodNameBuilder=new StringBuilder();
    //            prodNameBuilder.append(detail.getProduct_name());
    //
    //            if(detail.getOrder_detail_extra_features()!=null)
    //            {
    //                for(OrderDetailExtraFeature ode:detail.getOrder_detail_extra_features()){
    //                    prodNameBuilder.append("+");
    //                    prodNameBuilder.append(ode.getExtra_feature_name());
    //                }
    //            }
    //
    //
    //            transaction.addItem(new Transaction.Item.Builder(
    //                    detail.getProduct_id().toString(),                                    // (String) Product SKU
    //                    prodNameBuilder.toString(),                                  // (String) Product name
    //                    (long) (detail.getTotal_with_extra() * 1000000),                      // (long) Product price (in micros)
    //                    (long) detail.getQuantity())                               // (long) Product quantity
    //                    .setProductCategory(db.getCategoryForProduct(detail.getProduct_id().toString()).getName())    // (String) Product category
    //                    .build());
    //        }
    //
    //
    //        return transaction;
    //
    //    }

    public static double calculateDistance(double lat1, double lon1, double lat2, double lon2) {
        double dist = 6371 * Math.acos(Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(lon2) - Math.toRadians(lon1)) + Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2)));
        return dist / 1.60934d;
    }


    public static void buildAlertMessageNoGps(final Context context) {


        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("We need your location to search what's near you. Please go to settings and enable location.")
                .setCancelable(false)
                .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        if (context != null) {
                            context.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

//        double latDistance = Math.toRadians(lat2-lat1);
//        double lonDistance = Math.toRadians(lon2-lon1);
//        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +
//                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
//                        Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
//        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
//
//        return 6371 * c;

    private void wobbleView(View v) {

    }
}
