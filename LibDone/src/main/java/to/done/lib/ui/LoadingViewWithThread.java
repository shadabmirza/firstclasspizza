package to.done.lib.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;

import to.done.lib.R;

public class LoadingViewWithThread extends View implements Runnable {

    public final int INITIAL_RADIUS = 10;
    ScaleAnimation scaleAnimation;
    Handler handler;
    public String colorValue[] = new String[]{
            "#D3A05E",
            "#FCE2BF"

            // "#f13a49",
            // "#00b6f1",
            //"#ff8800",
            //"#68b861",
            //"#dcdc31"//added yellow
            // "#1d1d1d" //removed black
    };
    int counter = 0;
    int radius = 0;
    boolean drawingFlag;
    Bitmap bmplogo[] = new Bitmap[colorValue.length];
    Bitmap bmpDoneLogo;
    Thread t;
    boolean stopThread;
    long currentTime;

    public boolean isDrawingFlag() {
        return drawingFlag;
    }

    public void setDrawingFlag(boolean drawingFlag) {
        this.drawingFlag = drawingFlag;
    }

    public LoadingViewWithThread(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public LoadingViewWithThread(Context context) {
        super(context);
        float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                150, getResources().getDisplayMetrics());
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams((int) pixels, (int) pixels);
        setLayoutParams(lp);

    }


    @Override
    protected void onDraw(Canvas canvas) {

        super.onDraw(canvas);

        Paint p = new Paint();
        p.setAntiAlias(true);
        p.setColor(Color.WHITE);
        p.setStrokeWidth(4);
        Paint circleStrokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        circleStrokePaint.setStyle(Paint.Style.STROKE);
        circleStrokePaint.setStrokeWidth(2);
        circleStrokePaint.setColor(Color.WHITE);
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, (getWidth() / 4) - 2, p);
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, (getWidth() / 4) - 2, circleStrokePaint);

        int counter1 = counter - 1;
        if (counter1 < 0)
            counter1 = colorValue.length - 1;
        p.setStyle(Paint.Style.FILL);
        p.setColor(Color.parseColor(colorValue[counter1]));
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, (getWidth() / 2) - 5, p);
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, (getWidth() / 2) - 5, circleStrokePaint);

        p.setColor(Color.parseColor(colorValue[counter]));
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, radius - 5, p);
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, radius - 5, circleStrokePaint);

        if (!stopThread && isDrawingFlag())
            canvas.drawBitmap(bmplogo[counter], (getWidth() / 2) - (bmplogo[counter].getWidth() / 2), (getHeight() / 2) - (bmplogo[counter].getHeight() / 2), p);
        else {
            if (!isDrawingFlag())
                stopThread = false;
            canvas.drawBitmap(bmpDoneLogo, (getWidth() / 2) - (bmplogo[counter].getWidth() / 2), (getHeight() / 2) - (bmplogo[counter].getHeight() / 2), p);
        }

    }

    public void startLoadingAnimation() {
        radius = INITIAL_RADIUS;
        bmplogo[0] = BitmapFactory.decodeResource(getResources(), R.drawable.logo_custom);
        bmplogo[1] = BitmapFactory.decodeResource(getResources(), R.drawable.logo_custom);
       // bmplogo[2] = BitmapFactory.decodeResource(getResources(), R.drawable.logo_custom);
        //bmplogo[3] = BitmapFactory.decodeResource(getResources(), R.drawable.logo_custom);
        //bmplogo[4] = BitmapFactory.decodeResource(getResources(), R.drawable.logo_custom);
//        bmplogo[0] = BitmapFactory.decodeResource(getResources(), R.drawable.logo_in_head);
//        bmplogo[1] = BitmapFactory.decodeResource(getResources(), R.drawable.logo_in_head);
//        bmplogo[2] = BitmapFactory.decodeResource(getResources(), R.drawable.logo_in_head);
//        bmplogo[3] = BitmapFactory.decodeResource(getResources(), R.drawable.logo_in_head);
//        bmplogo[4] = BitmapFactory.decodeResource(getResources(), R.drawable.logo_in_head);


//        bmplogo[0] = BitmapFactory.decodeResource(getResources(), R.drawable.loading_food_1);
//        bmplogo[1] = BitmapFactory.decodeResource(getResources(), R.drawable.loading_food_2);
//        bmplogo[2] = BitmapFactory.decodeResource(getResources(), R.drawable.loading_food_3);
//        bmplogo[3] = BitmapFactory.decodeResource(getResources(), R.drawable.loading_food_4);
//        bmplogo[4] = BitmapFactory.decodeResource(getResources(), R.drawable.loading_food_5);
        t = null;
        stopThread = false;
        setDrawingFlag(true);

        handler = new Handler() {
            public void handleMessage(Message msg) {
                radius += 1;
                if (radius > getWidth() / 2) {
                    radius = INITIAL_RADIUS;
                    counter++;
                    if (counter >= colorValue.length)
                        counter = 0;
                }
                postInvalidate();
            }
        };
        t = new Thread(this);
        t.start();
    }

    public void stopAnimation() {
        bmpDoneLogo = BitmapFactory.decodeResource(getResources(), R.drawable.logo_shape);
        stopThread = true;
        currentTime = System.currentTimeMillis();
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        while (drawingFlag) {
            try {
                if ((System.currentTimeMillis() - currentTime) >= 2000 && radius >= (getWidth() / 2 - 2) && stopThread) {
                    setDrawingFlag(false);
                }
                Thread.sleep(8);
                handler.sendMessage(handler.obtainMessage());
            } catch (Exception e) {
                Log.d("Chandni", "Exception e=" + e);
            }
        }
    }


}
