package to.done.lib.ui;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by shadab mirza on 16/4/14.
 */
public class SearchAutoCompleteTextView extends FontAutoCompleteTextView {

    public SearchAutoCompleteTextView(Context context) {
        super(context);
    }

    public SearchAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SearchAutoCompleteTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void performFiltering(CharSequence text, int keyCode) {

    }


}
