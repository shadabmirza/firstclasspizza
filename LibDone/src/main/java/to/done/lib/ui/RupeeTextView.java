package to.done.lib.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by shadab mirza on 4/12/13.
 */
public class RupeeTextView extends TextView{

    public RupeeTextView(Context context) {
        super(context);
        init(context);
    }

    public RupeeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RupeeTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }


    private void init(Context context){
        setTypeface(FontManager.getRupeeTypeFace(context));
        setText("`");
    }

}
