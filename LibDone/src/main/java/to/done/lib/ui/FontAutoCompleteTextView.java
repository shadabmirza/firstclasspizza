package to.done.lib.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

import to.done.lib.R;

/**
 * Created by shadab mirza on 11/4/14.
 */
public class FontAutoCompleteTextView extends AutoCompleteTextView{


    public FontAutoCompleteTextView(Context context) {
        this(context, null);
    }

    public FontAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        String fontNameFromXML=null;
        if (!isInEditMode()) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.font);
            fontNameFromXML = a.getString(R.styleable.font_fontName);

            if(fontNameFromXML!=null)
                setTypeface(FontManager.getTypeface(getContext(), fontNameFromXML));
            a.recycle();
        }
    }

    public FontAutoCompleteTextView(Context context, AttributeSet attrs, int defStyle) {
        this(context, attrs);
    }
}
