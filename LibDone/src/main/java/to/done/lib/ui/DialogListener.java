package to.done.lib.ui;

/**
 * Created by shadab mirza on 2/12/13.
 */
public interface DialogListener {

    void onPositiveBtnClick();
    void onNegativeBtnClick();
    void onMiddleBtnClick();
}
