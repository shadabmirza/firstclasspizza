package to.done.lib.ui;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;
/**
 * @author shadab mirza Pradhan - Movivation Labs Pvt Ltd. (shadab mirza@movivation.com)
 */
 
public class FontManager {
 
    private static Typeface rupeeTypeFace;
    private static final String RUPEE_FONT_PATH="fonts/rupee.ttf";
    private static final String PATH_FONT="fonts/";
    private static Map<String,Typeface> fonts=new HashMap<String, Typeface>();
    public static Typeface getRupeeTypeFace(Context context) {

        if(rupeeTypeFace==null)
        {
            rupeeTypeFace=Typeface.createFromAsset(context.getAssets(), RUPEE_FONT_PATH);
        }

        return rupeeTypeFace;
    }
    public static Typeface getTypeface(Context context, String fontNameFromXML) {
        if(fonts.containsKey(fontNameFromXML)){
            return fonts.get(fontNameFromXML);
        }
        else
        {
            fonts.put(fontNameFromXML,Typeface.createFromAsset(context.getAssets(), PATH_FONT+fontNameFromXML));

            return fonts.get(fontNameFromXML);
        }


    }


}