package to.done.lib.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import to.done.lib.R;

/**
 * Created by shadab mirza on 4/17/14.
 */
public class ProgressDotView extends View {

    private static final int LEFT=1,RIGHT=2,TOP=3,BOTTOM=4;
    private int dotSelectedColor,dotUnselectedColor,lineSelectedColor,lineUnselectedColor;
    private Paint paint;
    private float lineThickness=3,circleRadius=7;
    private RectF rectLeft, rectRight;

    public static final int STATE_INCOMPLETE=0,STATE_IN_PROGRESS=1,STATE_COMPLETE=2;
    private int state=STATE_INCOMPLETE;


    public ProgressDotView(Context context) {
        super(context);
    }

    public ProgressDotView(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (!isInEditMode()) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ProgressDotView);
            dotSelectedColor = a.getColor(R.styleable.ProgressDotView_progressDotSelectedColor,0);
            dotUnselectedColor= a.getColor(R.styleable.ProgressDotView_progressDotUnselectedColor,0);

            lineSelectedColor = a.getColor(R.styleable.ProgressDotView_progressLineSelectedColor,0);
            lineUnselectedColor = a.getColor(R.styleable.ProgressDotView_progressLineUnselectedColor,0);

            lineThickness =a.getDimension(R.styleable.ProgressDotView_lineThickness, 3);
            circleRadius =a.getDimension(R.styleable.ProgressDotView_circleRadius,7);


            a.recycle();
        }


        Point a = new Point(0, 0);
        Point b = new Point(getWidth(),0);
        Point c = new Point((int)(0.5f*getWidth()),getHeight());

        paint = new Paint();
        paint.setAntiAlias(true);
    }


    public ProgressDotView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(rectLeft==null)
        {
            rectLeft = new RectF(0,0.5f*getHeight()-0.5f*lineThickness,0.5f*getWidth(),0.5f*getHeight()+0.5f*lineThickness);
        }
        if(rectRight==null)
        {
            rectRight = new RectF(0.5f*getWidth(),0.5f*getHeight()-0.5f*lineThickness,getWidth(),0.5f*getHeight()+0.5f*lineThickness);
        }
//        if(isInEditMode())
//            return;


        switch (state){

            case STATE_INCOMPLETE:{
                paint.setColor(lineUnselectedColor);
                canvas.drawRect(rectLeft,paint);

                paint.setColor(lineUnselectedColor);
                canvas.drawRect(rectRight,paint);

                paint.setColor(dotUnselectedColor);
                canvas.drawCircle(0.5f*getWidth(),0.5f*getHeight(),circleRadius,paint);
                break;
            }

            case STATE_IN_PROGRESS:{
                paint.setColor(lineSelectedColor);
                canvas.drawRect(rectLeft,paint);

                paint.setColor(lineUnselectedColor);
                canvas.drawRect(rectRight,paint);

                paint.setColor(dotSelectedColor);
                canvas.drawCircle(0.5f*getWidth(),0.5f*getHeight(),circleRadius,paint);
                break;

            }

            case STATE_COMPLETE:{
                paint.setColor(lineSelectedColor);
                canvas.drawRect(rectLeft,paint);

                paint.setColor(lineSelectedColor);
                canvas.drawRect(rectRight,paint);

                paint.setColor(dotSelectedColor);
                canvas.drawCircle(0.5f*getWidth(),0.5f*getHeight(),circleRadius,paint);
                break;

            }

        }



    }


    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
