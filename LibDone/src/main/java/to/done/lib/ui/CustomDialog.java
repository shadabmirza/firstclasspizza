package to.done.lib.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.Gravity;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;


/**
 * @author shadab mirza Pradhan - Movivation Labs Pvt. Ltd. (shadab mirza@movivation.com)
 * */

public class CustomDialog {

    public static AlertDialog createCustomDialog(Context context,String message,String positiveButton,String negativeButton,String middleButton,boolean isCancelable, final DialogListener dialogListener)
    {

        GoogleAnalytics gAnalytics = GoogleAnalytics.getInstance(context);
//        Tracker tracker = gAnalytics.getTracker(context.getResources().getString(R.string.ga_trackingId));

        AlertDialog.Builder dialogBuilder=null;

        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            dialogBuilder = new AlertDialog.Builder(context,AlertDialog.THEME_HOLO_LIGHT);
        }
        else
        {
            dialogBuilder = new AlertDialog.Builder(context);
        }

        if(message!=null&&message.length()!=0)
        {
//            tracker.sendEvent(GAConst.SCRN_DIALOG,GAConst.EVENT_CAT_DIALOG_SHOW,message,null);
            dialogBuilder.setMessage(message);
        }

        if(positiveButton!=null)
        {
        dialogBuilder.setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

                arg0.cancel();
                if(dialogListener!=null)
                dialogListener.onPositiveBtnClick();

            }
        });
    }
        if(negativeButton!=null)
        {
        dialogBuilder.setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

                arg0.cancel();
                if(dialogListener!=null)
                dialogListener.onNegativeBtnClick();

            }
        });
        }
        if(middleButton!=null)
        {
            dialogBuilder.setNeutralButton(middleButton, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {

                    arg0.cancel();
                    if(dialogListener!=null)
                    dialogListener.onMiddleBtnClick();

                }
            });
        }

        dialogBuilder.setCancelable(isCancelable);
        AlertDialog dialog=dialogBuilder.show();

        TextView txtMessage=(TextView)dialog.findViewById(android.R.id.message);
        if(txtMessage != null){
            txtMessage.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        return dialog;
    }




	@SuppressLint("NewApi")
	public static AlertDialog createCustomDialog(final Activity context, String strError,final  boolean finishActivity, boolean showRetryButton)
	    {

            GoogleAnalytics gAnalytics = GoogleAnalytics.getInstance(context);
//            Tracker tracker = gAnalytics.getTracker(context.getResources().getString(R.string.ga_trackingId));

            if(strError!=null&&strError.length()!=0)
            {
//                tracker.sendEvent("Dialog","show",strError,null);
            }


	    	AlertDialog.Builder customPonchoDialogBuilder=null;

	    	if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
	    		customPonchoDialogBuilder = new AlertDialog.Builder(context,AlertDialog.THEME_HOLO_LIGHT);
	    	}
	    	else
	    	{
	    		customPonchoDialogBuilder = new AlertDialog.Builder(context);
	    	}
	    	customPonchoDialogBuilder.setMessage(strError);
			
	        
	    	customPonchoDialogBuilder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface arg0, int arg1) {

                        arg0.cancel();
	                	if(finishActivity)
	                	{
                            context.finish();
	                	}

	                }
	        });
	    	
	    	/*if(showRetryButton)
	    		customPonchoDialogBuilder.setNegativeButton("Retry", new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface arg0, int arg1) {

                        context.retry();
	                	
	                }
	        });*/

            customPonchoDialogBuilder.setCancelable(false);

	    	AlertDialog dialog=customPonchoDialogBuilder.show();
	    	
	    	TextView txtMessage=(TextView)dialog.findViewById(android.R.id.message);
            if(txtMessage != null){
	    	    txtMessage.setGravity(Gravity.CENTER_HORIZONTAL);
            }
	    	return dialog;
	    }

    @SuppressLint("NewApi")
    public static AlertDialog createForceUpdateDialog(final Activity context, String strError, final boolean finishActivity, boolean showRetryButton)
    {

        AlertDialog.Builder customPonchoDialogBuilder=null;

        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            customPonchoDialogBuilder = new AlertDialog.Builder(context,AlertDialog.THEME_HOLO_LIGHT);
        }
        else
        {
            customPonchoDialogBuilder = new AlertDialog.Builder(context);
        }
        customPonchoDialogBuilder.setMessage(strError);


        customPonchoDialogBuilder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

                if(finishActivity)
                {
                    context.finish();
                }

            }
        });

        if(showRetryButton)
            customPonchoDialogBuilder.setNegativeButton("Update", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {

                    Intent goToMarket = new Intent(Intent.ACTION_VIEW)
                            .setData(Uri.parse("market://details?id=com.poncho"));
                    goToMarket.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

//                    ctx.startActivity(goToMarket);
//                    ctx.finish();
                }
            });

        AlertDialog dialog=customPonchoDialogBuilder.show();

        TextView txtMessage=(TextView)dialog.findViewById(android.R.id.message);
        txtMessage.setGravity(Gravity.CENTER_HORIZONTAL);

        return dialog;
    }


    public static ProgressDialog createProgressDialog(Context context,String message){

        ProgressDialog progressDialog;

        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            progressDialog = new ProgressDialog(context, ProgressDialog.THEME_HOLO_LIGHT);
        }
        else
        {
            progressDialog = new ProgressDialog(context);

        }
        progressDialog.setMessage(message);
        progressDialog.show();
        return progressDialog;
    }

}
