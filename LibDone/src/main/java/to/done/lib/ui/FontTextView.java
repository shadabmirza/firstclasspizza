package to.done.lib.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;

import to.done.lib.R;

/**
 * Created by shadab mirza on 4/12/13.
 */
public class FontTextView extends TextView{


    public FontTextView(Context context, AttributeSet attrs, int defStyle) {
        this(context, attrs);
    }

    public FontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        String fontNameFromXML=null;
        if (!isInEditMode()) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.font);
            fontNameFromXML = a.getString(R.styleable.font_fontName);

            if(fontNameFromXML!=null)
                setTypeface(FontManager.getTypeface(getContext(), fontNameFromXML));
        a.recycle();
        }
    }

    public FontTextView(Context context) {
        this(context,null);
    }



}
