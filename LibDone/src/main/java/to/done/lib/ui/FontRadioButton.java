package to.done.lib.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.RadioButton;

import to.done.lib.R;

/**
 * Created by shadab mirza on 4/12/13.
 */
public class FontRadioButton extends RadioButton {

    private String fontNameFromXML;

    public FontRadioButton(Context context, AttributeSet attrs, int defStyle) {
        this(context,attrs);
    }

    public FontRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (!isInEditMode()) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.font);

            if(a==null)return;

            fontNameFromXML = a.getString(R.styleable.font_fontName);
            if(fontNameFromXML!=null)
                setTypeface(FontManager.getTypeface(getContext(), fontNameFromXML));

        }
    }

    public FontRadioButton(Context context) {
        this(context,null);
    }



}
