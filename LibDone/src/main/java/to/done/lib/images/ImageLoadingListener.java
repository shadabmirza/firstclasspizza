package to.done.lib.images;


public interface ImageLoadingListener {

	void onLoadingStarted(String url);

	void onLoadingFailed(String url, String reason);

	void onLoadingComplete(String url);

	
	
}
