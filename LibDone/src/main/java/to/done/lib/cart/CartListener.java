package to.done.lib.cart;

/**
 * Created by shadab mirza on 4/24/14.
 */
public interface CartListener {

    void onCartChange();
}
