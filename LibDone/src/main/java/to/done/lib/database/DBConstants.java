package to.done.lib.database;

/**
 * Created by shadab mirza on 10/4/14.
 */
public final class DBConstants {


    public static final String TABLE_OUTLET="outlet";

    //    public static final String company_id=;
//    public static final String id
//    public static final String name
//    public static final String description
//    public static final String code
//    public static final String row_status
//    public static final String created_at
//    public static final String modified_at
//    public static final String min_order_amnt
//    public static final String taxes
//    public static final String enable_push_notification
//    public static final String enable_email
//    public static final String enable_call
//    public static final String enable_sms
//    public static final String delivery_type
//    public static final String address_id
//    public static final String lat
//    public static final String lon
//    public static final String delivery_time
//    public static final String service_type
    public static final String OUTLET_PRODUCT_MAPPING_TIMESTAMP="opm_timestamp";
    public static final String OUTLET_TIMESTAMP="outlet_timestamp";
    public static final String PRODUCT_TIMESTAMP="product_timestamp";
    public static final String CATEGORY_TIMESTAMP="category_timestamp";
    public static final String PRODUCT_ATTRIBUTE_GROUP_TIMESTAMP="product_attribute_group_timestamp";
    public static final String PRODUCT_ATTRIBUTE_MAPPING_TIMESTAMP="product_attribute_mapping_timestamp";
    public static final String PRODUCT_ATTRIBUTE_TIMESTAMP="product_attribute_timestamp";
    public static final String PRODUCT_CATEGORY_MAPPING="product_category_mapping_timestamp";
}