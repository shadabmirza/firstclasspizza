package to.done.lib.database;

import android.content.Context;
import android.database.Cursor;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;

import to.done.lib.Constants;
import to.done.lib.entity.cachingtables.CacheForData;
import to.done.lib.entity.nearestoutlets.Outlet;
import to.done.lib.entity.nearestoutlets.Outlet_attribute;
import to.done.lib.entity.nearestoutlets.Outlet_attributes_group;
import to.done.lib.entity.nearestoutlets.Outlet_attributes_mapping;
import to.done.lib.entity.nearestoutlets.Outlet_timing;
import to.done.lib.entity.order.Applied_extra_charge;
import to.done.lib.entity.order.Applied_offer;
import to.done.lib.entity.order.Customization;
import to.done.lib.entity.order.Order;
import to.done.lib.entity.order.Order_Details;
import to.done.lib.entity.outletmenu.Category;
import to.done.lib.entity.outletmenu.Outlet_product_mapping;
import to.done.lib.entity.outletmenu.Product;
import to.done.lib.entity.outletmenu.Product_attribute;
import to.done.lib.entity.outletmenu.Product_attribute_group;
import to.done.lib.entity.outletmenu.Product_attribute_mapping;
import to.done.lib.entity.outletmenu.Product_category_mapping;
import to.done.lib.entity.outletsubareas.Area;
import to.done.lib.entity.outletsubareas.Subarea;
import to.done.lib.entity.user.Address;
import to.done.lib.entity.user.User;
import to.done.lib.utils.Toolbox;

//import to.done.lib.entity.nearestoutlets.Outlet_Area_Mapping;
//import to.done.lib.entity.nearestoutlets.Outlet_SubArea_Mapping;

/**
 * Created by shadab mirza on 10/4/14.
 */
public final class DBManager extends DBTools {

    private static DBHelper dbHelper;
    private static DBManager dbManager;
    private Dao<Outlet, Integer> outletDao = null;
    private Dao<Outlet_attribute, Integer> outletAttributeDao = null;
    private Dao<Outlet_timing, Integer> outletTimingDao = null;
    private Dao<Outlet_attributes_group, Integer> outletAttributesGroupDao = null;
    private Dao<Outlet_attributes_mapping, Integer> outletAttributesMappingDao = null;
    private Dao<Category, Integer> categoryDao = null;
    private Dao<Product, Integer> productDao = null;
    private Dao<Product_category_mapping, Integer> productCategoryMappingDao = null;
    private Dao<Outlet_product_mapping, Integer> outletProductMappingDao = null;
    private Dao<Product_attribute, Integer> productAttributeDao = null;
    private Dao<Product_attribute_group, Integer> productAttributeGroupDao = null;
    private Dao<Product_attribute_mapping, Integer> productAttributeMappingDao = null;
    private Dao<CacheForData, Integer> cacheForDatasDao = null;
    private Dao<User, Long> userDao;
    private Dao<Address, Long> addressDao;
    private Dao<Order, Long> ordersDao;
    private Dao<Order_Details, Long> ordersDetailsDao;
    private Dao<Area,Long>areasDao;
    private Dao<Subarea,Long>subareasDao;
    private Dao<Applied_extra_charge,Long>extraChargesDao;
    private Dao<Applied_offer,Long>offerDao;
    public DBManager(Context context) {
        dbHelper = new DBHelper(context);
    }

    public static DBManager getInstance(Context context) {

        if (dbManager == null) {
            dbManager = new DBManager(context);
        }

        return dbManager;
    }

    private Dao<Area, Long> getAreasDao() throws SQLException {
        if (areasDao == null) {
            areasDao = dbHelper.getDao(Area.class);
        }
        return areasDao;
    }
    private Dao<Subarea, Long> getSubareasDao() throws SQLException {
        if (subareasDao == null) {
            subareasDao = dbHelper.getDao(Subarea.class);
        }
        return subareasDao;
    }

    private Dao<Order, Long> getOrdersDao() throws SQLException {
        if (ordersDao == null) {
            ordersDao = dbHelper.getDao(Order.class);
        }
        return ordersDao;
    }

    private Dao<Order_Details, Long> getOrdersDetailsDao() throws SQLException {
        if (ordersDetailsDao == null) {
            ordersDetailsDao = dbHelper.getDao(Order_Details.class);
        }
        return ordersDetailsDao;
    }


    private Dao<User, Long> getUserDao() throws SQLException {
        if (userDao == null) {
            userDao = dbHelper.getDao(User.class);
        }
        return userDao;
    }

    private Dao<CacheForData, Integer> getCacheForDatasDao() throws SQLException {
        if (cacheForDatasDao == null) {
            cacheForDatasDao = dbHelper.getDao(CacheForData.class);
        }
        return cacheForDatasDao;
    }

    private Dao<Address, Long> getAddressDao() throws SQLException {
        if (addressDao == null) {
            addressDao = dbHelper.getDao(Address.class);
        }
        return addressDao;
    }

    private Dao<Outlet, Integer> getOutletDao() throws SQLException {
        if (outletDao == null) {
            outletDao = dbHelper.getDao(Outlet.class);
        }
        return outletDao;
    }

    private Dao<Outlet_attribute, Integer> getOutletAttributeDao() throws SQLException {
        if (outletAttributeDao == null) {
            outletAttributeDao = dbHelper.getDao(Outlet_attribute.class);
        }
        return outletAttributeDao;
    }

    private Dao<Outlet_timing, Integer> getOutletTimingDao() throws SQLException {
        if (outletTimingDao == null) {
            outletTimingDao = dbHelper.getDao(Outlet_timing.class);
        }
        return outletTimingDao;
    }

    private Dao<Outlet_attributes_group, Integer> getOutletAttributesGroupDao() throws SQLException {
        if (outletAttributesGroupDao == null) {
            outletAttributesGroupDao = dbHelper.getDao(Outlet_attributes_group.class);
        }
        return outletAttributesGroupDao;
    }

    private Dao<Outlet_attributes_mapping, Integer> getOutletAttributesMappingDao() throws SQLException {
        if (outletAttributesMappingDao == null) {
            outletAttributesMappingDao = dbHelper.getDao(Outlet_attributes_mapping.class);
        }

        return outletAttributesMappingDao;
    }
    private Dao<Applied_extra_charge,Long>getExtraChargesDao()throws SQLException{
        if(extraChargesDao==null){
            extraChargesDao=dbHelper.getDao(Applied_extra_charge.class);
        }
        return extraChargesDao;
    }

    private Dao<Applied_offer,Long>getOffersDao()throws SQLException{
        if(offerDao==null){
            offerDao=dbHelper.getDao(Applied_offer.class);
        }
        return offerDao;
    }


    private Dao<Category, Integer> getCategoryDao() throws SQLException {
        if (categoryDao == null) {
            categoryDao = dbHelper.getDao(Category.class);
        }

        return categoryDao;
    }


    private Dao<Product, Integer> getProductDao() throws SQLException {
        if (productDao == null) {
            productDao = dbHelper.getDao(Product.class);
        }

        return productDao;
    }

    private Dao<Product_category_mapping, Integer> getProductCategoryMappingDao() throws SQLException {
        if (productCategoryMappingDao == null) {
            productCategoryMappingDao = dbHelper.getDao(Product_category_mapping.class);
        }

        return productCategoryMappingDao;
    }

    public Dao<Outlet_product_mapping, Integer> getOutletProductMappingDao() throws SQLException {
        if (outletProductMappingDao == null) {
            outletProductMappingDao = dbHelper.getDao(Outlet_product_mapping.class);
        }
        return outletProductMappingDao;
    }

    public Dao<Product_attribute, Integer> getProductAttributeDao() throws SQLException {
        if (productAttributeDao == null) {
            productAttributeDao = dbHelper.getDao(Product_attribute.class);
        }
        return productAttributeDao;
    }

    public Dao<Product_attribute_group, Integer> getProductAttributeGroupDao() throws SQLException {
        if (productAttributeGroupDao == null) {
            productAttributeGroupDao = dbHelper.getDao(Product_attribute_group.class);
        }
        return productAttributeGroupDao;
    }

    public Dao<Product_attribute_mapping, Integer> getProductAttributeMappingDao() throws SQLException {
        if (productAttributeMappingDao == null) {
            productAttributeMappingDao = dbHelper.getDao(Product_attribute_mapping.class);
        }
        return productAttributeMappingDao;
    }

    public void insertOutletList(List<Outlet> outletList) throws SQLException {
        if (outletList == null) {
            return;
        }

        for (Outlet o : outletList) {
            getOutletDao().create(o);
        }
    }

    public void insertOutlet(Outlet outlet) throws SQLException {
        if (outlet != null) {
            getOutletDao().create(outlet);

        }

    }

    public void insertUser(User user) throws SQLException {
        if (user == null) return;
        getUserDao().createOrUpdate(user);
    }

    public void insertAddress(Address address) throws SQLException {
        if (address == null) return;
        getAddressDao().createOrUpdate(address);
    }

    public void insertAddressTag(Long addressId, String tag) throws SQLException {
        if (addressId != null && tag!=null) {
            getAddressDao().executeRaw("update 'address' set tag= '" + tag + "' where id= " + addressId);
        }
    }

    public void insertOutletAttributesMappingList(List<Outlet_attributes_mapping> outletAttributesMappingList) throws SQLException {
        if (outletAttributesMappingList == null) {
            return;
        }

        for (Outlet_attributes_mapping oam : outletAttributesMappingList) {
            getOutletAttributesMappingDao().create(oam);
        }
    }

    public void insertOutletAttributesMapping(Outlet_attributes_mapping outletAttributesMapping) throws SQLException {
        if (outletAttributesMapping != null) {
            getOutletAttributesMappingDao().create(outletAttributesMapping);

        }

    }

    public void insertOutletAttributeList(List<Outlet_attribute> outletAttributeList) throws SQLException {
        if (outletAttributeList == null) {
            return;
        }

        for (Outlet_attribute oa : outletAttributeList) {
            getOutletAttributeDao().create(oa);
        }
    }

    public void insertOutletAttribute(Outlet_attribute outletAttribute) throws SQLException {
        if (outletAttribute != null) {
            getOutletAttributeDao().create(outletAttribute);

        }
    }


    public void insertOutletAttributesGroupList(List<Outlet_attributes_group> outletAttributesGroupList) throws SQLException {
        if (outletAttributesGroupList == null) {
            return;
        }

        for (Outlet_attributes_group oag : outletAttributesGroupList) {
            getOutletAttributesGroupDao().create(oag);
        }
    }

    public void insertOutletAttributeGroup(Outlet_attributes_group outletAttributesGroup) throws SQLException {
        if (outletAttributesGroup != null) {
            getOutletAttributesGroupDao().create(outletAttributesGroup);
        }
    }

    public void insertOutletTimingList(List<Outlet_timing> outletTimingList) throws SQLException {
        if (outletTimingList == null) {
            return;
        }
        for (Outlet_timing ot : outletTimingList) {

            getOutletTimingDao().create(ot);
        }
    }

    public void insertOutletTiming(Outlet_timing outletTiming) throws SQLException {
        if (outletTiming != null) {
            getOutletTimingDao().create(outletTiming);

        }

    }

    public void insertCategoryList(List<Category> categoryList) throws SQLException {
        if (categoryList == null) {
            return;
        }
        for (Category cat : categoryList) {
            getCategoryDao().create(cat);
        }
    }

    public void insertCategory(Category category) throws SQLException {
        if (category != null) {
            getCategoryDao().create(category);

        }

    }

    public List<Outlet> getOutletsForCompany() {
        try {
            return getOutletDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


    public void insertProductList(List<Product> productList) throws SQLException {
        if (productList == null) {
            return;
        }
        for (Product prod : productList) {
            getProductDao().create(prod);
        }
    }

    public void insertProduct(Product product) throws SQLException {
        if (product != null) {
            getProductDao().create(product);

        }

    }


    public void insertProductCategoryMappingList(List<Product_category_mapping> productCategoryMappingList) throws SQLException {
        if (productCategoryMappingList == null) {
            return;
        }
        for (Product_category_mapping pcm : productCategoryMappingList) {
            getProductCategoryMappingDao().create(pcm);
        }
    }

    public void insertProductCategoryMapping(Product_category_mapping productCategoryMapping) throws SQLException {
        if (productCategoryMapping != null) {
            getProductCategoryMappingDao().create(productCategoryMapping);
        }

    }


    public void insertOutletProductMappingList(List<Outlet_product_mapping> outletProductMappingList) throws SQLException {
        if (outletProductMappingList != null) {
            for (Outlet_product_mapping opm : outletProductMappingList) {
                getOutletProductMappingDao().create(opm);
            }
        }
    }

    public void insertOutletProductMapping(Outlet_product_mapping outletProductMapping) throws SQLException {
        if (outletProductMapping != null) {
            getOutletProductMappingDao().create(outletProductMapping);
        }

    }

    public void insertProductAttributeList(List<Product_attribute> productAttributeList) throws SQLException {
        if (productAttributeList != null) {
            for (Product_attribute pa : productAttributeList) {
                getProductAttributeDao().create(pa);
            }
        }
    }

    public void insertProductAttribute(Product_attribute productAttribute) throws SQLException {
        if (productAttribute != null) {
            getProductAttributeDao().create(productAttribute);
        }
    }

    public void insertProductAttributeGroupList(List<Product_attribute_group> productAttributeGroupList) throws SQLException {
        if (productAttributeGroupList != null) {
            for (Product_attribute_group pag : productAttributeGroupList) {
                getProductAttributeGroupDao().create(pag);
            }
        }
    }

    public void insertProductAttributeGroup(Product_attribute_group productAttributeGroup) throws SQLException {
        if (productAttributeGroup != null) {
            getProductAttributeGroupDao().create(productAttributeGroup);
        }
    }

    public void insertProductAttributeMappingList(List<Product_attribute_mapping> productAttributeMappingList) throws SQLException {
        if (productAttributeMappingList != null) {
            for (Product_attribute_mapping pam : productAttributeMappingList) {
                getProductAttributeMappingDao().create(pam);
            }
        }
    }

    public void insertProductAttributeMapping(Product_attribute_mapping productAttributeMapping) throws SQLException {
        if (productAttributeMapping != null) {
            getProductAttributeMappingDao().create(productAttributeMapping);
        }
    }

    public List<Outlet> getOutlets(double lat, double lon, Outlet.SortType sortType) {

        try {
            List<Outlet> outletList;

            if (sortType == Outlet.SortType.RATING) {
                outletList = getOutletDao().queryBuilder().orderBy("rating", false).query();
            } else {
                outletList = getOutletDao().queryForAll();
            }


            if (sortType == Outlet.SortType.DISTANCE) {

                for (Outlet o : outletList) {
                    if (lat != 0 && lon != 0 && o.getLat() != null && o.getLon() != null) {
                        double dist = Toolbox.calculateDistance(lat, lon, o.getLat(), o.getLon());
                        o.setDistanceReal((float) dist);
                    }
                }

                Comparator<Outlet> distanceComparator = new Comparator<Outlet>() {
                    @Override
                    public int compare(Outlet outlet, Outlet outlet2) {

                        if (outlet.getDistanceReal() != null && outlet.getDistanceReal() != null) {
                            return outlet.getDistanceReal().compareTo(outlet2.getDistanceReal());
                        } else {
                            return 0;
                        }
                    }
                };
                Collections.sort(outletList, distanceComparator);
            }
            return outletList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Order> getOrderDetailsForOrderId() throws SQLException {

        List<Order> ordersInDb = getOrdersDao().queryForAll();
        if (ordersInDb == null || ordersInDb.size() < 1) {
            return null;
        }
        for (Order order : ordersInDb) {
            if (order == null) {
                Toolbox.writeToLog("Order was null");
                return null;
            }
            ArrayList<Product> productsForOrder = new ArrayList<Product>();
            double total=0;
            ArrayList<Order_Details> detailsForOrder = (ArrayList) getOrdersDetailsDao().queryForEq("order_id", order.getOrder_id());
            if (detailsForOrder != null && detailsForOrder.size() > 0) {
                for (Order_Details d : detailsForOrder) {
                    if (d != null) {
                        if (d.getParent_order_detail_id() == null) {
                            Product p = null;

                            List<Product> productList = getProductList(dbHelper, "select * from product where id=" + d.getProduct_id());

                            if (productList != null && productList.size() > 0)
                                p = productList.get(0);
                            else  p=new Product();
                                   if (p != null) {
                                       p.setId(d.getProduct_id());
                                       p.setOutlet_product_id(d.getOutlet_product_id());
                                       p.setDefault_price(d.getPrice());
                                       total += d.getPrice();
                                       ArrayList<Customization> custForProduct = new ArrayList<Customization>();
                                       ArrayList<Order_Details> customisations = (ArrayList) getOrdersDetailsDao().queryForEq("parent_order_detail_id", d.getId());
                                       if (customisations != null && customisations.size() > 0) {
                                           for (Order_Details c : customisations) {
                                               Product cs = p;//roductList.get(0);
                                               Customization cst = new Customization();
                                               Toolbox.writeToLog("*" + c + "*");
                                               cst.setOutlet_product_id(c.getOutlet_product_id());
                                               cst.setName(c.getName());
                                               cst.setPrice(c.getPrice());
                                               total += c.getPrice();
                                               cst.setParent_outlet_product_id(d.getOutlet_product_id());
                                               Toolbox.writeToLog("**" + cst + "**");
                                               p.setDefault_price(p.getPrice() + cst.getPrice());
                                               cst.setId(cs.getId());
                                               custForProduct.add(cst);
                                           }
                                           p.setCustomization(custForProduct);

                                           Toolbox.writeToLog("***" + p + "***");

                                       } productsForOrder.add(p);
                                   }

                            }
                            }

                }
            }

            order.setProducts(productsForOrder);
            order.setTotal(total);
            order.setApplied_extra_charges(getExtraChrges());
            order.setApplied_offer(getOffers());
        }
        Toolbox.writeToLog("Order details  " + ordersInDb.size());
        return ordersInDb;
    }


    public List<Category> getCategories() {

        try {
            return getCategoryDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Category> getCategoriesByOutletIdWithCount(long outletId) {

        String query = "SELECT c.*,COUNT(*) AS products_count FROM outlet_product_mapping opm INNER JOIN product_category_mapping pcm ON opm.product_id=pcm.product_id INNER JOIN category c ON pcm.category_id=c.id WHERE opm.outlet_id='" + outletId + "' AND opm.parent_outlet_product_id IS NULL GROUP BY c.id ORDER BY c.sr";

        return getCategoryList(dbHelper, query);
    }

    public List<Outlet> getLastFiveOutlets() {
        // String query="select * from outlet order by id desc limit 5";

        ArrayList<Outlet> returnList = new ArrayList<Outlet>();
        try {

            List<Outlet> outs = getOutletDao().queryBuilder().limit(5l).query();
            return outs;

        } catch (Exception e) {

        }
        return null;
    }

    public List<Outlet> getOutletsByCompany(Long company_id) {
        try {
            if (getOutletDao() != null) {
                return getOutletDao().queryForAll();
            }
        } catch (Exception e) {

        }
        return null;
    }

    public List<Product_attribute> getAttributesForProduct(Long productId) {
        String query = "SELECT pa.* FROM `product` p inner join product_attribute_mapping pam on p.id=pam.product_id inner join product_attribute pa on pa.id=pam.product_attribute_id where p.id =" + productId + " order by pa.sr";
        if (productId == null)
            return null;
        else {
            try {
                GenericRawResults<Product_attribute> attribs = getProductAttributeDao().queryRaw(query, getProductAttributeDao().getRawRowMapper());
                return attribs.getResults();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }

    }


    public List<Outlet> getOutletsByAttributes(List<Long> outletAttributeIdsList, double lat, double lon, Outlet.SortType sortType) {

        String sortByString = "";
        if (sortType == Outlet.SortType.RATING) {
            sortByString = " ORDER BY rating DESC ";
        }

        StringBuilder inBuilder = new StringBuilder();

        for (int i = 0; i < outletAttributeIdsList.size(); i++) {
            Long id = outletAttributeIdsList.get(i);
            inBuilder.append("'");
            inBuilder.append(id);
            inBuilder.append("'");

            if (i < outletAttributeIdsList.size() - 1) {
                inBuilder.append(",");
            }

        }

        String query = "SELECT o.* FROM outlet_attributes_mapping oam INNER JOIN outlet o ON oam.outlet_id=o.id WHERE oam.outlet_attributes_id IN (" + inBuilder.toString() + ") GROUP BY o.id " + sortByString;

        try {
            GenericRawResults<Outlet> rawResults = getCategoryDao().queryRaw(query, getOutletDao().getRawRowMapper());

            List<Outlet> outletList = rawResults.getResults();
            rawResults.close();

            if (sortType == Outlet.SortType.DISTANCE) {

                for (Outlet o : outletList) {
                    if (lat != 0 && lon != 0 && o.getLat() != null && o.getLon() != null) {
                        double dist = Toolbox.calculateDistance(lat, lon, o.getLat(), o.getLon());
                        o.setDistanceReal((float) dist);
                    }
                }

                Comparator<Outlet> distanceComparator = new Comparator<Outlet>() {
                    @Override
                    public int compare(Outlet outlet, Outlet outlet2) {
                        if (outlet.getDistanceReal() != null && outlet2.getDistanceReal() != null) {
                            return outlet.getDistanceReal().compareTo(outlet2.getDistanceReal());
                        } else {
                            return 0;
                        }

                    }
                };
                Collections.sort(outletList, distanceComparator);
            }


            return outletList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void insertAreas(List<Area>areas,Long outletId){
        try{
            for(Area area:areas){
                area.setOutletId(outletId);
                getAreasDao().createIfNotExists(area);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void insertSubareas(List<Subarea>subareas,Long outlet_id){
        try{
            for(Subarea sub:subareas){
                sub.setOutlet_id(outlet_id);
                getSubareasDao().createIfNotExists(sub);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public List<Applied_extra_charge> getExtraChrges(){
        try{
            return getExtraChargesDao().queryForAll();
        }catch (Exception e){
            e.printStackTrace();
            return  null;
        }
    }

    public List<Applied_offer> getOffers(){
        try{
            return getOffersDao().queryForAll();
        }catch (Exception e){
            e.printStackTrace();
            return  null;
        }
    }
    public void insertExtraCharges(List<Applied_extra_charge>charges){
        try{
            if(charges==null || charges.size()==0) return;
            for(Applied_extra_charge charge:charges)
            getExtraChargesDao().createOrUpdate(charge);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void insertOffers(List<Applied_offer>offers){
        try{
            if(offers==null || offers.size()==0) return;
            for(Applied_offer charge:offers)
                getOffersDao().createOrUpdate(charge);
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    public void insertOrders(List<Order> orders) throws SQLException {
        if (orders == null || orders.size() <= 0) {
            return;
        }
        int quant=0;
        for (Order ord : orders) {

            getOrdersDao().create(ord);
            List<Order_Details> detailsForOrder = new ArrayList<Order_Details>();

            for (Product p : ord.getProducts()) {
                Order_Details det = new Order_Details();
                UUID id = UUID.randomUUID();
                det.setOutlet_product_id(p.getOutlet_product_id());
                det.setOrder_id(ord.getOrder_id());
                det.setPrice(p.getPrice());
                det.setId(id);
                det.setName(p.getName());
                det.setProduct_id(p.getId());
                det.setParent_order_detail_id(null);
                det.setParent_outlet_product_id(null);
                detailsForOrder.add(det);
                for (Customization cust : p.getCustomization()) {
                    Order_Details det1 = new Order_Details();
                    UUID id1 = UUID.randomUUID();
                    det1.setOutlet_product_id(cust.getOutlet_product_id());
                    det1.setParent_outlet_product_id(p.getOutlet_product_id());
                    det1.setParent_order_detail_id(id);
                    det1.setProduct_id(cust.getId());
                    det1.setPrice(cust.getPrice());
                    det1.setId(id1);
                    det1.setName(cust.getName());
                    det1.setOrder_id(null);
                    detailsForOrder.add(det1);
                }
            }
            insertOrderDetails(detailsForOrder);
        }

    }
public boolean isOrderPresentInDB(){
    try {
        ArrayList<Order>orders= (ArrayList<Order>) getOrdersDao().queryForAll();
        if(orders!=null && orders.size()>0){
            return true;
        }else return false;
    } catch (SQLException e) {
        e.printStackTrace();
        return false;
    }}

    public Order getOrder() {
        try {
            ArrayList<Order> orders = (ArrayList<Order>) getOrdersDao().queryForAll();
            if (orders != null && orders.size() > 0) {
                return orders.get(0);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }
    public void insertOrderDetails(List<Order_Details> details) throws SQLException {

        if (details == null || details.size() <= 0) {
            return;
        }
        for (Order_Details det : details) {
            getOrdersDetailsDao().create(det);
        }
    }

    public List<Outlet_attributes_group> getOutletAttributesGroups() {

        try {
            return getOutletAttributesGroupDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Outlet getOutletById(Long outletId) {
        try {
            return getOutletDao().queryForId(outletId.intValue());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Outlet_attributes_group> getOutletAttributesGroupsWithAttrs() {

        try {

            List<Outlet_attributes_group> outletAttributesGroupList = getOutletAttributesGroupDao().queryForAll();

            if (outletAttributesGroupList != null) {

                for (Outlet_attributes_group oag : outletAttributesGroupList) {

                    List<Outlet_attribute> outletAttributeList = getOutletAttributesForGroup(oag.getId().longValue());
                    if (outletAttributeList != null) {
                        oag.setOutletAttributeList(outletAttributeList);
                    }
                }
            }
            return outletAttributesGroupList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Outlet_attribute> getOutletAttributesForGroup(long outletAttributesGroupId) {

//        try {

        String query =
                "SELECT oa.*,count(*) as outlets_count FROM outlet_attribute oa INNER JOIN outlet_attributes_mapping oam ON oa.id=oam.outlet_attributes_id WHERE oa.outlet_attributes_group_id='" + outletAttributesGroupId + "' GROUP BY oa.id";


//            GenericRawResults<Outlet_attribute> rawResults = getOutletAttributeDao().queryRaw(query, getOutletAttributeDao().getRawRowMapper());

        List<Outlet_attribute> outletAttributeList = getOutletAttributesList(dbHelper, query);
        //rawResults.getResults();
//            rawResults.close();


        return outletAttributeList;
//            return getOutletAttributeDao().queryForEq("outlet_attributes_group_id", outletAttributesGroupId);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return null;
    }

    public List<Category> getCustCatsWithProds(long productId) {

        try {
            String query =
                    " SELECT c.*  FROM " +
                            " outlet_product_mapping opm " +
                            " INNER JOIN outlet_product_mapping opm_p ON  opm_p.id=opm.parent_outlet_product_id " +
                            " INNER JOIN product_category_mapping pcm ON opm.product_id=pcm.product_id " +
                            " INNER JOIN category c ON pcm.category_id=c.id " +
                            " WHERE opm.parent_outlet_product_id IS NOT NULL " +
                            " AND opm_p.product_id='" + productId + "' " +
                            " AND opm_p.parent_outlet_product_id IS NULL " +
                            " GROUP BY c.id ";
            GenericRawResults<Category> rawResults = getCategoryDao().queryRaw(query, getCategoryDao().getRawRowMapper());

            List<Category> categoryList = rawResults.getResults();
            rawResults.close();


            for (Category c : categoryList) {

                c.setProductList(getCustForCat(c.getId(), productId));

            }

            return categoryList;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Area>getAreasForOutlet(Long outletId){
        try {
            List<Area>areas=getAreasDao().queryForEq("outletId", outletId);
            return areas;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }


    public List<Subarea>getAllSubsForOutlet(Long outletid){
        try {
         //   String q="\"%"+key+"%\"";
            String query="SELECT s.id,s.subarea_name,s.area_id from subarea s inner join outlet o on o.id=s.outlet_id where o.id="+outletid;

            //inner join area a on a.id=s.area_id
            GenericRawResults<Subarea>suba=getSubareasDao().queryRaw(query, getSubareasDao().getRawRowMapper());

            return suba.getResults();

        }catch (Exception e){
            e.printStackTrace();
            return  null;
        }
    }
    public List<Subarea>getSubareasForOutlet(Long areaId){
        try {
            List<Subarea>subareas=getSubareasDao().queryForEq("area_id", areaId);
            return subareas;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public List<Subarea>searchSubareasForOutlet(Long outletid,String key){
        try {
            String q="\"%"+key+"%\"";
            String query = "SELECT s.id,s.subarea_name,s.area_id from subarea s inner join area a on a.id=s.area_id inner join outlet o on o.id=a.outletId where o.id="+outletid+" and subarea_name like "+q;

            GenericRawResults<Subarea>suba=getSubareasDao().queryRaw(query, getSubareasDao().getRawRowMapper());
            return suba.getResults();
        }catch (Exception e){
            e.printStackTrace();
            return  null;
        }
        //return null;
    }


    public List<Product> getCustForCat(long categoryId, long productId) {

        String query =
                " SELECT p.id,p.code,p.name,p.description,p.photo_url,p.row_status,p.created_at,p.modified_at,p.company_id, " +
                        " opm.id AS outlet_product_id, opm.parent_outlet_product_id,opm.outlet_id,opm.sort_no,opm.price " +
                        " FROM product_category_mapping pcm " +
                        " INNER JOIN product p ON pcm.product_id=p.id " +
                        " INNER JOIN outlet_product_mapping opm ON p.id=opm.product_id " +
                        " INNER JOIN outlet_product_mapping opm_p ON  opm_p.id=opm.parent_outlet_product_id " +
                        " WHERE pcm.category_id='" + categoryId + "' AND opm.parent_outlet_product_id IS NOT NULL AND opm_p.product_id='" + productId + "' " +
                        " AND opm_p.parent_outlet_product_id IS NULL  GROUP BY p.id ";
        ;


        List<Product> productList = getProductList(dbHelper, query);

        return productList;
    }

    public List<Category> getCustomCatsWithProdsByMappingId(List<Long> outletProductIdList) {


        StringBuilder inBuidlder = new StringBuilder();
        inBuidlder.append("(");
        for (int i = 0; i < outletProductIdList.size(); i++) {
            Long id = outletProductIdList.get(i);
            inBuidlder.append("'");
            inBuidlder.append(id);
            inBuidlder.append("'");

            if (i < outletProductIdList.size() - 1) {
                inBuidlder.append(",");
            }

        }
        inBuidlder.append(")");

        try {
            String query =
                    " SELECT c.*  FROM " +
                            " outlet_product_mapping opm " +
                            " INNER JOIN product_category_mapping pcm ON opm.product_id=pcm.product_id " +
                            " INNER JOIN category c ON pcm.category_id=c.id " +
//                            " WHERE opm.parent_outlet_product_id = '"+outletProductId+"' " +
                            " WHERE opm.parent_outlet_product_id IN " + inBuidlder.toString() +
                            " GROUP BY c.id ORDER BY c.sr";
            GenericRawResults<Category> rawResults = getCategoryDao().queryRaw(query, getCategoryDao().getRawRowMapper());

            List<Category> categoryList = rawResults.getResults();
            rawResults.close();


            for (Category c : categoryList) {

                c.setProductList(getCustForCatByMappingId(c.getId(), inBuidlder.toString()));

            }

            return categoryList;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Product> getCustForCatByMappingId(long categoryId, String inString) {//long outletProductId){

        String query =

                " SELECT p.id,p.code,p.name,p.description,p.photo_url,p.row_status,p.created_at,p.modified_at,p.company_id, " +
                        " opm.id AS outlet_product_id, opm.parent_outlet_product_id,opm.outlet_id,opm.sort_no,opm.price, opm.default_selected " +
                        " FROM product_category_mapping pcm " +
                        " INNER JOIN product p ON pcm.product_id=p.id " +
                        " INNER JOIN outlet_product_mapping opm ON p.id=opm.product_id " +
                        " WHERE pcm.category_id='" + categoryId + "' AND opm.parent_outlet_product_id IN " + inString +//='"+outletProductId+"' "+
                        " GROUP BY p.id ";
        ;


        List<Product> productList = getProductList(dbHelper, query);

        return productList;
    }


    public Product getProductByOPMID(long opmid){
        String query =
                "SELECT p.id,p.code,p.name,p.description,p.photo_url,p.row_status,p.created_at,p.modified_at,p.company_id," +
                        "opm.id AS outlet_product_id, opm.parent_outlet_product_id,opm.outlet_id,opm.sort_no,opm.price, " +
                        " count(def_opm.id) AS cust_count, def_opm.id AS default_outlet_product_id, def_opm.product_id AS default_product_id, def_opm.parent_outlet_product_id AS default_parent_outlet_product_id, " +
//                        " def_opm.price AS default_price, " +
//                        " CASE WHEN opm.price=0 THEN def_opm.price ELSE opm.price END AS default_price, " +
//                        " def_opm.price AS default_price, " +
                        " CASE WHEN def_opm.default_selected = 1 THEN def_opm.price ELSE 0 END AS default_price, " +
                        " CASE WHEN def_opm.default_selected = 1 THEN def_p.name ELSE '' END AS default_name, " +
                        " CASE WHEN def_opm.default_selected = 1 THEN def_p.description ELSE '' END AS default_description,  " +

//                        " def_p.name AS default_name, def_p.description AS default_description," +
                        " pcm.category_id, MAX(def_opm.default_selected) AS default_selected " +
                        " FROM product p INNER JOIN outlet_product_mapping opm ON p.id=opm.product_id " +
                        " LEFT JOIN outlet_product_mapping def_opm ON opm.id=def_opm.parent_outlet_product_id " +
                        " LEFT JOIN product def_p ON def_p.id=def_opm.product_id " +
                        " LEFT JOIN product_attribute_mapping pam ON pam.product_id=p.id " +
                        " INNER JOIN product_category_mapping pcm ON pcm.product_id=p.id" +
                        " INNER JOIN category c ON c.id=pcm.category_id "+
                        " WHERE opm.parent_outlet_product_id IS NULL AND opm.id="+opmid;//AND (def_opm.default_selected=1 OR def_opm.default_selected IS NULL)";

        List<Product> productList = getProductList(dbHelper, query);
        if(productList!=null && productList.size()>0)
            return productList.get(0);
        else return null;
    }

    public List<Product> getProductsForOutlet(long outletId, Product.SortType sortType) {
//        String query=
//                "SELECT p.id,p.code,p.name,p.description,p.photo_url,p.row_status,p.created_at,p.modified_at,p.company_id," +
//                        "opm.id AS outlet_product_id, opm.parent_outlet_product_id,opm.outlet_id,opm.sort_no,opm.price, " +
//                        " def_opm.id AS default_outlet_product_id, def_opm.product_id AS default_product_id, def_opm.parent_outlet_product_id AS default_parent_outlet_product_id, " +
////                            " def_opm.price AS default_price, " +
////                        " CASE WHEN opm.price=0 THEN def_opm.price ELSE opm.price END AS default_price, " +
////                        " def_opm.price AS default_price, "+
//                        " CASE WHEN def_opm.default_selected = 1 THEN def_opm.price ELSE 0 END AS default_price, "+
//                        " def_p.name AS default_name, def_p.description AS default_description, " +
//                        " pcm.category_id " +
//                        " FROM product p INNER JOIN outlet_product_mapping opm ON p.id=opm.product_id " +
//                        " LEFT JOIN outlet_product_mapping def_opm ON opm.id=def_opm.parent_outlet_product_id " +
//                        " LEFT JOIN product def_p ON def_p.id=def_opm.product_id " +
//                        " LEFT JOIN product_category_mapping pcm ON pcm.product_id=p.id" +
//                        " WHERE opm.parent_outlet_product_id IS NULL AND opm.outlet_id='"+outletId+"' ";//-3.AND (def_opm.default_selected=1 OR def_opm.default_selected IS NULL)";
//
//
//        StringBuilder queryBuilder = new StringBuilder();
//
//        queryBuilder.append(query);
//
//        queryBuilder.append(" GROUP BY p.id ");
//
//        if(sortType== Product.SortType.PRICE){
//            queryBuilder.append(" ORDER BY default_price ASC ");
//        }
//        else if(sortType== Product.SortType.ALPHABETICAL){
//            queryBuilder.append(" ORDER BY p.name ASC ");
//        }
//        else if(sortType== Product.SortType.CATEGORY){
//            queryBuilder.append(" ORDER BY pcm.category_id ASC ");
//        }
//
//
//        queryBuilder.append(" ,def_opm.default_selected DESC");
//
//
////            GenericRawResults<Product> rawResults = getProductDao().queryRaw(query, getProductDao().getRawRowMapper());
////dbHelper.getWritableDatabase().rawQuery()
////            List<Product> productList= rawResults.getResults();
////            rawResults.close();
////
////
////            for(Product p:productList){
////
////                p.setCustomizationList(getChildProducts(p.getId()));
////            }
//        List<Product> productList=getProductList(dbHelper,queryBuilder.toString());

        return getProductsForOutletByFilters(outletId, null, null, sortType);
//        return productList;
    }

    public List<Product> getProductsForOutletByFilters(long outletId, List<Long> productAttributeIdsList, List<Long> categoryIdsList, Product.SortType sortType) {

        StringBuilder attrInBuilder = new StringBuilder();

        if (productAttributeIdsList != null && productAttributeIdsList.size() > 0) {
            attrInBuilder.append(" AND ");

            if (categoryIdsList != null && categoryIdsList.size() > 0) {
                attrInBuilder.append(" ( ");
            }

            attrInBuilder.append(" pam.product_attribute_id IN ( ");

            for (int i = 0; i < productAttributeIdsList.size(); i++) {
                Long id = productAttributeIdsList.get(i);
                attrInBuilder.append("'");
                attrInBuilder.append(id);
                attrInBuilder.append("'");

                if (i < productAttributeIdsList.size() - 1) {
                    attrInBuilder.append(",");
                }

            }
            attrInBuilder.append(" ) ");
        }

        StringBuilder catInBuilder = new StringBuilder();

        if (categoryIdsList != null && categoryIdsList.size() > 0) {

            //if (productAttributeIdsList != null && productAttributeIdsList.size() > 0) {
//                catInBuilder.append(" OR ");
//            } else {
                catInBuilder.append(" AND ");
            //}

            catInBuilder.append(" pcm.category_id IN ( ");
            for (int i = 0; i < categoryIdsList.size(); i++) {
                Long id = categoryIdsList.get(i);
                catInBuilder.append("'");
                catInBuilder.append(id);
                catInBuilder.append("'");

                if (i < categoryIdsList.size() - 1) {
                    catInBuilder.append(",");
                }

            }
            catInBuilder.append(" ) ");
            if (productAttributeIdsList != null && productAttributeIdsList.size() > 0) {
                catInBuilder.append(" ) ");
            }
        }

        StringBuilder queryBuilder = new StringBuilder();
        String query =
                "SELECT p.id,p.code,p.name,p.description,p.photo_url,p.row_status,p.created_at,p.modified_at,p.company_id," +
                        "opm.id AS outlet_product_id, opm.parent_outlet_product_id,opm.outlet_id,opm.sort_no,opm.price, " +
                        " count(def_opm.id) AS cust_count, def_opm.id AS default_outlet_product_id, def_opm.product_id AS default_product_id, def_opm.parent_outlet_product_id AS default_parent_outlet_product_id, " +
//                        " def_opm.price AS default_price, " +
//                        " CASE WHEN opm.price=0 THEN def_opm.price ELSE opm.price END AS default_price, " +
//                        " def_opm.price AS default_price, " +
                        " CASE WHEN def_opm.default_selected = 1 THEN def_opm.price ELSE 0 END AS default_price, " +
                        " CASE WHEN def_opm.default_selected = 1 THEN def_p.name ELSE '' END AS default_name, " +
                        " CASE WHEN def_opm.default_selected = 1 THEN def_p.description ELSE '' END AS default_description,  " +

//                        " def_p.name AS default_name, def_p.description AS default_description," +
                        " pcm.category_id, MAX(def_opm.default_selected) AS default_selected " +
                        " FROM product p INNER JOIN outlet_product_mapping opm ON p.id=opm.product_id " +
                        " LEFT JOIN outlet_product_mapping def_opm ON opm.id=def_opm.parent_outlet_product_id " +
                        " LEFT JOIN product def_p ON def_p.id=def_opm.product_id " +
                        " LEFT JOIN product_attribute_mapping pam ON pam.product_id=p.id " +
                        " INNER JOIN product_category_mapping pcm ON pcm.product_id=p.id" +
                        " INNER JOIN category c ON c.id=pcm.category_id "+
                        " WHERE opm.parent_outlet_product_id IS NULL ";//AND (def_opm.default_selected=1 OR def_opm.default_selected IS NULL)";


        queryBuilder.append(query);

        queryBuilder.append("AND opm.outlet_id='" + outletId + "'");
        queryBuilder.append(attrInBuilder.toString());
        queryBuilder.append(catInBuilder.toString());

        queryBuilder.append("GROUP BY p.id");

        if (sortType == Product.SortType.PRICE) {
            queryBuilder.append(" ORDER BY (opm.price+default_price) ASC ");
        } else if (sortType == Product.SortType.ALPHABETICAL) {
            queryBuilder.append(" ORDER BY p.name ASC ");
        } else if (sortType == Product.SortType.CATEGORY) {
            queryBuilder.append(" ORDER BY c.sr,opm.sort_no ASC ");
            //queryBuilder.append(" ORDER BY pcm.category_id ASC ");
        }else{
            queryBuilder.append(" ORDER BY c.sr,opm.sort_no ASC ");
        }

//            GenericRawResults<Product> rawResults = getProductDao().queryRaw(query, getProductDao().getRawRowMapper());
//dbHelper.getWritableDatabase().rawQuery()
//            List<Product> productList= rawResults.getResults();
//            rawResults.close();
//
//
//            for(Product p:productList){
//
//                p.setCustomizationList(getChildProducts(p.getId()));
//            }

Toolbox.writeToLog("Query for filters "+queryBuilder.toString());
        List<Product> productList = getProductList(dbHelper, queryBuilder.toString());
        return productList;
    }

    public Product getProductByOutletProductId(long outletProductId) {

        String query =
                "SELECT p.id,p.code,p.name,p.description,p.photo_url,p.row_status,p.created_at,p.modified_at,p.company_id," +
                        "opm.id AS outlet_product_id, opm.parent_outlet_product_id,opm.outlet_id,opm.sort_no,opm.price, " +
                        " def_opm.id AS default_outlet_product_id, def_opm.product_id AS default_product_id, def_opm.parent_outlet_product_id AS default_parent_outlet_product_id,def_opm.price AS default_price, " +
                        " def_p.name AS default_name, def_p.description AS default_description," +
                        " pcm.category_id " +
                        " FROM product p INNER JOIN outlet_product_mapping opm ON p.id=opm.product_id " +
                        " INNER JOIN outlet_product_mapping def_opm ON opm.id=def_opm.parent_outlet_product_id " +
                        " INNER JOIN product def_p ON def_p.id=def_opm.product_id " +
                        " INNER JOIN product_category_mapping pcm ON pcm.product_id=p.id" +
                        " WHERE opm.parent_outlet_product_id IS NULL AND opm.id='" + outletProductId + "' ";//AND (def_opm.default_selected=1 OR def_opm.default_selected IS NULL)";

        List<Product> productList = getProductList(dbHelper, query);

        if (productList != null && productList.size() > 0) {
            return productList.get(0);
        } else {
            return null;
        }
    }

    public String getCustomizationString(List<Customization> customizationList) {


        StringBuilder values = new StringBuilder();

        for (int i = 0; i < customizationList.size(); i++) {
            values.append("'");
            values.append(customizationList.get(i).getOutlet_product_id());
            values.append("'");

            if (i < customizationList.size() - 1) {
                values.append(",");
            }

        }

        String query =
                "SELECT GROUP_CONCAT(p.name,', ') FROM product p inner join outlet_product_mapping opm ON p.id=opm.product_id WHERE opm.id IN (" + values.toString() + ")";

        Cursor c = dbHelper.getWritableDatabase().rawQuery(query, null);

        c.moveToFirst();
        String customizationString = c.getString(0);
        c.close();
        return customizationString;

    }

    public List<Product> getChildProducts(long outletProductMappingId) {

        try {

            String query =
                    "SELECT p.id,p.code,p.name,p.description,p.photo_url,p.row_status,p.created_at,p.modified_at,p.company_id," +
                            "opm.id AS outlet_product_id, opm.parent_outlet_product_id,opm.outlet_id,opm.sort_no,opm.price" +
                            " FROM product p INNER JOIN outlet_product_mapping opm ON p.id=opm.product_id WHERE opm.parent_outlet_product_id='" + outletProductMappingId + "'";

//            p.id,p.code,p.name,p.description,p.photo_url,p.row_status,p.created_at,p.modified_at,p.company_id,p.price

            GenericRawResults<Product> rawResults = getProductDao().queryRaw(query, getProductDao().getRawRowMapper());

            List<Product> productList = rawResults.getResults();
            rawResults.close();


            for (Product p : productList) {
//                dbManager

            }
            return productList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Product> getDefaultChildren(List<Long> outletProductMappingIdList) {

        StringBuilder values = new StringBuilder();

        for (int i = 0; i < outletProductMappingIdList.size(); i++) {
            values.append("'");
            values.append(outletProductMappingIdList.get(i));
            values.append("'");

            if (i < outletProductMappingIdList.size() - 1) {
                values.append(",");
            }

        }


        String query =
                "SELECT p.id,p.code,p.name,p.description,p.photo_url,p.row_status,p.created_at,p.modified_at,p.company_id, " +
                        " opm.id AS outlet_product_id, opm.parent_outlet_product_id,opm.outlet_id,opm.sort_no,opm.price " +
                        " FROM product p INNER JOIN outlet_product_mapping opm ON p.id=opm.product_id WHERE opm.parent_outlet_product_id IN (" + values.toString() + ") AND opm.default_selected=1 ";


        return getProductList(dbHelper, query);

    }


    public List<Product_attribute_group> getProductAttributeGroupsWithAttrs(Long outletId) {

        try {

//            List<Product_attribute_group>productAttributeGroupList=getProductAttributeGroupDao().queryForAll();
            String key=" pa.display_type IN ("+Constants.USE_IN_FILTER_AND_PRODUCT_CELL_AS_IMAGE+","+Constants.USE_IN_FILTER_AND_PRODUCT_CELL_AS_TEXT+","+Constants.FILTER_TEXT+") GROUP BY pag.id";
            String query = "SELECT pag.* FROM product_attribute_group pag INNER JOIN product_attribute pa ON pag.id=pa.product_attribute_group_id INNER JOIN product_attribute_mapping pam ON pa.id=pam.product_attribute_id INNER JOIN product p ON pam.product_id=p.id INNER JOIN outlet_product_mapping opm ON opm.product_id=p.id WHERE opm.outlet_id='" + outletId + "' AND"+key;

            Toolbox.writeToLog("Final query "+query+key);
            GenericRawResults<Product_attribute_group> rawResults = getProductAttributeGroupDao().queryRaw(query, getProductAttributeGroupDao().getRawRowMapper());

            List<Product_attribute_group> productAttributeGroupList = rawResults.getResults();
            rawResults.close();

            if (productAttributeGroupList != null) {

                for (Product_attribute_group pag : productAttributeGroupList) {

                    List<Product_attribute> productAttributeList = getProductAttributesForGroup(pag.getId().longValue(),true);
                    if (productAttributeList != null) {
                        pag.setProductAttributeList(productAttributeList);
                    }
                }
            }
            return productAttributeGroupList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void insertTimestamp(ArrayList<CacheForData> timestampData) throws SQLException {


        for (CacheForData data : timestampData) {
            Toolbox.writeToLog("Data log", "Timestamps " + data.getName() + " " + data.getTimestamp());
//                if(getCacheForDatasDao().executeRaw("select * from cachefordata where name like '"+data.getName()+"' and outlet_id = "+data.getOutlet_id())>0){
//                       Toolbox.writeToLog("select * from cachefordata where name like '"+data.getName()+"' and outlet_id = "+data.getOutlet_id()+"= "+getCacheForDatasDao().executeRaw("select * from cachefordata where name like '"+data.getName()+"' and outlet_id = "+data.getOutlet_id()));
//                        updateTimeStamps(data,data.getOutlet_id());
//                    getCacheForDatasDao().up
//                }else
            ArrayList<CacheForData> resultList = null;
            try {
                String query = "select * from cachefordata where outlet_id = " + data.getOutlet_id() + " and name like '" + data.getName() + "'";
                GenericRawResults<CacheForData> results = getCacheForDatasDao().queryRaw(query, getCacheForDatasDao().getRawRowMapper());
                resultList = (ArrayList<CacheForData>) results.getResults();
            } catch (Exception e) {
            } finally {
                if (resultList == null || resultList.size() == 0 || resultList.get(0).getId() == null) {
                    if (data.getTimestamp() != null)
                        getCacheForDatasDao().create(data);
                } else {
                    data.setId(resultList.get(0).getId());
                    if (data.getTimestamp() != null)
                        getCacheForDatasDao().createOrUpdate(data);
                }

            }
        }


    }

    public void updateTimeStamps(CacheForData objects, long outletid) {
        try {
            String query = "UPDATE 'cachefordata' SET timestamp = " + objects.getTimestamp() + " where name like '" + objects.getName() + "' and outlet_id = " + outletid;
            Toolbox.writeToLog("UPDATE 'cachefordata' SET timestamp = " + objects.getTimestamp() + " where name like '" + objects.getName() + "' and outlet_id = " + outletid);
            getCacheForDatasDao().updateRaw(query);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public List<CacheForData> getTimeStamp(Long outlet_id) {
        ArrayList<CacheForData> resultList = null;
        try {
            String query = "select * from cachefordata where outlet_id = " + outlet_id;

            GenericRawResults<CacheForData> results = getCacheForDatasDao().queryRaw(query, getCacheForDatasDao().getRawRowMapper());
            resultList = (ArrayList<CacheForData>) results.getResults();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            return resultList;
        }

    }

    public User getUserByPrimaryMobile(String mobile) {
        try {
            List<User> users = getUserDao().queryForEq("primary_phone", mobile);
            if (users != null && users.size() > 0) {

                return users.get(0);
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Address> getAddressesForUser(Long userId,Long outletId) {
        if (userId != null) {
            try {
                String query="select * from address ad where (ad.subarea_id in(select s.id from subarea s inner join area a on a.id=s.area_id inner join outlet o on a.outletId=o.id where o.id="+outletId+") or ad.subarea_id is null) and ad.user_id="+userId;
                Toolbox.writeToLog("Query "+query);
                GenericRawResults<Address>results= (GenericRawResults<Address>) getAddressDao().queryRaw(query, getAddressDao().getRawRowMapper());
                List<Address> addresses=results.getResults();//= getAddressDao().queryForEq("user_id", userId);
                Toolbox.writeToLog("Addresses "+addresses);
                if (addresses != null && addresses.size() > 0) {
                    return addresses;
                } else {
                    return null;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public List<Product_attribute> getProductAttributesForGroup(long producttAttributeGroupId,boolean filter) {

//        try {
        String key=" AND pa.display_type IN ("+Constants.USE_IN_FILTER_AND_PRODUCT_CELL_AS_IMAGE+","+Constants.USE_IN_FILTER_AND_PRODUCT_CELL_AS_TEXT+","+Constants.FILTER_TEXT+") GROUP BY pa.id";// AND p.row_status='active'
        String key2= " GROUP BY pa.id";
        String query = "SELECT pa.*,count(*) as products_count FROM product_attribute pa INNER JOIN product_attribute_mapping pam ON pa.id=pam.product_attribute_id WHERE pa.product_attribute_group_id='" + producttAttributeGroupId +"'";
        if(filter){
            query=query+key;
        }else{
            query=query+key2;
        }
                Toolbox.writeToLog("Query "+query);
//            GenericRawResults<Product_attribute> rawResults = getProductAttributeDao().queryRaw(query, getProductAttributeDao().getRawRowMapper());
//
        List<Product_attribute> productAttributeList = getProductAttributesList(dbHelper, query);
//                    rawResults.getResults();
//            rawResults.close();

        return productAttributeList;
//            return getProductAttributeDao().queryForEq("product_attribute_group_id", producttAttributeGroupId);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return null;
    }

    public boolean isOutletOpenNow(long outletId) {
        return isOutletOpenAt(outletId, Calendar.getInstance());
    }

    public Area getAreaById(Long areaID){
        try{
            return getAreasDao().queryForId(areaID);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    public Subarea getSubareaById(Long subareaID){
        try{
            Toolbox.writeToLog("Subs "+getSubareasDao().queryForId(subareaID));
//            String query="select s.id,s.subarea_name,s.area_id from subarea"
            return getSubareasDao().queryForId(subareaID);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public boolean isOutletOpenAt(long outletId, Calendar cal) {

        cal.set(Calendar.SECOND, 0);
        //cal.set(Calendar.HOUR_OF_DAY,cal.get)
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String currTimeString = sdf.format(cal.getTime());
        Toolbox.writeToLog("Timings data "+outletId+" date"+currTimeString);
        String timingsCountQuery  = "SELECT TIME(open_time,duration) AS close_time FROM outlet_timing WHERE outlet_id='" + outletId + "' AND day_of_week='" + cal.get(Calendar.DAY_OF_WEEK) + "'";

        String query =
                "SELECT COUNT(*) FROM outlet_timing WHERE '" + currTimeString + "' BETWEEN open_time AND time(open_time,duration) AND outlet_id='" + outletId + "' AND day_of_week='" + cal.get(Calendar.DAY_OF_WEEK) + "'";

        GenericRawResults<String[]> rawResults =
                null;
        GenericRawResults<String[]> rawResults_main =
                null;

        try {
rawResults_main=getOutletTimingDao().queryRaw(query);
            int count= Integer.parseInt(rawResults_main.getFirstResult()[0]);
            if(count==0)return false;
            rawResults=getOutletTimingDao().queryRaw(timingsCountQuery);
            String date=rawResults.getFirstResult()[0];
//            String date1=rawResults.getFirstResult()[1];
            if(timingsCountQuery!=null){
                Date d=sdf.parse(date);
//                Date d1=sdf.parse(date1);
                Calendar c2=Calendar.getInstance();
                c2.set(Calendar.HOUR_OF_DAY,Integer.parseInt(date.split(":")[0]));
                c2.set(Calendar.MINUTE,Integer.parseInt(date.split(":")[1]));
                c2.set(Calendar.SECOND,Integer.parseInt(date.split(":")[2]));
//                Calendar c3=Calendar.getInstance();
//                c2.set(Calendar.HOUR_OF_DAY,Integer.parseInt(date1.split(":")[0]));
//                c2.set(Calendar.MINUTE,Integer.parseInt(date1.split(":")[1]));
//                c2.set(Calendar.SECOND,Integer.parseInt(date1.split(":")[2]));
                if(d!=null){
                    Toolbox.writeToLog("Dates are "+cal.getTime()+" 2"+c2.getTime());
                    return  cal.getTime().before(c2.getTime());
                }
            }
//            rawResults = getOutletTimingDao().queryRaw(timingsCountQuery);cal.getTime().after(c3.getTime())&&
//            List<String[]> results = rawResults.getResults();
//            String[] resultArray = results.get(0);
//            Toolbox.writeToLog("Timings data "+resultArray[0]);
//            int timingsCount = Integer.parseInt(resultArray[0]);
//            Toolbox.writeToLog("Timings count "+timingsCount);
//            if (timingsCount == 0) {
//                return true;
//            }
//
//            rawResults = getOutletTimingDao().queryRaw(query);
//            results = rawResults.getResults();
//            resultArray = results.get(0);
//            Toolbox.writeToLog("Timings data "+resultArray+query);
//            int count = Integer.parseInt(resultArray[0]);
//            Toolbox.writeToLog("Timings count "+count+"   "+query);
//            return count != 0;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }

    public List<Outlet_timing> getOutletTimings(long outletId, Calendar cal) {

        String query = "SELECT *,TIME(open_time,duration) AS close_time FROM outlet_timing WHERE outlet_id='" + outletId + "' AND day_of_week='" + cal.get(Calendar.DAY_OF_WEEK) + "'";
        return getOutletTimingList(dbHelper, query);
    }
public void clearOrders(){
        try{
            TableUtils.clearTable(dbHelper.getConnectionSource(), Order.class);
            TableUtils.clearTable(dbHelper.getConnectionSource(), Order_Details.class);
            TableUtils.clearTable(dbHelper.getConnectionSource(), Applied_offer.class);
            TableUtils.clearTable(dbHelper.getConnectionSource(), Applied_extra_charge.class);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void clearOutletData() {
        try {
            TableUtils.clearTable(dbHelper.getConnectionSource(), Outlet.class);
            TableUtils.clearTable(dbHelper.getConnectionSource(), Outlet_timing.class);
            TableUtils.clearTable(dbHelper.getConnectionSource(), Outlet_attribute.class);
            TableUtils.clearTable(dbHelper.getConnectionSource(), Outlet_attributes_group.class);
            TableUtils.clearTable(dbHelper.getConnectionSource(), Outlet_attributes_mapping.class);

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }



    public void clearOutletMenuData() {
        try {
            TableUtils.clearTable(dbHelper.getConnectionSource(), Category.class);
            TableUtils.clearTable(dbHelper.getConnectionSource(), Product.class);
            TableUtils.clearTable(dbHelper.getConnectionSource(), Product_category_mapping.class);
            TableUtils.clearTable(dbHelper.getConnectionSource(), Outlet_product_mapping.class);
            TableUtils.clearTable(dbHelper.getConnectionSource(), Product_attribute.class);
            TableUtils.clearTable(dbHelper.getConnectionSource(), Product_attribute_mapping.class);
            TableUtils.clearTable(dbHelper.getConnectionSource(), Product_attribute_group.class);
            // TableUtils.clearTable(dbHelper.getConnectionSource(),CacheForData.class);

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public void clearTable(String tableName, Long outletId) {
        String query = "delete from " + tableName + " where outlet_id=" + outletId;
    }


    public void clearCache() {
        try {
            TableUtils.clearTable(dbHelper.getConnectionSource(), CacheForData.class);
        } catch (SQLException exp) {
            exp.printStackTrace();
        }
    }

    public void callInTransaction(Callable callable) throws SQLException {
        TransactionManager.callInTransaction(dbHelper.getConnectionSource(), callable);
    }

    public void close() {
        outletDao = null;
        dbHelper.close();
    }


}