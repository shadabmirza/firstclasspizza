package to.done.lib.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import to.done.lib.R;
import to.done.lib.entity.cachingtables.CacheForData;
import to.done.lib.entity.nearestoutlets.Outlet;
import to.done.lib.entity.nearestoutlets.Outlet_attribute;
import to.done.lib.entity.nearestoutlets.Outlet_attributes_group;
import to.done.lib.entity.nearestoutlets.Outlet_attributes_mapping;
import to.done.lib.entity.nearestoutlets.Outlet_timing;
import to.done.lib.entity.order.Applied_extra_charge;
import to.done.lib.entity.order.Applied_offer;
import to.done.lib.entity.order.Order;
import to.done.lib.entity.order.Order_Details;
import to.done.lib.entity.outletmenu.Category;
import to.done.lib.entity.outletmenu.Outlet_product_mapping;
import to.done.lib.entity.outletmenu.Product;
import to.done.lib.entity.outletmenu.Product_attribute;
import to.done.lib.entity.outletmenu.Product_attribute_group;
import to.done.lib.entity.outletmenu.Product_attribute_mapping;
import to.done.lib.entity.outletmenu.Product_category_mapping;
import to.done.lib.entity.outletsubareas.Area;
import to.done.lib.entity.outletsubareas.Subarea;
import to.done.lib.entity.user.Address;
import to.done.lib.entity.user.User;

/**
 * Database helper class used to manage the creation and upgrading of your database. This class also usually provides
 * the DAOs used by the other classes.
 */
public class DBHelper extends OrmLiteSqliteOpenHelper {

	// name of the database file for your application -- change to something appropriate for your app
	private static final String DATABASE_NAME = "done.db";
	// any time you make changes to your database objects, you may have to increase the database version
	private static final int DATABASE_VERSION = 1;

	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
	}

	/**
	 * This is called when the database is first created. Usually you should call createTable statements here to create
	 * the tables that will store your data.
	 */
	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
		try {
			Log.i(DBHelper.class.getName(), "onCreate");
			TableUtils.createTable(connectionSource, Outlet.class);
            TableUtils.createTable(connectionSource, Outlet_attribute.class);
            TableUtils.createTable(connectionSource, Outlet_attributes_group.class);
            TableUtils.createTable(connectionSource, Outlet_attributes_mapping.class);
            TableUtils.createTable(connectionSource, Outlet_timing.class);
            
            TableUtils.createTable(connectionSource,Category.class);
            TableUtils.createTable(connectionSource,Product.class);
            TableUtils.createTable(connectionSource,Product_category_mapping.class);
            TableUtils.createTable(connectionSource,Outlet_product_mapping.class);
            TableUtils.createTable(connectionSource,Product_attribute.class);
            TableUtils.createTable(connectionSource,Product_attribute_mapping.class);
            TableUtils.createTable(connectionSource,Product_attribute_group.class);
            TableUtils.createTable(connectionSource, CacheForData.class);
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, Address.class);
            TableUtils.createTable(connectionSource, Order.class);
            TableUtils.createTable(connectionSource, Order_Details.class);
            TableUtils.createTable(connectionSource, Area.class);
            TableUtils.createTable(connectionSource, Subarea.class);
            TableUtils.createTable(connectionSource, Applied_extra_charge.class);
            TableUtils.createTable(connectionSource, Applied_offer.class);


        } catch (SQLException e) {
			Log.e(DBHelper.class.getName(), "Can't create database", e);
			throw new RuntimeException(e);
		}


	}

	/**
	 * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
	 * the various data to match the new version number.
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
//		try {
//			Log.i(DBHelper.class.getName(), "onUpgrade");
//			TableUtils.dropTable(connectionSource, Outlet.class, true);
//			// after we drop the old databases, we create the new ones
//			onCreate(db, connectionSource);
//		} catch (SQLException e) {
//			Log.e(DBHelper.class.getName(), "Can't drop databases", e);
//			throw new RuntimeException(e);
//		}
	}



	/**
	 * Close the database connections and clear any cached DAOs.
	 */
	@Override
	public void close() {
		super.close();

	}
}
